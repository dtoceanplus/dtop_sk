# Overview

The Station Keeping (SK) module is a module of the DTOceanPlus application.

The main purpose of the Station Keeping module is to design and assess the mooring system, anchors and foundations of the devices and substation including:

  - Mooring lines for floating structure (design, ULS analysis and FLS analysis)

  - Anchors (design and ULS analysis)

  - Foundation for fixed structure (design and ULS analysis)

The Station Keeping (SK) Module produces outputs and assessments based on the following main functionalities:

1) Static Analysis: Static analysis of the device is performed. The main outputs are the equilibrium position of the device and the amplitude of the environmental forces that apply on the device and the tension in the lines of the mooring system.

2) Dynamic Analysis: Dynamic analysis of the floating device is performed in frequency domain. The main outputs are the main six degrees of freedom motions in terms of power spectra.

3) ULS Analysis: Ultimate Limit State analysis of the mooring lines segments is performed. Maximum expected tensions and ULS criteria check in each line segments are the main outputs. For a fixed structure, the main output is the maximum expected force on the foundation base.

4) FLS Analysis: Fatigue Limit State analysis of the mooring lines segments is performed. Cumulated fatigue damage and stress range long term distribution of each line segment are the main outputs.

5) Mooring System Design: A mooring system based on catenary lines is designed. Number of lines, chain diameter, length of lines and anchor points positions are the main outputs. For catenary system, an automated design algorithm is available.

6) Soil Properties: The mechanical and physical properties of the soil are determined based on the soil type.

7) Foundation Suitability: The type of foundation base (for fixed structure) and anchor (for floating structures) are selected based on the soil type and the type of external loads.

8) Foundation Design: Foundation base and anchors are designed. Dimensions and type of foundation base and anchors are the main outputs.

9) Hierarchy and Bill of Materials: a hierarchy and a bill of materials are produced based on the mooring system design and foundation design.

10) Environmental Impact: a set of metrics required by the Environmental and Social Acceptance (ESA) module is computed based on the mooring system design and foundation base/anchor design.

Station Keeping is distributed under the AGPL license see LICENSE file for more details.

# To install and launch the SK module with Docker:

Build, install and launch (or launch if already installed)
```
docker-compose up
```

Rebuild, install and launch
```
docker-compose up --build
```

# To install the SK module locally on a PC, without Docker:
Create a virtual environment to isolate the application development:
```python
conda create -n envtest
```
Activate this environment: 
```python
conda activate envtest
```
Install the pip installer:
```python
conda install pip
```
Install the required packages:
```bash
pip install -U pytest flask python-dotenv Flask-Babel matplotlib pytest-mock pytest-html pandas scipy flask-cors utm
```
Create the dist folder required for the installation:
```bash
python setup.py sdist
```

Install the dtop_stationkeep package from the dist folder:
```bash
pip install dtop-stationkeep --find-links dist 
```

Install the package for development:
```bash
pip install -e .
```

Download the shared library of the DTOCEAN project and install it from its source directory:
```bash
python setup.py install
```

Install a c++ compiler from conda-forge:
```bash
conda install -c conda-forge compilers
```

Change directory to pymap_submodule folder:
```bash
cd pymap_submodule
```

Build this submodule with python:
```bash
python setup.py build
```

Install this submodule with python:
```bash
python setup.py install
```

Install this package for development:
```bash
pip install -e .
```

Documentation:
  - [Python Packaging User Guide](https://packaging.python.org)
  - [Installing Packages](https://packaging.python.org/tutorials/installing-packages/)
  - [Packaging Python Projects](https://packaging.python.org/tutorials/packaging-projects/)


# Start Service locally on a PC, without docker

Install `python-dotenv` for taking service default configuration from `.flaskenv`:

```DOS .bat
pip install python-dotenv
```

Without `.flaskenv` you should set environment variables for `flask`:

```DOS .bat
set FLASK_APP=dtop_stationkeep.service
set FLASK_ENV=development
set FLASK_RUN_PORT=5000
```

Start the service:
```DOS .bat
flask run
```

Now you can open in browser this URL: http://localhost:5000/.


# Documentation

Documentation for the full suite of tools is available at the temporary link; https://wave-energy-scotland.gitlab.io/dtoceanplus/dtop_documentation/deployment/sk/docs/index.html.
This documentation will be moved to a permanent address upon the final release of the suite of tools. 
