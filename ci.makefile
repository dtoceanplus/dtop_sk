
all: clean pytest dredd

stationkeep-base:
	docker build --file sk.dockerfile --tag stationkeep-base .

rm-dist:
	rm -rf ./dist

create-dist:
	python setup.py sdist

x-build: rm-dist create-dist

build: stationkeep-base x-build

stationkeep-deploy:
	docker build --cache-from=stationkeep-deploy --tag stationkeep-deploy .

stationkeep-pytest:
	docker build --file sk.dockerfile --tag stationkeep-pytest .

pytest: stationkeep-pytest
	docker run --name stationkeep-pytest-cont stationkeep-pytest make --file test.makefile pytest
	docker cp stationkeep-pytest-cont:/app/report.xml .
	docker container rm stationkeep-pytest-cont

pytest-mb: stationkeep-deploy
	docker-compose \
	--file docker-compose.yml \
	--file docker-compose.test.yml \
	up --abort-on-container-exit --exit-code-from=pytest \
	stationkeep pytest mc sc

shell-pytest: stationkeep-pytest
	docker run --tty --interactive stationkeep-pytest bash

dredd:
	touch ./dredd-hooks-output.txt
	docker-compose --file docker-compose.yml --file docker-compose-test.yml down
	docker-compose --file docker-compose.yml --file docker-compose-test.yml up \
	--build --abort-on-container-exit --exit-code-from=dredd  dredd

clean:
	rm -fr dist
	docker image rm --force stationkeep-base stationkeep-deploy stationkeep-pytest


## !
## Set the required module nickname
MODULE_SHORT_NAME=sk

## !
## Set the required docker image TAG
MODULE_TAG=v1.5.6

## !
# Set the required MODULE_DB_INIT value that defines a necessity to initialize module db or data files
#   1 - reinitialize and repopulate db or data files with initial data
#   0 - reuse the current database or data files created after the previous deployment
# ! make sure that MODULE_DB_INIT = 1 for initial deployment
MODULE_DB_INIT=1
#MODULE_DB_INIT=0

## !
## Update the values of CI_REGISTRY_IMAGE to your module registry
CI_REGISTRY_IMAGE?=registry.gitlab.com/dtoceanplus/dtop_sk

## !
## Set the value to cors urls
ALL_MODULES_NICKNAMES := si sg sc mc ec et ed sk lmo spey rams slc esa cm mm
CORS_URLS := $(foreach module,${ALL_MODULES_NICKNAMES},http://${module}.${DTOP_DOMAIN},https://${module}.${DTOP_DOMAIN},)

login:
	echo ${CI_REGISTRY_PASSWORD} | docker login --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY_IMAGE}

logout:
	docker logout ${CI_REGISTRY_IMAGE}

build-prod-be:
	docker pull ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} || true
	docker build --cache-from ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} \
	  --tag ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} \
	  --file ./${MODULE_SHORT_NAME}-prod.dockerfile \
          .
	docker push ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG}
	docker images

build-prod-fe:
	docker pull ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} || true
	docker build --build-arg DTOP_BASIC_AUTH="${DTOP_BASIC_AUTH}" --build-arg DTOP_MODULE_SHORT_NAME="${MODULE_SHORT_NAME}" \
		--cache-from ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} \
		--tag ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} \
		--file ./src/dtop_stationkeep/GUI/frontend-prod.dockerfile \
		./src/dtop_stationkeep
	docker push ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG}
	docker images