context('Device_Properties', () => {
    beforeEach(() => {
      cy.visit('/#/skhome')
      cy.get('#create_project').click()
      cy.get('#entity_name').type('test')
      cy.get('#run_mode1').click()
      cy.get('#complexity3').click()
      cy.get('#confirm_creation').click()
      cy.wait(3000)
      cy.get('#Next_page').click()
      cy.wait(3000)
    })

    it('Type of machine section', () => {
      cy.get('#section1').click()

      cy.get('#positioning_type1.el-radio.is-checked').should('exist')
      cy.get('#section4').should('be.visible')
      cy.get('#section5').should('not.be.visible')
      cy.get('#section6').should('be.visible')
      cy.get('#positioning_type2').click()
      cy.get('#section4').should('not.be.visible')
      cy.get('#section5').should('be.visible')
      cy.get('#section6').should('not.be.visible')

      cy.get('#machine_type2.el-radio.is-checked').should('exist')
    })

    it('Wind force model section', () => {
      cy.get('#section2').click()

      cy.get('#data_description').should('not.be.visible')
      cy.get('#data_description1.el-radio.is-checked').should('exist')
      
      cy.get('#device_dry_profile').should('not.be.visible')
      cy.get('.el-switch__core').click()
      cy.get('#device_dry_profile').should('be.visible')
      
      cy.get('#device_dry_profile1.el-radio.is-checked').should('exist')

      cy.get('#device_dry_width').should('be.visible')
      cy.get('#device_dry_width > .el-input > .el-input__inner')
      .should('have.value','0')

      cy.get('#device_dry_height').should('be.visible')
      cy.get('#device_dry_height > .el-input > .el-input__inner')
      .should('have.value','0')
      
    })

    it('Current and mean wave drift force model section', () => {
      cy.get('#section3').click()

      cy.get('#device_wet_profile1.el-radio.is-checked').should('exist')

      cy.get('#device_wet_width').should('be.visible')
      cy.get('#device_wet_width > .el-input > .el-input__inner')
      .should('have.value','0')

      cy.get('#device_wet_height').should('be.visible')
      cy.get('#device_wet_height > .el-input > .el-input__inner')
      .should('have.value','0')
      
    })

    it('Floating structure hydrodynamic', () => {
      cy.get('#section4').click()

      cy.get('#device_selected').click()
      cy.get('#device_selected3').should('be.visible').click()

      cy.get('#nemohfile_button').should('be.visible')
      cy.get('#nemohfile_checkbox1').should('be.visible')
      cy.get('#nemohfile_checkbox1.el-checkbox.is-disabled').should('exist')
    })

    it('Current and mean wave drift force model section', () => {

        cy.get('#section1').click()
        cy.get('#positioning_type2').click()
        cy.get('#section1').click()

        cy.get('#section5').click()

        cy.get('#mass > .el-input > .el-input__inner')
        .should('have.value','0')

        cy.get('#thrust_add_button').should('not.be.visible')
        cy.get('#section1').click()
        cy.get('#machine_type1').click()
        cy.get('#section1').click()
        cy.get('#thrust_add_button').should('be.visible')

        cy.get('#TCV > .el-input > .el-input__inner').should('not.exist')
        cy.get('#TCCT > .el-input > .el-input__inner').should('not.exist')
        cy.get('#thrust_add_button').click()
        cy.get('#TCV > .el-input > .el-input__inner').should('have.value','0')
        cy.get('#TCCT > .el-input > .el-input__inner').should('have.value','0')
        cy.get(':nth-child(2) > :nth-child(1) > #thrust_delete_button').click()
        cy.get('#TCV > .el-input > .el-input__inner').should('not.exist')
        cy.get('#TCCT > .el-input > .el-input__inner').should('not.exist')

        cy.get('#thrust_load_button').click()
        cy.get('[data-cy=uploadBox]').should('be.visible')
        cy.get('#cancel_dialog_button').click()
        cy.get('[data-cy=uploadBox]').should('not.be.visible')

        cy.get('#rotor_diameter > .el-input > .el-input__inner').should('have.value','0')

        cy.get(':nth-child(2) > :nth-child(2) > #hub_position1 > .el-input > .el-input__inner').should('not.exist')        
        cy.get('#add_rotor_button1').click()
        cy.get(':nth-child(2) > :nth-child(2) > #hub_position1 > .el-input > .el-input__inner').should('have.value','0')        
        cy.get(':nth-child(2) > :nth-child(1) > #delete_rotor_button').click()
        cy.get(':nth-child(2) > :nth-child(2) > #hub_position1 > .el-input > .el-input__inner').should('not.exist')        


    })

  })