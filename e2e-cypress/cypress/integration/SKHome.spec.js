context('SKHome', () => {
    beforeEach(() => {
      cy.visit('/#/skhome')
    })
  
    it('Have 3 images', () => {
      cy.url().should('include', 'skhome')
      cy.get('#create_project')
      cy.get('#load_project')
      cy.get('#DTOCEAN_Logo')
    })
  
    it('Create project popup functionning', () => {
      cy.get('#creation').should('not.be.visible')
      cy.get('#create_project').click()
      cy.get('#creation').should('be.visible')
      cy.get('[id="cancel_creation"]').click()
      cy.get('#creation').should('not.be.visible')
    })
  
    it('Create project popup run_mode buttons', () => {
      cy.get('#create_project').click()
      cy.get('#run_mode1.el-radio.is-checked').should('exist')
      cy.get('#complexity1.el-radio.is-checked').should('exist')
    })
  
    it('Create project', () => {
      cy.get('#create_project').click()
      cy.get('#entity_name').type('test')
      cy.get('#run_mode1').click()
      cy.get('#complexity3').click()
      cy.get('#confirm_creation').click()
      cy.wait(3000)
      cy.url().should('include', 'devices_positioning')
    })
    
  })
