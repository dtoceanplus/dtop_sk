context('Devices_Positioning', () => {
    beforeEach(() => {
      cy.visit('/#/skhome')
      cy.get('#create_project').click()
      cy.get('#entity_name').type('test')
      cy.get('#run_mode1').click()
      cy.get('#complexity3').click()
      cy.get('#confirm_creation').click()
      cy.wait(3000)
    })
  
    it('Water density field', () => {
      cy.get("#water_density > .el-input > .el-input__inner").should('have.value','1025')
    })
  
    it('Table fields', () => {
      cy.get('#deviceId > .el-input > .el-input__inner').should('have.value','1')
      cy.get('#deviceId > .el-input > .el-input__inner').should('be.disabled')
      cy.get('#north > .el-input > .el-input__inner').should('have.value','0')
      cy.get('#east > .el-input > .el-input__inner').should('have.value','0')
      cy.get('#yaw > .el-input > .el-input__inner').should('have.value','0')
      cy.get('#water_depth > .el-input > .el-input__inner').should('have.value','0')
    })
  
    it('Next page button', () => {
      cy.get('#Next_page').click()
      cy.wait(2000)
      cy.url().should('include', 'device_properties')
    })
  
    it('Run module button', () => {
      cy.get('#Run_module').should('not.be.visible')
    })

  })