FROM continuumio/miniconda3

COPY environment.yml .
RUN conda env create -f environment.yml

RUN echo "source activate envsk" >> ~/.bashrc
ENV PATH /opt/conda/envs/envsk/bin:$PATH

RUN apt-get --yes update \
&& apt-get --yes install build-essential \
&& apt-get --yes install liblapacke-dev

COPY . /app

WORKDIR /app/dtop-shared-library
RUN python setup.py install

WORKDIR /app/pymap_submodule
RUN python setup.py install

WORKDIR /app
RUN pip install -e .

RUN conda install nodejs
RUN npm install dredd@12.2.1 --global
