# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections 
import numpy as np 
import os
import pandas as pd
try: 
    import json 
except: 
    print('WARNING: json is not installed.') 
from foundationdesign.AnchorCatalogue import AnchorCatalogue
#------------------------------------------------------------------------------------------------
## Pile foundation design test
#
#-------------------------------------------------------------------------------------------------

drag = AnchorCatalogue()

catalogue = drag.drag_anchor_catalogue()
 
a= catalogue['Rel_soft_soil'].idxmin()
b= catalogue['NameId'][catalogue['Rel_soft_soil'].idxmin()]
c= catalogue['b_stiff_soil_sand'][0] + 2

d= catalogue[catalogue["NameId"]=='anchor_n_2'].index.values

e=catalogue['b_soft_soil'][d]

#inputs from user or other modules


#print(design)
print(catalogue)
print(a)
print(b)
print(c)
print(d)
print(e)

