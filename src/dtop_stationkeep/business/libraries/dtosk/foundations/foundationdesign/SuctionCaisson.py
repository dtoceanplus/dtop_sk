# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
from scipy import interpolate, optimize
from .parameters_for_suction import x_nq, y_nq
import pandas as pd
pd.options.display.float_format = '{:,.2g}'.format
import matplotlib.pyplot as plt
import logging
logger = logging.getLogger() # grabs root logger
# @ USER DEFINED IMPORTS END
#------------------------------------

class SuctionCaisson():

    """data model for input data needed to perform the pile foundation design
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._moor_V_min=0.0
        self._moor_H_x=0.0
        self._moor_H_y=0.0
        self._u_shear_strenght_d=0.0
        self._int_friction_angle_d=0.0
        self._n_iterations_max=500
        self._d_ext=3
        self._w_t=0
        self._length=0
        self._H_ult=0
        self._H_d=0
        self._V_d=0
        self._V_ult=0
        self._inc_verif=0
        self._cs_area=0
        self._weight=0
        self._g_b=0
        self._z_ap=0
        self._theta_m=0
        self._theta_a=0
        self._t_a=0
        self._t_m=0
        self._d_chain=0
        self._criteria_uplift_capacity=False
        self._criteria_horizontal_capacity=False
        self._criteria_failure_verification=False
        self._SF=1.8
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
        self.loads_from_mooring=np.zeros(shape=(1), dtype=float)
        self.g = 9.81
        self.d_int = 0
        self.int_friction_angle_d_rad=0
        self.soil = ''
        self.H_d=0
        self.V_d=0
        self.shaft_area = 0
        self.slenderness = 0
        self.mu = 0
        self.Q = 0
        self.inc_verif = 0
        self.volume = 0
        self.water_d = 1025
        self.steel_d = 7850
        self.anchor_mass = 0
        self.convergence = pd.DataFrame()
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START

    def desing_loads_from_mooring(self):
        '''Calculte loads'''
        self.moor_V_min = abs(self.loads_from_mooring[0])
        self.moor_H_x = self.loads_from_mooring[2]
        self.moor_H_y = self.loads_from_mooring[3]
        H_res = np.sqrt(self.moor_H_x**2 + self.moor_H_y**2)
        self.theta_m = (np.arctan(self.moor_V_min / H_res))
        self.t_m = self.SF * np.sqrt((H_res**2) + (self.moor_V_min**2))

        return self.moor_V_min, H_res, np.rad2deg(self.theta_m), self.t_m

    def load_at_padeye(self):
        '''load at the padeye used for design'''
        E_n = 2.5
        if self.soil == 'cohesive':
            self.mu = 0.4
            Nc = 11.5
            self.Q = E_n * self.d_chain * Nc * self.u_shear_strenght_d
        elif self.soil == 'cohesionless':
            self.mu = np.arctan(0.5 * self.int_friction_angle_d_rad)
            f_int = interpolate.interp1d(x_nq, y_nq)
            Nq = f_int(self.int_friction_angle_d)
            self.Q = E_n * self.d_chain * Nq * self.g_b * self.z_ap

        def f_theta_a(th_a):
            '''maximal value for theta_a from horizontal is 90° '''
            if th_a > np.pi/2:
                th_a = np.pi/2
            return np.sqrt((self.theta_m**2) + ((2 * self.Q * self.z_ap * np.exp(self.mu * (th_a - self.theta_m))) / self.t_m))

        th_a = optimize.fixed_point(f_theta_a, 0.0)

        if th_a > np.pi/2:
            self.theta_a = np.pi/2
        else:
            self.theta_a = th_a

        self.t_a = (self.t_m) / (np.exp(self.mu * (self.theta_a - self.theta_m)))
        self.H_d = self.t_a * np.cos(self.theta_a)
        self.V_d = self.t_a * np.sin(self.theta_a)




    def soil_properties (self):
        '''soil properties definition for design'''
        self.int_friction_angle_d_rad = np.deg2rad(self.int_friction_angle_d)

        if self.u_shear_strenght_d > 0:
            self.soil = 'cohesive'
        elif self.int_friction_angle_d > 0:
            self.soil = 'cohesionless'

        return self.soil


    def init_geometry (self):
        '''minimal dimensions for suction anchor dependind on diameter: D_min/w_t= 250, L/D=3
        initial external diameter 3m'''
        self.w_t = round(self.d_ext/100 , 2)
        self.length = self.d_ext * 3
        self.d_int = self.d_ext - (2 * self.w_t)
        return self.w_t, self.length, self.d_ext


    def geometry_prop (self):
        '''determine geometry properties: cross section area, volume, mass, weight'''
        self.d_int = self.d_ext - (2 * self.w_t)
        self.cs_area = (np.pi/ 4)*(self.d_ext)**2
        self.volume = (((np.pi/4) * ((self.d_ext**2) - (self.d_int**2))) * self.length)
        self.anchor_mass = self.volume * self.steel_d
        self.weight = self.anchor_mass * self.g
        self.sub_weight = self.volume * (self.steel_d - self.water_d) * self.g
        self.shaft_area = (np.pi * self.d_ext)
        self.slenderness = (self.length / self.d_ext)
        self.z_ap = 0.7 * self.length

        return self.anchor_mass, self.z_ap



    def compute_uplift_capacity(self):
        '''Uplift capacity of sucction anchors '''
        if self.soil == 'cohesive':
            alpha = 0.65
            if self.slenderness <= 4.5:
                Nc = 6.2 * (1 + (0.34 * np.arctan(self.slenderness)))
            elif self.slenderness > 4.5:
                Nc = 9
            self.V_ult = (self.cs_area * Nc * self.u_shear_strenght_d) + (alpha * self.shaft_area) + self.sub_weight

        elif self.soil == 'cohesionless':

            delta = 0.5 * self.int_friction_angle_d_rad
            k = 1 - (np.sin(self.int_friction_angle_d_rad))
            self.V_ult = ((np.pi / 2) * (self.d_ext + self.d_int) * (self.length**2) * self.g_b * k * (np.tan(delta))) + self.sub_weight

        return self.V_ult, self.d_ext, self.d_int


    def compute_horizontal_capacity(self):
        '''Lateral capacity of sucction anchors '''
        if self.soil == 'cohesive':
            Np = (3.6)/(np.sqrt((0.75 - (self.z_ap / self.length))**2 + (0.45 * self.z_ap / self.length)**2))
            self.H_ult =  self.length * self.d_ext * Np * self.u_shear_strenght_d

        elif self.soil == 'cohesionless':
            Nq = (np.exp(np.pi * np.tan(self.int_friction_angle_d_rad))) * (np.tan(np.deg2rad((45) + (self.int_friction_angle_d / 2))))**2
            self.H_ult = (1/2) * self.d_ext * Nq * self.g_b * (self.length**2)


    def increase_section(self):
        '''Increase section max values L/D=8 and D/wt = 100'''
        if ((self.d_ext / self.w_t) <= 50) or (self.length / self.d_ext >= 7):
            self.d_ext = round(self.d_ext * 1.10 , 2)
            self.init_geometry()
        else :
            self.length = round(self.length*1.10 , 2)
            self.w_t = round(self.w_t * 1.30, 2)
        self.d_int = self.d_ext - (2 * self.w_t)


    def global_load_verif (self):
        self.inc_verif = (((self.H_d / self.H_ult)**(self.slenderness + 0.5)) + ((self.V_d / self.V_ult)**((1/3 * self.slenderness) + 4.5)))
        return self.inc_verif

    def update_suction(self):
        self.geometry_prop()
        self.load_at_padeye()
        self.compute_horizontal_capacity()
        self.compute_uplift_capacity()
        self.global_load_verif()

    def assess(self):

        logger.info('------------------------------------')
        logger.info('Assessment of suction caisson anchor')
        logger.info('------------------------------------')

        self.soil_properties()
        self.desing_loads_from_mooring()
        self.d_int = self.d_ext - (2 * self.w_t)
        self.update_suction()

        if self.H_ult >= self.H_d:
            self.criteria_horizontal_capacity = True
        else:
            self.criteria_horizontal_capacity = False

        if self.V_ult >= self.V_d:
            self.criteria_uplift_capacity = True
        else:
            self.criteria_uplift_capacity = False

        if self.inc_verif <= 1:
            self.criteria_failure_verification = True
        else:
            self.criteria_failure_verification = False

        logger.info('Final horizontal capacity: ' + str(self.H_ult) + str(' vs ') + str(self.H_d))
        logger.info('Final uplift capacity: ' + str(self.V_ult ) + str(' vs ') + str(self.V_d))
        logger.info('Final failure verification: ' + str(self.inc_verif) + ' (should be <= 1.0)')

        logger.info('Final design:')
        logger.info('Diameter [m]: ' + str(self.d_ext))
        logger.info('Thickness [m]: ' + str(self.w_t))
        logger.info('Length [m]: ' + str(self.length))
        logger.info('Mass [kg]: ' + str(self.anchor_mass))

    def design(self):


        logger.info('--------------------------------')
        logger.info('Design of suction caisson anchor')
        logger.info('--------------------------------')

        logger.info('Applied load Vmin, Vmax : ' + str(self.loads_from_mooring[0:2]))
        logger.info('Applied load Hx, Hy : ' + str(self.loads_from_mooring[2:4]))
        logger.info('Applied load Mx, My : ' + str(self.loads_from_mooring[4:6]))
        logger.info('Applied load vertical position zap : ' + str(self.loads_from_mooring[6]))

        n_iterations = 1
        self.soil_properties()
        self.desing_loads_from_mooring()
        self.init_geometry()
        self.update_suction()
        ''' variable to create a list with all data from each iteration '''
        a_list = []
        b_list = []
        c_list = []
        d_list = []
        e_list = []
        f_list = []
        g_list = []
        a_list.append(n_iterations)
        b_list.append(self.d_ext)
        c_list.append(self.w_t)
        d_list.append(self.length)
        e_list.append(self.H_d / self.H_ult)
        f_list.append(self.V_d / self.V_ult)
        g_list.append(self.inc_verif)

        while self.H_d > self.H_ult :

            if n_iterations >= self.n_iterations_max :
                message_a = "to many iterations " + str(n_iterations)
                logger.info(message_a)
                break
            else:
                n_iterations = n_iterations + 1
                self.increase_section()

            self.update_suction()
            a_list.append(n_iterations)
            b_list.append(self.d_ext)
            c_list.append(self.w_t)
            d_list.append(self.length)
            e_list.append(self.H_d / self.H_ult)
            f_list.append(self.V_d / self.V_ult)
            g_list.append(self.inc_verif)

        while self.V_d > self.V_ult :

            if n_iterations >= self.n_iterations_max :
                message_a = "to many iterations " + str(n_iterations)
                logger.info(message_a)
                break
            else:
                n_iterations = n_iterations + 1
                self.increase_section()

            self.update_suction()
            a_list.append(n_iterations)
            b_list.append(self.d_ext)
            c_list.append(self.w_t)
            d_list.append(self.length)
            e_list.append(self.H_d / self.H_ult)
            f_list.append(self.V_d / self.V_ult)
            g_list.append(self.inc_verif)

        while self.inc_verif >= 1 :

            if n_iterations >= self.n_iterations_max :
                message_a = "to many iterations " + str(n_iterations)
                logger.info(message_a)
                break
            else:
                n_iterations = n_iterations + 1
                self.increase_section()

            self.update_suction()
            a_list.append(n_iterations)
            b_list.append(self.d_ext)
            c_list.append(self.w_t)
            d_list.append(self.length)
            e_list.append(self.H_d / self.H_ult)
            f_list.append(self.V_d / self.V_ult)
            g_list.append(self.inc_verif)

        self.assess()

        '''Dataframe with convergence information'''
        self.convergence =pd.DataFrame({'n_i' :a_list, 'Diam': b_list,  'wt': c_list,'Length': d_list, 'Hd/Hu': e_list, 'Vd/Vu': f_list, 'verif': g_list})

        return n_iterations, self.length, self.d_ext, self.w_t

    def outputs(self):
        bom_esa = pd.DataFrame ({"Materials": ["steel"],
                             "density [kg/m3]": [self.steel_d],
                             "mass [kg]": [self.anchor_mass],
                             "submerge weight  [N]": [self.sub_weight],
                             "Volume [m3]" : [self.volume],
                             "Footprint [m2]": [self.cs_area]})

        anchor_properties = pd.DataFrame ({"External diameter [m]": [self.d_ext], "wall_thickness[m]": [self.w_t], "Length [m]": [self.length], "Length at pad-eye [m]":[self.z_ap]})

        anchor_resistance = pd.DataFrame ({"Lateral_capacity [N]": [self.H_ult], "Uplift capacity [N]": [self.V_ult],"H_d [N]" : [self.H_d] , "H_ult [N]" : [self.H_ult], "V_d [N]" : [self.V_d] , "V_ult [N]" : [self.V_ult],"H_d/H_ult" : [(self.H_d / self.H_ult)], "V_d/V_ult": [(self.V_d / self.V_ult)]})

        angle = pd.DataFrame ({"theta_a": [np.rad2deg(self.theta_a)], "padaye [m]": [self.z_ap],"Ta [N]" : [round(self.t_a) ] })

        return bom_esa, anchor_properties, anchor_resistance, angle


    def outputs_convergence(self):

        self.convergence.plot(kind = 'line', x='n_i', y = 'Length', color = 'green')
        self.convergence.plot(kind = 'line', x='n_i', y = 'Diam', color = 'red')
        # self.convergence.plot(kind = 'line', x='n_i', y = 'Hd/Hu', color= 'green', ax = ax)
        # self.convergence.plot(kind = 'line', x='n_i', y = 'Vd/Vu', color = 'orange', ax = ax)

        with pd.option_context('display.max_rows', None, 'display.max_columns', None ):
            print (self.convergence)

        plt.show()
        return self.convergence

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def moor_V_min(self): # pragma: no cover
        """float: maximal moroing vertical upward load transfert to foundation []
        """
        return self._moor_V_min
    #------------
    @ property
    def moor_H_x(self): # pragma: no cover
        """float: maximal mooring horizontal x load transfert to foundation []
        """
        return self._moor_H_x
    #------------
    @ property
    def moor_H_y(self): # pragma: no cover
        """float: maximal mooring horizontal y load transfert to foundation []
        """
        return self._moor_H_y
    #------------
    @ property
    def u_shear_strenght_d(self): # pragma: no cover
        """float: undrained shear strenght value for design []
        """
        return self._u_shear_strenght_d
    #------------
    @ property
    def int_friction_angle_d(self): # pragma: no cover
        """float: internal friction angle for design rad []
        """
        return self._int_friction_angle_d
    #------------
    @ property
    def n_iterations_max(self): # pragma: no cover
        """float: none []
        """
        return self._n_iterations_max
    #------------
    @ property
    def d_ext(self): # pragma: no cover
        """float: none []
        """
        return self._d_ext
    #------------
    @ property
    def w_t(self): # pragma: no cover
        """float: none []
        """
        return self._w_t
    #------------
    @ property
    def length(self): # pragma: no cover
        """float: none []
        """
        return self._length
    #------------
    @ property
    def H_ult(self): # pragma: no cover
        """float: lateral capacity of pile []
        """
        return self._H_ult
    #------------
    @ property
    def H_d(self): # pragma: no cover
        """float: lateral load on pile []
        """
        return self._H_d
    #------------
    @ property
    def V_d(self): # pragma: no cover
        """float: calculated vertical load []
        """
        return self._V_d
    #------------
    @ property
    def V_ult(self): # pragma: no cover
        """float: uplift capacity of the pile []
        """
        return self._V_ult
    #------------
    @ property
    def inc_verif(self): # pragma: no cover
        """float: Failure verification (should be <=1.0) []
        """
        return self._inc_verif
    #------------
    @ property
    def cs_area(self): # pragma: no cover
        """float: cross section surface []
        """
        return self._cs_area
    #------------
    @ property
    def weight(self): # pragma: no cover
        """float: pile mass in kg []
        """
        return self._weight
    #------------
    @ property
    def g_b(self): # pragma: no cover
        """float: bouyant unit weight of soil  []
        """
        return self._g_b
    #------------
    @ property
    def z_ap(self): # pragma: no cover
        """float: depth of the pad-eye  []
        """
        return self._z_ap
    #------------
    @ property
    def theta_m(self): # pragma: no cover
        """float: chain angle at seabed []
        """
        return self._theta_m
    #------------
    @ property
    def theta_a(self): # pragma: no cover
        """float: chain angle at pad-eye []
        """
        return self._theta_a
    #------------
    @ property
    def t_a(self): # pragma: no cover
        """float: tensio at pad-eye []
        """
        return self._t_a
    #------------
    @ property
    def t_m(self): # pragma: no cover
        """float: resultat load at soil bed []
        """
        return self._t_m
    #------------
    @ property
    def d_chain(self): # pragma: no cover
        """float: chain diameter []
        """
        return self._d_chain
    #------------
    @ property
    def criteria_uplift_capacity(self): # pragma: no cover
        """bool: Uplift capacity criterion. True or False []
        """
        return self._criteria_uplift_capacity
    #------------
    @ property
    def criteria_horizontal_capacity(self): # pragma: no cover
        """bool: Horizontal capacity criterion. True or False []
        """
        return self._criteria_horizontal_capacity
    #------------
    @ property
    def criteria_failure_verification(self): # pragma: no cover
        """bool: Failure verification criterion. True or False []
        """
        return self._criteria_failure_verification
    #------------
    @ property
    def SF(self): # pragma: no cover
        """float: Load safety factor []
        """
        return self._SF
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ moor_V_min.setter
    def moor_V_min(self,val): # pragma: no cover
        self._moor_V_min=float(val)
    #------------
    @ moor_H_x.setter
    def moor_H_x(self,val): # pragma: no cover
        self._moor_H_x=float(val)
    #------------
    @ moor_H_y.setter
    def moor_H_y(self,val): # pragma: no cover
        self._moor_H_y=float(val)
    #------------
    @ u_shear_strenght_d.setter
    def u_shear_strenght_d(self,val): # pragma: no cover
        self._u_shear_strenght_d=float(val)
    #------------
    @ int_friction_angle_d.setter
    def int_friction_angle_d(self,val): # pragma: no cover
        self._int_friction_angle_d=float(val)
    #------------
    @ n_iterations_max.setter
    def n_iterations_max(self,val): # pragma: no cover
        self._n_iterations_max=float(val)
    #------------
    @ d_ext.setter
    def d_ext(self,val): # pragma: no cover
        self._d_ext=float(val)
    #------------
    @ w_t.setter
    def w_t(self,val): # pragma: no cover
        self._w_t=float(val)
    #------------
    @ length.setter
    def length(self,val): # pragma: no cover
        self._length=float(val)
    #------------
    @ H_ult.setter
    def H_ult(self,val): # pragma: no cover
        self._H_ult=float(val)
    #------------
    @ H_d.setter
    def H_d(self,val): # pragma: no cover
        self._H_d=float(val)
    #------------
    @ V_d.setter
    def V_d(self,val): # pragma: no cover
        self._V_d=float(val)
    #------------
    @ V_ult.setter
    def V_ult(self,val): # pragma: no cover
        self._V_ult=float(val)
    #------------
    @ inc_verif.setter
    def inc_verif(self,val): # pragma: no cover
        self._inc_verif=float(val)
    #------------
    @ cs_area.setter
    def cs_area(self,val): # pragma: no cover
        self._cs_area=float(val)
    #------------
    @ weight.setter
    def weight(self,val): # pragma: no cover
        self._weight=float(val)
    #------------
    @ g_b.setter
    def g_b(self,val): # pragma: no cover
        self._g_b=float(val)
    #------------
    @ z_ap.setter
    def z_ap(self,val): # pragma: no cover
        self._z_ap=float(val)
    #------------
    @ theta_m.setter
    def theta_m(self,val): # pragma: no cover
        self._theta_m=float(val)
    #------------
    @ theta_a.setter
    def theta_a(self,val): # pragma: no cover
        self._theta_a=float(val)
    #------------
    @ t_a.setter
    def t_a(self,val): # pragma: no cover
        self._t_a=float(val)
    #------------
    @ t_m.setter
    def t_m(self,val): # pragma: no cover
        self._t_m=float(val)
    #------------
    @ d_chain.setter
    def d_chain(self,val): # pragma: no cover
        self._d_chain=float(val)
    #------------
    @ criteria_uplift_capacity.setter
    def criteria_uplift_capacity(self,val): # pragma: no cover
        self._criteria_uplift_capacity=val
    #------------
    @ criteria_horizontal_capacity.setter
    def criteria_horizontal_capacity(self,val): # pragma: no cover
        self._criteria_horizontal_capacity=val
    #------------
    @ criteria_failure_verification.setter
    def criteria_failure_verification(self,val): # pragma: no cover
        self._criteria_failure_verification=val
    #------------
    @ SF.setter
    def SF(self,val): # pragma: no cover
        self._SF=float(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:foundations:foundationdesign:SuctionCaisson"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:foundations:foundationdesign:SuctionCaisson"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("moor_V_min"):
            rep["moor_V_min"] = self.moor_V_min
        if self.is_set("moor_H_x"):
            rep["moor_H_x"] = self.moor_H_x
        if self.is_set("moor_H_y"):
            rep["moor_H_y"] = self.moor_H_y
        if self.is_set("u_shear_strenght_d"):
            rep["u_shear_strenght_d"] = self.u_shear_strenght_d
        if self.is_set("int_friction_angle_d"):
            rep["int_friction_angle_d"] = self.int_friction_angle_d
        if self.is_set("n_iterations_max"):
            rep["n_iterations_max"] = self.n_iterations_max
        if self.is_set("d_ext"):
            rep["d_ext"] = self.d_ext
        if self.is_set("w_t"):
            rep["w_t"] = self.w_t
        if self.is_set("length"):
            rep["length"] = self.length
        if self.is_set("H_ult"):
            rep["H_ult"] = self.H_ult
        if self.is_set("H_d"):
            rep["H_d"] = self.H_d
        if self.is_set("V_d"):
            rep["V_d"] = self.V_d
        if self.is_set("V_ult"):
            rep["V_ult"] = self.V_ult
        if self.is_set("inc_verif"):
            rep["inc_verif"] = self.inc_verif
        if self.is_set("cs_area"):
            rep["cs_area"] = self.cs_area
        if self.is_set("weight"):
            rep["weight"] = self.weight
        if self.is_set("g_b"):
            rep["g_b"] = self.g_b
        if self.is_set("z_ap"):
            rep["z_ap"] = self.z_ap
        if self.is_set("theta_m"):
            rep["theta_m"] = self.theta_m
        if self.is_set("theta_a"):
            rep["theta_a"] = self.theta_a
        if self.is_set("t_a"):
            rep["t_a"] = self.t_a
        if self.is_set("t_m"):
            rep["t_m"] = self.t_m
        if self.is_set("d_chain"):
            rep["d_chain"] = self.d_chain
        if self.is_set("criteria_uplift_capacity"):
            rep["criteria_uplift_capacity"] = self.criteria_uplift_capacity
        if self.is_set("criteria_horizontal_capacity"):
            rep["criteria_horizontal_capacity"] = self.criteria_horizontal_capacity
        if self.is_set("criteria_failure_verification"):
            rep["criteria_failure_verification"] = self.criteria_failure_verification
        if self.is_set("SF"):
            rep["SF"] = self.SF
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["moor_V_min"] = np.array([self.moor_V_min],dtype=float)
        handle["moor_H_x"] = np.array([self.moor_H_x],dtype=float)
        handle["moor_H_y"] = np.array([self.moor_H_y],dtype=float)
        handle["u_shear_strenght_d"] = np.array([self.u_shear_strenght_d],dtype=float)
        handle["int_friction_angle_d"] = np.array([self.int_friction_angle_d],dtype=float)
        handle["n_iterations_max"] = np.array([self.n_iterations_max],dtype=float)
        handle["d_ext"] = np.array([self.d_ext],dtype=float)
        handle["w_t"] = np.array([self.w_t],dtype=float)
        handle["length"] = np.array([self.length],dtype=float)
        handle["H_ult"] = np.array([self.H_ult],dtype=float)
        handle["H_d"] = np.array([self.H_d],dtype=float)
        handle["V_d"] = np.array([self.V_d],dtype=float)
        handle["V_ult"] = np.array([self.V_ult],dtype=float)
        handle["inc_verif"] = np.array([self.inc_verif],dtype=float)
        handle["cs_area"] = np.array([self.cs_area],dtype=float)
        handle["weight"] = np.array([self.weight],dtype=float)
        handle["g_b"] = np.array([self.g_b],dtype=float)
        handle["z_ap"] = np.array([self.z_ap],dtype=float)
        handle["theta_m"] = np.array([self.theta_m],dtype=float)
        handle["theta_a"] = np.array([self.theta_a],dtype=float)
        handle["t_a"] = np.array([self.t_a],dtype=float)
        handle["t_m"] = np.array([self.t_m],dtype=float)
        handle["d_chain"] = np.array([self.d_chain],dtype=float)
        handle["criteria_uplift_capacity"] = np.array([self.criteria_uplift_capacity],dtype=bool)
        handle["criteria_horizontal_capacity"] = np.array([self.criteria_horizontal_capacity],dtype=bool)
        handle["criteria_failure_verification"] = np.array([self.criteria_failure_verification],dtype=bool)
        handle["SF"] = np.array([self.SF],dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "moor_V_min"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_H_x"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_H_y"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "u_shear_strenght_d"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "int_friction_angle_d"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "n_iterations_max"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "d_ext"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "w_t"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "length"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "H_ult"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "H_d"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "V_d"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "V_ult"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "inc_verif"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "cs_area"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "weight"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "g_b"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "z_ap"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "theta_m"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "theta_a"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "t_a"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "t_m"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "d_chain"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "criteria_uplift_capacity"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "criteria_horizontal_capacity"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "criteria_failure_verification"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "SF"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("moor_V_min" in list(gr.keys())):
            self.moor_V_min = gr["moor_V_min"][0]
        if ("moor_H_x" in list(gr.keys())):
            self.moor_H_x = gr["moor_H_x"][0]
        if ("moor_H_y" in list(gr.keys())):
            self.moor_H_y = gr["moor_H_y"][0]
        if ("u_shear_strenght_d" in list(gr.keys())):
            self.u_shear_strenght_d = gr["u_shear_strenght_d"][0]
        if ("int_friction_angle_d" in list(gr.keys())):
            self.int_friction_angle_d = gr["int_friction_angle_d"][0]
        if ("n_iterations_max" in list(gr.keys())):
            self.n_iterations_max = gr["n_iterations_max"][0]
        if ("d_ext" in list(gr.keys())):
            self.d_ext = gr["d_ext"][0]
        if ("w_t" in list(gr.keys())):
            self.w_t = gr["w_t"][0]
        if ("length" in list(gr.keys())):
            self.length = gr["length"][0]
        if ("H_ult" in list(gr.keys())):
            self.H_ult = gr["H_ult"][0]
        if ("H_d" in list(gr.keys())):
            self.H_d = gr["H_d"][0]
        if ("V_d" in list(gr.keys())):
            self.V_d = gr["V_d"][0]
        if ("V_ult" in list(gr.keys())):
            self.V_ult = gr["V_ult"][0]
        if ("inc_verif" in list(gr.keys())):
            self.inc_verif = gr["inc_verif"][0]
        if ("cs_area" in list(gr.keys())):
            self.cs_area = gr["cs_area"][0]
        if ("weight" in list(gr.keys())):
            self.weight = gr["weight"][0]
        if ("g_b" in list(gr.keys())):
            self.g_b = gr["g_b"][0]
        if ("z_ap" in list(gr.keys())):
            self.z_ap = gr["z_ap"][0]
        if ("theta_m" in list(gr.keys())):
            self.theta_m = gr["theta_m"][0]
        if ("theta_a" in list(gr.keys())):
            self.theta_a = gr["theta_a"][0]
        if ("t_a" in list(gr.keys())):
            self.t_a = gr["t_a"][0]
        if ("t_m" in list(gr.keys())):
            self.t_m = gr["t_m"][0]
        if ("d_chain" in list(gr.keys())):
            self.d_chain = gr["d_chain"][0]
        if ("criteria_uplift_capacity" in list(gr.keys())):
            self.criteria_uplift_capacity = gr["criteria_uplift_capacity"][0]
        if ("criteria_horizontal_capacity" in list(gr.keys())):
            self.criteria_horizontal_capacity = gr["criteria_horizontal_capacity"][0]
        if ("criteria_failure_verification" in list(gr.keys())):
            self.criteria_failure_verification = gr["criteria_failure_verification"][0]
        if ("SF" in list(gr.keys())):
            self.SF = gr["SF"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
