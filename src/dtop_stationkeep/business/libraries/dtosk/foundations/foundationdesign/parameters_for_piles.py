# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

'''Table 2.3 from DNV-C30.4, limits for axial resistance of piles
x_phi = friction angle, y_fi = unit friction resistance, y_nq = bearing capacity, y_qi = unit bearing capacity'''

x_phi= [0, 15, 20, 25, 30, 35, 70]
y_fi= [0, 48e+03, 67e+03, 81e+03, 96e+03, 115e+03, 115e+03]
y_nq= [0, 8, 12, 20, 40, 50, 50]
y_qi= [0, 1.9e+06, 2.9e+06, 4.8e+06, 9.06e+06, 12e+06, 12e+06]