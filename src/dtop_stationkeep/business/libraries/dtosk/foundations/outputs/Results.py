# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from . import ArrayFoundType
from . import Dimensions
from . import BOM
from . import ArrayFoundFoot
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class Results():

    """data model for input data needed to perform the gravity foundation design
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._foundation_type=ArrayFoundType.ArrayFoundType()
        self._foundation_type.description = 'device type- floating or fixed'
        self._foundation_geometry=Dimensions.Dimensions()
        self._foundation_geometry.description = 'foundation geometry description'
        self._BoM_and_Cost=BOM.BOM()
        self._BoM_and_Cost.description = 'bill of materials and costs'
        self._foundation_env_impact=ArrayFoundFoot.ArrayFoundFoot()
        self._foundation_env_impact.description = 'bill of materials and costs'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def foundation_type(self): # pragma: no cover
        """:obj:`~.ArrayFoundType.ArrayFoundType`: device type- floating or fixed
        """
        return self._foundation_type
    #------------
    @ property
    def foundation_geometry(self): # pragma: no cover
        """:obj:`~.Dimensions.Dimensions`: foundation geometry description
        """
        return self._foundation_geometry
    #------------
    @ property
    def BoM_and_Cost(self): # pragma: no cover
        """:obj:`~.BOM.BOM`: bill of materials and costs
        """
        return self._BoM_and_Cost
    #------------
    @ property
    def foundation_env_impact(self): # pragma: no cover
        """:obj:`~.ArrayFoundFoot.ArrayFoundFoot`: bill of materials and costs
        """
        return self._foundation_env_impact
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ foundation_type.setter
    def foundation_type(self,val): # pragma: no cover
        self._foundation_type=val
    #------------
    @ foundation_geometry.setter
    def foundation_geometry(self,val): # pragma: no cover
        self._foundation_geometry=val
    #------------
    @ BoM_and_Cost.setter
    def BoM_and_Cost(self,val): # pragma: no cover
        self._BoM_and_Cost=val
    #------------
    @ foundation_env_impact.setter
    def foundation_env_impact(self,val): # pragma: no cover
        self._foundation_env_impact=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:foundations:outputs:Results"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:foundations:outputs:Results"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("foundation_type"):
            if (short and not(deep)):
                rep["foundation_type"] = self.foundation_type.type_rep()
            else:
                rep["foundation_type"] = self.foundation_type.prop_rep(short, deep)
        if self.is_set("foundation_geometry"):
            if (short and not(deep)):
                rep["foundation_geometry"] = self.foundation_geometry.type_rep()
            else:
                rep["foundation_geometry"] = self.foundation_geometry.prop_rep(short, deep)
        if self.is_set("BoM_and_Cost"):
            if (short and not(deep)):
                rep["BoM_and_Cost"] = self.BoM_and_Cost.type_rep()
            else:
                rep["BoM_and_Cost"] = self.BoM_and_Cost.prop_rep(short, deep)
        if self.is_set("foundation_env_impact"):
            if (short and not(deep)):
                rep["foundation_env_impact"] = self.foundation_env_impact.type_rep()
            else:
                rep["foundation_env_impact"] = self.foundation_env_impact.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        subgroup = handle.create_group("foundation_type")
        self.foundation_type.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("foundation_geometry")
        self.foundation_geometry.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("BoM_and_Cost")
        self.BoM_and_Cost.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("foundation_env_impact")
        self.foundation_env_impact.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "foundation_type"
        try :
            if data[varName] != None:
                self.foundation_type=ArrayFoundType.ArrayFoundType()
                self.foundation_type.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "foundation_geometry"
        try :
            if data[varName] != None:
                self.foundation_geometry=Dimensions.Dimensions()
                self.foundation_geometry.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "BoM_and_Cost"
        try :
            if data[varName] != None:
                self.BoM_and_Cost=BOM.BOM()
                self.BoM_and_Cost.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "foundation_env_impact"
        try :
            if data[varName] != None:
                self.foundation_env_impact=ArrayFoundFoot.ArrayFoundFoot()
                self.foundation_env_impact.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("foundation_type" in list(gr.keys())):
            subgroup = gr["foundation_type"]
            self.foundation_type.loadFromHDF5Handle(subgroup)
        if ("foundation_geometry" in list(gr.keys())):
            subgroup = gr["foundation_geometry"]
            self.foundation_geometry.loadFromHDF5Handle(subgroup)
        if ("BoM_and_Cost" in list(gr.keys())):
            subgroup = gr["BoM_and_Cost"]
            self.BoM_and_Cost.loadFromHDF5Handle(subgroup)
        if ("foundation_env_impact" in list(gr.keys())):
            subgroup = gr["foundation_env_impact"]
            self.foundation_env_impact.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
