# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections 
import numpy as np 
import os
import pandas as pd
import pytest
try: 
    import json 
except: 
    print('WARNING: json is not installed.') 
from .foundationdesign.DragAnchors import DragAnchors
from .foundationdesign.SuctionCaisson import SuctionCaisson
from .foundationdesign.GravityFoundation import GravityFoundation
from .foundationdesign.PileFoundation import PileFoundation
from .foundationtype.FoundationConditions import FoundationConditions
from .inputs.FoundationInputs import FoundationInputs


def run_foundation_design(found_preference_usr,dist_attachment_pa,pile_tip,pile_length_above_seabed,deflection_criteria_for_pile,soil_type_from_sc,soil_slope_from_sc,seabed_connection_type,device_geometry,load_ap,SF,
                        # User defined properties (optionnal)
                        soil_type_def,foundation_material,foundation_material_density,internal_friction_angle,relative_density,undrained_shear_strength,soil_buoyant_weight,soil_properties_sf,user_soil_properties_sf,
                        # Line diameter (used only for suction anchor)
                        d_chain,
                        # Custom foundation input
                        foundation_design_flag,custom_foundation_input,uls_env,water_depth, foundation_type_selection, catalogue_anchors, catalogue_index):

    inputs = FoundationInputs()
    found_def = FoundationConditions()


    ''' Class inputs'''

    inputs.soil_type = soil_type_from_sc
    inputs.user_foundation_pref = found_preference_usr
    inputs.foundation_material = foundation_material
    inputs.foundation_material_density = foundation_material_density
    inputs.geometry_pref = custom_foundation_input.geometry_pref
    #user soil data or Non
    inputs.int_friction_angle = internal_friction_angle
    inputs.sand_dr = relative_density
    inputs.undrained_shear_strength = undrained_shear_strength 
    inputs.g_b = soil_buoyant_weight
    inputs.soil_type_def = soil_type_def

    #user soil properties safety factor
    inputs.soil_properties_sf = soil_properties_sf
    inputs.soil_properties_sf_value = user_soil_properties_sf
        
    inputs.soil_slope = soil_slope_from_sc
    inputs.seabed_connection_type = seabed_connection_type
    inputs.loads_ap = load_ap

    inputs.device_geometry = device_geometry
    inputs.loads_from_mooring = load_ap
    inputs.dist_attachment_pa = dist_attachment_pa
    inputs.deflection_criteria_for_pile = deflection_criteria_for_pile
    inputs.pile_tip = pile_tip
    inputs.pile_length_above_seabed  =  pile_length_above_seabed 

    inputs.outputs_for_design()
        
    ''' Foundation selection'''
    if inputs.user_foundation_pref != 'evaluation' and inputs.user_foundation_pref !='none':

        found_def.foundation_type = inputs.user_foundation_pref
    else :
        found_def.soil_type = inputs.soil_type
        found_def.slope_class = inputs.slope_class
        found_def.main_load_direction = inputs.main_load_direction
        found_def.seabed_connection_type = inputs.seabed_connection_type
        found_def.foundation_preference = inputs.user_foundation_pref

        found_def.foundation_type_evaluation()
        found_def.outputs()

    '''Foundation_design : shallow, gravity_anchor, pile, drag_anchor, suction_caisson'''

    if found_def.foundation_type == 'shallow' or found_def.foundation_type =='gravity_anchor' or found_def.foundation_type == 'gravity': 
        found_design = GravityFoundation()
        found_design.geometry_pref = inputs.geometry_pref
        found_design.device_geometry = inputs.device_geometry
        found_design.slope = inputs.soil_slope
        found_design.seabed_connection_type = inputs.seabed_connection_type
        found_design.material =  inputs.foundation_material
        found_design.material_density = inputs.foundation_material_density
        if foundation_design_flag == 'custom' or foundation_design_flag =='manual':
            found_design.base_radius = custom_foundation_input.base_radius
            found_design.base_lx = custom_foundation_input.base_lx
            found_design.base_ly = custom_foundation_input.base_ly
            found_design.thickness = custom_foundation_input.thickness
            found_design.base_r1 = custom_foundation_input.base_r1
            found_design.base_r2 = custom_foundation_input.base_r2
            found_design.base_r3 = custom_foundation_input.base_r3
            found_design.base_alpha01 = custom_foundation_input.base_alpha01
            found_design.base_alpha12 = custom_foundation_input.base_alpha12
            found_design.base_alpha23 = custom_foundation_input.base_alpha23
            found_design.contact_points_mass = custom_foundation_input.contact_points_mass

    elif found_def.foundation_type == 'pile':
        found_design = PileFoundation()
        found_design.dist_attachment = inputs.dist_attachment_pa
        found_design.tip_type = inputs.pile_tip
        found_design.pile_length_above_seabed = inputs.pile_length_above_seabed
        found_design.deflection_crit = inputs.deflection_criteria_for_pile
        found_design.sand_dr = inputs.sand_dr
        found_design.seabed_connection_type = inputs.seabed_connection_type
        found_design.water_depth = water_depth
        found_design.uls_env = uls_env
        if foundation_design_flag == 'custom'or foundation_design_flag =='manual':
            found_design.d_ext = custom_foundation_input.d_ext
            found_design.w_t = custom_foundation_input.w_t
            found_design.plength = custom_foundation_input.length

    elif found_def.foundation_type =='drag_anchor':
        found_design = DragAnchors()
        found_design.set_catalogue_anchors(catalogue_anchors)
        if foundation_design_flag == 'custom'or foundation_design_flag =='manual':
            found_design.weight = custom_foundation_input.mass
            found_design.m_anchor = custom_foundation_input.drag_anchor_coefficients[0]
            found_design.b_anchor = custom_foundation_input.drag_anchor_coefficients[1]
            found_design.m_A = custom_foundation_input.drag_anchor_coefficients[2]
            found_design.b_A = custom_foundation_input.drag_anchor_coefficients[3]
            found_design.m_B = custom_foundation_input.drag_anchor_coefficients[4]
            found_design.b_B = custom_foundation_input.drag_anchor_coefficients[5]
            found_design.m_EF = custom_foundation_input.drag_anchor_coefficients[6]
            found_design.b_EF = custom_foundation_input.drag_anchor_coefficients[7]

    elif found_def.foundation_type == 'suction_caisson':
        found_design = SuctionCaisson()
        found_design.d_chain = d_chain
        if foundation_design_flag == 'custom'or foundation_design_flag =='manual':
            found_design.d_ext = custom_foundation_input.d_ext
            found_design.w_t = custom_foundation_input.w_t
            found_design.length = custom_foundation_input.length

    found_design.loads_from_mooring = inputs.loads_from_mooring
    found_design.int_friction_angle_d = inputs.int_friction_angle_d
    found_design.sand_dr = inputs.sand_dr
    found_design.u_shear_strenght_d = inputs.u_shear_strenght_d
    found_design.g_b = inputs.g_b
    found_design.SF = SF

    # Run automatic design or only assessment
    if foundation_design_flag == 'auto':
        if found_def.foundation_type =='drag_anchor':
            found_design.design(foundation_type_selection, catalogue_index)
        else:
            found_design.design()
    elif foundation_design_flag == 'custom' or foundation_design_flag == 'manual':
        if found_def.foundation_type =='drag_anchor':
            found_design.assess(catalogue_index)
        else:
            found_design.assess()
    else:
        raise Exception('Unknown foundation design flag: ' + str(foundation_design_flag))

    found_design.outputs()

    return inputs,found_def,found_design




# def run_foundation_design(found_preference_usr,dist_attachment_pa,pile_tip,deflection_criteria_for_pile,soil_type_from_sc,soil_slope_from_sc,seabed_connection_type,device_geometry,load_ap):

#     inputs = FoundationInputs()
#     found_def = FoundationConditions()


#     ''' Class inputs'''
    
        
#     inputs.soil_type = soil_type_from_sc
#     inputs.soil_slope = soil_slope_from_sc
#     inputs.seabed_connection_type = seabed_connection_type
#     inputs.loads_ap = load_ap
#     inputs.foundation_preference = found_preference_usr
#     inputs.device_geometry = device_geometry
#     inputs.loads_from_mooring = load_ap
#     inputs.dist_attachment_pa = dist_attachment_pa
#     inputs.deflection_criteria_for_pile = deflection_criteria_for_pile
#     inputs.pile_tip = pile_tip

#     inputs.outputs_for_design()
        

        
#     ''' Foundation selection'''


#     found_def.soil_type = inputs.soil_type
#     found_def.slope_class = inputs.slope_class
#     found_def.main_load_direction = inputs.main_load_direction
#     found_def.seabed_connection_type = inputs.seabed_connection_type
#     found_def.foundation_preference = inputs.foundation_preference
        

#     found_def.foundation_type_evaluation()

#     found_def.outputs()

#     '''Foundation_design : "shallow", "gravity_anchor", "pile","direct_embedment_anchor", "drag_anchor", "suction_caisson"'''

#     if found_def.foundation_type == 'shallow' or found_def.foundation_type =='gravity_anchor': 
#         found_design = GravityFoundation()
#         found_design.device_geometry = inputs.device_geometry
#         found_design.slope = inputs.soil_slope


#     elif found_def.foundation_type == 'pile':
#         found_design = PileFoundation()
#         found_design.dist_attachment = inputs.dist_attachment_pa
#         found_design.tip_type = inputs.pile_tip
#         found_design.deflection_crit = inputs.deflection_criteria_for_pile

#     elif found_def.foundation_type =='drag_anchor':
#         found_design = DragAnchors()


#     found_design.loads_from_mooring = inputs.loads_from_mooring
#     found_design.seabed_connection_type = inputs.seabed_connection_type
#     found_design.int_friction_angle = inputs.int_friction_angle
#     found_design.u_shear_strenght = inputs.u_shear_strenght
#     found_design.sand_dr = inputs.sand_dr
#     found_design.g_b = inputs.g_b

#     found_design.design()
#     found_design.outputs()

#     return inputs,found_def,found_design