# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections 
import numpy as np 
import os
try: 
    import json 
except: 
    print('WARNING: json is not installed.') 

from foundationtype.FoundationConditions import FoundationConditions
from foundationdesign.GravityFoundation import GravityFoundation


#-----------------------------------------------------------------------------------------------
## Test for foundation condition: it allows to determine the more suitable type of foundation
#
#-----------------------------------------------------------------------------------------------
found = FoundationConditions()
found.foundation_preference="none"
found.soil_type="soft_clay"
found.soil_slope="moderate"
found.main_load_direction="horizontal"
found.device_type="fixed"
a = found.foundation_type_evaluation()
soilclass = found.soil_class()

print(a, soilclass)

r = json.load(open('datafortest\\ex_loads_uls.json'))
Vmax = r ["V"]

print(Vmax)

found.saveJSON("test_conditions")
#------------------------------------------------------------------------------------------------
## Gravity foundation design test
#
#-------------------------------------------------------------------------------------------------

gravity = GravityFoundation()
gravity.soil_type = "stiff_clay"
gravity.geometry_pref = "rectangular"
gravity.int_friction_angle = 30
gravity.u_shear_strenght = 150

gravity.soil_classification = found.soil_class()

geometry = gravity.foundation_initial_geometry()

gravity.soil_design_properties()

print(gravity.soil_type)
print(gravity.design_loads_uls)
print(gravity.soil_classification)
print(geometry)
print(design_materials)

gravity.saveJSON("test_shallowDesign")

