# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
import pandas as pd
pd.options.display.float_format = '{:,.2g}'.format
# @ USER DEFINED IMPORTS END
#------------------------------------

class SoilProperties():

    """data model for input data needed to perform the foundation design
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._bouyant_weight=0.0
        self._friction_angle=0.0
        self._uss=0.0
        self._relative_density=0.0
        self._soil_type=''
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START

# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def soil_properties_cat(self):

        soil_catalogue = pd.DataFrame({"Soil_type": ["very_soft_clay", "soft_clay","firm_clay","stiff_clay","very_stiff_clay", "hard_clay", "very_loose_sand", "loose_sand", "medium_dense_sand", "dense_sand","very_dense_sand","gravels_pebbles"],
                                        "Bouyant_weight [N/m3]":[ 7200, 7200, 7200, 9800, 9800, 9800, 8700, 8700, 8070, 10600, 10600, 10600],
                                        "Friction_angle [°]": [0, 0, 0, 0, 0, 0, 25, 30, 32, 35, 38, 45],
                                        "Uss [Pa]": [10e+03, 20e+03, 33e+03, 75e+03, 150e+03, 200e+03, 0, 0, 0, 0, 0, 0],
                                        "Dr": [0, 0, 0, 0, 0, 0, 0.1, 0.25, 0.45, 0.75, 0.85, 0.85],
                                        "soil_type_def":["cohesive","cohesive","cohesive","cohesive","cohesive","cohesive","cohesionless","cohesionless","cohesionless","cohesionless","cohesionless","cohesionless"] })
        soil_catalogue.to_csv()

        return soil_catalogue
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def bouyant_weight(self): # pragma: no cover
        """float: none []
        """
        return self._bouyant_weight
    #------------
    @ property
    def friction_angle(self): # pragma: no cover
        """float:  []
        """
        return self._friction_angle
    #------------
    @ property
    def uss(self): # pragma: no cover
        """float:  []
        """
        return self._uss
    #------------
    @ property
    def relative_density(self): # pragma: no cover
        """float:  []
        """
        return self._relative_density
    #------------
    @ property
    def soil_type(self): # pragma: no cover
        """str: 
        """
        return self._soil_type
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ bouyant_weight.setter
    def bouyant_weight(self,val): # pragma: no cover
        self._bouyant_weight=float(val)
    #------------
    @ friction_angle.setter
    def friction_angle(self,val): # pragma: no cover
        self._friction_angle=float(val)
    #------------
    @ uss.setter
    def uss(self,val): # pragma: no cover
        self._uss=float(val)
    #------------
    @ relative_density.setter
    def relative_density(self,val): # pragma: no cover
        self._relative_density=float(val)
    #------------
    @ soil_type.setter
    def soil_type(self,val): # pragma: no cover
        self._soil_type=str(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:foundations:inputs:SoilProperties"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:foundations:inputs:SoilProperties"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("bouyant_weight"):
            rep["bouyant_weight"] = self.bouyant_weight
        if self.is_set("friction_angle"):
            rep["friction_angle"] = self.friction_angle
        if self.is_set("uss"):
            rep["uss"] = self.uss
        if self.is_set("relative_density"):
            rep["relative_density"] = self.relative_density
        if self.is_set("soil_type"):
            rep["soil_type"] = self.soil_type
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["bouyant_weight"] = np.array([self.bouyant_weight],dtype=float)
        handle["friction_angle"] = np.array([self.friction_angle],dtype=float)
        handle["uss"] = np.array([self.uss],dtype=float)
        handle["relative_density"] = np.array([self.relative_density],dtype=float)
        ar = []
        ar.append(self.soil_type.encode("ascii"))
        handle["soil_type"] = np.asarray(ar)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "bouyant_weight"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "friction_angle"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "uss"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "relative_density"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "soil_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("bouyant_weight" in list(gr.keys())):
            self.bouyant_weight = gr["bouyant_weight"][0]
        if ("friction_angle" in list(gr.keys())):
            self.friction_angle = gr["friction_angle"][0]
        if ("uss" in list(gr.keys())):
            self.uss = gr["uss"][0]
        if ("relative_density" in list(gr.keys())):
            self.relative_density = gr["relative_density"][0]
        if ("soil_type" in list(gr.keys())):
            self.soil_type = gr["soil_type"][0].decode("ascii")
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
