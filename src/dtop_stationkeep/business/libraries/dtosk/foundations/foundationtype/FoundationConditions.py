# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from . import FoundationMatrixSuitability
#------------------------------------
# @ USER DEFINED IMPORTS START
import pandas as pd
import logging
logger = logging.getLogger()
# @ USER DEFINED IMPORTS END
#------------------------------------

class FoundationConditions():

    """data model for input data needed to perform the foundation design
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._soil_type=''
        self._slope_class=''
        self._foundation_preference='none'
        self._main_load_direction=''
        self._foundation_type=[]
        self._seabed_connection_type=''
        self._suit_matrix=FoundationMatrixSuitability.FoundationMatrixSuitability()
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START

        self.foundation_type_acceptance = {}
        self.device, self.l_d, self.s_s, self.s_t, self.envt = self.suit_matrix.suit_matrix()
        self.slope_class=''
        self.i_s = self.device[self.device["Foundation_type"]== "shallow"].index.values.astype(int)[0]
        self.i_g = self.device[self.device["Foundation_type"]== "gravity"].index.values.astype(int)[0]
        self.i_p = self.device[self.device["Foundation_type"]== "pile"].index.values.astype(int)[0]
        self.i_dr = self.device[self.device["Foundation_type"]== "drag"].index.values.astype(int)[0]
        self.i_sc = self.device[self.device["Foundation_type"]== "suction_caisson"].index.values.astype(int)[0]

# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# def init_dictionnary(self,file):



    def foundation_type_evaluation(self):

        logger.info('------------------------------------------')
        logger.info('Selection of most suitable foundation type')
        logger.info('------------------------------------------')

        if self.foundation_preference == 'none' or self.foundation_preference == 'evaluation':
            impact = 'total_impact'

            self.foundation_type_acceptance["shallow"] = (self.s_t[self.soil_type][self.i_s]) + (self.s_s[self.slope_class][self.i_s]) + (self.l_d[self.main_load_direction][self.i_s]) + (self.device[self.seabed_connection_type][self.i_s]) + (self.envt[impact][self.i_s])

            self.foundation_type_acceptance["gravity_anchor"] = (self.s_t[self.soil_type][self.i_g]) + (self.s_s[self.slope_class][self.i_g]) + (self.l_d[self.main_load_direction][self.i_g]) + (self.device[self.seabed_connection_type][self.i_g]) + (self.envt[impact][self.i_g])

            self.foundation_type_acceptance["pile"] = (self.s_t[self.soil_type][self.i_p]) + (self.s_s[self.slope_class][self.i_p]) + (self.l_d[self.main_load_direction][self.i_p]) + (self.device[self.seabed_connection_type][self.i_p]) + (self.envt[impact][self.i_p])

            self.foundation_type_acceptance["drag_anchor"] = (self.s_t[self.soil_type][self.i_dr]) + (self.s_s[self.slope_class][self.i_dr]) + (self.l_d[self.main_load_direction][self.i_dr]) + (self.device[self.seabed_connection_type][self.i_dr]) + (self.envt[impact][self.i_dr])

            self.foundation_type_acceptance["suction_caisson"] = (self.s_t[self.soil_type][self.i_sc]) + (self.s_s[self.slope_class][self.i_sc]) + (self.l_d[self.main_load_direction][self.i_sc]) + (self.device[self.seabed_connection_type][self.i_sc]) + (self.envt[impact][self.i_sc])

        else:
            message = 'Error from foundation_type_evalution : not possible for CL 1 and CL 2'
            raise Exception(message)


    def outputs(self):

        foundation_score = pd.DataFrame({ "Type of foundation": ["shallow", "gravity_anchor", "pile", "drag_anchor", "suction_caisson" ],
                                        "score": [self.foundation_type_acceptance["shallow"], self.foundation_type_acceptance["gravity_anchor"], self.foundation_type_acceptance["pile"],
                                        self.foundation_type_acceptance["drag_anchor"], self.foundation_type_acceptance["suction_caisson"] ] })

        foundation_score = foundation_score.sort_values(by=['score'])
        foundation_score = foundation_score.reset_index(drop=True)

        self.foundation_type = foundation_score["Type of foundation"][0]


        logger.info('\t'+ foundation_score.to_string().replace('\n', '\n\t'))

        logger.info("The most suitable type of foundation is " + self.foundation_type)

        #return soil_properties, foundation_score

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def soil_type(self): # pragma: no cover
        """str: none
        """
        return self._soil_type
    #------------
    @ property
    def slope_class(self): # pragma: no cover
        """str: soil slope, steep >= 10° or moderate < 10°
        """
        return self._slope_class
    #------------
    @ property
    def foundation_preference(self): # pragma: no cover
        """str: User foundation preference: shallow, gravity, dirembedment, drag, suctioncaisson, none
        """
        return self._foundation_preference
    #------------
    @ property
    def main_load_direction(self): # pragma: no cover
        """str: main load direction: gravity, uplift, horizontal
        """
        return self._main_load_direction
    #------------
    @ property
    def foundation_type(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:foundation type solution, it could be one or two ex: shallow or shallow, suction caisson
        """
        return self._foundation_type
    #------------
    @ property
    def seabed_connection_type(self): # pragma: no cover
        """str: fixed or moored
        """
        return self._seabed_connection_type
    #------------
    @ property
    def suit_matrix(self): # pragma: no cover
        """:obj:`~.FoundationMatrixSuitability.FoundationMatrixSuitability`: none
        """
        return self._suit_matrix
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ soil_type.setter
    def soil_type(self,val): # pragma: no cover
        self._soil_type=str(val)
    #------------
    @ slope_class.setter
    def slope_class(self,val): # pragma: no cover
        self._slope_class=str(val)
    #------------
    @ foundation_preference.setter
    def foundation_preference(self,val): # pragma: no cover
        self._foundation_preference=str(val)
    #------------
    @ main_load_direction.setter
    def main_load_direction(self,val): # pragma: no cover
        self._main_load_direction=str(val)
    #------------
    @ foundation_type.setter
    def foundation_type(self,val): # pragma: no cover
        self._foundation_type=val
    #------------
    @ seabed_connection_type.setter
    def seabed_connection_type(self,val): # pragma: no cover
        self._seabed_connection_type=str(val)
    #------------
    @ suit_matrix.setter
    def suit_matrix(self,val): # pragma: no cover
        self._suit_matrix=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:foundations:foundationtype:FoundationConditions"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:foundations:foundationtype:FoundationConditions"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("soil_type"):
            rep["soil_type"] = self.soil_type
        if self.is_set("slope_class"):
            rep["slope_class"] = self.slope_class
        if self.is_set("foundation_preference"):
            rep["foundation_preference"] = self.foundation_preference
        if self.is_set("main_load_direction"):
            rep["main_load_direction"] = self.main_load_direction
        if self.is_set("foundation_type"):
            if short:
                rep["foundation_type"] = str(len(self.foundation_type))
            else:
                rep["foundation_type"] = self.foundation_type
        else:
            rep["foundation_type"] = []
        if self.is_set("seabed_connection_type"):
            rep["seabed_connection_type"] = self.seabed_connection_type
        if self.is_set("suit_matrix"):
            if (short and not(deep)):
                rep["suit_matrix"] = self.suit_matrix.type_rep()
            else:
                rep["suit_matrix"] = self.suit_matrix.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        ar = []
        ar.append(self.soil_type.encode("ascii"))
        handle["soil_type"] = np.asarray(ar)
        ar = []
        ar.append(self.slope_class.encode("ascii"))
        handle["slope_class"] = np.asarray(ar)
        ar = []
        ar.append(self.foundation_preference.encode("ascii"))
        handle["foundation_preference"] = np.asarray(ar)
        ar = []
        ar.append(self.main_load_direction.encode("ascii"))
        handle["main_load_direction"] = np.asarray(ar)
        asciiList = [n.encode("ascii", "ignore") for n in self.foundation_type]
        handle["foundation_type"] = asciiList
        ar = []
        ar.append(self.seabed_connection_type.encode("ascii"))
        handle["seabed_connection_type"] = np.asarray(ar)
        subgroup = handle.create_group("suit_matrix")
        self.suit_matrix.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "soil_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "slope_class"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "foundation_preference"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "main_load_direction"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "foundation_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "seabed_connection_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "suit_matrix"
        try :
            if data[varName] != None:
                self.suit_matrix=FoundationMatrixSuitability.FoundationMatrixSuitability()
                self.suit_matrix.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("soil_type" in list(gr.keys())):
            self.soil_type = gr["soil_type"][0].decode("ascii")
        if ("slope_class" in list(gr.keys())):
            self.slope_class = gr["slope_class"][0].decode("ascii")
        if ("foundation_preference" in list(gr.keys())):
            self.foundation_preference = gr["foundation_preference"][0].decode("ascii")
        if ("main_load_direction" in list(gr.keys())):
            self.main_load_direction = gr["main_load_direction"][0].decode("ascii")
        if ("foundation_type" in list(gr.keys())):
            self.foundation_type = [n.decode("ascii", "ignore") for n in gr["foundation_type"][:]]
        if ("seabed_connection_type" in list(gr.keys())):
            self.seabed_connection_type = gr["seabed_connection_type"][0].decode("ascii")
        if ("suit_matrix" in list(gr.keys())):
            subgroup = gr["suit_matrix"]
            self.suit_matrix.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
