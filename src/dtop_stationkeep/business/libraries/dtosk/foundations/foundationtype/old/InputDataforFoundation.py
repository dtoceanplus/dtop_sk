# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class InputDataforFoundation():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._device_type=''
        self._depth=0.0
        self._soil_type=''
        self._depth_to_bedrock=0.0
        self._soil_slope=''
        self._foundation_prefrence=''
        self._main_load_direction=''
        self._foundation_type=[]
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def device_type(self):
        return self._device_type
    #------------
    @ property
    def depth(self):
        return self._depth
    #------------
    @ property
    def soil_type(self):
        return self._soil_type
    #------------
    @ property
    def depth_to_bedrock(self):
        return self._depth_to_bedrock
    #------------
    @ property
    def soil_slope(self):
        return self._soil_slope
    #------------
    @ property
    def foundation_prefrence(self):
        return self._foundation_prefrence
    #------------
    @ property
    def main_load_direction(self):
        return self._main_load_direction
    #------------
    @ property
    def foundation_type(self):
        return self._foundation_type
    #------------
    @ property
    def name(self):
        return self._name
    #------------
    @ property
    def description(self):
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ device_type.setter
    def device_type(self,val):
        self._device_type=str(val)
    #------------
    @ depth.setter
    def depth(self,val):
        self._depth=float(val)
    #------------
    @ soil_type.setter
    def soil_type(self,val):
        self._soil_type=str(val)
    #------------
    @ depth_to_bedrock.setter
    def depth_to_bedrock(self,val):
        self._depth_to_bedrock=float(val)
    #------------
    @ soil_slope.setter
    def soil_slope(self,val):
        self._soil_slope=str(val)
    #------------
    @ foundation_prefrence.setter
    def foundation_prefrence(self,val):
        self._foundation_prefrence=str(val)
    #------------
    @ main_load_direction.setter
    def main_load_direction(self,val):
        self._main_load_direction=str(val)
    #------------
    @ foundation_type.setter
    def foundation_type(self,val):
        self._foundation_type=val
    #------------
    @ name.setter
    def name(self,val):
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val):
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self):
        rep = collections.OrderedDict()
        rep["__type__"] = "foundations:InputDataforFoundation"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "foundations:InputDataforFoundation"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("device_type"):
            rep["device_type"] = self.device_type
        if self.is_set("depth"):
            rep["depth"] = self.depth
        if self.is_set("soil_type"):
            rep["soil_type"] = self.soil_type
        if self.is_set("depth_to_bedrock"):
            rep["depth_to_bedrock"] = self.depth_to_bedrock
        if self.is_set("soil_slope"):
            rep["soil_slope"] = self.soil_slope
        if self.is_set("foundation_prefrence"):
            rep["foundation_prefrence"] = self.foundation_prefrence
        if self.is_set("main_load_direction"):
            rep["main_load_direction"] = self.main_load_direction
        if self.is_set("foundation_type"):
            if short:
                rep["foundation_type"] = str(len(foundation_type))
            else:
                rep["foundation_type"] = self.foundation_type
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "device_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "depth"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "soil_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "depth_to_bedrock"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "soil_slope"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "foundation_prefrence"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "main_load_direction"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "foundation_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName):
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
