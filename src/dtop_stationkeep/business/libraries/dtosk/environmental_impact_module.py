# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
from .object_list_module import get_linetype_data_from_linetypename
from .outputs.MaterialQuantity import MaterialQuantity


def compute_mooring_footprint(mooring):

	# Init
	footprint = 0.0
	Lb = 0.0

	# Get maximum line length touching the seabed
	for line_index in range(0,mooring.map_model.size_lines()):
		[psi,Lb_temp,alpha,alpha_anchor] = mooring.get_line_geometry_data(line_index)
		Lb = max(Lb,Lb_temp)

	# Compute the footprint as the disk of radius Lb
	# Note: the footprint is thus independent of the number of lines. This is a coarse model, and more precise models should be further developped
	# pro of this simple model: footprint of a catenary system is larger than a taut system (which is zero), which is ok
	# cons of this simple model: footprint of a catenary system might be overconservative
	# Ideally: footprint should be computed based on the area screened by Lb for different device offsets (advanced model)
	footprint = np.pi * Lb **2

	return footprint

def	compute_mooring_submerged_surface(mooring):

	# Init
	submerged_surface = 0.0

	# Get and sum submerged surface of each line segment
	for idx in range(0,len(mooring.line_properties)):
		line_type = get_linetype_data_from_linetypename(mooring.line_dictionnary,mooring.line_properties[idx].line_type_name)
		if line_type.type == 'chain':
			ss_temp = 2*mooring.line_properties[idx].unstretched_length * np.pi * line_type.diameter
		else:
			ss_temp = mooring.line_properties[idx].unstretched_length * np.pi * line_type.diameter
		submerged_surface = submerged_surface + ss_temp

	return submerged_surface

def	compute_material_list(mooring,material_list):

	# Properties of MaterialQuantity are:
	#  - material_name
    #  - material_quantity

	# Get material name and quantity of each line segment
	for idx in range(0,len(mooring.line_properties)):
		line_type = get_linetype_data_from_linetypename(mooring.line_dictionnary,mooring.line_properties[idx].line_type_name)

		# Get quantity in kg
		quantity = line_type.weight_in_air * mooring.line_properties[idx].unstretched_length

		# Register the material quantity
		material_found = False
		idx_entry = None
		for idx2 in range(0,len(material_list)):
			if material_list[idx2].material_name == line_type.material:
				idx_entry = idx2
				material_found = True
		
		if not material_found:
			material_list.append(MaterialQuantity())
			idx_entry = -1
			material_list[idx_entry].material_name = line_type.material

		material_list[idx_entry].material_quantity = material_list[idx_entry].material_quantity + quantity
	
	pass # material_list is modified after this routine is called

def	add_to_material_list(material_name,quantity,material_list):

	# Register the material quantity
	material_found = False
	idx_entry = None
	for idx2 in range(0,len(material_list)):
		if material_list[idx2].material_name == material_name:
			idx_entry = idx2
			material_found = True

	if not material_found:
		material_list.append(MaterialQuantity())
		idx_entry = -1
		material_list[idx_entry].material_name = material_name

	material_list[idx_entry].material_quantity = material_list[idx_entry].material_quantity + quantity

def merge_material_list(material_quantity_list,material_quantity_sublist):

	for idx in range(0,len(material_quantity_sublist)):

		# Register the material quantity
		material_found = False
		idx_entry = None
		for idx2 in range(0,len(material_quantity_list)):
			if material_quantity_list[idx2].material_name == material_quantity_sublist[idx].material_name:
				idx_entry = idx2
				material_found = True
		
		if not material_found:
			material_quantity_list.append(MaterialQuantity())
			idx_entry = -1
			material_quantity_list[idx_entry].material_name = material_quantity_sublist[idx].material_name

		material_quantity_list[idx_entry].material_quantity = material_quantity_list[idx_entry].material_quantity + material_quantity_sublist[idx].material_quantity	

	pass # material_quantity_list is modified after this routine is called
