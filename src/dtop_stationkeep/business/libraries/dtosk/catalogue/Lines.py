# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class Lines():

    """data model representing a catalogue of all type of mooring lines
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._catalogue_id=[]
        self._type=[]
        self._material=[]
        self._quality=[]
        self._diameter=np.zeros(shape=(1), dtype=float)
        self._weight_in_air=np.zeros(shape=(1), dtype=float)
        self._weight_in_water=np.zeros(shape=(1), dtype=float)
        self._mbl=np.zeros(shape=(1), dtype=float)
        self._ea=np.zeros(shape=(1), dtype=float)
        self._cost_per_meter=np.zeros(shape=(1), dtype=float)
        self._ad=np.zeros(shape=(1), dtype=float)
        self._m=np.zeros(shape=(1), dtype=float)
        self._failure_rate_repair=np.zeros(shape=(1), dtype=float)
        self._failure_rate_replacement=np.zeros(shape=(1), dtype=float)
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
        self.create_default_catalogue()
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def create_default_catalogue(self):

        self.name='default_catalogue'
        self._catalogue_id=[]
        self._type=[]
        self._material=[]
        self._quality=[]
        self._failure_rate_repair=[]
        self._failure_rate_replacement=[]

        ####################
        # Chains catalogue #
        ####################
        # We build here a default catalogue based on R3 chains.
        steel_young_modulus = 200*10**9
        rho_steel = 7850.0
        sea_water_density = 1025.0
        cost_manufactured_steel = 1.5 # €/m

        # Classical chain values
        self.diameter = np.array([0.019,	0.0205,	0.022,	0.024,	0.026,	0.028,	0.03,	0.032,	0.034,	0.036,	0.038,	0.04,	0.042,	0.044,	0.046,	0.048,	0.05,	0.052,	0.054,	0.056,	0.058,	0.06,	0.062,	0.064,	0.066,	0.068,	0.07,	0.073,	0.076,	0.078,	0.081,	0.084,	0.087,	0.09,	0.092,	0.095,	0.097,	0.1,	0.102,	0.105,	0.107,	0.111,	0.114,	0.117,	0.12,	0.122,	0.124,	0.127,	0.13,	0.132,	0.137,	0.142,	0.147,	0.152,	0.157,	0.162,	0.165,	0.168,	0.171,	0.175,	0.178,	0.18,	0.185])
        self.weight_in_air= np.array([7,	8,	10,	12,	14,	16,	18,	20,	23,	26,	29,	32,	35,	39,	42,	46,	50,	54,	58,	63,	67,	72,	77,	82,	87,	92,	98,	107,	116,	122,	131,	141,	151,	162,	169,	181,	188,	200,	208,	221,	229,	246,	260,	274,	288,	298,	308,	323,	338,	348,	375,	403,	432,	462,	493,	525,	545,	564,	585,	613,	634,	648,	685])
        self.mbl = 1000.0 * np.array([342,	397,	456,	541,	632,	730,	835,	946,	1064,	1188,	1319,	1456,	1599,	1748,	1903,	2063,	2230,	2402,	2580,	2764,	2953,	3147,	3347,	3551,	3761,	3976,	4196,	4535,	4884,	5123,	5490,	5866,	6252,	6647,	6916,	7326,	7604,	8028,	8315,	8753,	9048,	9650,	10109,	10574,	11047,	11365,	11686,	12171,	12663,	12993,	13829,	14677,	15536,	16405,	17282,	18166,	18699,	19234,	19771,	20488,	21027,	21387,	22286])

        # Additional data
        self.ea = np.zeros(shape=(len(self.diameter)),dtype=float)
        self.weight_in_water = np.zeros(shape=(len(self.diameter)),dtype=float)
        self.ad = np.zeros(shape=(len(self.diameter)),dtype=float)
        self.m = np.zeros(shape=(len(self.diameter)),dtype=float)
        self.cost_per_meter = np.zeros(shape=(len(self.diameter)),dtype=float)
        for idx in range(0,len(self.diameter)):

            # Metadata
            self.catalogue_id.append('chain_R3_' + str(int(self.diameter[idx]*1000)))
            self.type.append('chain')
            self.material.append('steel')
            self.quality.append('R3')
            self.failure_rate_repair.append(0.0)
            self.failure_rate_replacement.append(0.00722)

            #Elasticity
            self.ea[idx] = 2*np.pi*self.diameter[idx]**2/4*steel_young_modulus

            # Weight in water per meter
            self.weight_in_water[idx] = self.weight_in_air[idx]*(rho_steel - sea_water_density)/rho_steel

            # Cost per meter
            self.cost_per_meter[idx] = cost_manufactured_steel*self.weight_in_air[idx]

        # Default value of SN curve from DNVGL-OS-E301 for studless link
        self.ad[:] = 6.0*(10**10)
        self.m[:] = 3.0

        ####################
        # Nylon catalogue #
        ####################
        # We build here a default catalogue based on nylon ropes
        # Classical chain values
        diameter = np.array([0.019,	0.0205,	0.022,	0.024,	0.026,	0.028,	0.03,	0.032,	0.034,	0.036,	0.038,	0.04,	0.042,	0.044,	0.046,	0.048,	0.05,	0.052,	0.054,	0.056,	0.058,	0.06,	0.062,	0.064,	0.066,	0.068,	0.07,	0.073,	0.076,	0.078,	0.081,	0.084,	0.087,	0.09,	0.092,	0.095,	0.097,	0.1,	0.102,	0.105,	0.107,	0.111,	0.114,	0.117,	0.12,	0.122,	0.124,	0.127,	0.13,	0.132,	0.137,	0.142, 0.146, 0.147,	0.152,	0.157,	0.162,	0.165,	0.168,	0.171,	0.175,	0.178,	0.18,	0.185])

        # Additional data
        ea = np.zeros(shape=(len(diameter)),dtype=float)
        weight_in_water = np.zeros(shape=(len(diameter)),dtype=float)
        ad = np.zeros(shape=(len(diameter)),dtype=float)
        m = np.zeros(shape=(len(diameter)),dtype=float)
        mbl = np.zeros(shape=(len(diameter)),dtype=float)
        weight_in_air = np.zeros(shape=(len(diameter)),dtype=float)
        cost_per_meter = np.zeros(shape=(len(diameter)),dtype=float)
        catalogue_id = []
        typee = []
        material = []
        quality = []
        for idx in range(0,len(diameter)):

            # Metadata
            catalogue_id.append('nylon_rope_' + str(int(diameter[idx]*1000)))
            typee.append('nylon_rope')
            material.append('nylon')
            quality.append('basic')
            self.failure_rate_repair.append(0.0)
            self.failure_rate_replacement.append(0.00417)

            # Weight in air
            weight_in_air[idx] = 0.6476e3*diameter[idx]**2

            # MBL
            mbl[idx] = 139357000*diameter[idx]**2

            #Elasticity
            ea[idx] = 1.18*10**8*diameter[idx]**2

            # Weight in water per meter
            od = 0.85046*diameter[idx]
            weight_in_water[idx] = weight_in_air[idx] - np.pi*od**2/4*1025

            # Cost per meter
            cost_per_meter[idx] = 2392*diameter[idx]**2 # Qingdao Florescence Co., LTD, based on 8 Strands 80mm nylon rope. https://florescence.en.made-in-china.com/product-group/rokQtqdOHRYU/NYLON-ROPE-catalog-1.html

        # Default value of TN curve from DNVGL-OS-E301
        ad[:] = 0.259 # T-N curve for fiber ropes!
        m[:] = 13.46 # T-N curve for fiber ropes!

        # Append
        self.diameter = np.append(self.diameter,diameter)
        self.weight_in_air = np.append(self.weight_in_air,weight_in_air)
        self.mbl = np.append(self.mbl,mbl)
        self.catalogue_id = self.catalogue_id + catalogue_id # Concatenation of lists
        self.type = self.type + typee # Concatenation of lists
        self.material = self.material + material # Concatenation of lists
        self.quality = self.quality + quality # Concatenation of lists
        self.ea=np.append(self.ea,ea)
        self.weight_in_water=np.append(self.weight_in_water,weight_in_water)
        self.cost_per_meter=np.append(self.cost_per_meter,cost_per_meter)
        self.ad=np.append(self.ad,ad)
        self.m =np.append(self.m,m)


    def extract(self,ltype,quality):

        # Create new catalogue instance
        catalogue_part = Lines()

        # Fill the catalogues with the lines that have the correct type and quality
        idx_list=[]
        for idx in range(0,len(self.diameter)):
            if self.type[idx] == ltype and self.quality[idx] == quality:
                idx_list.append(idx)


        # Number of valid entries:
        nv = len(idx_list)
        if nv==0:
            raise Exception('No entries in line catalogue for type ' + str(ltype) + ' and quality ' +str(quality))

        # Init
        catalogue_part.diameter=np.zeros(shape=(nv), dtype=float)
        catalogue_part.weight_in_air=np.zeros(shape=(nv), dtype=float)
        catalogue_part.weight_in_water=np.zeros(shape=(nv), dtype=float)
        catalogue_part.mbl=np.zeros(shape=(nv), dtype=float)
        catalogue_part.ea=np.zeros(shape=(nv), dtype=float)
        catalogue_part.cost_per_meter=np.zeros(shape=(nv), dtype=float)
        catalogue_part.ad=np.zeros(shape=(nv), dtype=float)
        catalogue_part.m=np.zeros(shape=(nv), dtype=float)

        # Sort by increasing diameter
        def diam(elem):
            return self.diameter[elem]
        idx_list.sort(key=diam)

        # Extract relevant data
        for idx in range(0,len(idx_list)):
            catalogue_part.catalogue_id.append(self.catalogue_id[idx_list[idx]])
            catalogue_part.type.append(self.type[idx_list[idx]])
            catalogue_part.material.append(self.material[idx_list[idx]])
            catalogue_part.quality.append(self.quality[idx_list[idx]])

            catalogue_part.diameter[idx] = self.diameter[idx_list[idx]]
            catalogue_part.weight_in_air[idx] = self.weight_in_air[idx_list[idx]]
            catalogue_part.weight_in_water[idx] = self.weight_in_water[idx_list[idx]]
            catalogue_part.mbl[idx] = self.mbl[idx_list[idx]]
            catalogue_part.ea[idx] = self.ea[idx_list[idx]]
            catalogue_part.cost_per_meter[idx] = self.cost_per_meter[idx_list[idx]]
            catalogue_part.ad[idx] = self.ad[idx_list[idx]]
            catalogue_part.m[idx] = self.m[idx_list[idx]]

        catalogue_part.name='extract_'+str(self.name)
        catalogue_part.description = 'extracted from ' + self.name + ' for type ' + str(ltype) + ' and quality ' +str(quality)

        return catalogue_part

    def select_one_diameter_larger(self,diameter):

        # Init
        candidate_found = False

        idx=0

        while diameter>=self.diameter[idx] and idx<len(self.diameter):

            idx = idx+1

        if diameter<self.diameter[idx]:
            candidate_found = True

        return candidate_found, idx

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def catalogue_id(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:catalogue ID
        """
        return self._catalogue_id
    #------------
    @ property
    def type(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:line type : chain, rope, polyester
        """
        return self._type
    #------------
    @ property
    def material(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:line material : steel, polyester
        """
        return self._material
    #------------
    @ property
    def quality(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:line quality (used for chain) : R3,R4
        """
        return self._quality
    #------------
    @ property
    def diameter(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:rope diameter, or chain nominal diameter dim(*) []
        """
        return self._diameter
    #------------
    @ property
    def weight_in_air(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:weight per meter in air [kg/m] dim(*) []
        """
        return self._weight_in_air
    #------------
    @ property
    def weight_in_water(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:weight per meter in water [kg/m] dim(*) []
        """
        return self._weight_in_water
    #------------
    @ property
    def mbl(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:breaking load [N] dim(*) []
        """
        return self._mbl
    #------------
    @ property
    def ea(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:elasticity of the line [N] dim(*) []
        """
        return self._ea
    #------------
    @ property
    def cost_per_meter(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:cost per meter line [euros/m] dim(*) []
        """
        return self._cost_per_meter
    #------------
    @ property
    def ad(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:intercept parameter of the S-N curve, where the stress is given in [MPa] dim(*) []
        """
        return self._ad
    #------------
    @ property
    def m(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:slope of the S-N curve dim(*) []
        """
        return self._m
    #------------
    @ property
    def failure_rate_repair(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:failure rate repair of the line dim(*) []
        """
        return self._failure_rate_repair
    #------------
    @ property
    def failure_rate_replacement(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:failure rate replacement of the line dim(*) []
        """
        return self._failure_rate_replacement
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ catalogue_id.setter
    def catalogue_id(self,val): # pragma: no cover
        self._catalogue_id=val
    #------------
    @ type.setter
    def type(self,val): # pragma: no cover
        self._type=val
    #------------
    @ material.setter
    def material(self,val): # pragma: no cover
        self._material=val
    #------------
    @ quality.setter
    def quality(self,val): # pragma: no cover
        self._quality=val
    #------------
    @ diameter.setter
    def diameter(self,val): # pragma: no cover
        self._diameter=val
    #------------
    @ weight_in_air.setter
    def weight_in_air(self,val): # pragma: no cover
        self._weight_in_air=val
    #------------
    @ weight_in_water.setter
    def weight_in_water(self,val): # pragma: no cover
        self._weight_in_water=val
    #------------
    @ mbl.setter
    def mbl(self,val): # pragma: no cover
        self._mbl=val
    #------------
    @ ea.setter
    def ea(self,val): # pragma: no cover
        self._ea=val
    #------------
    @ cost_per_meter.setter
    def cost_per_meter(self,val): # pragma: no cover
        self._cost_per_meter=val
    #------------
    @ ad.setter
    def ad(self,val): # pragma: no cover
        self._ad=val
    #------------
    @ m.setter
    def m(self,val): # pragma: no cover
        self._m=val
    #------------
    @ failure_rate_repair.setter
    def failure_rate_repair(self,val): # pragma: no cover
        self._failure_rate_repair=val
    #------------
    @ failure_rate_replacement.setter
    def failure_rate_replacement(self,val): # pragma: no cover
        self._failure_rate_replacement=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:catalogue:Lines"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:catalogue:Lines"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("catalogue_id"):
            if short:
                rep["catalogue_id"] = str(len(self.catalogue_id))
            else:
                rep["catalogue_id"] = self.catalogue_id
        else:
            rep["catalogue_id"] = []
        if self.is_set("type"):
            if short:
                rep["type"] = str(len(self.type))
            else:
                rep["type"] = self.type
        else:
            rep["type"] = []
        if self.is_set("material"):
            if short:
                rep["material"] = str(len(self.material))
            else:
                rep["material"] = self.material
        else:
            rep["material"] = []
        if self.is_set("quality"):
            if short:
                rep["quality"] = str(len(self.quality))
            else:
                rep["quality"] = self.quality
        else:
            rep["quality"] = []
        if self.is_set("diameter"):
            if (short):
                rep["diameter"] = str(self.diameter.shape)
            else:
                try:
                    rep["diameter"] = self.diameter.tolist()
                except:
                    rep["diameter"] = self.diameter
        if self.is_set("weight_in_air"):
            if (short):
                rep["weight_in_air"] = str(self.weight_in_air.shape)
            else:
                try:
                    rep["weight_in_air"] = self.weight_in_air.tolist()
                except:
                    rep["weight_in_air"] = self.weight_in_air
        if self.is_set("weight_in_water"):
            if (short):
                rep["weight_in_water"] = str(self.weight_in_water.shape)
            else:
                try:
                    rep["weight_in_water"] = self.weight_in_water.tolist()
                except:
                    rep["weight_in_water"] = self.weight_in_water
        if self.is_set("mbl"):
            if (short):
                rep["mbl"] = str(self.mbl.shape)
            else:
                try:
                    rep["mbl"] = self.mbl.tolist()
                except:
                    rep["mbl"] = self.mbl
        if self.is_set("ea"):
            if (short):
                rep["ea"] = str(self.ea.shape)
            else:
                try:
                    rep["ea"] = self.ea.tolist()
                except:
                    rep["ea"] = self.ea
        if self.is_set("cost_per_meter"):
            if (short):
                rep["cost_per_meter"] = str(self.cost_per_meter.shape)
            else:
                try:
                    rep["cost_per_meter"] = self.cost_per_meter.tolist()
                except:
                    rep["cost_per_meter"] = self.cost_per_meter
        if self.is_set("ad"):
            if (short):
                rep["ad"] = str(self.ad.shape)
            else:
                try:
                    rep["ad"] = self.ad.tolist()
                except:
                    rep["ad"] = self.ad
        if self.is_set("m"):
            if (short):
                rep["m"] = str(self.m.shape)
            else:
                try:
                    rep["m"] = self.m.tolist()
                except:
                    rep["m"] = self.m
        if self.is_set("failure_rate_repair"):
            if (short):
                rep["failure_rate_repair"] = str(self.failure_rate_repair.shape)
            else:
                try:
                    rep["failure_rate_repair"] = self.failure_rate_repair.tolist()
                except:
                    rep["failure_rate_repair"] = self.failure_rate_repair
        if self.is_set("failure_rate_replacement"):
            if (short):
                rep["failure_rate_replacement"] = str(self.failure_rate_replacement.shape)
            else:
                try:
                    rep["failure_rate_replacement"] = self.failure_rate_replacement.tolist()
                except:
                    rep["failure_rate_replacement"] = self.failure_rate_replacement
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        asciiList = [n.encode("ascii", "ignore") for n in self.catalogue_id]
        handle["catalogue_id"] = asciiList
        asciiList = [n.encode("ascii", "ignore") for n in self.type]
        handle["type"] = asciiList
        asciiList = [n.encode("ascii", "ignore") for n in self.material]
        handle["material"] = asciiList
        asciiList = [n.encode("ascii", "ignore") for n in self.quality]
        handle["quality"] = asciiList
        handle["diameter"] = np.array(self.diameter,dtype=float)
        handle["weight_in_air"] = np.array(self.weight_in_air,dtype=float)
        handle["weight_in_water"] = np.array(self.weight_in_water,dtype=float)
        handle["mbl"] = np.array(self.mbl,dtype=float)
        handle["ea"] = np.array(self.ea,dtype=float)
        handle["cost_per_meter"] = np.array(self.cost_per_meter,dtype=float)
        handle["ad"] = np.array(self.ad,dtype=float)
        handle["m"] = np.array(self.m,dtype=float)
        handle["failure_rate_repair"] = np.array(self.failure_rate_repair,dtype=float)
        handle["failure_rate_replacement"] = np.array(self.failure_rate_replacement,dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "catalogue_id"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "material"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "quality"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "diameter"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "weight_in_air"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "weight_in_water"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "mbl"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "ea"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "cost_per_meter"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "ad"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "m"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "failure_rate_repair"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "failure_rate_replacement"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("catalogue_id" in list(gr.keys())):
            self.catalogue_id = [n.decode("ascii", "ignore") for n in gr["catalogue_id"][:]]
        if ("type" in list(gr.keys())):
            self.type = [n.decode("ascii", "ignore") for n in gr["type"][:]]
        if ("material" in list(gr.keys())):
            self.material = [n.decode("ascii", "ignore") for n in gr["material"][:]]
        if ("quality" in list(gr.keys())):
            self.quality = [n.decode("ascii", "ignore") for n in gr["quality"][:]]
        if ("diameter" in list(gr.keys())):
            self.diameter = gr["diameter"][:]
        if ("weight_in_air" in list(gr.keys())):
            self.weight_in_air = gr["weight_in_air"][:]
        if ("weight_in_water" in list(gr.keys())):
            self.weight_in_water = gr["weight_in_water"][:]
        if ("mbl" in list(gr.keys())):
            self.mbl = gr["mbl"][:]
        if ("ea" in list(gr.keys())):
            self.ea = gr["ea"][:]
        if ("cost_per_meter" in list(gr.keys())):
            self.cost_per_meter = gr["cost_per_meter"][:]
        if ("ad" in list(gr.keys())):
            self.ad = gr["ad"][:]
        if ("m" in list(gr.keys())):
            self.m = gr["m"][:]
        if ("failure_rate_repair" in list(gr.keys())):
            self.failure_rate_repair = gr["failure_rate_repair"][:]
        if ("failure_rate_replacement" in list(gr.keys())):
            self.failure_rate_replacement = gr["failure_rate_replacement"][:]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
