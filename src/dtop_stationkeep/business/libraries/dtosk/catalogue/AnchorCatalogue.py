# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
import pandas as pd
pd.options.display.float_format = '{:,.4g}'.format
# @ USER DEFINED IMPORTS END
#------------------------------------

class AnchorCatalogue():

    """data model Drag anchor catalog
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._catalogue_id=[]
        self._m_soft_soil=np.zeros(shape=(1), dtype=float)
        self._b_soft_soil=np.zeros(shape=(1), dtype=float)
        self._m_mid_soil=np.zeros(shape=(1), dtype=float)
        self._b_mid_soil=np.zeros(shape=(1), dtype=float)
        self._m_stiff_soil_sand=np.zeros(shape=(1), dtype=float)
        self._b_stiff_soil_sand=np.zeros(shape=(1), dtype=float)
        self._m_A=np.zeros(shape=(1), dtype=float)
        self._b_A=np.zeros(shape=(1), dtype=float)
        self._m_B=np.zeros(shape=(1), dtype=float)
        self._b_B=np.zeros(shape=(1), dtype=float)
        self._m_EF=np.zeros(shape=(1), dtype=float)
        self._b_EF=np.zeros(shape=(1), dtype=float)
        self._Rel_soft_soil=np.zeros(shape=(1), dtype=float)
        self._Rel_mid_soil=np.zeros(shape=(1), dtype=float)
        self._Rel_stiff_soil=np.zeros(shape=(1), dtype=float)
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
        self.create_default_catalogue()
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START

    def create_default_catalogue(self):

        self.name='default_catalogue'
        self._catalogue_id=['anchor_1', 'anchor_2']
        self._m_soft_soil = [569.26754221, 784.76661745]
        self._b_soft_soil = [0.93771259623, 0.90020490328]
        self._m_mid_soil = [569.26754221, 1040.229976]
        self._b_mid_soil = [0.93771259623, 0.90820718302]
        self._m_stiff_soil_sand = [779.048058, 1238.7656958]
        self._b_stiff_soil_sand = [0.92864857452, 0.91006198174]
        self._m_A = [0.22648820263, 0.25801019888]
        self._b_A = [0.33598055403, 0.33334431544]
        self._m_B = [0.27453552794, 0.27806143152]
        self._b_B = [0.33594454709, 0.33336556698]
        self._m_EF = [0.16404368449, 0.15506619019]
        self._b_EF = [0.33590684254, 0.33341638395]
        self._Rel_soft_soil = [1, 3]
        self._Rel_mid_soil = [3, 1]
        self._Rel_stiff_soil = [3, 1]
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def catalogue_id(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:catalogue ID
        """
        return self._catalogue_id
    #------------
    @ property
    def m_soft_soil(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:m value for soft soil dim(*) []
        """
        return self._m_soft_soil
    #------------
    @ property
    def b_soft_soil(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:b value for soft soil dim(*) []
        """
        return self._b_soft_soil
    #------------
    @ property
    def m_mid_soil(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:m value for mid soil dim(*) []
        """
        return self._m_mid_soil
    #------------
    @ property
    def b_mid_soil(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:b value for mid soil dim(*) []
        """
        return self._b_mid_soil
    #------------
    @ property
    def m_stiff_soil_sand(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:m value for stiff soil dim(*) []
        """
        return self._m_stiff_soil_sand
    #------------
    @ property
    def b_stiff_soil_sand(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:b value for stiff soil dim(*) []
        """
        return self._b_stiff_soil_sand
    #------------
    @ property
    def m_A(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:m value for dimmension A dim(*) []
        """
        return self._m_A
    #------------
    @ property
    def b_A(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:b value for dimmension A dim(*) []
        """
        return self._b_A
    #------------
    @ property
    def m_B(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:m value for dimmension B dim(*) []
        """
        return self._m_B
    #------------
    @ property
    def b_B(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:b value for dimmension B dim(*) []
        """
        return self._b_B
    #------------
    @ property
    def m_EF(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:m value for dimmension EF dim(*) []
        """
        return self._m_EF
    #------------
    @ property
    def b_EF(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:b value for dimmension EF dim(*) []
        """
        return self._b_EF
    #------------
    @ property
    def Rel_soft_soil(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Realiability in soft soil dim(*) []
        """
        return self._Rel_soft_soil
    #------------
    @ property
    def Rel_mid_soil(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Realiability in mid soil dim(*) []
        """
        return self._Rel_mid_soil
    #------------
    @ property
    def Rel_stiff_soil(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Realiability in hard soil dim(*) []
        """
        return self._Rel_stiff_soil
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ catalogue_id.setter
    def catalogue_id(self,val): # pragma: no cover
        self._catalogue_id=val
    #------------
    @ m_soft_soil.setter
    def m_soft_soil(self,val): # pragma: no cover
        self._m_soft_soil=val
    #------------
    @ b_soft_soil.setter
    def b_soft_soil(self,val): # pragma: no cover
        self._b_soft_soil=val
    #------------
    @ m_mid_soil.setter
    def m_mid_soil(self,val): # pragma: no cover
        self._m_mid_soil=val
    #------------
    @ b_mid_soil.setter
    def b_mid_soil(self,val): # pragma: no cover
        self._b_mid_soil=val
    #------------
    @ m_stiff_soil_sand.setter
    def m_stiff_soil_sand(self,val): # pragma: no cover
        self._m_stiff_soil_sand=val
    #------------
    @ b_stiff_soil_sand.setter
    def b_stiff_soil_sand(self,val): # pragma: no cover
        self._b_stiff_soil_sand=val
    #------------
    @ m_A.setter
    def m_A(self,val): # pragma: no cover
        self._m_A=val
    #------------
    @ b_A.setter
    def b_A(self,val): # pragma: no cover
        self._b_A=val
    #------------
    @ m_B.setter
    def m_B(self,val): # pragma: no cover
        self._m_B=val
    #------------
    @ b_B.setter
    def b_B(self,val): # pragma: no cover
        self._b_B=val
    #------------
    @ m_EF.setter
    def m_EF(self,val): # pragma: no cover
        self._m_EF=val
    #------------
    @ b_EF.setter
    def b_EF(self,val): # pragma: no cover
        self._b_EF=val
    #------------
    @ Rel_soft_soil.setter
    def Rel_soft_soil(self,val): # pragma: no cover
        self._Rel_soft_soil=val
    #------------
    @ Rel_mid_soil.setter
    def Rel_mid_soil(self,val): # pragma: no cover
        self._Rel_mid_soil=val
    #------------
    @ Rel_stiff_soil.setter
    def Rel_stiff_soil(self,val): # pragma: no cover
        self._Rel_stiff_soil=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:catalogue:AnchorCatalogue"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:catalogue:AnchorCatalogue"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("catalogue_id"):
            if short:
                rep["catalogue_id"] = str(len(self.catalogue_id))
            else:
                rep["catalogue_id"] = self.catalogue_id
        else:
            rep["catalogue_id"] = []
        if self.is_set("m_soft_soil"):
            if (short):
                rep["m_soft_soil"] = str(self.m_soft_soil.shape)
            else:
                try:
                    rep["m_soft_soil"] = self.m_soft_soil.tolist()
                except:
                    rep["m_soft_soil"] = self.m_soft_soil
        if self.is_set("b_soft_soil"):
            if (short):
                rep["b_soft_soil"] = str(self.b_soft_soil.shape)
            else:
                try:
                    rep["b_soft_soil"] = self.b_soft_soil.tolist()
                except:
                    rep["b_soft_soil"] = self.b_soft_soil
        if self.is_set("m_mid_soil"):
            if (short):
                rep["m_mid_soil"] = str(self.m_mid_soil.shape)
            else:
                try:
                    rep["m_mid_soil"] = self.m_mid_soil.tolist()
                except:
                    rep["m_mid_soil"] = self.m_mid_soil
        if self.is_set("b_mid_soil"):
            if (short):
                rep["b_mid_soil"] = str(self.b_mid_soil.shape)
            else:
                try:
                    rep["b_mid_soil"] = self.b_mid_soil.tolist()
                except:
                    rep["b_mid_soil"] = self.b_mid_soil
        if self.is_set("m_stiff_soil_sand"):
            if (short):
                rep["m_stiff_soil_sand"] = str(self.m_stiff_soil_sand.shape)
            else:
                try:
                    rep["m_stiff_soil_sand"] = self.m_stiff_soil_sand.tolist()
                except:
                    rep["m_stiff_soil_sand"] = self.m_stiff_soil_sand
        if self.is_set("b_stiff_soil_sand"):
            if (short):
                rep["b_stiff_soil_sand"] = str(self.b_stiff_soil_sand.shape)
            else:
                try:
                    rep["b_stiff_soil_sand"] = self.b_stiff_soil_sand.tolist()
                except:
                    rep["b_stiff_soil_sand"] = self.b_stiff_soil_sand
        if self.is_set("m_A"):
            if (short):
                rep["m_A"] = str(self.m_A.shape)
            else:
                try:
                    rep["m_A"] = self.m_A.tolist()
                except:
                    rep["m_A"] = self.m_A
        if self.is_set("b_A"):
            if (short):
                rep["b_A"] = str(self.b_A.shape)
            else:
                try:
                    rep["b_A"] = self.b_A.tolist()
                except:
                    rep["b_A"] = self.b_A
        if self.is_set("m_B"):
            if (short):
                rep["m_B"] = str(self.m_B.shape)
            else:
                try:
                    rep["m_B"] = self.m_B.tolist()
                except:
                    rep["m_B"] = self.m_B
        if self.is_set("b_B"):
            if (short):
                rep["b_B"] = str(self.b_B.shape)
            else:
                try:
                    rep["b_B"] = self.b_B.tolist()
                except:
                    rep["b_B"] = self.b_B
        if self.is_set("m_EF"):
            if (short):
                rep["m_EF"] = str(self.m_EF.shape)
            else:
                try:
                    rep["m_EF"] = self.m_EF.tolist()
                except:
                    rep["m_EF"] = self.m_EF
        if self.is_set("b_EF"):
            if (short):
                rep["b_EF"] = str(self.b_EF.shape)
            else:
                try:
                    rep["b_EF"] = self.b_EF.tolist()
                except:
                    rep["b_EF"] = self.b_EF
        if self.is_set("Rel_soft_soil"):
            if (short):
                rep["Rel_soft_soil"] = str(self.Rel_soft_soil.shape)
            else:
                try:
                    rep["Rel_soft_soil"] = self.Rel_soft_soil.tolist()
                except:
                    rep["Rel_soft_soil"] = self.Rel_soft_soil
        if self.is_set("Rel_mid_soil"):
            if (short):
                rep["Rel_mid_soil"] = str(self.Rel_mid_soil.shape)
            else:
                try:
                    rep["Rel_mid_soil"] = self.Rel_mid_soil.tolist()
                except:
                    rep["Rel_mid_soil"] = self.Rel_mid_soil
        if self.is_set("Rel_stiff_soil"):
            if (short):
                rep["Rel_stiff_soil"] = str(self.Rel_stiff_soil.shape)
            else:
                try:
                    rep["Rel_stiff_soil"] = self.Rel_stiff_soil.tolist()
                except:
                    rep["Rel_stiff_soil"] = self.Rel_stiff_soil
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        asciiList = [n.encode("ascii", "ignore") for n in self.catalogue_id]
        handle["catalogue_id"] = asciiList
        handle["m_soft_soil"] = np.array(self.m_soft_soil,dtype=float)
        handle["b_soft_soil"] = np.array(self.b_soft_soil,dtype=float)
        handle["m_mid_soil"] = np.array(self.m_mid_soil,dtype=float)
        handle["b_mid_soil"] = np.array(self.b_mid_soil,dtype=float)
        handle["m_stiff_soil_sand"] = np.array(self.m_stiff_soil_sand,dtype=float)
        handle["b_stiff_soil_sand"] = np.array(self.b_stiff_soil_sand,dtype=float)
        handle["m_A"] = np.array(self.m_A,dtype=float)
        handle["b_A"] = np.array(self.b_A,dtype=float)
        handle["m_B"] = np.array(self.m_B,dtype=float)
        handle["b_B"] = np.array(self.b_B,dtype=float)
        handle["m_EF"] = np.array(self.m_EF,dtype=float)
        handle["b_EF"] = np.array(self.b_EF,dtype=float)
        handle["Rel_soft_soil"] = np.array(self.Rel_soft_soil,dtype=float)
        handle["Rel_mid_soil"] = np.array(self.Rel_mid_soil,dtype=float)
        handle["Rel_stiff_soil"] = np.array(self.Rel_stiff_soil,dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "catalogue_id"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "m_soft_soil"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "b_soft_soil"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "m_mid_soil"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "b_mid_soil"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "m_stiff_soil_sand"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "b_stiff_soil_sand"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "m_A"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "b_A"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "m_B"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "b_B"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "m_EF"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "b_EF"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "Rel_soft_soil"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "Rel_mid_soil"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "Rel_stiff_soil"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("catalogue_id" in list(gr.keys())):
            self.catalogue_id = [n.decode("ascii", "ignore") for n in gr["catalogue_id"][:]]
        if ("m_soft_soil" in list(gr.keys())):
            self.m_soft_soil = gr["m_soft_soil"][:]
        if ("b_soft_soil" in list(gr.keys())):
            self.b_soft_soil = gr["b_soft_soil"][:]
        if ("m_mid_soil" in list(gr.keys())):
            self.m_mid_soil = gr["m_mid_soil"][:]
        if ("b_mid_soil" in list(gr.keys())):
            self.b_mid_soil = gr["b_mid_soil"][:]
        if ("m_stiff_soil_sand" in list(gr.keys())):
            self.m_stiff_soil_sand = gr["m_stiff_soil_sand"][:]
        if ("b_stiff_soil_sand" in list(gr.keys())):
            self.b_stiff_soil_sand = gr["b_stiff_soil_sand"][:]
        if ("m_A" in list(gr.keys())):
            self.m_A = gr["m_A"][:]
        if ("b_A" in list(gr.keys())):
            self.b_A = gr["b_A"][:]
        if ("m_B" in list(gr.keys())):
            self.m_B = gr["m_B"][:]
        if ("b_B" in list(gr.keys())):
            self.b_B = gr["b_B"][:]
        if ("m_EF" in list(gr.keys())):
            self.m_EF = gr["m_EF"][:]
        if ("b_EF" in list(gr.keys())):
            self.b_EF = gr["b_EF"][:]
        if ("Rel_soft_soil" in list(gr.keys())):
            self.Rel_soft_soil = gr["Rel_soft_soil"][:]
        if ("Rel_mid_soil" in list(gr.keys())):
            self.Rel_mid_soil = gr["Rel_mid_soil"][:]
        if ("Rel_stiff_soil" in list(gr.keys())):
            self.Rel_stiff_soil = gr["Rel_stiff_soil"][:]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
