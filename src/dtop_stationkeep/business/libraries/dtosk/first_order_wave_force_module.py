# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import copy
from .rotation_module import rotation_about_z,Rab
from .geometry_module import get_wet_cylinder_properties
from .linear_wave_module import compute_cplx_pressure,compute_cplx_velocity,compute_cplx_acceleration

def compute_transfer_function(body_position_in_n,body_part,omega,beta,water_depth,rho):

	# Outputs:
	# - Hf: force per unit wave amplitude (complex value, frequency and direction dependant)

	# Init
	Hf=np.zeros(shape=(6), dtype=np.complex)
	a_w = np.zeros(shape=(3), dtype=np.complex)
	u_w = np.zeros(shape=(3), dtype=np.complex)
	a_p = np.zeros(shape=(3), dtype=np.complex)
	u_p = np.zeros(shape=(3), dtype=np.complex)
	df_p = np.zeros(shape=(3), dtype=np.complex)
	df_b = np.zeros(shape=(6), dtype=np.complex)
	amp = 1.0
	x0 = 0.0
	nstrip = body_part.n_strip
	

	# Coordinate system {w} is such that wx is in the wave direction, wz is upward
	# Compute euler angle of body in w
	body_position_in_w = np.zeros(shape=(6), dtype=float)
	body_position_in_w[3:6] = copy.deepcopy(body_position_in_n[3:6])
	body_position_in_w[5] = body_position_in_w[5] - beta
	Rbw = Rab(body_position_in_w[3:6])

	# Compute body position in w
	body_position_in_w[0:3] = rotation_about_z( body_position_in_n[0:3], -beta ) # Note that we use a minus sign for -beta here because we want to compute the position in w, not the position in n after a rotation beta

	# Compute rotation matrix of body part in b
	Rpb = Rab(body_part.position_in_b[3:6])

	# Compute rotation matrix of the body_part in w
	Rpw = Rbw.dot(Rpb)

	if body_part.geometry_type=='cylinder':

		D = body_part.cylinder.diameter
		Ap = np.pi*D**2 / 4.0

		[l,pa_w,z,c1_w,c2_w] = get_wet_cylinder_properties(body_position_in_w,body_part.position_in_b,body_part.cylinder)

        # Descritize the cylinder into strips
		ds = (c2_w-c1_w)/nstrip
		delta_s = np.linalg.norm(ds)

		for idx in range(0,int(nstrip)):

			# Init
			u_w = np.zeros(shape=(3), dtype=np.complex)
			a_w = np.zeros(shape=(3), dtype=np.complex)

			# Compute position of the strip no. idx in w
			s1_w = c1_w + idx*ds
			s2_w = c1_w + (idx+1)*ds
			sa_w = s1_w + 0.5*(s2_w - s1_w)

			# Compute water particle acceleration and velocity in w at strip origin
			[u_w[0],u_w[2]] = compute_cplx_velocity(omega,amp,water_depth,x0,sa_w[0],sa_w[2])
			[a_w[0],a_w[2]] = compute_cplx_acceleration(omega,amp,water_depth,x0,sa_w[0],sa_w[2])

			# Compute water particle acceleration and velocity in body_part c.s. at strip origin
			u_p = (Rpw.T).dot(u_w.T).T
			a_p = (Rpw.T).dot(a_w.T).T

			# Compute inertia force on strip (based on generalized Morrison formulation, Eq.3.34, Sea Loads, Faltinsen)
			Cm = 2.0
			#Cd = 1.0
			#std_up = np.linalg.norm(u_p[0:2])/(2**0.5)
			df_p[0] = rho * Ap * Cm * delta_s * a_p[0] #+ 0.5 * rho * Cd * D * (8/np.pi)**0.5 * std_up * u_p[0]
			df_p[1] = rho * Ap * Cm * delta_s * a_p[1] #+ 0.5 * rho * Cd * D * (8/np.pi)**0.5 * std_up * u_p[1]


			# Compute vertical force component (Froude-Krilov formulation) at the end of the cylinder if the end is wetted (i.e. not at still water level, and not at sea bottom)
			if (idx == 0) or (idx == int(nstrip)-1):
				if (idx == 0):
					s_check = s1_w[2]
					pressure_force_sign = -1.0
				else:
					s_check = s2_w[2]
					pressure_force_sign = 1.0
				if s_check == 0.0 or s_check <= -water_depth:
					df_p[2] = 0.0
				else:
					pressure = compute_cplx_pressure(rho,omega,amp,water_depth,x0,s1_w[0],s1_w[2])
					df_p[2] = pressure_force_sign * rho * Ap * pressure
			else:
				df_p[2] = 0.0*(1j)

			# Get the force in b
			df_b[0:3] = Rpb.dot(df_p[0:3].T).T

			# Compute moment in {b}
			sa_b = (Rbw.T).dot((sa_w-body_position_in_w[0:3]).T).T
			df_b[3:6]=np.cross(sa_b,df_b[0:3])

			# Add strip contribution to the total force
			Hf = Hf + df_b
		
	return Hf



