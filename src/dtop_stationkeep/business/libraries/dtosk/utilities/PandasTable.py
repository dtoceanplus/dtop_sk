# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import PandasLine
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class PandasTable():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._lines_names=[]
        self._column_names=[]
        self._line_of_cells=[]
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def lines_names(self): # pragma: no cover
        return self._lines_names
    #------------
    @ property
    def column_names(self): # pragma: no cover
        return self._column_names
    #------------
    @ property
    def line_of_cells(self): # pragma: no cover
        return self._line_of_cells
    #------------
    @ property
    def name(self): # pragma: no cover
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ lines_names.setter
    def lines_names(self,val): # pragma: no cover
        self._lines_names=val
    #------------
    @ column_names.setter
    def column_names(self,val): # pragma: no cover
        self._column_names=val
    #------------
    @ line_of_cells.setter
    def line_of_cells(self,val): # pragma: no cover
        self._line_of_cells=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:utilities:PandasTable"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:utilities:PandasTable"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("lines_names"):
            if short:
                rep["lines_names"] = str(len(self.lines_names))
            else:
                rep["lines_names"] = self.lines_names
        else:
            rep["lines_names"] = []
        if self.is_set("column_names"):
            if short:
                rep["column_names"] = str(len(self.column_names))
            else:
                rep["column_names"] = self.column_names
        else:
            rep["column_names"] = []
        if self.is_set("line_of_cells"):
            rep["line_of_cells"] = []
            for i in range(0,len(self.line_of_cells)):
                if (short and not(deep)):
                    itemType = self.line_of_cells[i].type_rep()
                    rep["line_of_cells"].append( itemType )
                else:
                    rep["line_of_cells"].append( self.line_of_cells[i].prop_rep(short, deep) )
        else:
            rep["line_of_cells"] = []
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "lines_names"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "column_names"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "line_of_cells"
        try :
            if data[varName] != None:
                self.line_of_cells=[]
                for i in range(0,len(data[varName])):
                    self.line_of_cells.append(PandasLine.PandasLine())
                    self.line_of_cells[i].loadFromJSONDict(data[varName][i])
            else:
                self.line_of_cells = []
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
