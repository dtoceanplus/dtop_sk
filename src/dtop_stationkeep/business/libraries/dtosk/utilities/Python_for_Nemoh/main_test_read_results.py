# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""
test c.spraul des routines python pour nemoh
"""
import numpy as np
from .NemohReader import NemohReader


for name in ['TEST-AXI']:
    ### Load Nemoh mesh and results
    nemoh = NemohReader( name )
    nemoh.mesh.plot_refined_mesh()
    
    ### Write HDB file for inputs to DeepLines
    nemoh.write_HDB_file_for_DeepLines()
    nemoh.mesh.create_mesh_file_for_DeepLines()

# ### Visualization
for i in range( 4 ):
    nemoh.plot_Fe( i_dir=i, abs='T')


