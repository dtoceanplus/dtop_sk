# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Class definition for NemohLauncher.
# Purpose: launch Nemoh calcultations and save results, require that a mesh has previously been defined.
# Written by: C.Spraul
#
import os
import numpy as np
import matplotlib.pyplot as plt

class Nemoh():
    """
    Purpose: Python wrapper for calculation of hydrodynamic coefficients using Nemoh.
    --> Edit input file (add w, dir and depth);
    --> Launch Nemoh's preProcessor, Solver and postProcessor;
    --> Read results.
    """
    def __init__(self, mesh, w, dir, depth, solver=0):
        """
        Inputs:
            - mesh  : a Mesh object
            - w     : np.array(), list of wave frequencies (rad/s)
            - dir   : np.array(), list of wave directions (degrees)
            - depth : float, water depth (m), 0. for deep water.
        kwargs:
            - solver: int (0 or 1), solver selection (0 for direct gauss, 1 for GMRES).
        Notes:
            - Only use w[0], w[-1] and len(w) !!! (min, max and number of freq.)
            - Idem for dir.
            - Then equivalent of np.linspace() is applied.
        """
        self.mesh = mesh
        self.name = self.mesh.name
        self.solver = solver
        self.depth = depth
        #
        self.nw = len(w)
        self.w = np.linspace(w[0], w[-1], self.nw)
        self.ndir = len(dir)
        self.dir = np.linspace(dir[0], dir[-1], self.ndir)
        #
        self.edit_Nemoh_input_file()
        self.launch_Nemoh()
        A, B, Fe = self.read_results()
        self.A = A
        self.B = B
        self.Fe = Fe
    
    def edit_Nemoh_input_file( self ):
        # create input.txt
        fid = open(self.name + os.sep + 'input.txt','w')
        fid.write('--- Calculation parameters ------------------------------------------------------------------------------------\n')
        fid.write('{:d}\t\t\t\t! Indiq_solver \t\t! - \t\t! Solver (0) Direct Gauss (1) GMRES\n'.format(self.solver))
        fid.write('20\t\t\t\t! IRES \t\t! - \t\t! Restart parameter for GMRES\n')
        fid.write('5.E-07\t\t\t! TOL_GMRES \t\t! - \t\t! Stopping criterion for GMRES\n')
        fid.write('100\t\t\t\t! MAXIT \t\t\t! - \t\t! Maximum iterations for GMRES\n')
        fid.close()
        # Nemoh.cal file, need to edit to specify w, dir, and depth
        fid = open(self.name + os.sep + 'Nemoh.cal','r')
        n=1
        textline = []
        for line in fid:
            textline.append( line )
            if n == 4: 
                textline[-1] = '{:<8.2f}\t\t\t! DEPTH \t\t! M \t\t! Water depth\n'.format(self.depth)
            if (n == 9+18*self.mesh.nBodies):
                textline[-1] = '{:<4d} {:<6.2f} {:<6.2f}\t\t! Number of wave frequencies, Min, and Max (rad/s)\n'.format(self.nw, self.w[0], self.w[-1])
            if (n == 10+18*self.mesh.nBodies):
                textline[-1] = '{:<4d} {:<6.1f} {:<6.1f}\t\t! Number of wave directions, Min and Max (degrees)\n'.format(self.ndir, self.dir[0], self.dir[-1])
            n+=1
        fid.close()
        #
        fid = open(self.name + os.sep + 'Nemoh.cal','w')
        for i in range(n-1):
            fid.write( textline[i] )
        fid.close()
        
    def launch_Nemoh( self ):
        # File that identify the directories for Nemoh computations
        fid = open('ID.dat','w')
        fid.write('{:d} \n{} \n'.format(len(self.name), self.name))
        fid.close()
        # Calcul des coefficients hydrodynamiques
        print('\n------ Starting NEMOH ----------- \n')
        os.system(os.curdir + os.sep +'Nemoh'+ os.sep +'preProcessor.exe')
        print('------ Solving BVPs ------------- \n')
        os.system(os.curdir + os.sep +'Nemoh'+ os.sep +'Solver.exe')
        print('------ Postprocessing results --- \n')
        os.system(os.curdir + os.sep +'Nemoh'+ os.sep +'postProcessor.exe')
    
    def read_results( self ):
        # Lecture des resultats Fe, A et B
        fid = open(self.name + os.sep +'results'+ os.sep +'ExcitationForce.tec','r')
        fid.readline()
        for i in range(6*self.mesh.nBodies):
            fid.readline()
        Famp = np.zeros((self.ndir, self.nw, 6*self.mesh.nBodies))
        Fphi = np.zeros((self.ndir, self.nw, 6*self.mesh.nBodies))
        for i in range(self.ndir):
            fid.readline()
            for k in range(self.nw):
                line = fid.readline()[:-1]
                line = np.fromstring(line, dtype=float, sep=' ')
                for j in range(6*self.mesh.nBodies):
                    Famp[i,k,j] = line[2*j+1]
                    Fphi[i,k,j] = line[2*j+2]
        fid.close()
        fid = open(self.name + os.sep +'results'+ os.sep +'RadiationCoefficients.tec','r')
        fid.readline()
        for i in range(6*self.mesh.nBodies):
            fid.readline()
        A = np.zeros((6*self.mesh.nBodies, 6*self.mesh.nBodies, self.nw))
        B = np.zeros((6*self.mesh.nBodies, 6*self.mesh.nBodies, self.nw))
        for i in range(6*self.mesh.nBodies):
            fid.readline()
            for k in range(self.nw):
                line = fid.readline()[:-1]
                line = np.fromstring(line, dtype=float, sep=' ')
                for j in range(6*self.mesh.nBodies):
                    A[i,j,k] = line[2*j+1]
                    B[i,j,k] = line[2*j+2]
                #fid.readline()
        fid.close()
        # Expression des efforts d excitation de houle sous la forme Famp*exp(i*Fphi)
        Fe = Famp *(np.cos(Fphi)+1j*np.sin(Fphi))
        return A, B, Fe
        
    def get_results( self ):
        """
        Outputs:
            - A  : np.array() of shape (6*nBodies, 6*nBodies, nw), added mass coefficients (1 matrice per pulsation);
            - B  : np.array() of shape (6*nBodies, 6*nBodies, nw), radiation damping coefficients (1 matrice per pulsation);
            - Fe : np.array() of shape (6*nBodies, nw), Exciation Forces and Moments (complex values), per DoF and per pulsation.
        """
        return self.A, self.B, self.Fe
    
    def set_inertia( self, inertia ):
        """
        Purpose:
            - To replace the inertia matrix computed from the mesh before writting the HDB file 
            if more realistic values are available.
        input:
            - inertia: np.array() of shape(6, 6)
        """
        self.mesh.inertia = inertia
        print("Inertia matrix has been modified manually in python.")
        print("--> Not saved to the Nemoh's files!")
    
    def write_HDB_file_for_DeepLines( self ):
        """
        Write HDB file, based on Nemoh results and Mesh hydrostatic properties.
        
        Notes:
            - mesh.inertia is computed based on the assumptions that:
                - the system is freely floating (mass = displacement *rho)
                - the mass is distributed on the meshed surface.
                --> use ".set_inertia()" method to modify the inertia matrix before writting HDB file.
            - Mooring stiffness matrix is set to 0.
            - Motions RAO are set to 0.
            - Water depth used for Nemoh calculations is not written in the HDB file, should be checked mannually.
        """
        # Phases: lead or lag? --> voir conventions Nemoh (lag?) / DeepLines (lead)
        # Phases: wrt wave elevation or wave potential --> voir conventions Nemoh (?) / DeepLines (elevation)
        # Phases: angles en radians pour les efforts d'excitations dans les exemples fournis (pas ce qui est dit dans la doc!!!)
        # Directions: en degrees
        # Water depth: pas renseignee dans le fichier HDB ! faire attention cohérance avec modèle DeepLines !
        T = 2*np.pi /self.w # seconds
        nT = len(T)
        #
        print('Writing HDB file:')
        os.system('mkdir '+ os.curdir + os.sep + 'DeepLines_HDB')
        fid = open( os.curdir + os.sep + 'DeepLines_HDB'+ os.sep + self.name +'.hdb', 'w', encoding='ascii')
        fid.write('[SOFT]          NEMOH \n')
        fid.write('[VERSION]       VERSION 2 - REV 03  \n')
        fid.write('[Date]          \n')
        fid.write('[INPUT_FILE]    \n')
        fid.write('[Locally_At]    \n')
        fid.write('[UNIT]          \n')
        fid.write('[HYDRO_PARA]        Y\n') #
        fid.write('[RAO]               N\n') #
        fid.write('[DRIFT_FORCE]       N\n') #
        fid.write('[QTF]               N\n') #
        fid.write('[PERIODS_NUMBER]    {:>4d}\n'.format(nT))
        fid.write('[INTER_PERIODS_NB]     0\n')
        fid.write('[HEADINGS_NUMBER]   {:>4d}\n'.format(self.ndir))
        fid.write('[LOWEST_HEADING]     {:>6.2f}\n'.format(self.dir[0]))
        fid.write('[HIGHEST_HEADING]    {:>6.2f}\n'.format(self.dir[-1]))
        fid.write('[List_calculated_periods]   \n')
        for j in range(nT-1, -1, -1):
            fid.write('{:>10.3f}\n'.format(T[j]))
        fid.write('[List_calculated_headings]  \n')
        for i in range(self.ndir):
            fid.write('{:>10.3f}\n'.format(self.dir[i]))
        fid.write('[STRUCTURES_NUMBER]        1\n')
        # Hydrostatic
        fid.write('[STRUCTURE_01] {}    \n'.format(self.name))
        fid.write('[UNDERWATER_VOLUME] {:>16.3f}\n'.format(self.mesh.displacement))
        fid.write('[CENTER_OF_BUOYANCY]      {:>8.4f}   {:>8.4f}   {:>8.4f}\n'.format(*self.mesh.CoB))
        fid.write('[CENTER_OF_GRAVITY]       {:>8.4f}   {:>8.4f}   {:>8.4f}\n'.format(*self.mesh.CoG))
        fid.write('[Mass_Inertia_matrix]   \n')
        for i in range(6):
            fid.write('{:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E}\n'.format(*self.mesh.inertia[i,:]))
        fid.write('[Hydrostatic_matrix]    \n')
        for i in range(6):
            fid.write('{:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E}\n'.format(*self.mesh.KH[i,:]))
        fid.write('[Stiffness_matrix_of_the_mooring_system]\n')
        for i in range(6):
            fid.write('{:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E}\n'.format(*np.zeros((6))))
        # Excitation Forces and Moments (first order wave loads)
        """
        !!! attention !!! convention : lead or lag ? DeepLines: lead, Nemoh: lag??? !!! attention !!!
        # Phases: wrt wave elevation or wave potential --> voir conventions Nemoh (?) / DeepLines (elevation)
        !!! attention !!! en radians !!! attention !!!
        """
        fid.write('[EXCITATION_FORCES_AND_MOMENTS]  \n')
        for i in range(self.ndir):
            fid.write('[INCIDENCE_EFM_MOD_{:03d}]    {:>8.3f}\n'.format(i+1, self.dir[i]))
            for j in range(nT-1, -1, -1):
                fid.write('{:>7.2f} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E}\n'.format(T[j], *np.abs(self.Fe[i,j,:])))
        for i in range(self.ndir):
            fid.write('[INCIDENCE_EFM_PH_{:0>3d}]     {:>8.3f}\n'.format(i+1, self.dir[i]))
            for j in range(nT-1, -1, -1):
                fid.write('{:>7.2f} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E}\n'.format(T[j], *-np.angle(self.Fe[i,j,:])))
        # !!! RAO = 0. !!!
        """
        It appears that writing the motion RAO to the HDB file is compulsory...
        Even when not used???
        Here I set it to 0.
        """
        fid.write('[RAO]  \n')
        for i in range(self.ndir):
            fid.write('[INCIDENCE_RAO_MOD_{:03d}]    {:>8.3f}\n'.format(i+1, self.dir[i]))
            for j in range(nT-1, -1, -1):
                fid.write('{:>7.2f} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E}\n'.format(T[j], *np.zeros((6))))
        for i in range(self.ndir):
            fid.write('[INCIDENCE_EFM_PH_{:0>3d}]     {:>8.3f}\n'.format(i+1, self.dir[i]))
            for j in range(nT-1, -1, -1):
                fid.write('{:>7.2f} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E}\n'.format(T[j], *-np.zeros((6))))
        # Added Mass and Damping
        fid.write('[Added_mass_Radiation_Damping]\n')
        for i in range(6):
            fid.write('[ADDED_MASS_LINE_{:d}]\n'.format(i+1))
            for j in range(nT-1, -1, -1):
                fid.write('{:>7.2f} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E}\n'.format(T[j], *self.A[i,:,j]))
        for i in range(6):
            fid.write('[DAMPING_TERM_{:d}]\n'.format(i+1))
            for j in range(nT-1, -1, -1):
                fid.write('{:>7.2f} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E} {:>14.7E}\n'.format(T[j], *self.B[i,:,j]))
        #
        """
        Translation of Drift_force_calculation.m ongoing...
        """
        # fid.write('[DRIFT_FORCES_AND_MOMENTS]\n')
        # for i in range(self.ndir):
        #     fid.write('[INCIDENCE_DFM_{:03d}]    {:>8.3f}\n'.format(i+1, self.dir[i]))
        #     for j in range(nT):
        #         fid.write(
        #
        fid.close()
        # Create HDB_info file in same folder
        fid = open( os.curdir + os.sep + 'DeepLines_HDB'+ os.sep + self.name +'.info', 'w')
        fid.write('Excitation Forces and Moments, Radiation Damping and Added Mass coefficients were computed using Nemoh.\n')
        fid.write('Nemoh is an open source sofware developped at Ecole Centrale de Nantes LHEEA lab.\n')
        fid.write('-----------------------------------------------------------\n')
        fid.write('\n\t\t Nemoh calculations were performed for the following conditions \t\t\n\n')
        fid.write('\t--> water depth: {:.1f} m \n'.format(self.depth))
        fid.write('\t--> water density: {:.1f} kg/m3 \n'.format(self.mesh.rho))
        fid.write('\t--> gravity acceleration: {:.2f} m/s2 \n'.format(self.mesh.g))
        fid.write('\t--> reference location for wave elevation: [0.00, 0.00, 0.00] m \n')
        fid.write('\n-----------------------------------------------------------\n')
        fid.write('\n\t\t Position and orientation of the body \t\t\n\n')
        fid.write('\t--> floating body reference point: [{:.2f}, {:.2f}, {:.2f}] m \n'.format(*self.mesh.translation))
        fid.write('\t--> roll/pitch/yaw: [{:.2f}, {:.2f}, {:.2f}] deg. \n'.format(*self.mesh.rotation))
        fid.write('\n\t--> local coordinates of gravity center: [{:.2f} {:.2f} {:.2f}] m \n'.format(*(self.mesh.CoG-self.mesh.translation)))
        fid.write('\n-----------------------------------------------------------\n')
        fid.close()
        print('--> Done.')
    
    def plot_Fe( self, i_dir=0, abs='T'):
        if abs=='T':
            x = 2*np.pi/self.w
        elif abs=='f':
            x = self.w/2*np.pi
        else:
            x = self.w
        fig, ax = plt.subplots(nrows=2, ncols=6)
        fig.suptitle('Excitation Forces and Moments (dir={:.1f} deg)'.format(self.dir[i_dir]))
        ax[0,0].set_title('Surge Ampl.')
        ax[0,0].plot( x, np.abs(self.Fe[i_dir,:,0]), 'r')
        ax[1,0].set_title('Surge Phase')
        ax[1,0].plot( x, np.angle(self.Fe[i_dir,:,0]), 'b')
        ax[0,1].set_title('Sway Ampl.')
        ax[0,1].plot( x, np.abs(self.Fe[i_dir,:,1]), 'r')
        ax[1,1].set_title('Sway Phase')
        ax[1,1].plot( x, np.angle(self.Fe[i_dir,:,1]), 'b')
        ax[0,2].set_title('Heave Ampl.')
        ax[0,2].plot( x, np.abs(self.Fe[i_dir,:,2]), 'r')
        ax[1,2].set_title('Heave Phase')
        ax[1,2].plot( x, np.angle(self.Fe[i_dir,:,2]), 'b')
        ax[0,3].set_title('Roll Ampl.')
        ax[0,3].plot( x, np.abs(self.Fe[i_dir,:,3]), 'r')
        ax[1,3].set_title('Roll Phase')
        ax[1,3].plot( x, np.angle(self.Fe[i_dir,:,3]), 'b')
        ax[0,4].set_title('Pitch Ampl.')
        ax[0,4].plot( x, np.abs(self.Fe[i_dir,:,4]), 'r')
        ax[1,4].set_title('Pitch Phase')
        ax[1,4].plot( x, np.angle(self.Fe[i_dir,:,4]), 'b')
        ax[0,5].set_title('Yaw Ampl.')
        ax[0,5].plot( x, np.abs(self.Fe[i_dir,:,5]), 'r')
        ax[1,5].set_title('Yaw Phase')
        ax[1,5].plot( x, np.angle(self.Fe[i_dir,:,5]), 'b')
        #
        ax[0,0].set_ylabel('Force [N/m]')
        ax[1,0].set_ylabel('Dephasage [rad]')
        ax[0,1].set_ylabel('Force [N/m]')
        ax[1,1].set_ylabel('Dephasage [rad]')
        ax[0,2].set_ylabel('Force [N/m]')
        ax[1,2].set_ylabel('Dephasage [rad]')
        ax[0,3].set_ylabel('Moment au CoG [N.m/m]')
        ax[1,3].set_ylabel('Dephasage [rad]')
        ax[0,4].set_ylabel('Moment au CoG [N.m/m]')
        ax[1,4].set_ylabel('Dephasage [rad]')
        ax[0,5].set_ylabel('Moment au CoG [N.m/m]')
        ax[1,5].set_ylabel('Dephasage [rad]')
        #
        if abs=='T':
            ax[1,0].set_xlabel('Periode [s]')
            ax[1,1].set_xlabel('Periode [s]')
            ax[1,2].set_xlabel('Periode [s]')
            ax[1,3].set_xlabel('Periode [s]')
            ax[1,4].set_xlabel('Periode [s]')
            ax[1,5].set_xlabel('Periode [s]')
        elif abs=='f':
            ax[1,0].set_xlabel('Frequence [Hz]')
            ax[1,1].set_xlabel('Frequence [Hz]')
            ax[1,2].set_xlabel('Frequence [Hz]')
            ax[1,3].set_xlabel('Frequence [Hz]')
            ax[1,4].set_xlabel('Frequence [Hz]')
            ax[1,5].set_xlabel('Frequence [Hz]')
        else:
            ax[1,0].set_xlabel('Pulsation [rad/s]')
            ax[1,1].set_xlabel('Pulsation [rad/s]')
            ax[1,2].set_xlabel('Pulsation [rad/s]')
            ax[1,3].set_xlabel('Pulsation [rad/s]')
            ax[1,4].set_xlabel('Pulsation [rad/s]')
            ax[1,5].set_xlabel('Pulsation [rad/s]')
        plt.show()
