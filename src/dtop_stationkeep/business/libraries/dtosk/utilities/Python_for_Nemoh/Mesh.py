# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Class definition for Mesh.
# Purpose: prepare meshes for Nemoh calcultations.
# Written by: C.Spraul
#
import os
import numpy as np
#from mayavi import mlab

class Mesh():
    """
    Purpose: Mesh generation of a body for use with Nemoh.
                Symetry about the xOz plane can be used efficiently.
                Then, because of symmetry, only half of the body must be described (y>0). 
                
    Init: Create the mesh and write Nemoh input files.
    
    Call the "get_hydrostatic()" method to obtain Mass, Inertia, Hydrostatic stiffness
    and Buoyancy center coordinates.
    """
    def __init__(self, name, description_mesh, CoG, nfobj=200, sym=True, plot=True):
        """
        inputs:
            - name: string, used to name the directory / files where the mesh and calculation results are stored.
            - description_mesh: np.array of shape (n,4,3), initial mesh of n quadrangular panels (4 nodes each), 3 coordinates (x,y,z) for each nodes.
            - CoG: np.array of shape (3,), coordinates of the gravity center of the body.
        kwargs:
            - nfobj: int, target number of panels for Nemoh's refined mesh.
            - sym: bool, True if symetry is to be used by Nemoh, then only half the mesh should be provided (y>0).
            - plot: if True the meshes will be displayed:
                --> from inputs (description mesh);
                --> created for Nemoh (refined mesh, for z<0).
        """
        self.g = 9.81 # kg/N # m.s-2
        self.rho = 1025. # kg/m3
        #
        self.nBodies = 1 # for multi-body, not yet implemented in this class but used in NemohLauncher
        #
        self.name = name
        self.description_mesh = description_mesh
        self.CoG = CoG
        self.sym = sym
        self.translation = np.array([0., 0., 0.])
        self.rotation = np.array([0., 0., 0.])
        #
        self.create_computation_dir()
        self.save_description_mesh()
        if plot:
            self. plot_description_mesh()
        self.create_Mesh_calculation_file( nfobj )
        self.refine_mesh()
        nn, nf = self.read_refined_mesh_properties()
        self.nn = nn
        self.nf = nf
        mass, inertia, KH, CoB, displacement, WPA = self.read_hydrostatic()
        self.mass = mass
        self.inertia = inertia
        self.KH = KH
        self.CoB = CoB
        self.displacement = displacement
        self.WPA = WPA
        if plot:
            self.plot_refined_mesh()
        self.write_Nemoh_input_file()
    
    def create_computation_dir( self ):
        # Create the directories to save mesh and results
        os.system('mkdir '+ os.curdir + os.sep + self.name)
        os.system('mkdir '+ os.curdir + os.sep + self.name + os.sep + 'mesh')
        os.system('mkdir '+ os.curdir + os.sep + self.name + os.sep + 'results')
        # File that identify these directories for Nemoh computations
        fid = open('ID.dat','w')
        fid.write('{:d} \n{} \n'.format(len(self.name), self.name))
        fid.close()
    
    def save_description_mesh( self ):
        nf = self.description_mesh.shape[0]  # initial number of panels
        nn = 4*nf               # initial number of nodes
        fid = open( self.name + os.sep +'mesh'+ os.sep +'mesh','w')
        fid.write('{:d} \n{:d} \n'.format(nn, nf))
        for i in range(nf):
            for j in range(4):
                fid.write('{:>16.7E}{:>16.7E}{:>16.7E}\n'.format(*self.description_mesh[i,j,:]))
        for i in range(nf):
            fid.write('{:>6d}{:>8d}{:>8d}{:>8d}\n'.format(4*i+1, 4*i+2, 4*i+3, 4*i+4))
        fid.close()
        print('\n Characteristics of the input discretisation')
        print('--> Number of nodes             : ', nn)
        print('--> Number of panels (max 2000) : ', nf)
    
    def plot_description_mesh( self ):
        """
        Display the mesh that describes the modelled body as it was provided.
        This mesh can be read from the Description_Full.tec file.
        """
        nf = self.description_mesh.shape[0]
        x = np.zeros((4*nf))
        y = np.zeros((4*nf))
        z = np.zeros((4*nf))
        tri = np.zeros((2*nf,3), dtype=int)
        for i in range(nf):
            tri[2*i,:] = np.array([4*i, 4*i+1, 4*i+2])
            tri[2*i+1,:] = np.array([4*i, 4*i+2, 4*i+3])
            for j in range(4):
                x[4*i+j] = self.description_mesh[i,j,0]
                y[4*i+j] = self.description_mesh[i,j,1]
                z[4*i+j] = self.description_mesh[i,j,2]
        #mlab.figure()
        #mlab.triangular_mesh(x, y, z, tri, representation='mesh')
        #mlab.title('Input shape')
    
    def create_Mesh_calculation_file( self, nfobj ):
        fid = open('mesh.cal','w')
        fid.write('mesh \n')
        if self.sym: fid.write('1 \n')                      # sym (1=True)
        else: fid.write('0 \n')                             # (0=False)
        fid.write('0. 0. \n ')                              # translations (x,y)
        fid.write('{:f} {:f} {:f} \n'.format(*self.CoG))    # CoG
        fid.write('{:d} \n'.format(nfobj))                  # target nb of panels
        fid.write('2 \n')                                   # ?
        fid.write('0. \n')                                  # ?
        fid.write('1.\n')                                   # ?
        fid.write('{:.0f} \n'.format(self.rho))             # rho
        fid.write('{:.2f} \n'.format(self.g))               # g
        fid.close()
    
    def refine_mesh( self ):
        os.system(os.curdir + os.sep +'Nemoh'+ os.sep +'Mesh.exe >Nemoh'+ os.sep +'Mesh.log')
    
    def read_refined_mesh_properties( self ):
        # Read Nemoh Mesh properties
        fid = open( self.name + os.sep +'mesh'+ os.sep +'mesh_info.dat','r')
        nn = int( fid.read(8) )
        nf = int( fid.read(8) )
        fid.close()
        print('\n Characteristics of the mesh for Nemoh')
        print('--> Number of nodes : ', nn)
        print('--> Number of panels : ', nf)
        return nn, nf
    
    def plot_refined_mesh( self ):
        """
        Read the mesh generated for Nemoh from the mesh.tec file and display it.
        Nemoh use the mesh.dat file which should be equivalent except for formatting.
        Only the part of the body below the waterline is meshed.
        """
        # Get data to plot the mesh
        fid = open( self.name + os.sep +'mesh'+ os.sep +'mesh.tec','r')
        if fid.read(9)=='VARIABLES':
            fid.readline() # 1 header liner is added when Nemoh is run
        fid.readline()
        # Nodes and panels
        x = np.zeros((self.nn))
        y = np.zeros((self.nn))
        z = np.zeros((self.nn))
        for i in range(self.nn):
            line = fid.readline()[:-1]
            line = np.fromstring(line, dtype=float, sep=' ')
            x[i] = line[0]
            y[i] = line[1]
            z[i] = line[2]
        quad = np.zeros((self.nf, 4), dtype=int)
        for i in range(self.nf):
            line = fid.readline()[:-1]
            line = np.fromstring(line, dtype=int, sep=' ')
            quad[i,:] = line[:]-1
        tri = np.zeros((2*self.nf,3), dtype=int)
        nftri=0
        for i in range(self.nf):
            tri[nftri,:] = [quad[i,0], quad[i,1], quad[i,2]]
            nftri+=1
            tri[nftri,:] = [quad[i,0], quad[i,2], quad[i,3]]
            nftri+=1
        # Normals
        xu = np.zeros((self.nf))
        yv = np.zeros((self.nf))
        zw = np.zeros((self.nf))
        u = np.zeros((self.nf))
        v = np.zeros((self.nf))
        w = np.zeros((self.nf))
        line = fid.readline()
        for i in range(self.nf):
            line = fid.readline()[:-1]
            line = np.fromstring(line, dtype=float, sep=' ')
            xu[i] = line[0]
            yv[i] = line[1]
            zw[i] = line[2]
            u[i] = line[3]
            v[i] = line[4]
            w[i] = line[5]
        fid.close()
        # Display mesh
        #mlab.figure()
        #mlab.triangular_mesh(x, y, z, tri, representation='mesh')
        #mlab.quiver3d(xu, yv, zw, u, v, w)
        #mlab.title('Mesh for Nemoh')
        #mlab.show()
    
    def write_Nemoh_input_file( self ):
        # Nemoh.cal
        fid = open( self.name + os.sep +'Nemoh.cal','w')
        fid.write('--- Environment ------------------------------------------------------------------------------------------------------------------ \n')
        fid.write('{:<8.1f}\t\t\t! RHO   \t\t! KG/M**3 \t! Fluid specific volume \n'.format(self.rho))
        fid.write('{:<8.2f}\t\t\t! G     \t\t! M/S**2 \t! Gravity \n'.format(self.g))
        fid.write('0.      \t\t\t! DEPTH \t\t! M \t\t! Water depth\n')
        fid.write('0.0 0.0 \t\t\t! XEFF YEFF \t! M \t\t! Wave measurement point\n')
        fid.write('--- Description of floating bodies -----------------------------------------------------------------------------------------------\n')
        fid.write('1       \t\t\t! Number of bodies\n')
        fid.write('--- Body 1 -----------------------------------------------------------------------------------------------------------------------\n')
        fid.write( self.name + os.sep +'mesh'+ os.sep +'mesh.dat\t\t\t! Name of mesh file\n')
        fid.write('{:<6d} {:<6d}\t\t\t\t\t\t! Number of points and number of panels\n'.format(self.nn, self.nf))
        fid.write('6 \t\t\t\t\t\t\t\t\t! Number of degrees of freedom\n')
        fid.write('1  1. 0. 0.   0.00   0.00   0.00\t! Surge\n')
        fid.write('1  0. 1. 0.   0.00   0.00   0.00\t! Sway\n')
        fid.write('1  0. 0. 1.   0.00   0.00   0.00\t! Heave\n')
        fid.write('2  1. 0. 0. {:>6.2f} {:>6.2f} {:>6.2f}\t! Roll about a point\n'.format(*self.CoG))
        fid.write('2  0. 1. 0. {:>6.2f} {:>6.2f} {:>6.2f}\t! Pitch about a point\n'.format(*self.CoG))
        fid.write('2  0. 0. 1. {:>6.2f} {:>6.2f} {:>6.2f}\t! Yaw about a point\n'.format(*self.CoG))
        fid.write('6 \t\t\t\t\t\t\t\t\t! Number of resulting generalised forces\n')
        fid.write('1  1. 0. 0.   0.00   0.00   0.00\t! Force in x direction\n')
        fid.write('1  0. 1. 0.   0.00   0.00   0.00\t! Force in y direction\n')
        fid.write('1  0. 0. 1.   0.00   0.00   0.00\t! Force in z direction\n')
        fid.write('2  1. 0. 0. {:>6.2f} {:>6.2f} {:>6.2f}\t! Moment force in x direction about a point\n'.format(*self.CoG))
        fid.write('2  0. 1. 0. {:>6.2f} {:>6.2f} {:>6.2f}\t! Moment force in y direction about a point\n'.format(*self.CoG))
        fid.write('2  0. 0. 1. {:>6.2f} {:>6.2f} {:>6.2f}\t! Moment force in z direction about a point\n'.format(*self.CoG))
        fid.write('0 \t\t\t\t\t\t\t\t\t! Number of lines of additional information \n')
        fid.write('--- Load cases to be solved -------------------------------------------------------------------------------------------------------\n')
        fid.write('1   0.8    0.8   \t\t! Number of wave frequencies, Min, and Max (rad/s)\n');
        fid.write('1   0.0    0.0   \t\t! Number of wave directions, Min and Max (degrees)\n');
        fid.write('--- Post processing ---------------------------------------------------------------------------------------------------------------\n');
        fid.write('1   0.1    10.   \t\t! IRF \t\t\t\t\t\t! IRF calculation (0 for no calculation), time step and duration\n')
        fid.write('0                \t\t! Show pressure\n')
        fid.write('0   0.     180.  \t\t! Kochin function \t\t\t! Number of directions of calculation (0 for no calculations), Min and Max (degrees)\n')
        fid.write('0   50   400.  400.  \t! Free surface elevation \t! Number of points in x direction (0 for no calcutions) and y direction and dimensions of domain in x and y direction\n')	
        fid.write('---')
        fid.close()
        # to compute drift forces --> need to compute Kochin functions --> to edit here !!!
    
    def read_hydrostatic( self ):
        """
        Read hydrostatic properties of the mesh.
        """
        # Matrice raideur hydrostatique
        KH = np.zeros((6, 6))
        fid = open( self.name + os.sep +'mesh'+ os.sep +'KH.dat','r')
        for i in range(6):   
            line = fid.readline()[:-1]
            line = np.fromstring(line, dtype=float, sep=' ')
            KH[i,:] = line
        fid.close()
        # Centre of Buoyancy
        fid = open( self.name + os.sep +'mesh'+ os.sep +'Hydrostatics.dat','r')
        fid.read(5) #' XF ='
        xB = float(fid.read(8))
        fid.readline()
        fid.read(5) #' YF ='
        yB = float(fid.read(8))
        fid.readline()
        fid.read(5) #' ZF ='
        zB = float(fid.read(8))
        fid.readline()
        CoB = np.array([xB, yB, zB])
        # Mass
        fid.read(15) #' Displacement ='
        displacement = float(fid.readline())
        mass = displacement *self.rho
        # Waterplane area
        fid.read(18) #' Waterplane area ='
        WPA = float(fid.readline())
        fid.close()
        # Matrice Inertie
        inertia = np.zeros((6, 6))
        fid = open( self.name + os.sep +'mesh'+ os.sep +'Inertia_hull.dat','r')
        for i in range(3):
            line = fid.readline()[:-1]
            line = np.fromstring(line, dtype=float, sep=' ')
            inertia[i+3,3:] = line
        inertia[0,0] = mass
        inertia[1,1] = mass
        inertia[2,2] = mass
        #
        return mass, inertia, KH, CoB, displacement, WPA
    
    def get_hydrostatic( self ):
        """
        Outputs: hydrostatic properties
        - mass         : mass of body
        - inertia(6,6) : inertia matrice (estimated assuming mass is distributed on wetted surface)
        - KH(6,6)       : hydrostatic stiffness matrice
        - CoB: [XB,YB,ZB] : coordinates of buoyancy center
        """
        return self.mass, self.inertia, self.KH, self.CoB
    
    def translate( self, translation ):
        """
        Purpose: Translate the mesh.
        Create a new directory (same name + '_T_X_Y_Z') to save the translated mesh.
        Compute and save all the new mesh properties.
        
        Input:
        - tanslation: np.array() of shape (3,), vector [X, Y, Z] (m) to be applied for the mesh translation.
        """
        if self.sym and translation[0] != 0.:
            print("Can't translate a mesh using symetry along X axis")
            return 
        self.name += '_T_{:.2f}_{:.2f}_{:.2f}'.format( *translation )
        self.description_mesh += translation
        self.CoG += translation
        self.translation = translation
        #
        self.create_computation_dir()
        self.save_description_mesh()
        self.create_Mesh_calculation_file( self.nf )
        self.refine_mesh()
        nn, nf = self.read_refined_mesh_properties()
        self.nn = nn
        self.nf = nf
        mass, inertia, KH, CoB, displacement, WPA = self.read_hydrostatic()
        self.mass = mass
        self.inertia = inertia
        self.KH = KH
        self.CoB = CoB
        self.displacement = displacement
        self.WPA = WPA
        self.write_Nemoh_input_file()
    
    def rotate( self, rotation ):
        """
        Purpose: Rotate the mesh around the CoG.
        Create a new directory (same name + '_R_rX_rY_rZ') to save the totated mesh.
        Compute and save all the new mesh properties.
        
        Input:
        - rotation: np.array() of shape (3,), vector [rX, rY, rZ] (deg) to be applied to rotate the mesh.
        """
        if self.sym and any( rotation[(1,2)] != 0. ):
            print("Can't rotate a mesh using symetry around Y or Z axis")
            return 
        self.name += '_R_{:.2f}_{:.2f}_{:.2f}'.format( *rotation )
        Rx = rotation[0]*np.pi/180.
        Ry = rotation[1]*np.pi/180.
        Rz = rotation[2]*np.pi/180.
        Rx = np.array([[1., 0., 0.],
                       [0., np.cos(Rx), -np.sin(Rx)],
                       [0., np.sin(Rx), np.cos(Rx)]])
        Ry = np.array([[np.cos(Ry), 0., np.sin(Ry)],
                       [0., 1., 0.],
                       [-np.sin(Ry), 0., np.cos(Ry)]])
        Rz = np.array([[np.cos(Rz), -np.sin(Rz), 0.],
                       [np.sin(Rz), np.cos(Rz), 0.],
                       [0., 0., 1.]])
        R = np.dot(Rz, np.dot(Ry, Rx))
        self.description_mesh = np.tensordot(R, (self.description_mesh - self.CoG).T, axes=1).T + self.CoG
        self.rotation = rotation
        #
        self.create_computation_dir()
        self.save_description_mesh()
        self.create_Mesh_calculation_file( self.nf )
        self.refine_mesh()
        nn, nf = self.read_refined_mesh_properties()
        self.nn = nn
        self.nf = nf
        mass, inertia, KH, CoB, displacement, WPA = self.read_hydrostatic()
        self.mass = mass
        self.inertia = inertia
        self.KH = KH
        self.CoB = CoB
        self.displacement = displacement
        self.WPA = WPA
        self.write_Nemoh_input_file()
    
    def create_mesh_file_for_DeepLines( self ):
        # Read description mesh
        fid = open( self.name + os.sep +'mesh'+ os.sep +'Description_Full.tec','r')
        line = fid.readline()
        nn = int( line[12:20] )
        nf = int( line[28:36] )
        xyz = np.zeros((nn,3))
        for i in range(nn):
            line = fid.readline()[:-1]
            line = np.fromstring(line, dtype=float, sep=' ')
            xyz[i,:] = line[:3]
        quad = np.zeros((nf, 4), dtype=int)
        for i in range(nf):
            line = fid.readline()[:-1]
            line = np.fromstring(line, dtype=int, sep=' ')
            quad[i,:] = line[:]-1
        fid.close()
        # Triangles
        tri = np.zeros((2*nf,3), dtype=int)
        for i in range(nf):
            tri[2*i,:] = [quad[i,0], quad[i,1], quad[i,2]]
            tri[2*i+1,:] = [quad[i,0], quad[i,2], quad[i,3]]
        # Write mesh file for DeepLines
        os.system('mkdir '+ os.curdir + os.sep + 'DeepLines_Mesh')
        fid = open( os.curdir + os.sep + 'DeepLines_Mesh'+ os.sep + self.name +'.dat','w', encoding='ascii')
        fid.write('*NODES\n')
        for i in range(nn):
            fid.write('{:>10d}{:>15.7E}{:>15.7E}{:>15.7E}\n'.format(i+1, *xyz[i,:]))
        if self.sym:
            for i in range(nn):
                fid.write('{:>10d}{:>15.7E}{:>15.7E}{:>15.7E}\n'.format(nn+i+1, *(xyz[i,:]*np.array([-1,-1,1]))))
        fid.write('*TRIANGLE\n')
        for i in range(2*nf):
            fid.write('{:>10d}{:>10d}{:>10d}{:>10d}\n'.format(i+1, *(tri[i,:]+1)))
        if self.sym:
            for i in range(2*nf):
                fid.write('{:>10d}{:>10d}{:>10d}{:>10d}\n'.format(2*nf+i+1, *(tri[i,:]+1+nn)))



