# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Class definition for AxiMesh.
# Purpose: prepare axisymetric meshes for Nemoh calcultations.
# Written by: C.Spraul
#
import numpy as np
from .Mesh import Mesh

class AxiMesh( Mesh ):
    """
    Purpose: Mesh generation of an axisymetric body for use with Nemoh.
                Extend the Mesh class to provide a conveniant way to create the 
                initial descrition mesh for the simple case of axisymetric bodies.
                Symetry about the xOz plane can be used efficiently.
                
    Init: Create the mesh and write Nemoh input files.
    
    Call the "get_hydrostatic()" method to obtain Mass, Inertia, Hydrostatic stiffness
    and Buoyancy center coordinates.
    """
    def __init__(self, name, r, z_r, CoG, ntheta=13, nfobj=200, sym=True, plot=True):
        """
        inputs:
            - name: string, used to name the directory / files where the mesh and calculation results are stored.
            - r: list of radial coordinates (length n)
            - z_r: corresponding list of vertical coordinates (same length: n)
            - CoG: np.array of shape (3), coordinates of the gravity center of the body.
        kwargs:
            - ntheta: int, number of points for angular discretisation (default: 13, suited if sym is True)
            - nfobj: int, target number of panels for Nemoh's refined mesh.
            - sym: bool, True if symetry is to be used by Nemoh.
            - plot: if True the meshes will be displayed:
                --> from inputs (description mesh);
                --> created for Nemoh (refined mesh, for z<0).
        Notes :
            - (r, z_r) describe a plan curve
            - the floating body shape will be obtained by a revolution of this curve;
            - if sym is True, only half the revolution is performed;
            - the axis of revolution is the r=0 axis;
            - closed body: r[0] = r[-1] = 0 (or r[0] = r[-1] and z_r[0] = z_r[-1])
            - from top to bottom : z[0] > z[1] > ... > z[-1] (enventually back to top)
            - only the part below the water line will be meshed for Nemoh.
        """
        description_mesh = self.get_panels( r, z_r, ntheta, sym )
        super().__init__(name, description_mesh, CoG, nfobj=nfobj, sym=sym, plot=plot)
        
    def get_panels( self, r, z_r, ntheta, sym ):
        n = len( r )
        if sym:
            theta = np.linspace(0., np.pi, ntheta)
        else:
            theta = np.linspace(0., 2*np.pi, ntheta)
        # Calcul des sommets du maillage
        x = np.zeros((ntheta*n))
        y = np.zeros((ntheta*n))
        z = np.zeros((ntheta*n))
        nn = 0
        for j in range(ntheta):
            for i in range(n):
                x[nn]=r[i]*np.cos(theta[j])
                y[nn]=r[i]*np.sin(theta[j])
                z[nn]=z_r[i]
                nn += 1
        # Calcul des facettes
        quad = np.zeros(((n-1)*(ntheta-1), 4), dtype=int)
        nf = 0
        for i in range(n-1):
            for j in range(ntheta-1):
                quad[nf,0] = i+n*j
                quad[nf,1] = i+1+n*j
                quad[nf,2] = i+1+n*(j+1)
                quad[nf,3] = i+n*(j+1)
                nf += 1
        # Mise au format pour Mesh
        description_mesh = np.zeros((nf, 4, 3))
        for i in range(nf):
            description_mesh[i,:,0] = x[quad[i,:]]
            description_mesh[i,:,1] = y[quad[i,:]]
            description_mesh[i,:,2] = z[quad[i,:]]
        return description_mesh