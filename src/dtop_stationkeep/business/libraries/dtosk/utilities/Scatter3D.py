# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class Scatter3D():

    """data model representing a plotly scatter3D object
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._x=np.zeros(shape=(1), dtype=float)
        self._y=np.zeros(shape=(1), dtype=float)
        self._z=np.zeros(shape=(1), dtype=float)
        self._mode=''
        self._rgba=np.zeros(shape=(1), dtype=float)
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def x(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Vector x dim(*) []
        """
        return self._x
    #------------
    @ property
    def y(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Vector y dim(*) []
        """
        return self._y
    #------------
    @ property
    def z(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Vector z dim(*) []
        """
        return self._z
    #------------
    @ property
    def mode(self): # pragma: no cover
        """str: Mode of scatter3d plot. E.g.: markers, lines
        """
        return self._mode
    #------------
    @ property
    def rgba(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:RGBA code color. Must be of length 4. If empty, use default color. dim(*) []
        """
        return self._rgba
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ x.setter
    def x(self,val): # pragma: no cover
        self._x=val
    #------------
    @ y.setter
    def y(self,val): # pragma: no cover
        self._y=val
    #------------
    @ z.setter
    def z(self,val): # pragma: no cover
        self._z=val
    #------------
    @ mode.setter
    def mode(self,val): # pragma: no cover
        self._mode=str(val)
    #------------
    @ rgba.setter
    def rgba(self,val): # pragma: no cover
        self._rgba=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:utilities:Scatter3D"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:utilities:Scatter3D"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("x"):
            if (short):
                rep["x"] = str(self.x.shape)
            else:
                try:
                    rep["x"] = self.x.tolist()
                except:
                    rep["x"] = self.x
        if self.is_set("y"):
            if (short):
                rep["y"] = str(self.y.shape)
            else:
                try:
                    rep["y"] = self.y.tolist()
                except:
                    rep["y"] = self.y
        if self.is_set("z"):
            if (short):
                rep["z"] = str(self.z.shape)
            else:
                try:
                    rep["z"] = self.z.tolist()
                except:
                    rep["z"] = self.z
        if self.is_set("mode"):
            rep["mode"] = self.mode
        if self.is_set("rgba"):
            if (short):
                rep["rgba"] = str(self.rgba.shape)
            else:
                try:
                    rep["rgba"] = self.rgba.tolist()
                except:
                    rep["rgba"] = self.rgba
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["x"] = np.array(self.x,dtype=float)
        handle["y"] = np.array(self.y,dtype=float)
        handle["z"] = np.array(self.z,dtype=float)
        ar = []
        ar.append(self.mode.encode("ascii"))
        handle["mode"] = np.asarray(ar)
        handle["rgba"] = np.array(self.rgba,dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "x"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "y"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "z"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "mode"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "rgba"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("x" in list(gr.keys())):
            self.x = gr["x"][:]
        if ("y" in list(gr.keys())):
            self.y = gr["y"][:]
        if ("z" in list(gr.keys())):
            self.z = gr["z"][:]
        if ("mode" in list(gr.keys())):
            self.mode = gr["mode"][0].decode("ascii")
        if ("rgba" in list(gr.keys())):
            self.rgba = gr["rgba"][:]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
