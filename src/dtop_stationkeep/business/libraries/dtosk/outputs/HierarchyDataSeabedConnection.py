# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class HierarchyDataSeabedConnection():

    """Physical properties of an anchor/foundation node in the hierarchy
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._design_id=''
        self._catalogue_id=''
        self._type=''
        self._height=0.0
        self._width=0.0
        self._length=0.0
        self._buried_height=0.0
        self._mass=0.0
        self._cost=0.0
        self._upstream_id=[]
        self._downstream_id=[]
        self._coordinates=np.zeros(shape=(3), dtype=float)
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def design_id(self): # pragma: no cover
        """str: design id corresponding the the design ID of the hierarchy node
        """
        return self._design_id
    #------------
    @ property
    def catalogue_id(self): # pragma: no cover
        """str: catalogue id, if any
        """
        return self._catalogue_id
    #------------
    @ property
    def type(self): # pragma: no cover
        """str: type of anchor/foundation
        """
        return self._type
    #------------
    @ property
    def height(self): # pragma: no cover
        """float: height of anchor/foundation []
        """
        return self._height
    #------------
    @ property
    def width(self): # pragma: no cover
        """float: width of anchor/foundation []
        """
        return self._width
    #------------
    @ property
    def length(self): # pragma: no cover
        """float: length of anchor/foundation []
        """
        return self._length
    #------------
    @ property
    def buried_height(self): # pragma: no cover
        """float: Buried height of pile, monopile and suction anchor [m]
        """
        return self._buried_height
    #------------
    @ property
    def mass(self): # pragma: no cover
        """float: mass of anchor/foundation []
        """
        return self._mass
    #------------
    @ property
    def cost(self): # pragma: no cover
        """float: cost of anchor/foundation []
        """
        return self._cost
    #------------
    @ property
    def upstream_id(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:design id of the node that is connected upstream in the hierarchy
        """
        return self._upstream_id
    #------------
    @ property
    def downstream_id(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:design id of the node that is connected downstream in the hierarchy
        """
        return self._downstream_id
    #------------
    @ property
    def coordinates(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:seabed connection coordinates dim(3) []
        """
        return self._coordinates
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ design_id.setter
    def design_id(self,val): # pragma: no cover
        self._design_id=str(val)
    #------------
    @ catalogue_id.setter
    def catalogue_id(self,val): # pragma: no cover
        self._catalogue_id=str(val)
    #------------
    @ type.setter
    def type(self,val): # pragma: no cover
        self._type=str(val)
    #------------
    @ height.setter
    def height(self,val): # pragma: no cover
        self._height=float(val)
    #------------
    @ width.setter
    def width(self,val): # pragma: no cover
        self._width=float(val)
    #------------
    @ length.setter
    def length(self,val): # pragma: no cover
        self._length=float(val)
    #------------
    @ buried_height.setter
    def buried_height(self,val): # pragma: no cover
        self._buried_height=float(val)
    #------------
    @ mass.setter
    def mass(self,val): # pragma: no cover
        self._mass=float(val)
    #------------
    @ cost.setter
    def cost(self,val): # pragma: no cover
        self._cost=float(val)
    #------------
    @ upstream_id.setter
    def upstream_id(self,val): # pragma: no cover
        self._upstream_id=val
    #------------
    @ downstream_id.setter
    def downstream_id(self,val): # pragma: no cover
        self._downstream_id=val
    #------------
    @ coordinates.setter
    def coordinates(self,val): # pragma: no cover
        self._coordinates=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:outputs:HierarchyDataSeabedConnection"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:outputs:HierarchyDataSeabedConnection"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("design_id"):
            rep["design_id"] = self.design_id
        if self.is_set("catalogue_id"):
            rep["catalogue_id"] = self.catalogue_id
        if self.is_set("type"):
            rep["type"] = self.type
        if self.is_set("height"):
            rep["height"] = self.height
        if self.is_set("width"):
            rep["width"] = self.width
        if self.is_set("length"):
            rep["length"] = self.length
        if self.is_set("buried_height"):
            rep["buried_height"] = self.buried_height
        if self.is_set("mass"):
            rep["mass"] = self.mass
        if self.is_set("cost"):
            rep["cost"] = self.cost
        if self.is_set("upstream_id"):
            if short:
                rep["upstream_id"] = str(len(self.upstream_id))
            else:
                rep["upstream_id"] = self.upstream_id
        else:
            rep["upstream_id"] = []
        if self.is_set("downstream_id"):
            if short:
                rep["downstream_id"] = str(len(self.downstream_id))
            else:
                rep["downstream_id"] = self.downstream_id
        else:
            rep["downstream_id"] = []
        if self.is_set("coordinates"):
            if (short):
                rep["coordinates"] = str(self.coordinates.shape)
            else:
                try:
                    rep["coordinates"] = self.coordinates.tolist()
                except:
                    rep["coordinates"] = self.coordinates
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        ar = []
        ar.append(self.design_id.encode("ascii"))
        handle["design_id"] = np.asarray(ar)
        ar = []
        ar.append(self.catalogue_id.encode("ascii"))
        handle["catalogue_id"] = np.asarray(ar)
        ar = []
        ar.append(self.type.encode("ascii"))
        handle["type"] = np.asarray(ar)
        handle["height"] = np.array([self.height],dtype=float)
        handle["width"] = np.array([self.width],dtype=float)
        handle["length"] = np.array([self.length],dtype=float)
        handle["buried_height"] = np.array([self.buried_height],dtype=float)
        handle["mass"] = np.array([self.mass],dtype=float)
        handle["cost"] = np.array([self.cost],dtype=float)
        asciiList = [n.encode("ascii", "ignore") for n in self.upstream_id]
        handle["upstream_id"] = asciiList
        asciiList = [n.encode("ascii", "ignore") for n in self.downstream_id]
        handle["downstream_id"] = asciiList
        handle["coordinates"] = np.array(self.coordinates,dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "design_id"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "catalogue_id"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "height"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "width"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "length"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "buried_height"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mass"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "cost"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "upstream_id"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "downstream_id"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "coordinates"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("design_id" in list(gr.keys())):
            self.design_id = gr["design_id"][0].decode("ascii")
        if ("catalogue_id" in list(gr.keys())):
            self.catalogue_id = gr["catalogue_id"][0].decode("ascii")
        if ("type" in list(gr.keys())):
            self.type = gr["type"][0].decode("ascii")
        if ("height" in list(gr.keys())):
            self.height = gr["height"][0]
        if ("width" in list(gr.keys())):
            self.width = gr["width"][0]
        if ("length" in list(gr.keys())):
            self.length = gr["length"][0]
        if ("buried_height" in list(gr.keys())):
            self.buried_height = gr["buried_height"][0]
        if ("mass" in list(gr.keys())):
            self.mass = gr["mass"][0]
        if ("cost" in list(gr.keys())):
            self.cost = gr["cost"][0]
        if ("upstream_id" in list(gr.keys())):
            self.upstream_id = [n.decode("ascii", "ignore") for n in gr["upstream_id"][:]]
        if ("downstream_id" in list(gr.keys())):
            self.downstream_id = [n.decode("ascii", "ignore") for n in gr["downstream_id"][:]]
        if ("coordinates" in list(gr.keys())):
            self.coordinates = gr["coordinates"][:]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
