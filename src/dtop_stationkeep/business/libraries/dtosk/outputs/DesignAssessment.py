# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from ..results import BodyResults
from ..environment import Environment
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class DesignAssessment():

    """Results from stationkeeping design assessment
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._devices=[]
        self._master_structure=BodyResults.BodyResults()
        self._master_structure.description = 'Design assessment results of the master structure, if present.'
        self._master_structure_is_present=False
        self._substation=[]
        self._substation_is_present=False
        self._uls_environments=[]
        self._uls_weather_directions=np.zeros(shape=(1), dtype=float)
        self._fls_environments=[]
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def devices(self): # pragma: no cover
        """:obj:`list` of :obj:`~.BodyResults.BodyResults`: Design assessment results of the devices
        """
        return self._devices
    #------------
    @ property
    def master_structure(self): # pragma: no cover
        """:obj:`~.BodyResults.BodyResults`: Design assessment results of the master structure, if present.
        """
        return self._master_structure
    #------------
    @ property
    def master_structure_is_present(self): # pragma: no cover
        """bool: Indicates if a masterstructure is defined in the farm []
        """
        return self._master_structure_is_present
    #------------
    @ property
    def substation(self): # pragma: no cover
        """:obj:`list` of :obj:`~.BodyResults.BodyResults`: Design assessment results of the substation, if present.
        """
        return self._substation
    #------------
    @ property
    def substation_is_present(self): # pragma: no cover
        """bool: Indicates if a substation is defined in the farm []
        """
        return self._substation_is_present
    #------------
    @ property
    def uls_environments(self): # pragma: no cover
        """:obj:`list` of :obj:`~.Environment.Environment`: Environmental conditions used for ULS analysis
        """
        return self._uls_environments
    #------------
    @ property
    def uls_weather_directions(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Weather directions used for ULS analysis dim(*) []
        """
        return self._uls_weather_directions
    #------------
    @ property
    def fls_environments(self): # pragma: no cover
        """:obj:`list` of :obj:`~.Environment.Environment`: Environmental conditions used for FLS analysis
        """
        return self._fls_environments
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ devices.setter
    def devices(self,val): # pragma: no cover
        self._devices=val
    #------------
    @ master_structure.setter
    def master_structure(self,val): # pragma: no cover
        self._master_structure=val
    #------------
    @ master_structure_is_present.setter
    def master_structure_is_present(self,val): # pragma: no cover
        self._master_structure_is_present=val
    #------------
    @ substation.setter
    def substation(self,val): # pragma: no cover
        self._substation=val
    #------------
    @ substation_is_present.setter
    def substation_is_present(self,val): # pragma: no cover
        self._substation_is_present=val
    #------------
    @ uls_environments.setter
    def uls_environments(self,val): # pragma: no cover
        self._uls_environments=val
    #------------
    @ uls_weather_directions.setter
    def uls_weather_directions(self,val): # pragma: no cover
        self._uls_weather_directions=val
    #------------
    @ fls_environments.setter
    def fls_environments(self,val): # pragma: no cover
        self._fls_environments=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:outputs:DesignAssessment"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:outputs:DesignAssessment"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("devices"):
            rep["devices"] = []
            for i in range(0,len(self.devices)):
                if (short and not(deep)):
                    itemType = self.devices[i].type_rep()
                    rep["devices"].append( itemType )
                else:
                    rep["devices"].append( self.devices[i].prop_rep(short, deep) )
        else:
            rep["devices"] = []
        if self.is_set("master_structure"):
            if (short and not(deep)):
                rep["master_structure"] = self.master_structure.type_rep()
            else:
                rep["master_structure"] = self.master_structure.prop_rep(short, deep)
        if self.is_set("master_structure_is_present"):
            rep["master_structure_is_present"] = self.master_structure_is_present
        if self.is_set("substation"):
            rep["substation"] = []
            for i in range(0,len(self.substation)):
                if (short and not(deep)):
                    itemType = self.substation[i].type_rep()
                    rep["substation"].append( itemType )
                else:
                    rep["substation"].append( self.substation[i].prop_rep(short, deep) )
        else:
            rep["substation"] = []
        if self.is_set("substation_is_present"):
            rep["substation_is_present"] = self.substation_is_present
        if self.is_set("uls_environments"):
            rep["uls_environments"] = []
            for i in range(0,len(self.uls_environments)):
                if (short and not(deep)):
                    itemType = self.uls_environments[i].type_rep()
                    rep["uls_environments"].append( itemType )
                else:
                    rep["uls_environments"].append( self.uls_environments[i].prop_rep(short, deep) )
        else:
            rep["uls_environments"] = []
        if self.is_set("uls_weather_directions"):
            if (short):
                rep["uls_weather_directions"] = str(self.uls_weather_directions.shape)
            else:
                try:
                    rep["uls_weather_directions"] = self.uls_weather_directions.tolist()
                except:
                    rep["uls_weather_directions"] = self.uls_weather_directions
        if self.is_set("fls_environments"):
            rep["fls_environments"] = []
            for i in range(0,len(self.fls_environments)):
                if (short and not(deep)):
                    itemType = self.fls_environments[i].type_rep()
                    rep["fls_environments"].append( itemType )
                else:
                    rep["fls_environments"].append( self.fls_environments[i].prop_rep(short, deep) )
        else:
            rep["fls_environments"] = []
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        subgroup = handle.create_group("devices")
        for idx in range(0,len(self.devices)):
            subgroup1 = subgroup.create_group("devices" + "_" + str(idx))
            self.devices[idx].saveToHDF5Handle(subgroup1)
        subgroup = handle.create_group("master_structure")
        self.master_structure.saveToHDF5Handle(subgroup)
        handle["master_structure_is_present"] = np.array([self.master_structure_is_present],dtype=bool)
        subgroup = handle.create_group("substation")
        for idx in range(0,len(self.substation)):
            subgroup1 = subgroup.create_group("substation" + "_" + str(idx))
            self.substation[idx].saveToHDF5Handle(subgroup1)
        handle["substation_is_present"] = np.array([self.substation_is_present],dtype=bool)
        subgroup = handle.create_group("uls_environments")
        for idx in range(0,len(self.uls_environments)):
            subgroup1 = subgroup.create_group("uls_environments" + "_" + str(idx))
            self.uls_environments[idx].saveToHDF5Handle(subgroup1)
        handle["uls_weather_directions"] = np.array(self.uls_weather_directions,dtype=float)
        subgroup = handle.create_group("fls_environments")
        for idx in range(0,len(self.fls_environments)):
            subgroup1 = subgroup.create_group("fls_environments" + "_" + str(idx))
            self.fls_environments[idx].saveToHDF5Handle(subgroup1)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "devices"
        try :
            if data[varName] != None:
                self.devices=[]
                for i in range(0,len(data[varName])):
                    self.devices.append(BodyResults.BodyResults())
                    self.devices[i].loadFromJSONDict(data[varName][i])
            else:
                self.devices = []
        except :
            pass
        varName = "master_structure"
        try :
            if data[varName] != None:
                self.master_structure=BodyResults.BodyResults()
                self.master_structure.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "master_structure_is_present"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "substation"
        try :
            if data[varName] != None:
                self.substation=[]
                for i in range(0,len(data[varName])):
                    self.substation.append(BodyResults.BodyResults())
                    self.substation[i].loadFromJSONDict(data[varName][i])
            else:
                self.substation = []
        except :
            pass
        varName = "substation_is_present"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "uls_environments"
        try :
            if data[varName] != None:
                self.uls_environments=[]
                for i in range(0,len(data[varName])):
                    self.uls_environments.append(Environment.Environment())
                    self.uls_environments[i].loadFromJSONDict(data[varName][i])
            else:
                self.uls_environments = []
        except :
            pass
        varName = "uls_weather_directions"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "fls_environments"
        try :
            if data[varName] != None:
                self.fls_environments=[]
                for i in range(0,len(data[varName])):
                    self.fls_environments.append(Environment.Environment())
                    self.fls_environments[i].loadFromJSONDict(data[varName][i])
            else:
                self.fls_environments = []
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("devices" in list(gr.keys())):
            subgroup = gr["devices"]
            self.devices=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["devices"+"_"+str(idx)]
                self.devices.append(BodyResults.BodyResults())
                self.devices[idx].loadFromHDF5Handle(ssubgroup)
        if ("master_structure" in list(gr.keys())):
            subgroup = gr["master_structure"]
            self.master_structure.loadFromHDF5Handle(subgroup)
        if ("master_structure_is_present" in list(gr.keys())):
            self.master_structure_is_present = gr["master_structure_is_present"][0]
        if ("substation" in list(gr.keys())):
            subgroup = gr["substation"]
            self.substation=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["substation"+"_"+str(idx)]
                self.substation.append(BodyResults.BodyResults())
                self.substation[idx].loadFromHDF5Handle(ssubgroup)
        if ("substation_is_present" in list(gr.keys())):
            self.substation_is_present = gr["substation_is_present"][0]
        if ("uls_environments" in list(gr.keys())):
            subgroup = gr["uls_environments"]
            self.uls_environments=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["uls_environments"+"_"+str(idx)]
                self.uls_environments.append(Environment.Environment())
                self.uls_environments[idx].loadFromHDF5Handle(ssubgroup)
        if ("uls_weather_directions" in list(gr.keys())):
            self.uls_weather_directions = gr["uls_weather_directions"][:]
        if ("fls_environments" in list(gr.keys())):
            subgroup = gr["fls_environments"]
            self.fls_environments=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["fls_environments"+"_"+str(idx)]
                self.fls_environments.append(Environment.Environment())
                self.fls_environments[idx].loadFromHDF5Handle(ssubgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
