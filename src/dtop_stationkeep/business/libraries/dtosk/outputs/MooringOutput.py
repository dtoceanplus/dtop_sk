# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from ..mooring import LineTypeData
from ..mooring import NodeData
from ..mooring import LineData
from ..mooring import SolverOptions
from ..mooring import Ancillaries
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class MooringOutput():

    """data model representing a design of mooring as an output
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._reference_system='farm'
        self._line_dictionnary=[]
        self._node_properties=[]
        self._line_properties=[]
        self._solver_options=SolverOptions.SolverOptions()
        self._solver_options.description = 'solver options'
        self._line_cost=np.zeros(shape=(1), dtype=float)
        self._ancillaries=Ancillaries.Ancillaries()
        self._ancillaries.description = 'ancillaries'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def reference_system(self): # pragma: no cover
        """str: Reference system that defines the origin (0,0,0). farm or device
        """
        return self._reference_system
    #------------
    @ property
    def line_dictionnary(self): # pragma: no cover
        """:obj:`list` of :obj:`~.LineTypeData.LineTypeData`: dictionnary documenting line type
        """
        return self._line_dictionnary
    #------------
    @ property
    def node_properties(self): # pragma: no cover
        """:obj:`list` of :obj:`~.NodeData.NodeData`: list of nodes
        """
        return self._node_properties
    #------------
    @ property
    def line_properties(self): # pragma: no cover
        """:obj:`list` of :obj:`~.LineData.LineData`: list of lines
        """
        return self._line_properties
    #------------
    @ property
    def solver_options(self): # pragma: no cover
        """:obj:`~.SolverOptions.SolverOptions`: solver options
        """
        return self._solver_options
    #------------
    @ property
    def line_cost(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Cost of each line segment [€] dim(*) []
        """
        return self._line_cost
    #------------
    @ property
    def ancillaries(self): # pragma: no cover
        """:obj:`~.Ancillaries.Ancillaries`: ancillaries
        """
        return self._ancillaries
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ reference_system.setter
    def reference_system(self,val): # pragma: no cover
        self._reference_system=str(val)
    #------------
    @ line_dictionnary.setter
    def line_dictionnary(self,val): # pragma: no cover
        self._line_dictionnary=val
    #------------
    @ node_properties.setter
    def node_properties(self,val): # pragma: no cover
        self._node_properties=val
    #------------
    @ line_properties.setter
    def line_properties(self,val): # pragma: no cover
        self._line_properties=val
    #------------
    @ solver_options.setter
    def solver_options(self,val): # pragma: no cover
        self._solver_options=val
    #------------
    @ line_cost.setter
    def line_cost(self,val): # pragma: no cover
        self._line_cost=val
    #------------
    @ ancillaries.setter
    def ancillaries(self,val): # pragma: no cover
        self._ancillaries=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:outputs:MooringOutput"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:outputs:MooringOutput"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("reference_system"):
            rep["reference_system"] = self.reference_system
        if self.is_set("line_dictionnary"):
            rep["line_dictionnary"] = []
            for i in range(0,len(self.line_dictionnary)):
                if (short and not(deep)):
                    itemType = self.line_dictionnary[i].type_rep()
                    rep["line_dictionnary"].append( itemType )
                else:
                    rep["line_dictionnary"].append( self.line_dictionnary[i].prop_rep(short, deep) )
        else:
            rep["line_dictionnary"] = []
        if self.is_set("node_properties"):
            rep["node_properties"] = []
            for i in range(0,len(self.node_properties)):
                if (short and not(deep)):
                    itemType = self.node_properties[i].type_rep()
                    rep["node_properties"].append( itemType )
                else:
                    rep["node_properties"].append( self.node_properties[i].prop_rep(short, deep) )
        else:
            rep["node_properties"] = []
        if self.is_set("line_properties"):
            rep["line_properties"] = []
            for i in range(0,len(self.line_properties)):
                if (short and not(deep)):
                    itemType = self.line_properties[i].type_rep()
                    rep["line_properties"].append( itemType )
                else:
                    rep["line_properties"].append( self.line_properties[i].prop_rep(short, deep) )
        else:
            rep["line_properties"] = []
        if self.is_set("solver_options"):
            if (short and not(deep)):
                rep["solver_options"] = self.solver_options.type_rep()
            else:
                rep["solver_options"] = self.solver_options.prop_rep(short, deep)
        if self.is_set("line_cost"):
            if (short):
                rep["line_cost"] = str(self.line_cost.shape)
            else:
                try:
                    rep["line_cost"] = self.line_cost.tolist()
                except:
                    rep["line_cost"] = self.line_cost
        if self.is_set("ancillaries"):
            if (short and not(deep)):
                rep["ancillaries"] = self.ancillaries.type_rep()
            else:
                rep["ancillaries"] = self.ancillaries.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        ar = []
        ar.append(self.reference_system.encode("ascii"))
        handle["reference_system"] = np.asarray(ar)
        subgroup = handle.create_group("line_dictionnary")
        for idx in range(0,len(self.line_dictionnary)):
            subgroup1 = subgroup.create_group("line_dictionnary" + "_" + str(idx))
            self.line_dictionnary[idx].saveToHDF5Handle(subgroup1)
        subgroup = handle.create_group("node_properties")
        for idx in range(0,len(self.node_properties)):
            subgroup1 = subgroup.create_group("node_properties" + "_" + str(idx))
            self.node_properties[idx].saveToHDF5Handle(subgroup1)
        subgroup = handle.create_group("line_properties")
        for idx in range(0,len(self.line_properties)):
            subgroup1 = subgroup.create_group("line_properties" + "_" + str(idx))
            self.line_properties[idx].saveToHDF5Handle(subgroup1)
        subgroup = handle.create_group("solver_options")
        self.solver_options.saveToHDF5Handle(subgroup)
        handle["line_cost"] = np.array(self.line_cost,dtype=float)
        subgroup = handle.create_group("ancillaries")
        self.ancillaries.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "reference_system"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "line_dictionnary"
        try :
            if data[varName] != None:
                self.line_dictionnary=[]
                for i in range(0,len(data[varName])):
                    self.line_dictionnary.append(LineTypeData.LineTypeData())
                    self.line_dictionnary[i].loadFromJSONDict(data[varName][i])
            else:
                self.line_dictionnary = []
        except :
            pass
        varName = "node_properties"
        try :
            if data[varName] != None:
                self.node_properties=[]
                for i in range(0,len(data[varName])):
                    self.node_properties.append(NodeData.NodeData())
                    self.node_properties[i].loadFromJSONDict(data[varName][i])
            else:
                self.node_properties = []
        except :
            pass
        varName = "line_properties"
        try :
            if data[varName] != None:
                self.line_properties=[]
                for i in range(0,len(data[varName])):
                    self.line_properties.append(LineData.LineData())
                    self.line_properties[i].loadFromJSONDict(data[varName][i])
            else:
                self.line_properties = []
        except :
            pass
        varName = "solver_options"
        try :
            if data[varName] != None:
                self.solver_options=SolverOptions.SolverOptions()
                self.solver_options.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "line_cost"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "ancillaries"
        try :
            if data[varName] != None:
                self.ancillaries=Ancillaries.Ancillaries()
                self.ancillaries.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("reference_system" in list(gr.keys())):
            self.reference_system = gr["reference_system"][0].decode("ascii")
        if ("line_dictionnary" in list(gr.keys())):
            subgroup = gr["line_dictionnary"]
            self.line_dictionnary=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["line_dictionnary"+"_"+str(idx)]
                self.line_dictionnary.append(LineTypeData.LineTypeData())
                self.line_dictionnary[idx].loadFromHDF5Handle(ssubgroup)
        if ("node_properties" in list(gr.keys())):
            subgroup = gr["node_properties"]
            self.node_properties=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["node_properties"+"_"+str(idx)]
                self.node_properties.append(NodeData.NodeData())
                self.node_properties[idx].loadFromHDF5Handle(ssubgroup)
        if ("line_properties" in list(gr.keys())):
            subgroup = gr["line_properties"]
            self.line_properties=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["line_properties"+"_"+str(idx)]
                self.line_properties.append(LineData.LineData())
                self.line_properties[idx].loadFromHDF5Handle(ssubgroup)
        if ("solver_options" in list(gr.keys())):
            subgroup = gr["solver_options"]
            self.solver_options.loadFromHDF5Handle(subgroup)
        if ("line_cost" in list(gr.keys())):
            self.line_cost = gr["line_cost"][:]
        if ("ancillaries" in list(gr.keys())):
            subgroup = gr["ancillaries"]
            self.ancillaries.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
