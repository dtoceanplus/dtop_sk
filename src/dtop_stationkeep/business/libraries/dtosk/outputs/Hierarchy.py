# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from . import HierarchyData
#------------------------------------
# @ USER DEFINED IMPORTS START
import pandas as pd
# @ USER DEFINED IMPORTS END
#------------------------------------

class Hierarchy():

    """Hierarchy from the SK-module
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._system=[]
        self._name_of_node=[]
        self._design_id=[]
        self._node_type=[]
        self._node_subtype=[]
        self._category=[]
        self._parent=[]
        self._child=[]
        self._gate_type=[]
        self._failure_rate_repair=[]
        self._failure_rate_replacement=[]
        self._hierarchy_data=HierarchyData.HierarchyData()
        self._hierarchy_data.description = 'object that contains physical properties of the nodes present in the hierarchy'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
        # Default values for the failure rates
        self.ml_failure_rate = 1.33E-02
        self.chain_failure_rate = 7.22E-03
        self.wire_rope_failure_rate = 4.17E-03
        self.polyester_rope_failure_rate = 2.78E-04
        self.connector_failure_rate = 1.11E-03
        self.yoke_failure_rate = 2.78E-04
        self.anchor_failure_rate = 2.78E-04
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def populate(self,farm,failure_rate_level):

        # Set hierarchy name
        self.name = 'SK-hierarchy'

        # If master structure is present, register its stationkeeping system
        if farm.master_structure_is_present:
            subname = 'ms'
            parent_name = 'NA'
            self.populate_body(farm.master_structure,parent_name,subname,failure_rate_level)

        # Add each stationkeeping system for each device
        cpt = 1
        for idx in range(0,len(farm.body_array)):
            subname = str(cpt) + '_x'
            parent_name = 'SK' + str(cpt)
            self.populate_body(farm.body_array[idx],parent_name,subname,failure_rate_level)
            cpt = cpt +1

        # Pack each SKi_x in SKi, and add SKms in SKi if needed
        cpt = 1
        for idx in range(0,len(farm.body_array)):
            child_list=[]
            child_list.append('SK'+str(cpt)+'_x')
            if farm.body_array[idx].positioning_reference == 'masterstructure':
                child_list.append('SKms')
            self.add_one_hierarchy_line('SK','SK'+ str(cpt),'NA','System','stationkeeping','Level 3','NA',child_list,'AND','NA','NA')
            cpt = cpt+1

    def populate_body(self,body,parent_name,subname,failure_rate_level):

        # Register one hierarchy line per SK-system
        child_list=[]

        if body.positioning_type=='moored':

            [ml_of_segment,ml_of_node,nml] = body.mooring_system_force.identify_mooring_line_hierarchy()
            for idx_child in range(0,nml):
                child_list.append('SK'+ subname + '_ml_' + str(idx_child))

            self.add_one_hierarchy_line('SK','SK'+ subname,'NA','System','stationkeeping','Level 2',parent_name,child_list,'AND','NA','NA')
            #self.add_one_hierarchy_line('SK','SK'+ subname,'NA','System','stationkeeping','Level 2','NA',child_list,str(max(nml-1,1)) + '/' + str(nml),'NA','NA')

            # Populate mooring line
            parent_id = 'SK'+ subname
            self.populate_mooring_line(body.mooring_system_force,child_list,parent_id,ml_of_segment,ml_of_node,failure_rate_level,body.results)


        elif body.positioning_type=='fixed':

            # Find name of foundation
            if body.results.design_results.anchor_and_foundation.foundation_type == 'pile':
                name_short = body.results.design_results.anchor_and_foundation.pile_foundation_design.name
            elif body.results.design_results.anchor_and_foundation.foundation_type == 'shallow':
                name_short = body.results.design_results.anchor_and_foundation.gravity_foundation_design.name
            elif body.results.design_results.anchor_and_foundation.foundation_type == '':
                pass
            else:
                raise Exception("Error: cannot populate results of foundation type " + str(body.results.design_results.anchor_and_foundation.foundation_type) + ' for fixed structure')

            # Level 2
            child_list=[]
            parent_id = 'SK'+ subname
            name = parent_id + '_found'
            child_list.append(name)

            self.add_one_hierarchy_line('SK','SK'+ subname,'NA','System','stationkeeping','Level 2',parent_name,child_list,'AND','NA','NA')
            
            # Level 1
            child_list=[]
            parent_id = 'SK'+ subname
            name = parent_id + '_' + name_short
            child_list.append(name)

            self.add_one_hierarchy_line('SK','SK'+ subname + '_found','NA','System','stationkeeping','Level 1','SK'+ subname,child_list,'AND','NA','NA')

            # Populate foundation (Level 0)
            parent_id = 'SK'+ subname
            self.populate_foundation(parent_id,body.results,body.initial_position)


    def populate_foundation(self,parent_id,body_results,body_position):

        # Populate hierarchy (only one foundation per device)
        # Define failure rate
        failure_rate = 'NA'
        buried_height = 0.0

        # Define name and design id
        if body_results.design_results.anchor_and_foundation.foundation_type == 'pile':
            name_short = body_results.design_results.anchor_and_foundation.pile_foundation_design.name
            height=body_results.design_results.anchor_and_foundation.pile_foundation_design.total_length
            width=body_results.design_results.anchor_and_foundation.pile_foundation_design.d_ext
            length=body_results.design_results.anchor_and_foundation.pile_foundation_design.d_ext
            mass=body_results.design_results.anchor_and_foundation.pile_foundation_design.pile_weight/9.81
            buried_height=body_results.design_results.anchor_and_foundation.pile_foundation_design.plength
            failure_rate = 0.0
        elif body_results.design_results.anchor_and_foundation.foundation_type == 'shallow':
            name_short = body_results.design_results.anchor_and_foundation.gravity_foundation_design.name
            height = body_results.design_results.anchor_and_foundation.gravity_foundation_design.thickness
            mass = body_results.design_results.anchor_and_foundation.gravity_foundation_design.found_weight/9.81
            if body_results.design_results.anchor_and_foundation.gravity_foundation_design.geometry_pref == 'rectangular':
                width = body_results.design_results.anchor_and_foundation.gravity_foundation_design.base_ly
                length = body_results.design_results.anchor_and_foundation.gravity_foundation_design.base_lx
            elif body_results.design_results.anchor_and_foundation.gravity_foundation_design.geometry_pref == 'cylinder':
                width = body_results.design_results.anchor_and_foundation.gravity_foundation_design.base_radius
                length = body_results.design_results.anchor_and_foundation.gravity_foundation_design.base_radius
            elif body_results.design_results.anchor_and_foundation.gravity_foundation_design.geometry_pref == 'contact_points':
                r1 = body_results.design_results.anchor_and_foundation.gravity_foundation_design.base_r1
                r2 = body_results.design_results.anchor_and_foundation.gravity_foundation_design.base_r2
                r3 = body_results.design_results.anchor_and_foundation.gravity_foundation_design.base_r3
                width = 2*max([r1,r2,r3])
                length = width
            failure_rate = 0.0
        elif body_results.design_results.anchor_and_foundation.foundation_type == '':
            pass
        else:
            raise Exception("Error: cannot populate results of foundation type " + str(body_results.design_results.anchor_and_foundation.foundation_type) + ' for fixed structure')

        name = parent_id + '_' + name_short

        # Add hierarchy line
        self.add_one_hierarchy_line('SK',name,name,'Component','foundation','Level 0',parent_id,['NA'],'NA','NA',str(failure_rate))

        # Add hierarchy data
        upstream_id=[parent_id]
        downstream_id=['NA']
        coordinates = body_position
        cost = body_results.design_results.anchor_and_foundation.unit_cost
        self.hierarchy_data.add_foundation(name,'NA',body_results.design_results.anchor_and_foundation.foundation_type,coordinates,height,width,length,buried_height,mass,cost,upstream_id,downstream_id)

    def populate_mooring_line(self,mooring,ml_list,parent_id,ml_of_segment,ml_of_node,failure_rate_level,body_results) :

        for iml in range(0,len(ml_list)):

            # Register one hierarchy line per mooring line
            child_list=[]
            seg_list_idx=[]
            nseg = 0
            for iseg in range(0,len(ml_of_segment)):
                if ml_of_segment[iseg] == iml+1: # Note: iml + 1 here because the mooring line are numbered from 1
                    nseg = nseg +1
                    seg_list_idx.append(iseg)
                    mooring.line_properties[iseg].name = ml_list[iml] + '_seg_' + str(nseg-1)
                    child_list.append(mooring.line_properties[iseg].name)
            if failure_rate_level == 'mooring line':
                ml_failure_rate = self.ml_failure_rate
            elif failure_rate_level == 'component':
                ml_failure_rate = 'NA'
            else:
                raise Exception("Error in hierarchy_populate: invalid value of failure_rate_level: " + str(failure_rate_level))

            # Write the lines corresponding to the line segments
            self.populate_line_segment(child_list,seg_list_idx,ml_list[iml],failure_rate_level,mooring)

            # Write the lines corresponding to the anchors connected to the mooring line ml and add hierarchy data
            if body_results.design_results.anchor_and_foundation.n_anchors>0:
                anchor_names = self.populate_anchor(iml,ml_of_node,ml_list[iml],failure_rate_level,mooring,body_results)
                for ida in range(0,len(anchor_names)):
                    child_list.append(anchor_names[ida])

            # Hierarchy line representing the mooring line
            self.add_one_hierarchy_line('SK',ml_list[iml],'NA','System','mooring_line','Level 1',parent_id,child_list,'AND','NA',str(ml_failure_rate))


            # Add data for the line segments to the hierarchy data
            for idx in range(0,len(seg_list_idx)):
                [design_id,catalogue_id,material,length,total_mass,diameter,cost,upstream_id,downstream_id] = mooring.get_line_segment_data_for_hierarchy(seg_list_idx[idx])
                self.hierarchy_data.add_line_segment(design_id,catalogue_id,material,length,total_mass,diameter,cost,upstream_id,downstream_id)


    def populate_anchor(self,iml,ml_of_node,parent_id,failure_rate_level,mooring,body_results):

        # Init
        n_anchors_in_ml = 0
        coord_list = []
        anchor_names = []

        # Find how many anchors are needed for the current mooring line
        for inode in range(0,len(ml_of_node)):
            if ml_of_node[inode] == iml+1:
                if mooring.node_properties[inode].anchor_required:
                    n_anchors_in_ml = n_anchors_in_ml +1
                    coord_list.append(mooring.node_properties[inode].position)

        # Fill the hierarchy table
        for idx in range(0,n_anchors_in_ml):

            # Define failure rate
            failure_rate = 'NA'
            buried_height = 0.0
            if failure_rate_level ==  'component':
                failure_rate = self.anchor_failure_rate

            # Define name and design id
            if body_results.design_results.anchor_and_foundation.foundation_type == 'pile':
                name_short = body_results.design_results.anchor_and_foundation.pile_foundation_design.name
                height=body_results.design_results.anchor_and_foundation.pile_foundation_design.total_length
                width=body_results.design_results.anchor_and_foundation.pile_foundation_design.d_ext
                length=body_results.design_results.anchor_and_foundation.pile_foundation_design.d_ext
                mass=body_results.design_results.anchor_and_foundation.pile_foundation_design.pile_weight/9.81
                buried_height=body_results.design_results.anchor_and_foundation.pile_foundation_design.plength
            elif body_results.design_results.anchor_and_foundation.foundation_type == 'gravity_anchor':
                name_short = body_results.design_results.anchor_and_foundation.gravity_foundation_design.name
                height = body_results.design_results.anchor_and_foundation.gravity_foundation_design.thickness
                mass = body_results.design_results.anchor_and_foundation.gravity_foundation_design.found_weight/9.81
                if body_results.design_results.anchor_and_foundation.gravity_foundation_design.geometry_pref == 'rectangular':
                    width = body_results.design_results.anchor_and_foundation.gravity_foundation_design.base_ly
                    length = body_results.design_results.anchor_and_foundation.gravity_foundation_design.base_lx
                elif body_results.design_results.anchor_and_foundation.gravity_foundation_design.geometry_pref == 'cylinder':
                    width = body_results.design_results.anchor_and_foundation.gravity_foundation_design.base_radius
                    length = body_results.design_results.anchor_and_foundation.gravity_foundation_design.base_radius
                elif body_results.design_results.anchor_and_foundation.gravity_foundation_design.geometry_pref == 'contact_points':
                    r1 = body_results.design_results.anchor_and_foundation.gravity_foundation_design.base_r1
                    r2 = body_results.design_results.anchor_and_foundation.gravity_foundation_design.base_r2
                    r3 = body_results.design_results.anchor_and_foundation.gravity_foundation_design.base_r3
                    width = 2*max([r1,r2,r3])
                    length = width
            elif body_results.design_results.anchor_and_foundation.foundation_type == 'drag_anchor':
                name_short = body_results.design_results.anchor_and_foundation.drag_anchor_design.anchor_id
                height=body_results.design_results.anchor_and_foundation.drag_anchor_design.dim_ef
                width=body_results.design_results.anchor_and_foundation.drag_anchor_design.dim_b
                length=body_results.design_results.anchor_and_foundation.drag_anchor_design.dim_a
                mass=body_results.design_results.anchor_and_foundation.drag_anchor_design.weight # drag_anchor_design.weight is the mass, not the weight
            elif body_results.design_results.anchor_and_foundation.foundation_type == 'suction_caisson':
                name_short = body_results.design_results.anchor_and_foundation.suction_caisson_design.name
                height=body_results.design_results.anchor_and_foundation.suction_caisson_design.length
                width=body_results.design_results.anchor_and_foundation.suction_caisson_design.d_ext
                length=body_results.design_results.anchor_and_foundation.suction_caisson_design.d_ext
                mass=body_results.design_results.anchor_and_foundation.suction_caisson_design.anchor_mass
                buried_height=body_results.design_results.anchor_and_foundation.suction_caisson_design.length
            elif body_results.design_results.anchor_and_foundation.foundation_type == '':
                pass
            else:
                raise Exception("Error: cannot populate results of foundation type " + str(body_results.design_results.anchor_and_foundation.foundation_type))
            name = parent_id + '_' + name_short + '_' + str(idx)
            anchor_names.append(name)

            # Add hierarchy line
            self.add_one_hierarchy_line('SK',name,name,'Component','anchor','Level 0',parent_id,['NA'],'NA','NA',str(failure_rate))

            # Add hierarchy data
            upstream_id=[parent_id]
            downstream_id=['NA']
            coordinates = coord_list[idx]
            cost = body_results.design_results.anchor_and_foundation.unit_cost
            self.hierarchy_data.add_anchor(name,'NA',body_results.design_results.anchor_and_foundation.foundation_type,coordinates,height,width,length,buried_height,mass,cost,upstream_id,downstream_id)

        return anchor_names

    def populate_line_segment(self,seg_list_name,seg_list_idx,parent_id,failure_rate_level,mooring):

        for idx_seg in range(0,len(seg_list_name)):

            # Register one hierarchy line per line segment
            failure_rate = 'NA'
            if failure_rate_level ==  'component':
                if (mooring.get_type_of_line(seg_list_idx[idx_seg]).failure_rate_replacement > 0):
                    failure_rate = mooring.get_type_of_line(seg_list_idx[idx_seg]).failure_rate_replacement
                else:
                    type_of_line = mooring.get_type_of_line(seg_list_idx[idx_seg]).material
                    if type_of_line == 'chain':
                        failure_rate = self.chain_failure_rate
                    elif ((type_of_line == 'wire_rope')or(type_of_line == 'nylon')):
                        failure_rate = self.wire_rope_failure_rate
                    elif type_of_line == 'polyester':
                        failure_rate = self.polyester_rope_failure_rate

            self.add_one_hierarchy_line('SK',seg_list_name[idx_seg],seg_list_name[idx_seg],'Component','line_segment','Level 0',parent_id,['NA'],'NA','NA',str(failure_rate))


    def add_one_hierarchy_line(self,system,name_of_node,design_id,node_type,node_subtype,category,parent,child_list,gate_type,failure_rate_repair,failure_rate_replacement):

        self.system.append(system)
        self.name_of_node.append(name_of_node)
        self.design_id.append(design_id)
        self.node_type.append(node_type)
        self.node_subtype.append(node_subtype)
        self.category.append(category)
        self.parent.append(parent)
        self.child.append(child_list)
        self.gate_type.append(gate_type)
        self.failure_rate_repair.append(failure_rate_repair)
        self.failure_rate_replacement.append(failure_rate_replacement)

        pass

    def fill_pandas_table(self):

        d = {'System': self.system, 'Name of Node': self.name_of_node,'Design ID': self.design_id,'Node Type': self.node_type,'Node Subtype': self.node_subtype,'Category': self.category,'Parent': self.parent,'Child': self.child,'Gate Type': self.gate_type,'Failure Rate Repair': self.failure_rate_repair,'Failure Rate Replacement': self.failure_rate_replacement}
        hierarchy_as_pandas_table = pd.DataFrame(data=d)
        hierarchy_as_pandas_table = hierarchy_as_pandas_table.to_json()

        return hierarchy_as_pandas_table

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def system(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:system of the node
        """
        return self._system
    #------------
    @ property
    def name_of_node(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:name of each node
        """
        return self._name_of_node
    #------------
    @ property
    def design_id(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:design id of the node, used to fetch more data in the appropriate list
        """
        return self._design_id
    #------------
    @ property
    def node_type(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:type of node, System or Component
        """
        return self._node_type
    #------------
    @ property
    def node_subtype(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:subtype of node: anchor, foundation, mooring_line or mooring_line_segment
        """
        return self._node_subtype
    #------------
    @ property
    def category(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:node category (Level x)
        """
        return self._category
    #------------
    @ property
    def parent(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:node parent
        """
        return self._parent
    #------------
    @ property
    def child(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:node child (can be several)
        """
        return self._child
    #------------
    @ property
    def gate_type(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:node gate type AND (all must be ok), x/N (x/N must be ok), NA (not applicable)
        """
        return self._gate_type
    #------------
    @ property
    def failure_rate_repair(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:failure rate leading to reparation [1/year]
        """
        return self._failure_rate_repair
    #------------
    @ property
    def failure_rate_replacement(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:failure rate leading to failure [1/year]
        """
        return self._failure_rate_replacement
    #------------
    @ property
    def hierarchy_data(self): # pragma: no cover
        """:obj:`~.HierarchyData.HierarchyData`: object that contains physical properties of the nodes present in the hierarchy
        """
        return self._hierarchy_data
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ system.setter
    def system(self,val): # pragma: no cover
        self._system=val
    #------------
    @ name_of_node.setter
    def name_of_node(self,val): # pragma: no cover
        self._name_of_node=val
    #------------
    @ design_id.setter
    def design_id(self,val): # pragma: no cover
        self._design_id=val
    #------------
    @ node_type.setter
    def node_type(self,val): # pragma: no cover
        self._node_type=val
    #------------
    @ node_subtype.setter
    def node_subtype(self,val): # pragma: no cover
        self._node_subtype=val
    #------------
    @ category.setter
    def category(self,val): # pragma: no cover
        self._category=val
    #------------
    @ parent.setter
    def parent(self,val): # pragma: no cover
        self._parent=val
    #------------
    @ child.setter
    def child(self,val): # pragma: no cover
        self._child=val
    #------------
    @ gate_type.setter
    def gate_type(self,val): # pragma: no cover
        self._gate_type=val
    #------------
    @ failure_rate_repair.setter
    def failure_rate_repair(self,val): # pragma: no cover
        self._failure_rate_repair=val
    #------------
    @ failure_rate_replacement.setter
    def failure_rate_replacement(self,val): # pragma: no cover
        self._failure_rate_replacement=val
    #------------
    @ hierarchy_data.setter
    def hierarchy_data(self,val): # pragma: no cover
        self._hierarchy_data=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:outputs:Hierarchy"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:outputs:Hierarchy"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("system"):
            if short:
                rep["system"] = str(len(self.system))
            else:
                rep["system"] = self.system
        else:
            rep["system"] = []
        if self.is_set("name_of_node"):
            if short:
                rep["name_of_node"] = str(len(self.name_of_node))
            else:
                rep["name_of_node"] = self.name_of_node
        else:
            rep["name_of_node"] = []
        if self.is_set("design_id"):
            if short:
                rep["design_id"] = str(len(self.design_id))
            else:
                rep["design_id"] = self.design_id
        else:
            rep["design_id"] = []
        if self.is_set("node_type"):
            if short:
                rep["node_type"] = str(len(self.node_type))
            else:
                rep["node_type"] = self.node_type
        else:
            rep["node_type"] = []
        if self.is_set("node_subtype"):
            if short:
                rep["node_subtype"] = str(len(self.node_subtype))
            else:
                rep["node_subtype"] = self.node_subtype
        else:
            rep["node_subtype"] = []
        if self.is_set("category"):
            if short:
                rep["category"] = str(len(self.category))
            else:
                rep["category"] = self.category
        else:
            rep["category"] = []
        if self.is_set("parent"):
            if short:
                rep["parent"] = str(len(self.parent))
            else:
                rep["parent"] = self.parent
        else:
            rep["parent"] = []
        if self.is_set("child"):
            if short:
                rep["child"] = str(len(self.child))
            else:
                rep["child"] = self.child
        else:
            rep["child"] = []
        if self.is_set("gate_type"):
            if short:
                rep["gate_type"] = str(len(self.gate_type))
            else:
                rep["gate_type"] = self.gate_type
        else:
            rep["gate_type"] = []
        if self.is_set("failure_rate_repair"):
            if short:
                rep["failure_rate_repair"] = str(len(self.failure_rate_repair))
            else:
                rep["failure_rate_repair"] = self.failure_rate_repair
        else:
            rep["failure_rate_repair"] = []
        if self.is_set("failure_rate_replacement"):
            if short:
                rep["failure_rate_replacement"] = str(len(self.failure_rate_replacement))
            else:
                rep["failure_rate_replacement"] = self.failure_rate_replacement
        else:
            rep["failure_rate_replacement"] = []
        if self.is_set("hierarchy_data"):
            if (short and not(deep)):
                rep["hierarchy_data"] = self.hierarchy_data.type_rep()
            else:
                rep["hierarchy_data"] = self.hierarchy_data.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        asciiList = [n.encode("ascii", "ignore") for n in self.system]
        handle["system"] = asciiList
        asciiList = [n.encode("ascii", "ignore") for n in self.name_of_node]
        handle["name_of_node"] = asciiList
        asciiList = [n.encode("ascii", "ignore") for n in self.design_id]
        handle["design_id"] = asciiList
        asciiList = [n.encode("ascii", "ignore") for n in self.node_type]
        handle["node_type"] = asciiList
        asciiList = [n.encode("ascii", "ignore") for n in self.node_subtype]
        handle["node_subtype"] = asciiList
        asciiList = [n.encode("ascii", "ignore") for n in self.category]
        handle["category"] = asciiList
        asciiList = [n.encode("ascii", "ignore") for n in self.parent]
        handle["parent"] = asciiList
        asciiList = [n.encode("ascii", "ignore") for n in self.child]
        handle["child"] = asciiList
        asciiList = [n.encode("ascii", "ignore") for n in self.gate_type]
        handle["gate_type"] = asciiList
        asciiList = [n.encode("ascii", "ignore") for n in self.failure_rate_repair]
        handle["failure_rate_repair"] = asciiList
        asciiList = [n.encode("ascii", "ignore") for n in self.failure_rate_replacement]
        handle["failure_rate_replacement"] = asciiList
        subgroup = handle.create_group("hierarchy_data")
        self.hierarchy_data.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "system"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name_of_node"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "design_id"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "node_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "node_subtype"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "category"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "parent"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "child"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "gate_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "failure_rate_repair"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "failure_rate_replacement"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "hierarchy_data"
        try :
            if data[varName] != None:
                self.hierarchy_data=HierarchyData.HierarchyData()
                self.hierarchy_data.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("system" in list(gr.keys())):
            self.system = [n.decode("ascii", "ignore") for n in gr["system"][:]]
        if ("name_of_node" in list(gr.keys())):
            self.name_of_node = [n.decode("ascii", "ignore") for n in gr["name_of_node"][:]]
        if ("design_id" in list(gr.keys())):
            self.design_id = [n.decode("ascii", "ignore") for n in gr["design_id"][:]]
        if ("node_type" in list(gr.keys())):
            self.node_type = [n.decode("ascii", "ignore") for n in gr["node_type"][:]]
        if ("node_subtype" in list(gr.keys())):
            self.node_subtype = [n.decode("ascii", "ignore") for n in gr["node_subtype"][:]]
        if ("category" in list(gr.keys())):
            self.category = [n.decode("ascii", "ignore") for n in gr["category"][:]]
        if ("parent" in list(gr.keys())):
            self.parent = [n.decode("ascii", "ignore") for n in gr["parent"][:]]
        if ("child" in list(gr.keys())):
            self.child = [n.decode("ascii", "ignore") for n in gr["child"][:][:]]
        if ("gate_type" in list(gr.keys())):
            self.gate_type = [n.decode("ascii", "ignore") for n in gr["gate_type"][:]]
        if ("failure_rate_repair" in list(gr.keys())):
            self.failure_rate_repair = [n.decode("ascii", "ignore") for n in gr["failure_rate_repair"][:]]
        if ("failure_rate_replacement" in list(gr.keys())):
            self.failure_rate_replacement = [n.decode("ascii", "ignore") for n in gr["failure_rate_replacement"][:]]
        if ("hierarchy_data" in list(gr.keys())):
            subgroup = gr["hierarchy_data"]
            self.hierarchy_data.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
