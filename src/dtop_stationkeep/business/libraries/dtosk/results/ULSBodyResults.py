# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class ULSBodyResults():

    """data model representing uls results for one body
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._forces_on_fixed_structure=np.zeros(shape=(1,1,1), dtype=float)
        self._mooring_pretension=np.zeros(shape=(1,1), dtype=float)
        self._mooring_stiffness=np.zeros(shape=(6,6), dtype=float)
        self._eigenperiod=np.zeros(shape=(6), dtype=float)
        self._total_offset=np.zeros(shape=(1,1), dtype=float)
        self._total_position=np.zeros(shape=(1,1,1), dtype=float)
        self._steady_position=np.zeros(shape=(1,1,1), dtype=float)
        self._steady_force=np.zeros(shape=(1,1,1), dtype=float)
        self._steady_offset=np.zeros(shape=(1,1), dtype=float)
        self._steady_mooring_force=np.zeros(shape=(1,1,1,1,1), dtype=float)
        self._steady_mooring_tension=np.zeros(shape=(1,1,1,1), dtype=float)
        self._dynamic_offset=np.zeros(shape=(1,1), dtype=float)
        self._motion_spectrum=np.zeros(shape=(1,1,1,1), dtype=float)
        self._omega=np.zeros(shape=(1), dtype=float)
        self._damping_matrix=np.zeros(shape=(1,1,1), dtype=float)
        self._mooring_force=np.zeros(shape=(1,1,1,1,1), dtype=float)
        self._mooring_tension=np.zeros(shape=(1,1,1,1), dtype=float)
        self._tension_versus_mbl=np.zeros(shape=(1,1,1), dtype=float)
        self._mbl_uls=np.zeros(shape=(1), dtype=float)
        self._mooring_touchdown=np.zeros(shape=(1,1,1), dtype=float)
        self._safety_factor=0.0
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def forces_on_fixed_structure(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(beta),len(environment_array),6) dim(*,*,*) []
        """
        return self._forces_on_fixed_structure
    #------------
    @ property
    def mooring_pretension(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(nlines,2) dim(*,*) []
        """
        return self._mooring_pretension
    #------------
    @ property
    def mooring_stiffness(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Stiffness of mooring system at initial position. [N/m], [N.m/rad] dim(6,6) []
        """
        return self._mooring_stiffness
    #------------
    @ property
    def eigenperiod(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Estimated eigenperiods of the anchored floater [s] dim(6) []
        """
        return self._eigenperiod
    #------------
    @ property
    def total_offset(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(beta),len(environment_array)) dim(*,*) []
        """
        return self._total_offset
    #------------
    @ property
    def total_position(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(beta),len(environment_array),6) dim(*,*,*) []
        """
        return self._total_position
    #------------
    @ property
    def steady_position(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(beta),len(environment_array),6) dim(*,*,*) []
        """
        return self._steady_position
    #------------
    @ property
    def steady_force(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(beta),len(environment_array),6) dim(*,*,*) []
        """
        return self._steady_force
    #------------
    @ property
    def steady_offset(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(beta),len(environment_array)) dim(*,*) []
        """
        return self._steady_offset
    #------------
    @ property
    def steady_mooring_force(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(beta),len(environment_array),len(body.mooring_system_force.line_properties),2,3) dim(*,*,*,*,*) []
        """
        return self._steady_mooring_force
    #------------
    @ property
    def steady_mooring_tension(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(beta),len(environment_array),len(body.mooring_system_force.line_properties),2) dim(*,*,*,*) []
        """
        return self._steady_mooring_tension
    #------------
    @ property
    def dynamic_offset(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(beta),len(environment_array)) dim(*,*) []
        """
        return self._dynamic_offset
    #------------
    @ property
    def motion_spectrum(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(beta),len(environment_array),n_omega,6) dim(*,*,*,*) []
        """
        return self._motion_spectrum
    #------------
    @ property
    def omega(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(n_omega) dim(*) []
        """
        return self._omega
    #------------
    @ property
    def damping_matrix(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(beta),len(environment_array),6) dim(*,*,*) []
        """
        return self._damping_matrix
    #------------
    @ property
    def mooring_force(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(beta),len(environment_array),len(body.mooring_system_force.line_properties),2,3) dim(*,*,*,*,*) []
        """
        return self._mooring_force
    #------------
    @ property
    def mooring_tension(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(beta),len(environment_array),len(body.mooring_system_force.line_properties),2) dim(*,*,*,*) []
        """
        return self._mooring_tension
    #------------
    @ property
    def tension_versus_mbl(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(beta),len(environment_array),len(body.mooring_system_force.line_properties)) dim(*,*,*) []
        """
        return self._tension_versus_mbl
    #------------
    @ property
    def mbl_uls(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:MBL of each segment, used for the uls analysis. shape=(len(body.mooring_system_force.line_properties)) dim(*) []
        """
        return self._mbl_uls
    #------------
    @ property
    def mooring_touchdown(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(beta),len(environment_array),len(body.mooring_system_force.line_properties)) dim(*,*,*) []
        """
        return self._mooring_touchdown
    #------------
    @ property
    def safety_factor(self): # pragma: no cover
        """float: Safety factor used to check tension against mbl : Tension * SF < MBL is ok. []
        """
        return self._safety_factor
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ forces_on_fixed_structure.setter
    def forces_on_fixed_structure(self,val): # pragma: no cover
        self._forces_on_fixed_structure=val
    #------------
    @ mooring_pretension.setter
    def mooring_pretension(self,val): # pragma: no cover
        self._mooring_pretension=val
    #------------
    @ mooring_stiffness.setter
    def mooring_stiffness(self,val): # pragma: no cover
        self._mooring_stiffness=val
    #------------
    @ eigenperiod.setter
    def eigenperiod(self,val): # pragma: no cover
        self._eigenperiod=val
    #------------
    @ total_offset.setter
    def total_offset(self,val): # pragma: no cover
        self._total_offset=val
    #------------
    @ total_position.setter
    def total_position(self,val): # pragma: no cover
        self._total_position=val
    #------------
    @ steady_position.setter
    def steady_position(self,val): # pragma: no cover
        self._steady_position=val
    #------------
    @ steady_force.setter
    def steady_force(self,val): # pragma: no cover
        self._steady_force=val
    #------------
    @ steady_offset.setter
    def steady_offset(self,val): # pragma: no cover
        self._steady_offset=val
    #------------
    @ steady_mooring_force.setter
    def steady_mooring_force(self,val): # pragma: no cover
        self._steady_mooring_force=val
    #------------
    @ steady_mooring_tension.setter
    def steady_mooring_tension(self,val): # pragma: no cover
        self._steady_mooring_tension=val
    #------------
    @ dynamic_offset.setter
    def dynamic_offset(self,val): # pragma: no cover
        self._dynamic_offset=val
    #------------
    @ motion_spectrum.setter
    def motion_spectrum(self,val): # pragma: no cover
        self._motion_spectrum=val
    #------------
    @ omega.setter
    def omega(self,val): # pragma: no cover
        self._omega=val
    #------------
    @ damping_matrix.setter
    def damping_matrix(self,val): # pragma: no cover
        self._damping_matrix=val
    #------------
    @ mooring_force.setter
    def mooring_force(self,val): # pragma: no cover
        self._mooring_force=val
    #------------
    @ mooring_tension.setter
    def mooring_tension(self,val): # pragma: no cover
        self._mooring_tension=val
    #------------
    @ tension_versus_mbl.setter
    def tension_versus_mbl(self,val): # pragma: no cover
        self._tension_versus_mbl=val
    #------------
    @ mbl_uls.setter
    def mbl_uls(self,val): # pragma: no cover
        self._mbl_uls=val
    #------------
    @ mooring_touchdown.setter
    def mooring_touchdown(self,val): # pragma: no cover
        self._mooring_touchdown=val
    #------------
    @ safety_factor.setter
    def safety_factor(self,val): # pragma: no cover
        self._safety_factor=float(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:results:ULSBodyResults"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:results:ULSBodyResults"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("forces_on_fixed_structure"):
            if (short):
                rep["forces_on_fixed_structure"] = str(self.forces_on_fixed_structure.shape)
            else:
                try:
                    rep["forces_on_fixed_structure"] = self.forces_on_fixed_structure.tolist()
                except:
                    rep["forces_on_fixed_structure"] = self.forces_on_fixed_structure
        if self.is_set("mooring_pretension"):
            if (short):
                rep["mooring_pretension"] = str(self.mooring_pretension.shape)
            else:
                try:
                    rep["mooring_pretension"] = self.mooring_pretension.tolist()
                except:
                    rep["mooring_pretension"] = self.mooring_pretension
        if self.is_set("mooring_stiffness"):
            if (short):
                rep["mooring_stiffness"] = str(self.mooring_stiffness.shape)
            else:
                try:
                    rep["mooring_stiffness"] = self.mooring_stiffness.tolist()
                except:
                    rep["mooring_stiffness"] = self.mooring_stiffness
        if self.is_set("eigenperiod"):
            if (short):
                rep["eigenperiod"] = str(self.eigenperiod.shape)
            else:
                try:
                    rep["eigenperiod"] = self.eigenperiod.tolist()
                except:
                    rep["eigenperiod"] = self.eigenperiod
        if self.is_set("total_offset"):
            if (short):
                rep["total_offset"] = str(self.total_offset.shape)
            else:
                try:
                    rep["total_offset"] = self.total_offset.tolist()
                except:
                    rep["total_offset"] = self.total_offset
        if self.is_set("total_position"):
            if (short):
                rep["total_position"] = str(self.total_position.shape)
            else:
                try:
                    rep["total_position"] = self.total_position.tolist()
                except:
                    rep["total_position"] = self.total_position
        if self.is_set("steady_position"):
            if (short):
                rep["steady_position"] = str(self.steady_position.shape)
            else:
                try:
                    rep["steady_position"] = self.steady_position.tolist()
                except:
                    rep["steady_position"] = self.steady_position
        if self.is_set("steady_force"):
            if (short):
                rep["steady_force"] = str(self.steady_force.shape)
            else:
                try:
                    rep["steady_force"] = self.steady_force.tolist()
                except:
                    rep["steady_force"] = self.steady_force
        if self.is_set("steady_offset"):
            if (short):
                rep["steady_offset"] = str(self.steady_offset.shape)
            else:
                try:
                    rep["steady_offset"] = self.steady_offset.tolist()
                except:
                    rep["steady_offset"] = self.steady_offset
        if self.is_set("steady_mooring_force"):
            if (short):
                rep["steady_mooring_force"] = str(self.steady_mooring_force.shape)
            else:
                try:
                    rep["steady_mooring_force"] = self.steady_mooring_force.tolist()
                except:
                    rep["steady_mooring_force"] = self.steady_mooring_force
        if self.is_set("steady_mooring_tension"):
            if (short):
                rep["steady_mooring_tension"] = str(self.steady_mooring_tension.shape)
            else:
                try:
                    rep["steady_mooring_tension"] = self.steady_mooring_tension.tolist()
                except:
                    rep["steady_mooring_tension"] = self.steady_mooring_tension
        if self.is_set("dynamic_offset"):
            if (short):
                rep["dynamic_offset"] = str(self.dynamic_offset.shape)
            else:
                try:
                    rep["dynamic_offset"] = self.dynamic_offset.tolist()
                except:
                    rep["dynamic_offset"] = self.dynamic_offset
        if self.is_set("motion_spectrum"):
            if (short):
                rep["motion_spectrum"] = str(self.motion_spectrum.shape)
            else:
                try:
                    rep["motion_spectrum"] = self.motion_spectrum.tolist()
                except:
                    rep["motion_spectrum"] = self.motion_spectrum
        if self.is_set("omega"):
            if (short):
                rep["omega"] = str(self.omega.shape)
            else:
                try:
                    rep["omega"] = self.omega.tolist()
                except:
                    rep["omega"] = self.omega
        if self.is_set("damping_matrix"):
            if (short):
                rep["damping_matrix"] = str(self.damping_matrix.shape)
            else:
                try:
                    rep["damping_matrix"] = self.damping_matrix.tolist()
                except:
                    rep["damping_matrix"] = self.damping_matrix
        if self.is_set("mooring_force"):
            if (short):
                rep["mooring_force"] = str(self.mooring_force.shape)
            else:
                try:
                    rep["mooring_force"] = self.mooring_force.tolist()
                except:
                    rep["mooring_force"] = self.mooring_force
        if self.is_set("mooring_tension"):
            if (short):
                rep["mooring_tension"] = str(self.mooring_tension.shape)
            else:
                try:
                    rep["mooring_tension"] = self.mooring_tension.tolist()
                except:
                    rep["mooring_tension"] = self.mooring_tension
        if self.is_set("tension_versus_mbl"):
            if (short):
                rep["tension_versus_mbl"] = str(self.tension_versus_mbl.shape)
            else:
                try:
                    rep["tension_versus_mbl"] = self.tension_versus_mbl.tolist()
                except:
                    rep["tension_versus_mbl"] = self.tension_versus_mbl
        if self.is_set("mbl_uls"):
            if (short):
                rep["mbl_uls"] = str(self.mbl_uls.shape)
            else:
                try:
                    rep["mbl_uls"] = self.mbl_uls.tolist()
                except:
                    rep["mbl_uls"] = self.mbl_uls
        if self.is_set("mooring_touchdown"):
            if (short):
                rep["mooring_touchdown"] = str(self.mooring_touchdown.shape)
            else:
                try:
                    rep["mooring_touchdown"] = self.mooring_touchdown.tolist()
                except:
                    rep["mooring_touchdown"] = self.mooring_touchdown
        if self.is_set("safety_factor"):
            rep["safety_factor"] = self.safety_factor
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["forces_on_fixed_structure"] = np.array(self.forces_on_fixed_structure,dtype=float)
        handle["mooring_pretension"] = np.array(self.mooring_pretension,dtype=float)
        handle["mooring_stiffness"] = np.array(self.mooring_stiffness,dtype=float)
        handle["eigenperiod"] = np.array(self.eigenperiod,dtype=float)
        handle["total_offset"] = np.array(self.total_offset,dtype=float)
        handle["total_position"] = np.array(self.total_position,dtype=float)
        handle["steady_position"] = np.array(self.steady_position,dtype=float)
        handle["steady_force"] = np.array(self.steady_force,dtype=float)
        handle["steady_offset"] = np.array(self.steady_offset,dtype=float)
        handle["steady_mooring_force"] = np.array(self.steady_mooring_force,dtype=float)
        handle["steady_mooring_tension"] = np.array(self.steady_mooring_tension,dtype=float)
        handle["dynamic_offset"] = np.array(self.dynamic_offset,dtype=float)
        handle["motion_spectrum"] = np.array(self.motion_spectrum,dtype=float)
        handle["omega"] = np.array(self.omega,dtype=float)
        handle["damping_matrix"] = np.array(self.damping_matrix,dtype=float)
        handle["mooring_force"] = np.array(self.mooring_force,dtype=float)
        handle["mooring_tension"] = np.array(self.mooring_tension,dtype=float)
        handle["tension_versus_mbl"] = np.array(self.tension_versus_mbl,dtype=float)
        handle["mbl_uls"] = np.array(self.mbl_uls,dtype=float)
        handle["mooring_touchdown"] = np.array(self.mooring_touchdown,dtype=float)
        handle["safety_factor"] = np.array([self.safety_factor],dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "forces_on_fixed_structure"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "mooring_pretension"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "mooring_stiffness"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "eigenperiod"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "total_offset"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "total_position"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "steady_position"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "steady_force"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "steady_offset"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "steady_mooring_force"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "steady_mooring_tension"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "dynamic_offset"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "motion_spectrum"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "omega"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "damping_matrix"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "mooring_force"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "mooring_tension"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "tension_versus_mbl"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "mbl_uls"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "mooring_touchdown"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "safety_factor"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("forces_on_fixed_structure" in list(gr.keys())):
            self.forces_on_fixed_structure = gr["forces_on_fixed_structure"][:][:][:]
        if ("mooring_pretension" in list(gr.keys())):
            self.mooring_pretension = gr["mooring_pretension"][:][:]
        if ("mooring_stiffness" in list(gr.keys())):
            self.mooring_stiffness = gr["mooring_stiffness"][:][:]
        if ("eigenperiod" in list(gr.keys())):
            self.eigenperiod = gr["eigenperiod"][:]
        if ("total_offset" in list(gr.keys())):
            self.total_offset = gr["total_offset"][:][:]
        if ("total_position" in list(gr.keys())):
            self.total_position = gr["total_position"][:][:][:]
        if ("steady_position" in list(gr.keys())):
            self.steady_position = gr["steady_position"][:][:][:]
        if ("steady_force" in list(gr.keys())):
            self.steady_force = gr["steady_force"][:][:][:]
        if ("steady_offset" in list(gr.keys())):
            self.steady_offset = gr["steady_offset"][:][:]
        if ("steady_mooring_force" in list(gr.keys())):
            self.steady_mooring_force = gr["steady_mooring_force"][:][:][:][:][:]
        if ("steady_mooring_tension" in list(gr.keys())):
            self.steady_mooring_tension = gr["steady_mooring_tension"][:][:][:][:]
        if ("dynamic_offset" in list(gr.keys())):
            self.dynamic_offset = gr["dynamic_offset"][:][:]
        if ("motion_spectrum" in list(gr.keys())):
            self.motion_spectrum = gr["motion_spectrum"][:][:][:][:]
        if ("omega" in list(gr.keys())):
            self.omega = gr["omega"][:]
        if ("damping_matrix" in list(gr.keys())):
            self.damping_matrix = gr["damping_matrix"][:][:][:]
        if ("mooring_force" in list(gr.keys())):
            self.mooring_force = gr["mooring_force"][:][:][:][:][:]
        if ("mooring_tension" in list(gr.keys())):
            self.mooring_tension = gr["mooring_tension"][:][:][:][:]
        if ("tension_versus_mbl" in list(gr.keys())):
            self.tension_versus_mbl = gr["tension_versus_mbl"][:][:][:]
        if ("mbl_uls" in list(gr.keys())):
            self.mbl_uls = gr["mbl_uls"][:]
        if ("mooring_touchdown" in list(gr.keys())):
            self.mooring_touchdown = gr["mooring_touchdown"][:][:][:]
        if ("safety_factor" in list(gr.keys())):
            self.safety_factor = gr["safety_factor"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
