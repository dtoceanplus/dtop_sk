# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np 
import os
from .entity.Body import Body
from .outputs.MooringCheckResults import MooringCheckResults
import copy

# Additional routines used by the GUI for assisting the user modelling/checking the inputs
def run_mooring_check(mooring_check_inputs,show=False):
	# Inputs:
	# mooring_check_inputs is of type dtosk:inputs:MooringCheckInputs()

	# Create mooring object
	body = Body()
	body.initial_position[0:3] = copy.deepcopy(mooring_check_inputs.device_position[0:3])
	body.initial_position[3:6] = copy.deepcopy(mooring_check_inputs.device_position[3:6])*np.pi/180.0
	body.position = copy.deepcopy(body.initial_position)
	body.mooring_system_force.create_from_custom_input(mooring_check_inputs.custom_mooring_input,body.initial_position)

	# Initiate mooring system model (map++ solver)
	body.mooring_system_force.initiate_calculation(mooring_check_inputs.water_depth,mooring_check_inputs.water_density,body.initial_position)

	# Fetch mooring line tension, force and stiffness on body
	mooring_check_results = MooringCheckResults()
	[mooring_check_results.mooring_system_force,mooring_check_results.mooring_line_tension,dum1,dum2] = body.compute_tension_in_all_lines()
	mooring_check_results.mooring_stiffness = body.compute_mooring_stiffness_by_difference()
	
	# Save representation of plotly figure
	# body.mooring_system_force.plot_html(filename = mooring_check_inputs.html_plot_file_name,show=show,logo=mooring_check_inputs.logo_file_name)
	mooring_check_results.plot = body.mooring_system_force.generate_plotly_plot_representation()

	# Save results
	mooring_check_results.saveJSON(fileName=mooring_check_inputs.results_file_name)

	

		
