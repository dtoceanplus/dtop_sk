# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from . import CustomFoundationInputs
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class FoundationDesignParameters():

    """data model representing parameters for foundation design
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._deflection_criteria_for_pile=5.0
        self._pile_tip='open_end'
        self._dist_attachment_pa=0.0
        self._pile_length_above_seabed=0.0
        self._foundation_preference='evaluation'
        self._soil_type=''
        self._soil_type_SC_value=np.zeros(shape=(1), dtype=float)
        self._soil_type_SC_name=np.zeros(shape=(1), dtype=float)
        self._soil_slope=0.0
        self._soil_properties_sf='default'
        self._user_soil_properties_sf=0.0
        self._SF=1.3
        self._foundation_material='concrete'
        self._foundation_material_density=2400
        self._foundation_cost_per_kilo=0.1
        self._soil_type_def=''
        self._undrained_shear_strength=0.0
        self._internal_friction_angle=0.0
        self._soil_buoyant_weight=0.0
        self._relative_density=0
        self._design_load_flag='auto'
        self._design_load=np.zeros(shape=(6), dtype=float)
        self._foundation_design_flag='auto'
        self._custom_foundation_input=CustomFoundationInputs.CustomFoundationInputs()
        self._custom_foundation_input.description = 'Custom foundation inputs. Required if foundation_design_flag is custom.'
        self._foundation_type_selection='auto'
        self._catalogue_index=0
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def deflection_criteria_for_pile(self): # pragma: no cover
        """float: none []
        """
        return self._deflection_criteria_for_pile
    #------------
    @ property
    def pile_tip(self): # pragma: no cover
        """str: none
        """
        return self._pile_tip
    #------------
    @ property
    def dist_attachment_pa(self): # pragma: no cover
        """float: distance of the pile load attachement point above the seafloor []
        """
        return self._dist_attachment_pa
    #------------
    @ property
    def pile_length_above_seabed(self): # pragma: no cover
        """float: Pile length above seabed (typically >=0 for a pile foundation, =0 for a pile anchor). []
        """
        return self._pile_length_above_seabed
    #------------
    @ property
    def foundation_preference(self): # pragma: no cover
        """str: User foundation preference: shallow, gravity, pile, drag, suctioncaisson, evaluation
        """
        return self._foundation_preference
    #------------
    @ property
    def soil_type(self): # pragma: no cover
        """str: none
        """
        return self._soil_type
    #------------
    @ property
    def soil_type_SC_value(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`str`: List of seabed type values coming from SC outputs
        """
        return self._soil_type_SC_value
    #------------
    @ property
    def soil_type_SC_name(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`str`: List of seabed type names coming from SC outputs
        """
        return self._soil_type_SC_name
    #------------
    @ property
    def soil_slope(self): # pragma: no cover
        """float: soil slope, steep >= 10° or moderate < 10° []
        """
        return self._soil_slope
    #------------
    @ property
    def soil_properties_sf(self): # pragma: no cover
        """str: user or default. If default DNV-OSJ103 is used.)
        """
        return self._soil_properties_sf
    #------------
    @ property
    def user_soil_properties_sf(self): # pragma: no cover
        """float: Soil safety factor. Required only if soil_properties_sf is user. []
        """
        return self._user_soil_properties_sf
    #------------
    @ property
    def SF(self): # pragma: no cover
        """float: Load safety factor []
        """
        return self._SF
    #------------
    @ property
    def foundation_material(self): # pragma: no cover
        """str: Foundation material type
        """
        return self._foundation_material
    #------------
    @ property
    def foundation_material_density(self): # pragma: no cover
        """float: Foundation/anchor material density [kg/m3]
        """
        return self._foundation_material_density
    #------------
    @ property
    def foundation_cost_per_kilo(self): # pragma: no cover
        """float: Foundation/anchor material cost [€/kg]
        """
        return self._foundation_cost_per_kilo
    #------------
    @ property
    def soil_type_def(self): # pragma: no cover
        """str: user definition only for soil_type equal to user, cohesive or cohesionless 
        """
        return self._soil_type_def
    #------------
    @ property
    def undrained_shear_strength(self): # pragma: no cover
        """float: user definition only for soil_type_def equal to cohesive. [Pa] []
        """
        return self._undrained_shear_strength
    #------------
    @ property
    def internal_friction_angle(self): # pragma: no cover
        """float: internal friction angle [deg], user value required if soil_type_def equal to cohesionless []
        """
        return self._internal_friction_angle
    #------------
    @ property
    def soil_buoyant_weight(self): # pragma: no cover
        """float: soil bouyant weight in [N/m3], user definition only for soil_type equal to user  []
        """
        return self._soil_buoyant_weight
    #------------
    @ property
    def relative_density(self): # pragma: no cover
        """float: relative density of sand no units, user value required if soil_type_def equal to cohesionless  []
        """
        return self._relative_density
    #------------
    @ property
    def design_load_flag(self): # pragma: no cover
        """str: auto or manual. If manual, design_load must be given. If auto, loads from DTO+ ULS analysis will be used.
        """
        return self._design_load_flag
    #------------
    @ property
    def design_load(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Design load applied at the foundation at the sea bed level. [N] and [N.m]. [Vmin,Vmax,Hx,Hy,Mx,My]. Vmin and Vmax are vertical forces, positive downwards. Hx,Hy are horizontal force. Mx,My are moments. dim(6) []
        """
        return self._design_load
    #------------
    @ property
    def foundation_design_flag(self): # pragma: no cover
        """str: auto or custom. If custom, custom_foundation_input must be given. If auto, DTO+ will compute foundation dimensions.
        """
        return self._foundation_design_flag
    #------------
    @ property
    def custom_foundation_input(self): # pragma: no cover
        """:obj:`~.CustomFoundationInputs.CustomFoundationInputs`: Custom foundation inputs. Required if foundation_design_flag is custom.
        """
        return self._custom_foundation_input
    #------------
    @ property
    def foundation_type_selection(self): # pragma: no cover
        """str: auto or manual. If manual, the user needs to select the anchor in the catalogue.
        """
        return self._foundation_type_selection
    #------------
    @ property
    def catalogue_index(self): # pragma: no cover
        """int: Anchor index selected in the catalogue. []
        """
        return self._catalogue_index
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ deflection_criteria_for_pile.setter
    def deflection_criteria_for_pile(self,val): # pragma: no cover
        self._deflection_criteria_for_pile=float(val)
    #------------
    @ pile_tip.setter
    def pile_tip(self,val): # pragma: no cover
        self._pile_tip=str(val)
    #------------
    @ dist_attachment_pa.setter
    def dist_attachment_pa(self,val): # pragma: no cover
        self._dist_attachment_pa=float(val)
    #------------
    @ pile_length_above_seabed.setter
    def pile_length_above_seabed(self,val): # pragma: no cover
        self._pile_length_above_seabed=float(val)
    #------------
    @ foundation_preference.setter
    def foundation_preference(self,val): # pragma: no cover
        self._foundation_preference=str(val)
    #------------
    @ soil_type.setter
    def soil_type(self,val): # pragma: no cover
        self._soil_type=str(val)
    #------------
    @ soil_type_SC_value.setter
    def soil_type_SC_value(self,val): # pragma: no cover
        self._soil_type_SC_value=val
    #------------
    @ soil_type_SC_name.setter
    def soil_type_SC_name(self,val): # pragma: no cover
        self._soil_type_SC_name=val
    #------------
    @ soil_slope.setter
    def soil_slope(self,val): # pragma: no cover
        self._soil_slope=float(val)
    #------------
    @ soil_properties_sf.setter
    def soil_properties_sf(self,val): # pragma: no cover
        self._soil_properties_sf=str(val)
    #------------
    @ user_soil_properties_sf.setter
    def user_soil_properties_sf(self,val): # pragma: no cover
        self._user_soil_properties_sf=float(val)
    #------------
    @ SF.setter
    def SF(self,val): # pragma: no cover
        self._SF=float(val)
    #------------
    @ foundation_material.setter
    def foundation_material(self,val): # pragma: no cover
        self._foundation_material=str(val)
    #------------
    @ foundation_material_density.setter
    def foundation_material_density(self,val): # pragma: no cover
        self._foundation_material_density=float(val)
    #------------
    @ foundation_cost_per_kilo.setter
    def foundation_cost_per_kilo(self,val): # pragma: no cover
        self._foundation_cost_per_kilo=float(val)
    #------------
    @ soil_type_def.setter
    def soil_type_def(self,val): # pragma: no cover
        self._soil_type_def=str(val)
    #------------
    @ undrained_shear_strength.setter
    def undrained_shear_strength(self,val): # pragma: no cover
        self._undrained_shear_strength=float(val)
    #------------
    @ internal_friction_angle.setter
    def internal_friction_angle(self,val): # pragma: no cover
        self._internal_friction_angle=float(val)
    #------------
    @ soil_buoyant_weight.setter
    def soil_buoyant_weight(self,val): # pragma: no cover
        self._soil_buoyant_weight=float(val)
    #------------
    @ relative_density.setter
    def relative_density(self,val): # pragma: no cover
        self._relative_density=float(val)
    #------------
    @ design_load_flag.setter
    def design_load_flag(self,val): # pragma: no cover
        self._design_load_flag=str(val)
    #------------
    @ design_load.setter
    def design_load(self,val): # pragma: no cover
        self._design_load=val
    #------------
    @ foundation_design_flag.setter
    def foundation_design_flag(self,val): # pragma: no cover
        self._foundation_design_flag=str(val)
    #------------
    @ custom_foundation_input.setter
    def custom_foundation_input(self,val): # pragma: no cover
        self._custom_foundation_input=val
    #------------
    @ foundation_type_selection.setter
    def foundation_type_selection(self,val): # pragma: no cover
        self._foundation_type_selection=str(val)
    #------------
    @ catalogue_index.setter
    def catalogue_index(self,val): # pragma: no cover
        self._catalogue_index=int(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:FoundationDesignParameters"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:FoundationDesignParameters"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("deflection_criteria_for_pile"):
            rep["deflection_criteria_for_pile"] = self.deflection_criteria_for_pile
        if self.is_set("pile_tip"):
            rep["pile_tip"] = self.pile_tip
        if self.is_set("dist_attachment_pa"):
            rep["dist_attachment_pa"] = self.dist_attachment_pa
        if self.is_set("pile_length_above_seabed"):
            rep["pile_length_above_seabed"] = self.pile_length_above_seabed
        if self.is_set("foundation_preference"):
            rep["foundation_preference"] = self.foundation_preference
        if self.is_set("soil_type"):
            rep["soil_type"] = self.soil_type
        if self.is_set("soil_type_SC_value"):
            if (short):
                rep["soil_type_SC_value"] = str(self.soil_type_SC_value.shape)
            else:
                try:
                    rep["soil_type_SC_value"] = self.soil_type_SC_value.tolist()
                except:
                    rep["soil_type_SC_value"] = self.soil_type_SC_value
        if self.is_set("soil_type_SC_name"):
            if (short):
                rep["soil_type_SC_name"] = str(self.soil_type_SC_name.shape)
            else:
                try:
                    rep["soil_type_SC_name"] = self.soil_type_SC_name.tolist()
                except:
                    rep["soil_type_SC_name"] = self.soil_type_SC_name
        if self.is_set("soil_slope"):
            rep["soil_slope"] = self.soil_slope
        if self.is_set("soil_properties_sf"):
            rep["soil_properties_sf"] = self.soil_properties_sf
        if self.is_set("user_soil_properties_sf"):
            rep["user_soil_properties_sf"] = self.user_soil_properties_sf
        if self.is_set("SF"):
            rep["SF"] = self.SF
        if self.is_set("foundation_material"):
            rep["foundation_material"] = self.foundation_material
        if self.is_set("foundation_material_density"):
            rep["foundation_material_density"] = self.foundation_material_density
        if self.is_set("foundation_cost_per_kilo"):
            rep["foundation_cost_per_kilo"] = self.foundation_cost_per_kilo
        if self.is_set("soil_type_def"):
            rep["soil_type_def"] = self.soil_type_def
        if self.is_set("undrained_shear_strength"):
            rep["undrained_shear_strength"] = self.undrained_shear_strength
        if self.is_set("internal_friction_angle"):
            rep["internal_friction_angle"] = self.internal_friction_angle
        if self.is_set("soil_buoyant_weight"):
            rep["soil_buoyant_weight"] = self.soil_buoyant_weight
        if self.is_set("relative_density"):
            rep["relative_density"] = self.relative_density
        if self.is_set("design_load_flag"):
            rep["design_load_flag"] = self.design_load_flag
        if self.is_set("design_load"):
            if (short):
                rep["design_load"] = str(self.design_load.shape)
            else:
                try:
                    rep["design_load"] = self.design_load.tolist()
                except:
                    rep["design_load"] = self.design_load
        if self.is_set("foundation_design_flag"):
            rep["foundation_design_flag"] = self.foundation_design_flag
        if self.is_set("custom_foundation_input"):
            if (short and not(deep)):
                rep["custom_foundation_input"] = self.custom_foundation_input.type_rep()
            else:
                rep["custom_foundation_input"] = self.custom_foundation_input.prop_rep(short, deep)
        if self.is_set("foundation_type_selection"):
            rep["foundation_type_selection"] = self.foundation_type_selection
        if self.is_set("catalogue_index"):
            rep["catalogue_index"] = self.catalogue_index
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["deflection_criteria_for_pile"] = np.array([self.deflection_criteria_for_pile],dtype=float)
        ar = []
        ar.append(self.pile_tip.encode("ascii"))
        handle["pile_tip"] = np.asarray(ar)
        handle["dist_attachment_pa"] = np.array([self.dist_attachment_pa],dtype=float)
        handle["pile_length_above_seabed"] = np.array([self.pile_length_above_seabed],dtype=float)
        ar = []
        ar.append(self.foundation_preference.encode("ascii"))
        handle["foundation_preference"] = np.asarray(ar)
        ar = []
        ar.append(self.soil_type.encode("ascii"))
        handle["soil_type"] = np.asarray(ar)
        handle["soil_type_SC_value"] = np.array(self.soil_type_SC_value,dtype=str)
        handle["soil_type_SC_name"] = np.array(self.soil_type_SC_name,dtype=str)
        handle["soil_slope"] = np.array([self.soil_slope],dtype=float)
        ar = []
        ar.append(self.soil_properties_sf.encode("ascii"))
        handle["soil_properties_sf"] = np.asarray(ar)
        handle["user_soil_properties_sf"] = np.array([self.user_soil_properties_sf],dtype=float)
        handle["SF"] = np.array([self.SF],dtype=float)
        ar = []
        ar.append(self.foundation_material.encode("ascii"))
        handle["foundation_material"] = np.asarray(ar)
        handle["foundation_material_density"] = np.array([self.foundation_material_density],dtype=float)
        handle["foundation_cost_per_kilo"] = np.array([self.foundation_cost_per_kilo],dtype=float)
        ar = []
        ar.append(self.soil_type_def.encode("ascii"))
        handle["soil_type_def"] = np.asarray(ar)
        handle["undrained_shear_strength"] = np.array([self.undrained_shear_strength],dtype=float)
        handle["internal_friction_angle"] = np.array([self.internal_friction_angle],dtype=float)
        handle["soil_buoyant_weight"] = np.array([self.soil_buoyant_weight],dtype=float)
        handle["relative_density"] = np.array([self.relative_density],dtype=float)
        ar = []
        ar.append(self.design_load_flag.encode("ascii"))
        handle["design_load_flag"] = np.asarray(ar)
        handle["design_load"] = np.array(self.design_load,dtype=float)
        ar = []
        ar.append(self.foundation_design_flag.encode("ascii"))
        handle["foundation_design_flag"] = np.asarray(ar)
        subgroup = handle.create_group("custom_foundation_input")
        self.custom_foundation_input.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.foundation_type_selection.encode("ascii"))
        handle["foundation_type_selection"] = np.asarray(ar)
        handle["catalogue_index"] = np.array([self.catalogue_index],dtype=int)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "deflection_criteria_for_pile"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "pile_tip"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "dist_attachment_pa"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "pile_length_above_seabed"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "foundation_preference"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "soil_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "soil_type_SC_value"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "soil_type_SC_name"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "soil_slope"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "soil_properties_sf"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "user_soil_properties_sf"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "SF"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "foundation_material"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "foundation_material_density"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "foundation_cost_per_kilo"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "soil_type_def"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "undrained_shear_strength"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "internal_friction_angle"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "soil_buoyant_weight"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "relative_density"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "design_load_flag"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "design_load"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "foundation_design_flag"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "custom_foundation_input"
        try :
            if data[varName] != None:
                self.custom_foundation_input=CustomFoundationInputs.CustomFoundationInputs()
                self.custom_foundation_input.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "foundation_type_selection"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "catalogue_index"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("deflection_criteria_for_pile" in list(gr.keys())):
            self.deflection_criteria_for_pile = gr["deflection_criteria_for_pile"][0]
        if ("pile_tip" in list(gr.keys())):
            self.pile_tip = gr["pile_tip"][0].decode("ascii")
        if ("dist_attachment_pa" in list(gr.keys())):
            self.dist_attachment_pa = gr["dist_attachment_pa"][0]
        if ("pile_length_above_seabed" in list(gr.keys())):
            self.pile_length_above_seabed = gr["pile_length_above_seabed"][0]
        if ("foundation_preference" in list(gr.keys())):
            self.foundation_preference = gr["foundation_preference"][0].decode("ascii")
        if ("soil_type" in list(gr.keys())):
            self.soil_type = gr["soil_type"][0].decode("ascii")
        if ("soil_type_SC_value" in list(gr.keys())):
            self.soil_type_SC_value = gr["soil_type_SC_value"][:]
        if ("soil_type_SC_name" in list(gr.keys())):
            self.soil_type_SC_name = gr["soil_type_SC_name"][:]
        if ("soil_slope" in list(gr.keys())):
            self.soil_slope = gr["soil_slope"][0]
        if ("soil_properties_sf" in list(gr.keys())):
            self.soil_properties_sf = gr["soil_properties_sf"][0].decode("ascii")
        if ("user_soil_properties_sf" in list(gr.keys())):
            self.user_soil_properties_sf = gr["user_soil_properties_sf"][0]
        if ("SF" in list(gr.keys())):
            self.SF = gr["SF"][0]
        if ("foundation_material" in list(gr.keys())):
            self.foundation_material = gr["foundation_material"][0].decode("ascii")
        if ("foundation_material_density" in list(gr.keys())):
            self.foundation_material_density = gr["foundation_material_density"][0]
        if ("foundation_cost_per_kilo" in list(gr.keys())):
            self.foundation_cost_per_kilo = gr["foundation_cost_per_kilo"][0]
        if ("soil_type_def" in list(gr.keys())):
            self.soil_type_def = gr["soil_type_def"][0].decode("ascii")
        if ("undrained_shear_strength" in list(gr.keys())):
            self.undrained_shear_strength = gr["undrained_shear_strength"][0]
        if ("internal_friction_angle" in list(gr.keys())):
            self.internal_friction_angle = gr["internal_friction_angle"][0]
        if ("soil_buoyant_weight" in list(gr.keys())):
            self.soil_buoyant_weight = gr["soil_buoyant_weight"][0]
        if ("relative_density" in list(gr.keys())):
            self.relative_density = gr["relative_density"][0]
        if ("design_load_flag" in list(gr.keys())):
            self.design_load_flag = gr["design_load_flag"][0].decode("ascii")
        if ("design_load" in list(gr.keys())):
            self.design_load = gr["design_load"][:]
        if ("foundation_design_flag" in list(gr.keys())):
            self.foundation_design_flag = gr["foundation_design_flag"][0].decode("ascii")
        if ("custom_foundation_input" in list(gr.keys())):
            subgroup = gr["custom_foundation_input"]
            self.custom_foundation_input.loadFromHDF5Handle(subgroup)
        if ("foundation_type_selection" in list(gr.keys())):
            self.foundation_type_selection = gr["foundation_type_selection"][0].decode("ascii")
        if ("catalogue_index" in list(gr.keys())):
            self.catalogue_index = gr["catalogue_index"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
