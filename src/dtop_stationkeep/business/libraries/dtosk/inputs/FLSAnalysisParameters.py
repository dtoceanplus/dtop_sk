# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class FLSAnalysisParameters():

    """data model representing parameters for the FLS motion analysis of the floater, same data model as dtosc:devices:outputs:ENVSWaves in dtop-site module
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._latitude=0.0
        self._longitude=0.0
        self._hs_dp_proba=np.zeros(shape=(1), dtype=float)
        self._intervals_hs_min=np.zeros(shape=(1), dtype=float)
        self._intervals_hs_max=np.zeros(shape=(1), dtype=float)
        self._intervals_dp_min=np.zeros(shape=(1), dtype=float)
        self._intervals_dp_max=np.zeros(shape=(1), dtype=float)
        self._associated_tp=np.zeros(shape=(1), dtype=float)
        self._associated_cur=np.zeros(shape=(1), dtype=float)
        self._associated_dircur=np.zeros(shape=(1), dtype=float)
        self._associated_wind=np.zeros(shape=(1), dtype=float)
        self._associated_dirwind=np.zeros(shape=(1), dtype=float)
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def latitude(self): # pragma: no cover
        """float: latitude (WGS84) []
        """
        return self._latitude
    #------------
    @ property
    def longitude(self): # pragma: no cover
        """float: longitude (WGS84) []
        """
        return self._longitude
    #------------
    @ property
    def hs_dp_proba(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:probability (in %) of environments occurence regarding intervals dim(*) []
        """
        return self._hs_dp_proba
    #------------
    @ property
    def intervals_hs_min(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:lower limit of hs intervals dim(*) []
        """
        return self._intervals_hs_min
    #------------
    @ property
    def intervals_hs_max(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:upper limit of hs intervals dim(*) []
        """
        return self._intervals_hs_max
    #------------
    @ property
    def intervals_dp_min(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:lower limit of dp intervals dim(*) []
        """
        return self._intervals_dp_min
    #------------
    @ property
    def intervals_dp_max(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:upper limit of dp intervals dim(*) []
        """
        return self._intervals_dp_max
    #------------
    @ property
    def associated_tp(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:mean wave peak period associated with environments dim(*) []
        """
        return self._associated_tp
    #------------
    @ property
    def associated_cur(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:mean current magnitude associated with environments dim(*) []
        """
        return self._associated_cur
    #------------
    @ property
    def associated_dircur(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:current direction corresponding to maximum current magnitude associated with environments dim(*) []
        """
        return self._associated_dircur
    #------------
    @ property
    def associated_wind(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:mean wind magnitude associated with environments dim(*) []
        """
        return self._associated_wind
    #------------
    @ property
    def associated_dirwind(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:wind direction corresponding to maximum wind magnitude associated with environments dim(*) []
        """
        return self._associated_dirwind
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ latitude.setter
    def latitude(self,val): # pragma: no cover
        self._latitude=float(val)
    #------------
    @ longitude.setter
    def longitude(self,val): # pragma: no cover
        self._longitude=float(val)
    #------------
    @ hs_dp_proba.setter
    def hs_dp_proba(self,val): # pragma: no cover
        self._hs_dp_proba=val
    #------------
    @ intervals_hs_min.setter
    def intervals_hs_min(self,val): # pragma: no cover
        self._intervals_hs_min=val
    #------------
    @ intervals_hs_max.setter
    def intervals_hs_max(self,val): # pragma: no cover
        self._intervals_hs_max=val
    #------------
    @ intervals_dp_min.setter
    def intervals_dp_min(self,val): # pragma: no cover
        self._intervals_dp_min=val
    #------------
    @ intervals_dp_max.setter
    def intervals_dp_max(self,val): # pragma: no cover
        self._intervals_dp_max=val
    #------------
    @ associated_tp.setter
    def associated_tp(self,val): # pragma: no cover
        self._associated_tp=val
    #------------
    @ associated_cur.setter
    def associated_cur(self,val): # pragma: no cover
        self._associated_cur=val
    #------------
    @ associated_dircur.setter
    def associated_dircur(self,val): # pragma: no cover
        self._associated_dircur=val
    #------------
    @ associated_wind.setter
    def associated_wind(self,val): # pragma: no cover
        self._associated_wind=val
    #------------
    @ associated_dirwind.setter
    def associated_dirwind(self,val): # pragma: no cover
        self._associated_dirwind=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:FLSAnalysisParameters"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:FLSAnalysisParameters"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("latitude"):
            rep["latitude"] = self.latitude
        if self.is_set("longitude"):
            rep["longitude"] = self.longitude
        if self.is_set("hs_dp_proba"):
            if (short):
                rep["hs_dp_proba"] = str(self.hs_dp_proba.shape)
            else:
                try:
                    rep["hs_dp_proba"] = self.hs_dp_proba.tolist()
                except:
                    rep["hs_dp_proba"] = self.hs_dp_proba
        if self.is_set("intervals_hs_min"):
            if (short):
                rep["intervals_hs_min"] = str(self.intervals_hs_min.shape)
            else:
                try:
                    rep["intervals_hs_min"] = self.intervals_hs_min.tolist()
                except:
                    rep["intervals_hs_min"] = self.intervals_hs_min
        if self.is_set("intervals_hs_max"):
            if (short):
                rep["intervals_hs_max"] = str(self.intervals_hs_max.shape)
            else:
                try:
                    rep["intervals_hs_max"] = self.intervals_hs_max.tolist()
                except:
                    rep["intervals_hs_max"] = self.intervals_hs_max
        if self.is_set("intervals_dp_min"):
            if (short):
                rep["intervals_dp_min"] = str(self.intervals_dp_min.shape)
            else:
                try:
                    rep["intervals_dp_min"] = self.intervals_dp_min.tolist()
                except:
                    rep["intervals_dp_min"] = self.intervals_dp_min
        if self.is_set("intervals_dp_max"):
            if (short):
                rep["intervals_dp_max"] = str(self.intervals_dp_max.shape)
            else:
                try:
                    rep["intervals_dp_max"] = self.intervals_dp_max.tolist()
                except:
                    rep["intervals_dp_max"] = self.intervals_dp_max
        if self.is_set("associated_tp"):
            if (short):
                rep["associated_tp"] = str(self.associated_tp.shape)
            else:
                try:
                    rep["associated_tp"] = self.associated_tp.tolist()
                except:
                    rep["associated_tp"] = self.associated_tp
        if self.is_set("associated_cur"):
            if (short):
                rep["associated_cur"] = str(self.associated_cur.shape)
            else:
                try:
                    rep["associated_cur"] = self.associated_cur.tolist()
                except:
                    rep["associated_cur"] = self.associated_cur
        if self.is_set("associated_dircur"):
            if (short):
                rep["associated_dircur"] = str(self.associated_dircur.shape)
            else:
                try:
                    rep["associated_dircur"] = self.associated_dircur.tolist()
                except:
                    rep["associated_dircur"] = self.associated_dircur
        if self.is_set("associated_wind"):
            if (short):
                rep["associated_wind"] = str(self.associated_wind.shape)
            else:
                try:
                    rep["associated_wind"] = self.associated_wind.tolist()
                except:
                    rep["associated_wind"] = self.associated_wind
        if self.is_set("associated_dirwind"):
            if (short):
                rep["associated_dirwind"] = str(self.associated_dirwind.shape)
            else:
                try:
                    rep["associated_dirwind"] = self.associated_dirwind.tolist()
                except:
                    rep["associated_dirwind"] = self.associated_dirwind
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["latitude"] = np.array([self.latitude],dtype=float)
        handle["longitude"] = np.array([self.longitude],dtype=float)
        handle["hs_dp_proba"] = np.array(self.hs_dp_proba,dtype=float)
        handle["intervals_hs_min"] = np.array(self.intervals_hs_min,dtype=float)
        handle["intervals_hs_max"] = np.array(self.intervals_hs_max,dtype=float)
        handle["intervals_dp_min"] = np.array(self.intervals_dp_min,dtype=float)
        handle["intervals_dp_max"] = np.array(self.intervals_dp_max,dtype=float)
        handle["associated_tp"] = np.array(self.associated_tp,dtype=float)
        handle["associated_cur"] = np.array(self.associated_cur,dtype=float)
        handle["associated_dircur"] = np.array(self.associated_dircur,dtype=float)
        handle["associated_wind"] = np.array(self.associated_wind,dtype=float)
        handle["associated_dirwind"] = np.array(self.associated_dirwind,dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "latitude"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "longitude"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "hs_dp_proba"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "intervals_hs_min"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "intervals_hs_max"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "intervals_dp_min"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "intervals_dp_max"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "associated_tp"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "associated_cur"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "associated_dircur"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "associated_wind"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "associated_dirwind"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("latitude" in list(gr.keys())):
            self.latitude = gr["latitude"][0]
        if ("longitude" in list(gr.keys())):
            self.longitude = gr["longitude"][0]
        if ("hs_dp_proba" in list(gr.keys())):
            self.hs_dp_proba = gr["hs_dp_proba"][:]
        if ("intervals_hs_min" in list(gr.keys())):
            self.intervals_hs_min = gr["intervals_hs_min"][:]
        if ("intervals_hs_max" in list(gr.keys())):
            self.intervals_hs_max = gr["intervals_hs_max"][:]
        if ("intervals_dp_min" in list(gr.keys())):
            self.intervals_dp_min = gr["intervals_dp_min"][:]
        if ("intervals_dp_max" in list(gr.keys())):
            self.intervals_dp_max = gr["intervals_dp_max"][:]
        if ("associated_tp" in list(gr.keys())):
            self.associated_tp = gr["associated_tp"][:]
        if ("associated_cur" in list(gr.keys())):
            self.associated_cur = gr["associated_cur"][:]
        if ("associated_dircur" in list(gr.keys())):
            self.associated_dircur = gr["associated_dircur"][:]
        if ("associated_wind" in list(gr.keys())):
            self.associated_wind = gr["associated_wind"][:]
        if ("associated_dirwind" in list(gr.keys())):
            self.associated_dirwind = gr["associated_dirwind"][:]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
