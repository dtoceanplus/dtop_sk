# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from . import Cplx
from ..catalogue import Lines
from ..catalogue import AnchorCatalogue
from . import URL
#------------------------------------
# @ USER DEFINED IMPORTS START
import scipy
import utm
import copy
from . import SubstationProperties
from .SteadyForceModel import SteadyForceModel
from ..frequency_analysis_module import from_reduced_dof_to_dof
from array import array
# @ USER DEFINED IMPORTS END
#------------------------------------

class Inputs():

    """data model representing input data and catalogue
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._sk_inputs=Cplx.Cplx()
        self._sk_inputs.description = 'All input data, except catalogues, necessary to run SK module'
        self._catalogue_lines=Lines.Lines()
        self._catalogue_lines.description = 'Catalogue containing all the mooring lines data'
        self._catalogue_anchors=AnchorCatalogue.AnchorCatalogue()
        self._catalogue_anchors.description = 'Catalogue containing all drag anchor data'
        self._run_mode='standalone'
        self._url_ec_get_farm=URL.URL()
        self._url_ec_get_farm.description = 'url calling EC module for reading farm layout : /ec/{ecId}/farm, receiving dataformat Farm.yaml'
        self._url_ec_get_farm_inputs=URL.URL()
        self._url_ec_get_farm_inputs.description = 'url calling EC module for reading farm inputs : /ec/{ecId}/inputs/farm, receiving dataformat FarmInput.yaml'
        self._url_mc_get_single_machine_hydrodynamic=URL.URL()
        self._url_mc_get_single_machine_hydrodynamic.description = 'url calling MC module for reading floater hydrodynamics : /mc/{mcId}/single_machine_hydrodynamic, receiving dataformat mc_hydrodynamic_model.yaml'
        self._url_mc_get_wec_cplx1=URL.URL()
        self._url_mc_get_wec_cplx1.description = 'url calling MC module for reading wec cplx1 properties : /mc/{mcId}/model/wec/complexity1, receiving dataformat ModellingWecCplx1.yaml'
        self._url_mc_get_wec_cplx2=URL.URL()
        self._url_mc_get_wec_cplx2.description = 'url calling MC module for reading wec cplx2 properties : /mc/{mcId}/model/wec/complexity2, receiving dataformat ModellingWecCplx2.yaml'
        self._url_mc_get_wec_cplx3=URL.URL()
        self._url_mc_get_wec_cplx3.description = 'url calling MC module for reading wec cplx3 properties : /mc/{mcId}/model/wec/complexity3, receiving dataformat ModellingWecCplx3.yaml'
        self._url_mc_get_tec_cplx1=URL.URL()
        self._url_mc_get_tec_cplx1.description = 'url calling MC module for reading tec cplx1 properties : /mc/{mcId}/model/wec/complexity1, receiving dataformat ModellingTecCplx1.yaml'
        self._url_mc_get_tec_cplx2=URL.URL()
        self._url_mc_get_tec_cplx2.description = 'url calling MC module for reading tec cplx2 properties : /mc/{mcId}/model/wec/complexity2, receiving dataformat ModellingTecCplx2.yaml'
        self._url_mc_get_tec_cplx3=URL.URL()
        self._url_mc_get_tec_cplx3.description = 'url calling MC module for reading tec cplx3 properties : /mc/{mcId}/model/wec/complexity3, receiving dataformat ModellingTecCplx3.yaml'
        self._url_mc_get_general=URL.URL()
        self._url_mc_get_general.description = 'url calling MC module for general information : /mc/{mcId}/general, receiving dataformat General.yaml'
        self._url_mc_get_dimensions=URL.URL()
        self._url_mc_get_dimensions.description = 'url calling MC module for main dimensions of the machine : /mc/{mcId}/dimensions, receiving dataformat Dimensions.yaml'
        self._url_mc_get_machine=URL.URL()
        self._url_mc_get_machine.description = 'url calling MC module for mc project information : /mc/{mcId}, receiving dataformat Machine.yaml'
        self._url_sc_get_device_statistics=URL.URL()
        self._url_sc_get_device_statistics.description = 'url calling SC module for weather statistics : /sc/{ProjectId}/point/statistics, receiving dataformat dtosc_devices_outputs_Statistics.yaml'
        self._url_sc_get_farm_info=URL.URL()
        self._url_sc_get_farm_info.description = 'url calling SC module for farm location UTM : /sc/{ProjectId}/farm/info, receiving dataformat dtosc_farm_outputs_FarmInfo.yaml'
        self._url_sc_get_farm_bathymetry=URL.URL()
        self._url_sc_get_farm_bathymetry.description = 'url calling SC module for bathymetry grid : /sc/{ProjectId}/farm/direct_values/bathymetry, receiving dataformat dtosc_farm_outputs_FarmBathymetry.yaml'
        self._url_sc_get_farm_seabed_type=URL.URL()
        self._url_sc_get_farm_seabed_type.description = 'url calling SC module for seabed type grid : /sc/{ProjectId}/farm/direct_values/seabed_type, receiving dataformat dtosc_farm_outputs_FarmSeabedType.yaml'
        self._url_ed_get_study_results=URL.URL()
        self._url_ed_get_study_results.description = 'url calling ED module for substation properties : /api/energy-deliv-studies/{edId}/results, receiving dataformat EnergyDelivOutputResults.yaml'
        self._url_cm_drag_anchor=URL.URL()
        self._url_cm_drag_anchor.description = 'url calling catalog CM module for drag anchor properties : /api/drag_anchor'
        self._url_cm_mooring_line=URL.URL()
        self._url_cm_mooring_line.description = 'url calling catalog CM module for mooring lines properties : /api/mooring_line'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def populate(self):

        if self.run_mode=='dto':

            ##################
            # Catalog module #
            ##################

            # Populate data from cm_mooring_line
            print('Reading lines data from catalogue module ...')
            self.populate_from_url_cm_mooring_line()

            # Populate data from cm_drag_anchor
            print('Reading anchors data from catalogue module ...')
            self.populate_from_url_cm_drag_anchor()

            #############
            # EC module #
            #############

            # Populate data from ec_get_farm
            print('Reading ec farm data from ec module ...')
            self.populate_from_url_ec_get_farm()

            # Populate data from ec_get_farm
            print('Reading ec farm data inputs from ec module ...')
            self.populate_from_url_ec_get_farm_inputs()

            #############
            # MC module #
            #############

            # Populate data from mc_get_machine
            print('Reading machine data from mc module ...')
            [cplx, machine_type] = self.populate_from_url_mc_get_machine()
            print('Complexity level is ' + str(cplx))
            print('Machine type is ' +str(machine_type))

            # Populate data from mc_get_general
            print('Reading general data  from mc module ...')
            positioning_type = self.populate_from_url_mc_get_general()

            # Fetch data from mc_get_dimensions
            print('Reading dimensions data  from mc module ...')
            [characteristic_length, hub_height] = self.populate_from_url_mc_get_dimensions()

            if(characteristic_length != None):

                # Choose correct API to call
                if machine_type == 'tec':
                    self.sk_inputs.device_properties.rotor_diameter = characteristic_length
                    if cplx == 1:
                        print('Reading tec cplx1 data  from mc module ...')
                        self.populate_from_url_mc_get_tec_cplx1(positioning_type,hub_height)
                    elif cplx == 2:
                        print('Reading tec cplx2 data  from mc module ...')
                        self.populate_from_url_mc_get_tec_cplx2(positioning_type,hub_height)
                    elif cplx == 3:
                        print('Reading tec cplx3 data  from mc module ...')
                        self.populate_from_url_mc_get_tec_cplx3(positioning_type,hub_height)
                elif machine_type == 'wec':
                    if cplx == 3:
                        print('Reading wec cplx3 data  from mc module ...')
                        self.populate_from_url_mc_get_wec_cplx3()
                    pass

                if positioning_type == 'moored':
                    if cplx == 3:
                        print('Reading single machine hydrodynamics data  from mc module ...')
                        self.populate_from_url_mc_get_single_machine_hydrodynamic()

            #############
            # SC module #
            #############

            # Populate data from sc_get_device_statistics (uls environements, fls environements)
            print('Reading sc point data  from sc module ...')
            farm_lat_lon = self.populate_from_url_sc_get_device_statistics()

            # Populate data from sc_get_farm_direct_values
            print('Reading sc farm data  from sc module ...')
            self.populate_from_url_sc_get_farm(farm_lat_lon)

            #############
            # ED module #
            #############

            # Fetch maximum offset and substation properties
            print('Reading results data from ed module ...')
            self.populate_from_url_ed_get_study_results()

    def populate_from_url_ed_get_study_results(self):

        # Fetch data
        data = self.fetch_data_from_url(self.url_ed_get_study_results,'url_ed_get_study_results')

        if(len(data) != 0):

            # Fetch the maximum offset
            umbilical_dict = data.get("umbilical_dict")
            if(umbilical_dict == None):
                self.sk_inputs.device_properties.mooring_design_criteria.offset_max = 0
            else:
                self.sk_inputs.device_properties.mooring_design_criteria.offset_max = umbilical_dict[0].get("max_device_offset") # We fetch only the first one because the value is the same for all devices

            # Check if a substation is defined
            collection_point_dict = data.get("collection_point_dict")
            for idx in range(0,len(collection_point_dict)):

                self.sk_inputs.substation_is_present = True

                # Create input for new substation
                self.sk_inputs.substation_properties.append(SubstationProperties.SubstationProperties())

                # Fetch mass of the substation
                if(collection_point_dict[idx].get("mass") != None):
                    self.sk_inputs.substation_properties[idx].mass = collection_point_dict[idx].get("mass")
                else:
                    self.sk_inputs.substation_properties[idx].mass = 0

                # Fetch the position of the substation (UTM coordinates)
                if(collection_point_dict[idx].get("location") != None):
                    self.sk_inputs.substation_properties[idx].east = collection_point_dict[idx].get("location")[0]
                    self.sk_inputs.substation_properties[idx].north = collection_point_dict[idx].get("location")[1]
                else:
                    self.sk_inputs.substation_properties[idx].east = 0
                    self.sk_inputs.substation_properties[idx].north = 0

                try:
                    # Fetch the water depth
                    # Convert from UTM to lon/lat
                    [lat,lon] = utm.to_latlon(self.sk_inputs.substation_properties[idx].east, self.sk_inputs.substation_properties[idx].north, self.utm_number, self.utm_letter)
                    # Check that we are inside the boundaries
                    if not (lon<=max(self.longitude) and lon>=min(self.longitude)):
                        raise Exception('Error: the specified longitude ' + str(lon) + ' is outside the range for which the bathymetry is documented: [' +str(min(self.longitude)) +',' +str(max(self.longitude)) +']')
                    if not (lat<=max(self.latitude) and lat>=min(self.latitude)):
                        raise Exception('Error: the specified latitude ' + str(lat) + ' is outside the range for which the bathymetry is documented: [' +str(min(self.latitude)) +',' +str(max(self.latitude)) +']')
                    self.sk_inputs.substation_properties[idx].water_depth = self.f(lat,lon)
                except:
                    self.sk_inputs.substation_properties[idx].water_depth = 0

                # Fetch the dimensions of the substation (used to compute current and wind drag forces)
                self.sk_inputs.substation_properties[idx].steady_force_model=SteadyForceModel()
                self.sk_inputs.substation_properties[idx].steady_force_model.method='main_dimensions'
                self.sk_inputs.substation_properties[idx].steady_force_model.device_dry_profile='rectangle' # Default value, since we do not have more info from ED
                self.sk_inputs.substation_properties[idx].steady_force_model.device_wet_profile='cylinder' # Default value, since we do not have more info from ED

                CP_height_seabed = collection_point_dict[idx].get("CP_height_seabed")

                # Case where the substation completely submerged, underwater
                if((collection_point_dict[idx].get("length") != None)and(collection_point_dict[idx].get("width") != None)):
                    if CP_height_seabed == 0:
                        self.sk_inputs.substation_properties[idx].steady_force_model.device_dry_width=0.0
                        self.sk_inputs.substation_properties[idx].steady_force_model.device_wet_width=max(collection_point_dict[idx].get("length"),collection_point_dict[idx].get("width"))
                        self.sk_inputs.substation_properties[idx].steady_force_model.device_dry_height=0.0
                        self.sk_inputs.substation_properties[idx].steady_force_model.device_wet_height=collection_point_dict[idx].get("height")
                    else:
                        self.sk_inputs.substation_properties[idx].steady_force_model.device_dry_width=max(collection_point_dict[idx].get("length"),collection_point_dict[idx].get("width"))
                        self.sk_inputs.substation_properties[idx].steady_force_model.device_wet_width=0.0
                        self.sk_inputs.substation_properties[idx].steady_force_model.device_dry_height=collection_point_dict[idx].get("height")
                        self.sk_inputs.substation_properties[idx].steady_force_model.device_wet_height=0.0
                else:
                    self.sk_inputs.substation_properties[idx].steady_force_model.device_dry_width = 0
                    self.sk_inputs.substation_properties[idx].steady_force_model.device_wet_width = 0
                    self.sk_inputs.substation_properties[idx].steady_force_model.device_dry_height = 0
                    self.sk_inputs.substation_properties[idx].steady_force_model.device_wet_height = 0

    def populate_from_url_sc_get_device_statistics(self):

        # Fetch data
        try:
            try:
                self.url_sc_get_device_statistics = self.url_sc_get_device_statistics["data"]
            except:
                pass
            data = self.fetch_data_from_url(self.url_sc_get_device_statistics,'url_sc_get_device_statistics')

            if(len(data) != 0):
                # Populate data for ULS analysis

                # Read the farm position lat/lon
                farm_lat_lon = np.zeros(shape=(2))
                farm_lat_lon[0] = data["waves"]["EXC"]["hs_tp"]["latitude"]
                farm_lat_lon[1] = data["waves"]["EXC"]["hs_tp"]["longitude"]

                # 100-years Hs/Tp contour
                return_periods = data["waves"]["EXC"]["hs_tp"]["return_periods"]
                ind=None
                for idx in range(0,len(return_periods)):
                    if return_periods[idx]==100.0:
                        ind=idx
                if ind is None:
                    raise Exception('Error: waves hs-tp contour of 100-years return period is missing')
                hs_list = data["waves"]["EXC"]["hs_tp"]["return_values1"][:][ind]
                tp_list = data["waves"]["EXC"]["hs_tp"]["return_values2"][:][ind]
                self.sk_inputs.uls_analysis_parameters.hs_array = hs_list
                self.sk_inputs.uls_analysis_parameters.tp_array = tp_list

                # 10-years current
                return_periods = data["currents"]["EXT"]["mag"]["return_periods"]
                ind=None
                for idx in range(0,len(return_periods)):
                    if return_periods[idx]==10.0:
                        ind=idx
                if ind is None:
                    raise Exception('Error: current of 10-years return period is missing')
                current_vel = data["currents"]["EXT"]["mag"]["return_values"][ind]
                self.sk_inputs.uls_analysis_parameters.current_vel = current_vel

                # 100-years wind
                return_periods = data["winds"]["EXT"]["gust10"]["return_periods"]
                ind=None
                for idx in range(0,len(return_periods)):
                    if return_periods[idx]==100.0:
                        ind=idx
                if ind is None:
                    raise Exception('Error: wind of 100-years return period is missing')
                wind_gust_vel = data["winds"]["EXT"]["gust10"]["return_values"][ind]
                self.sk_inputs.uls_analysis_parameters.wind_vel = wind_gust_vel

                # Populate data for FLS analysis
                self.sk_inputs.fls_analysis_parameters.loadFromJSONDict(data["waves"]["ENVS"])

                return farm_lat_lon
            else:
                return None
        except:
            return None

    def populate_from_url_sc_get_farm(self,farm_lat_lon):

        # Fetch data
        try:
            self.url_sc_get_farm_info = self.url_sc_get_farm_info["data"]
            self.url_sc_get_farm_bathymetry = self.url_sc_get_farm_bathymetry["data"]
            self.url_sc_get_farm_seabed_type = self.url_sc_get_farm_seabed_type["data"]
        except:
            pass
        data = self.fetch_data_from_url(self.url_sc_get_farm_info,'url_sc_get_farm_info')
        data2 = self.fetch_data_from_url(self.url_sc_get_farm_bathymetry,'url_sc_get_farm_bathymetry')
        data3 = self.fetch_data_from_url(self.url_sc_get_farm_seabed_type,'url_sc_get_farm_seabed_type')

        if(len(data) != 0):
            # Read the UTM zone
            self.utm_number = data["zonenumber"]
            self.utm_letter = data["zoneletter"]

            # Populate bathymetry data
            self.longitude = data2["longitude"]
            self.latitude = data2["latitude"]
            self.wd = data2["value"]
            self.f = scipy.interpolate.interp2d(self.latitude, self.longitude, self.wd, kind='linear')
            self.sk_inputs.water_depth = np.zeros(shape=(len(self.sk_inputs.deviceId)))
            for idx in range(0,len(self.sk_inputs.deviceId)):
                try:
                    # Convert from UTM to lon/lat
                    [lat,lon] = utm.to_latlon(self.sk_inputs.east[idx], self.sk_inputs.north[idx], self.utm_number, self.utm_letter)

                    # Check that we are inside the boundaries
                    if not (lon<=max(self.longitude) and lon>=min(self.longitude)):
                        raise Exception('Error: the specified longitude ' + str(lon) + ' is outside the range for which the bathymetry is documented: [' +str(min(self.longitude)) +',' +str(max(self.longitude)) +']')
                    if not (lat<=max(self.latitude) and lat>=min(self.latitude)):
                        raise Exception('Error: the specified latitude ' + str(lat) + ' is outside the range for which the bathymetry is documented: [' +str(min(self.latitude)) +',' +str(max(self.latitude)) +']')
                    self.sk_inputs.water_depth[idx] = self.f(lat,lon)
                except:
                    self.sk_inputs.water_depth[idx] = 0

            # Read the center of the farm
            [self.sk_inputs.farm_east,self.sk_inputs.farm_north,dum1,dum2] = utm.from_latlon(farm_lat_lon[0],farm_lat_lon[1])

            self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_value = []
            self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_name = []

            seabed_type_list = data3['value']
            if 'Rocks_Gravels_Pebbles' in seabed_type_list:
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_value.append('gravels_pebbles')
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_name.append('Rock/Gravels/Pebbles')
            if 'Very dense sand' in seabed_type_list:
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_value.append('very_dense_sand')
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_name.append('Very Dense Sand')
            if 'Dense sand' in seabed_type_list:
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_value.append('dense_sand')
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_name.append('Dense Sand')
            if 'Medium dense sand' in seabed_type_list:
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_value.append('medium_dense_sand')
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_name.append('Medium Dense Sand')
            if 'Loose sand' in seabed_type_list:
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_value.append('loose_sand')
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_name.append('Loose Sand')
            if 'Very loose sand' in seabed_type_list:
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_value.append('very_loose_sand')
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_name.append('Very Loose Sand')
            if 'Hard clay' in seabed_type_list:
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_value.append('hard_clay')
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_name.append('Hard Clay')
            if 'Very stiff clay' in seabed_type_list:
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_value.append('very_stiff_clay')
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_name.append('Very Stiff Clay')
            if 'Stiff clay' in seabed_type_list:
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_value.append('stiff_clay')
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_name.append('Stiff Clay')
            if 'Firm clay' in seabed_type_list:
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_value.append('firm_clay')
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_name.append('Firm Clay')
            if 'Soft clay' in seabed_type_list:
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_value.append('soft_clay')
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_name.append('Soft Clay')
            if 'Very soft clay' in seabed_type_list:
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_value.append('very_soft_clay')
                self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_name.append('Very Soft Clay')

            self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_value.append('custom')
            self.sk_inputs.device_properties.foundation_design_parameters.soil_type_SC_name.append('custom')

            pass


    def populate_from_url_mc_get_wec_cplx3(self):

        # Fetch data
        data = self.fetch_data_from_url(self.url_mc_get_wec_cplx3,'url_mc_get_wec_cplx3')

        if(len(data) != 0):
            self.sk_inputs.device_properties.hdb_source = 'mc_module'

            # Populate
            self.sk_inputs.device_properties.hydro_data_MC_module.tp_capture_width = data.get('tp_capture_width')
            self.sk_inputs.device_properties.hydro_data_MC_module.hs_capture_width = data.get('hs_capture_width')
            self.sk_inputs.device_properties.hydro_data_MC_module.wave_angle_capture_width = data.get('wave_angle_capture_width') #already in [rad]
            self.sk_inputs.device_properties.hydro_data_MC_module.pto_damping = data.get('pto_damping')
            self.sk_inputs.device_properties.hydro_data_MC_module.mooring_stiffness = data.get('mooring_stiffness')
            self.sk_inputs.device_properties.hydro_data_MC_module.additional_damping = data.get('additional_damping')
            self.sk_inputs.device_properties.hydro_data_MC_module.additional_stiffness = data.get('additional_stiffness')
            self.sk_inputs.device_properties.hydro_data_MC_module.shared_dof = data.get('shared_dof')
            self.sk_inputs.device_properties.cog = data.get('bodies')[0].get('cog')

    def populate_from_url_mc_get_tec_cplx1(self,positioning_type,hub_height):

        # Fetch data
        data = self.fetch_data_from_url(self.url_mc_get_tec_cplx1,'url_mc_get_tec_cplx1')

        if(len(data) != 0):
            # Thrust coefficient
            self.sk_inputs.device_properties.thrust_curve_v = np.array([0,1000])
            self.sk_inputs.device_properties.thrust_curve_ct = np.array([1.0,1.0]) # Use 1.0 as default value, since we don't have more info at cplx1

            # Rotor position
            number_rotor = data.get('number_rotor')
            self.sk_inputs.device_properties.hub_position=np.zeros(shape=(3,number_rotor), dtype=float)
            if(hub_height != None):
                if positioning_type == 'fixed':
                    self.sk_inputs.device_properties.hub_position[2,:] = hub_height
                elif positioning_type == 'moored':
                    self.sk_inputs.device_properties.hub_position[2,:] = -hub_height

    def populate_from_url_mc_get_tec_cplx2(self,positioning_type,hub_height):

        # Fetch data
        data = self.fetch_data_from_url(self.url_mc_get_tec_cplx2,'url_mc_get_tec_cplx2')

        if(len(data) != 0):
            # Thrust coefficient
            ct = data.get('ct')
            self.sk_inputs.device_properties.thrust_curve_v = np.array([0,1000])
            self.sk_inputs.device_properties.thrust_curve_ct = np.array([ct,ct]) # Use constant value, since we dont have more info in cplx2

            # Rotor position
            number_rotor = data.get('number_rotor')
            interd = data.get('rotor_interdistance')
            L = (number_rotor-1)*interd
            self.sk_inputs.device_properties.hub_position=np.zeros(shape=(3,number_rotor), dtype=float)
            if(hub_height != None):
                if positioning_type == 'fixed':
                    self.sk_inputs.device_properties.hub_position[2,:] = hub_height
                elif positioning_type == 'moored':
                    self.sk_inputs.device_properties.hub_position[2,:] = -hub_height
                for idrot in range(0,number_rotor):
                    self.sk_inputs.device_properties.hub_position[0,idrot] = -L/2 + interd*idrot

    def populate_from_url_mc_get_tec_cplx3(self,positioning_type,hub_height):

        # Fetch data
        data = self.fetch_data_from_url(self.url_mc_get_tec_cplx3,'url_mc_get_tec_cplx3')

        if(len(data) != 0):
            # Thrust coefficient
            ct = data.get('ct')
            cp_ct_velocity = data.get('cp_ct_velocity')
            self.sk_inputs.device_properties.thrust_curve_v = np.array(cp_ct_velocity)
            self.sk_inputs.device_properties.thrust_curve_ct = np.array(ct)

            # Rotor position
            number_rotor = data.get('number_rotor')
            interd = data.get('rotor_interdistance')
            L = (number_rotor-1)*interd
            self.sk_inputs.device_properties.hub_position=np.zeros(shape=(3,number_rotor), dtype=float)
            if(hub_height != None):
                if positioning_type == 'fixed':
                    self.sk_inputs.device_properties.hub_position[2,:] = hub_height
                elif positioning_type == 'moored':
                    self.sk_inputs.device_properties.hub_position[2,:] = -hub_height
                for idrot in range(0,number_rotor):
                    self.sk_inputs.device_properties.hub_position[0,idrot] = -L/2 + interd*idrot

    def populate_from_url_mc_get_single_machine_hydrodynamic(self):

        # Fetch data
        data = self.fetch_data_from_url(self.url_mc_get_single_machine_hydrodynamic,'url_mc_get_single_machine_hydrodynamic')

        if(len(data) != 0):
            # Populate mass matrix (generalized dofs)
            self.sk_inputs.device_properties.mass_matrix = np.array(data.get('mass_matrix'))

            # Populate hydrostatic stiffness to be used in RAO computation (generalized dofs)
            self.sk_inputs.device_properties.hydro_data_MC_module.hydrostatic_matrix = np.array(data.get('hydrostatic_stiffness'))

            # Populate hydrostatic stiffness matrix to be used in static calculation
            K_gendof = np.array(data.get('hydrostatic_stiffness'))

            self.sk_inputs.device_properties.hydrostatic_matrix = from_reduced_dof_to_dof(K_gendof,self.sk_inputs.device_properties.hydro_data_MC_module.shared_dof)

            # Populate HydroDataMCFormat
            self.sk_inputs.device_properties.hydro_data_MC_module.added_mass = np.array(data.get('added_mass'))
            self.sk_inputs.device_properties.hydro_data_MC_module.radiation_damping = np.array(data.get('radiation_damping'))
            self.sk_inputs.device_properties.hydro_data_MC_module.excitation_force_real = np.array(data.get('excitation_force_real'))
            self.sk_inputs.device_properties.hydro_data_MC_module.excitation_force_imag = np.array(data.get('excitation_force_imag'))
            self.sk_inputs.device_properties.hydro_data_MC_module.fitting_damping = np.array(data.get('fitting_damping'))
            self.sk_inputs.device_properties.hydro_data_MC_module.fitting_stiffness = np.array(data.get('fitting_stiffness'))
            self.sk_inputs.device_properties.hydro_data_MC_module.velocity_transformation_matrix = np.array(data.get('velocity_transformation_matrix'))
            self.sk_inputs.device_properties.hydro_data_MC_module.direction = np.pi - np.array(data.get('wave_direction')) # In SK: going-to convention. 0 to North, 90 to West. In MC: going-from convention, 0 from NOrth, 90 from East. Direction in [rad] already.
            self.sk_inputs.device_properties.hydro_data_MC_module.frequency = np.array(data.get('wave_frequency')) # omega, in [rad/s]

    def populate_from_url_mc_get_dimensions(self):

        # Fetch data
        dimensions = self.fetch_data_from_url(self.url_mc_get_dimensions,'url_mc_get_dimensions')

        if(len(dimensions) != 0):
            # Fetch data
            height = dimensions.get('height')
            #width = dimensions.get('width')
            draft = dimensions.get('draft')
            #length = dimensions.get('length')
            mass = dimensions.get('mass')
            submerged_volume = dimensions.get('submerged_volume')
            wet_frontal_area = dimensions.get('wet_frontal_area')
            dry_frontal_area = dimensions.get('dry_frontal_area')

            # Characteristic_length has a different meaning depending on the machine type, so we pass it as an output
            characteristic_length = dimensions.get('characteristic_dimension')

            # Populate geometry used for wind force model
            self.sk_inputs.device_properties.steady_force_model.device_dry_profile = dimensions.get('dry_profile')
            if dimensions.get('dry_profile') == None:
                self.sk_inputs.device_properties.steady_force_model.device_dry_profile = 'cylinder'
            self.sk_inputs.device_properties.steady_force_model.device_dry_height = height - draft
            self.sk_inputs.device_properties.steady_force_model.device_dry_width = dry_frontal_area/(height - draft)

            # Populate geometry used for current force model, and mean wave drift force model
            self.sk_inputs.device_properties.steady_force_model.device_wet_profile = dimensions.get('wet_profile')
            if dimensions.get('wet_profile') == None:
                self.sk_inputs.device_properties.steady_force_model.device_wet_profile = 'cylinder'
            self.sk_inputs.device_properties.steady_force_model.device_wet_height = draft
            if(draft!=0):
                self.sk_inputs.device_properties.steady_force_model.device_wet_width = wet_frontal_area/draft
            else:
                self.sk_inputs.device_properties.steady_force_model.device_wet_width = 0

            # Fetch hub height in case the machine is a tidal machine
            hub_height = dimensions.get('hub_height')

            # Populate mass
            self.sk_inputs.device_properties.mass = mass
            self.sk_inputs.device_properties.submerged_volume = submerged_volume

            return characteristic_length,hub_height
        else:
            return None,None

    def populate_from_url_mc_get_general(self):

        # Fetch data
        data = self.fetch_data_from_url(self.url_mc_get_general,'url_mc_get_general')

        if(len(data) != 0):

            # Preferred foundation type
            preferred_fundation_type = data.get('preferred_fundation_type')

            # Positioning type
            floating = data.get('floating')
            if floating:
                positioning_type = 'moored'
                if(preferred_fundation_type == 'gravity_based'):
                    preferred_fundation_type = 'gravity_anchor'
                elif(preferred_fundation_type == 'drag_embedded'):
                    preferred_fundation_type = 'drag_anchor'
                elif(preferred_fundation_type == 'suction_bucket'):
                    preferred_fundation_type = 'suction_caisson'
            else:
                positioning_type = 'fixed'
                if((preferred_fundation_type == 'drag_embedded')or(preferred_fundation_type == 'gravity')):
                    preferred_fundation_type = 'shallow'
            self.sk_inputs.device_properties.positioning_type = positioning_type
            self.sk_inputs.device_properties.foundation_design_parameters.foundation_preference = preferred_fundation_type

            return positioning_type
        else:
            return None

    def populate_from_url_mc_get_machine(self):

        # Fetch data
        data = self.fetch_data_from_url(self.url_mc_get_machine,'url_mc_get_machine')

        if(len(data) != 0):
            # Fetch and return complexity level used in MC module
            cplx = int(data.get('complexity'))

            # Machine type (tec or wec)
            machine_type = str(data.get('type')).lower()
            self.sk_inputs.device_properties.machine_type = machine_type

            return cplx,machine_type
        else:
            return None,None

    def populate_from_url_ec_get_farm(self):
        try:
            self.url_ec_get_farm = self.url_ec_get_farm["data"]
        except:
            pass

        # Fetch data
        data = self.fetch_data_from_url(self.url_ec_get_farm,'url_ec_get_farm')

        if(len(data) != 0):
            # Populate input structure with data from EC module
            self.sk_inputs.east=np.array(data.get("layout")['easting'], dtype=float)
            self.sk_inputs.north=np.array(data.get("layout")['northing'], dtype=float)
            self.sk_inputs.deviceId=np.array(data.get("layout")['deviceID'], dtype=int)

    def populate_from_url_ec_get_farm_inputs(self):
        try:
            self.url_ec_get_farm_inputs = self.url_ec_get_farm_inputs["data"]
        except:
            pass

        # Fetch data
        data = self.fetch_data_from_url(self.url_ec_get_farm_inputs,'url_ec_get_farm_inputs')

        if(len(data) != 0):
            # Populate input structure with data from EC module
            temp = np.array(data.get('orientation_angle'), dtype=float)
            if ((temp.shape[0] == 1)and(self.sk_inputs.deviceId.shape[0]!=1)):
                self.sk_inputs.yaw = np.zeros(self.sk_inputs.yaw.shape[0])
                for i in range(self.sk_inputs.deviceId.shape[0]):
                    self.sk_inputs.yaw[i] = temp[i]
            else:
                self.sk_inputs.yaw = np.array(data.get('orientation_angle'), dtype=float)

    def populate_from_url_cm_mooring_line(self):

        # Fetch data
        data = self.fetch_data_from_url(self.url_cm_mooring_line,'url_cm_mooring_line')
        if(len(data) != 0):
            data = data["data"]
            self.catalogue_lines.catalogue_id = []
            self.catalogue_lines.type = []
            self.catalogue_lines.material = []
            self.catalogue_lines.quality = []
            self.catalogue_lines.diameter = []
            self.catalogue_lines.weight_in_air = []
            self.catalogue_lines.weight_in_water = []
            self.catalogue_lines.mbl = []
            self.catalogue_lines.ea = []
            self.catalogue_lines.cost_per_meter = []
            self.catalogue_lines.ad = []
            self.catalogue_lines.m = []
            self.catalogue_lines.failure_rate_repair = []
            self.catalogue_lines.failure_rate_replacement = []
            for i in range(len(data)):
                data_temp = data[i]["attributes"]
                self.catalogue_lines.catalogue_id.append(data_temp["catalogue_id"])
                self.catalogue_lines.type.append(data_temp["type"])
                self.catalogue_lines.material.append(data_temp["material"])
                self.catalogue_lines.quality.append(data_temp["quality"])
                self.catalogue_lines.diameter.append(data_temp["diameter"])
                self.catalogue_lines.weight_in_air.append(data_temp["weight_in_air"])
                self.catalogue_lines.weight_in_water.append(data_temp["weight_in_water"])
                self.catalogue_lines.mbl.append(data_temp["mbl"])
                self.catalogue_lines.ea.append(data_temp["ea"])
                self.catalogue_lines.cost_per_meter.append(data_temp["cost_per_meter"])
                self.catalogue_lines.ad.append(data_temp["ad"])
                self.catalogue_lines.m.append(data_temp["m"])
                self.catalogue_lines.failure_rate_repair.append(data_temp["failure_rate_repair"])
                self.catalogue_lines.failure_rate_replacement.append(data_temp["failure_rate_replacement"])

    def populate_from_url_cm_drag_anchor(self):

        # Fetch data
        data = self.fetch_data_from_url(self.url_cm_drag_anchor,'url_cm_drag_anchor')
        if(len(data) != 0):
            data = data["data"]
            self.catalogue_anchors.m_stiff_soil_sand = []
            self.catalogue_anchors.b_stiff_soil_sand = []
            self.catalogue_anchors.catalogue_id = []
            self.catalogue_anchors.m_soft_soil = []
            self.catalogue_anchors.b_soft_soil = []
            self.catalogue_anchors.m_mid_soil = []
            self.catalogue_anchors.b_mid_soil = []
            self.catalogue_anchors.m_A = []
            self.catalogue_anchors.b_A = []
            self.catalogue_anchors.m_B = []
            self.catalogue_anchors.b_B = []
            self.catalogue_anchors.m_EF = []
            self.catalogue_anchors.b_EF = []
            self.catalogue_anchors.Rel_soft_soil = []
            self.catalogue_anchors.Rel_mid_soil = []
            self.catalogue_anchors.Rel_stiff_soil = []
            for i in range(len(data)):
                data_temp = data[i]["attributes"]
                self.catalogue_anchors.m_stiff_soil_sand.append(data_temp["m_stiff_soil_sand"])
                self.catalogue_anchors.b_stiff_soil_sand.append(data_temp["b_stiff_soil_sand"])
                self.catalogue_anchors.catalogue_id.append(data_temp["catalogue_id"])
                self.catalogue_anchors.m_soft_soil.append(data_temp["m_soft_soil"])
                self.catalogue_anchors.b_soft_soil.append(data_temp["b_soft_soil"])
                self.catalogue_anchors.m_mid_soil.append(data_temp["m_mid_soil"])
                self.catalogue_anchors.b_mid_soil.append(data_temp["b_mid_soil"])
                self.catalogue_anchors.m_A.append(data_temp["m_A"])
                self.catalogue_anchors.b_A.append(data_temp["b_A"])
                self.catalogue_anchors.m_B.append(data_temp["m_B"])
                self.catalogue_anchors.b_B.append(data_temp["b_B"])
                self.catalogue_anchors.m_EF.append(data_temp["m_EF"])
                self.catalogue_anchors.b_EF.append(data_temp["b_EF"])
                self.catalogue_anchors.Rel_soft_soil.append(data_temp["Rel_soft_soil"])
                self.catalogue_anchors.Rel_mid_soil.append(data_temp["Rel_mid_soil"])
                self.catalogue_anchors.Rel_stiff_soil.append(data_temp["Rel_stiff_soil"])

    def fetch_data_from_url(self,url,url_name):
        if isinstance(url,URL.URL):
            #data = call API with self.url_ec_get_farm.url
            data = dict()
            pass
        else:
            if isinstance(url,dict):
                data = url # We also accept the structure data instead of the URL. This is for testing purposes only.
            else:
                raise Exception('Error : ' + url_name + ' should be a dictionnary or a url')
        return data
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def sk_inputs(self): # pragma: no cover
        """:obj:`~.Cplx.Cplx`: All input data, except catalogues, necessary to run SK module
        """
        return self._sk_inputs
    #------------
    @ property
    def catalogue_lines(self): # pragma: no cover
        """:obj:`~.Lines.Lines`: Catalogue containing all the mooring lines data
        """
        return self._catalogue_lines
    #------------
    @ property
    def catalogue_anchors(self): # pragma: no cover
        """:obj:`~.AnchorCatalogue.AnchorCatalogue`: Catalogue containing all drag anchor data
        """
        return self._catalogue_anchors
    #------------
    @ property
    def run_mode(self): # pragma: no cover
        """str: standalone or dto. If dto, urls need to be provided.
        """
        return self._run_mode
    #------------
    @ property
    def url_ec_get_farm(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling EC module for reading farm layout : /ec/{ecId}/farm, receiving dataformat Farm.yaml
        """
        return self._url_ec_get_farm
    #------------
    @ property
    def url_ec_get_farm_inputs(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling EC module for reading farm inputs : /ec/{ecId}/inputs/farm, receiving dataformat FarmInput.yaml
        """
        return self._url_ec_get_farm_inputs
    #------------
    @ property
    def url_mc_get_single_machine_hydrodynamic(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling MC module for reading floater hydrodynamics : /mc/{mcId}/single_machine_hydrodynamic, receiving dataformat mc_hydrodynamic_model.yaml
        """
        return self._url_mc_get_single_machine_hydrodynamic
    #------------
    @ property
    def url_mc_get_wec_cplx1(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling MC module for reading wec cplx1 properties : /mc/{mcId}/model/wec/complexity1, receiving dataformat ModellingWecCplx1.yaml
        """
        return self._url_mc_get_wec_cplx1
    #------------
    @ property
    def url_mc_get_wec_cplx2(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling MC module for reading wec cplx2 properties : /mc/{mcId}/model/wec/complexity2, receiving dataformat ModellingWecCplx2.yaml
        """
        return self._url_mc_get_wec_cplx2
    #------------
    @ property
    def url_mc_get_wec_cplx3(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling MC module for reading wec cplx3 properties : /mc/{mcId}/model/wec/complexity3, receiving dataformat ModellingWecCplx3.yaml
        """
        return self._url_mc_get_wec_cplx3
    #------------
    @ property
    def url_mc_get_tec_cplx1(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling MC module for reading tec cplx1 properties : /mc/{mcId}/model/wec/complexity1, receiving dataformat ModellingTecCplx1.yaml
        """
        return self._url_mc_get_tec_cplx1
    #------------
    @ property
    def url_mc_get_tec_cplx2(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling MC module for reading tec cplx2 properties : /mc/{mcId}/model/wec/complexity2, receiving dataformat ModellingTecCplx2.yaml
        """
        return self._url_mc_get_tec_cplx2
    #------------
    @ property
    def url_mc_get_tec_cplx3(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling MC module for reading tec cplx3 properties : /mc/{mcId}/model/wec/complexity3, receiving dataformat ModellingTecCplx3.yaml
        """
        return self._url_mc_get_tec_cplx3
    #------------
    @ property
    def url_mc_get_general(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling MC module for general information : /mc/{mcId}/general, receiving dataformat General.yaml
        """
        return self._url_mc_get_general
    #------------
    @ property
    def url_mc_get_dimensions(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling MC module for main dimensions of the machine : /mc/{mcId}/dimensions, receiving dataformat Dimensions.yaml
        """
        return self._url_mc_get_dimensions
    #------------
    @ property
    def url_mc_get_machine(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling MC module for mc project information : /mc/{mcId}, receiving dataformat Machine.yaml
        """
        return self._url_mc_get_machine
    #------------
    @ property
    def url_sc_get_device_statistics(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling SC module for weather statistics : /sc/{ProjectId}/point/statistics, receiving dataformat dtosc_devices_outputs_Statistics.yaml
        """
        return self._url_sc_get_device_statistics
    #------------
    @ property
    def url_sc_get_farm_info(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling SC module for farm location UTM : /sc/{ProjectId}/farm/info, receiving dataformat dtosc_farm_outputs_FarmInfo.yaml
        """
        return self._url_sc_get_farm_info
    #------------
    @ property
    def url_sc_get_farm_bathymetry(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling SC module for bathymetry grid : /sc/{ProjectId}/farm/direct_values/bathymetry, receiving dataformat dtosc_farm_outputs_FarmBathymetry.yaml
        """
        return self._url_sc_get_farm_bathymetry
    #------------
    #------------
    @ property
    def url_sc_get_farm_seabed_type(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling SC module for seabed_type grid : /sc/{ProjectId}/farm/direct_values/seabed_type, receiving dataformat dtosc_farm_outputs_FarmSeabedType.yaml
        """
        return self._url_sc_get_farm_seabed_type
    #------------
    @ property
    def url_ed_get_study_results(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling ED module for substation properties : /api/energy-deliv-studies/{edId}/results, receiving dataformat EnergyDelivOutputResults.yaml
        """
        return self._url_ed_get_study_results
    #------------
    @ property
    def url_cm_drag_anchor(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling catalog CM module for drag anchor properties : /api/drag_anchor
        """
        return self._url_cm_drag_anchor
    #------------
    @ property
    def url_cm_mooring_line(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling catalog CM module for mooring lines properties : /api/mooring_line
        """
        return self._url_cm_mooring_line
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ sk_inputs.setter
    def sk_inputs(self,val): # pragma: no cover
        self._sk_inputs=val
    #------------
    @ catalogue_lines.setter
    def catalogue_lines(self,val): # pragma: no cover
        self._catalogue_lines=val
    #------------
    @ catalogue_anchors.setter
    def catalogue_anchors(self,val): # pragma: no cover
        self._catalogue_anchors=val
    #------------
    @ run_mode.setter
    def run_mode(self,val): # pragma: no cover
        self._run_mode=str(val)
    #------------
    @ url_ec_get_farm.setter
    def url_ec_get_farm(self,val): # pragma: no cover
        self._url_ec_get_farm=val
    #------------
    @ url_ec_get_farm_inputs.setter
    def url_ec_get_farm_inputs(self,val): # pragma: no cover
        self._url_ec_get_farm_inputs=val
    #------------
    @ url_mc_get_single_machine_hydrodynamic.setter
    def url_mc_get_single_machine_hydrodynamic(self,val): # pragma: no cover
        self._url_mc_get_single_machine_hydrodynamic=val
    #------------
    @ url_mc_get_wec_cplx1.setter
    def url_mc_get_wec_cplx1(self,val): # pragma: no cover
        self._url_mc_get_wec_cplx1=val
    #------------
    @ url_mc_get_wec_cplx2.setter
    def url_mc_get_wec_cplx2(self,val): # pragma: no cover
        self._url_mc_get_wec_cplx2=val
    #------------
    @ url_mc_get_wec_cplx3.setter
    def url_mc_get_wec_cplx3(self,val): # pragma: no cover
        self._url_mc_get_wec_cplx3=val
    #------------
    @ url_mc_get_tec_cplx1.setter
    def url_mc_get_tec_cplx1(self,val): # pragma: no cover
        self._url_mc_get_tec_cplx1=val
    #------------
    @ url_mc_get_tec_cplx2.setter
    def url_mc_get_tec_cplx2(self,val): # pragma: no cover
        self._url_mc_get_tec_cplx2=val
    #------------
    @ url_mc_get_tec_cplx3.setter
    def url_mc_get_tec_cplx3(self,val): # pragma: no cover
        self._url_mc_get_tec_cplx3=val
    #------------
    @ url_mc_get_general.setter
    def url_mc_get_general(self,val): # pragma: no cover
        self._url_mc_get_general=val
    #------------
    @ url_mc_get_dimensions.setter
    def url_mc_get_dimensions(self,val): # pragma: no cover
        self._url_mc_get_dimensions=val
    #------------
    @ url_mc_get_machine.setter
    def url_mc_get_machine(self,val): # pragma: no cover
        self._url_mc_get_machine=val
    #------------
    @ url_sc_get_device_statistics.setter
    def url_sc_get_device_statistics(self,val): # pragma: no cover
        self._url_sc_get_device_statistics=val
    #------------
    @ url_sc_get_farm_info.setter
    def url_sc_get_farm_info(self,val): # pragma: no cover
        self._url_sc_get_farm_info=val
    #------------
    @ url_sc_get_farm_bathymetry.setter
    def url_sc_get_farm_bathymetry(self,val): # pragma: no cover
        self._url_sc_get_farm_bathymetry=val
    #------------
    @ url_sc_get_farm_seabed_type.setter
    def url_sc_get_farm_seabed_type(self,val): # pragma: no cover
        self._url_sc_get_farm_seabed_type=val
    #------------
    @ url_ed_get_study_results.setter
    def url_ed_get_study_results(self,val): # pragma: no cover
        self._url_ed_get_study_results=val
    #------------
    @ url_cm_drag_anchor.setter
    def url_cm_drag_anchor(self,val): # pragma: no cover
        self._url_cm_drag_anchor=val
    #------------
    @ url_cm_mooring_line.setter
    def url_cm_mooring_line(self,val): # pragma: no cover
        self._url_cm_mooring_line=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:Inputs"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:Inputs"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("sk_inputs"):
            if (short and not(deep)):
                rep["sk_inputs"] = self.sk_inputs.type_rep()
            else:
                rep["sk_inputs"] = self.sk_inputs.prop_rep(short, deep)
        if self.is_set("catalogue_lines"):
            if (short and not(deep)):
                rep["catalogue_lines"] = self.catalogue_lines.type_rep()
            else:
                rep["catalogue_lines"] = self.catalogue_lines.prop_rep(short, deep)
        if self.is_set("catalogue_anchors"):
            if (short and not(deep)):
                rep["catalogue_anchors"] = self.catalogue_anchors.type_rep()
            else:
                rep["catalogue_anchors"] = self.catalogue_anchors.prop_rep(short, deep)
        if self.is_set("run_mode"):
            rep["run_mode"] = self.run_mode
        if self.is_set("url_ec_get_farm"):
            if isinstance(self.url_ec_get_farm,dict):
                if (short and not(deep)):
                    rep["url_ec_get_farm"] = "dict"
                else:
                    rep["url_ec_get_farm"] = self.url_ec_get_farm
            else:
                if (short and not(deep)):
                    rep["url_ec_get_farm"] = self.url_ec_get_farm.type_rep()
                else:
                    rep["url_ec_get_farm"] = self.url_ec_get_farm.prop_rep(short, deep)
        if self.is_set("url_ec_get_farm_inputs"):
            if isinstance(self.url_ec_get_farm_inputs,dict):
                if (short and not(deep)):
                    rep["url_ec_get_farm_inputs"] = "dict"
                else:
                    rep["url_ec_get_farm_inputs"] = self.url_ec_get_farm_inputs
            else:
                if (short and not(deep)):
                    rep["url_ec_get_farm_inputs"] = self.url_ec_get_farm_inputs.type_rep()
                else:
                    rep["url_ec_get_farm_inputs"] = self.url_ec_get_farm_inputs.prop_rep(short, deep)
        if self.is_set("url_mc_get_single_machine_hydrodynamic"):
            if isinstance(self.url_mc_get_single_machine_hydrodynamic,dict):
                if (short and not(deep)):
                    rep["url_mc_get_single_machine_hydrodynamic"] = "dict"
                else:
                    rep["url_mc_get_single_machine_hydrodynamic"] = self.url_mc_get_single_machine_hydrodynamic
            else:
                if (short and not(deep)):
                    rep["url_mc_get_single_machine_hydrodynamic"] = self.url_mc_get_single_machine_hydrodynamic.type_rep()
                else:
                    rep["url_mc_get_single_machine_hydrodynamic"] = self.url_mc_get_single_machine_hydrodynamic.prop_rep(short, deep)
        if self.is_set("url_mc_get_wec_cplx1"):
            if isinstance(self.url_mc_get_wec_cplx1,dict):
                if (short and not(deep)):
                    rep["url_mc_get_wec_cplx1"] = "dict"
                else:
                    rep["url_mc_get_wec_cplx1"] = self.url_mc_get_wec_cplx1
            else:
                if (short and not(deep)):
                    rep["url_mc_get_wec_cplx1"] = self.url_mc_get_wec_cplx1.type_rep()
                else:
                    rep["url_mc_get_wec_cplx1"] = self.url_mc_get_wec_cplx1.prop_rep(short, deep)
        if self.is_set("url_mc_get_wec_cplx2"):
            if isinstance(self.url_mc_get_wec_cplx2,dict):
                if (short and not(deep)):
                    rep["url_mc_get_wec_cplx2"] = "dict"
                else:
                    rep["url_mc_get_wec_cplx2"] = self.url_mc_get_wec_cplx2
            else:
                if (short and not(deep)):
                    rep["url_mc_get_wec_cplx2"] = self.url_mc_get_wec_cplx2.type_rep()
                else:
                    rep["url_mc_get_wec_cplx2"] = self.url_mc_get_wec_cplx2.prop_rep(short, deep)
        if self.is_set("url_mc_get_wec_cplx3"):
            if isinstance(self.url_mc_get_wec_cplx3,dict):
                if (short and not(deep)):
                    rep["url_mc_get_wec_cplx3"] = "dict"
                else:
                    rep["url_mc_get_wec_cplx3"] = self.url_mc_get_wec_cplx3
            else:
                if (short and not(deep)):
                    rep["url_mc_get_wec_cplx3"] = self.url_mc_get_wec_cplx3.type_rep()
                else:
                    rep["url_mc_get_wec_cplx3"] = self.url_mc_get_wec_cplx3.prop_rep(short, deep)
        if self.is_set("url_mc_get_tec_cplx1"):
            if isinstance(self.url_mc_get_tec_cplx1,dict):
                if (short and not(deep)):
                    rep["url_mc_get_tec_cplx1"] = "dict"
                else:
                    rep["url_mc_get_tec_cplx1"] = self.url_mc_get_tec_cplx1
            else:
                if (short and not(deep)):
                    rep["url_mc_get_tec_cplx1"] = self.url_mc_get_tec_cplx1.type_rep()
                else:
                    rep["url_mc_get_tec_cplx1"] = self.url_mc_get_tec_cplx1.prop_rep(short, deep)
        if self.is_set("url_mc_get_tec_cplx2"):
            if isinstance(self.url_mc_get_tec_cplx2,dict):
                if (short and not(deep)):
                    rep["url_mc_get_tec_cplx2"] = "dict"
                else:
                    rep["url_mc_get_tec_cplx2"] = self.url_mc_get_tec_cplx2
            else:
                if (short and not(deep)):
                    rep["url_mc_get_tec_cplx2"] = self.url_mc_get_tec_cplx2.type_rep()
                else:
                    rep["url_mc_get_tec_cplx2"] = self.url_mc_get_tec_cplx2.prop_rep(short, deep)
        if self.is_set("url_mc_get_tec_cplx3"):
            if isinstance(self.url_mc_get_tec_cplx3,dict):
                if (short and not(deep)):
                    rep["url_mc_get_tec_cplx3"] = "dict"
                else:
                    rep["url_mc_get_tec_cplx3"] = self.url_mc_get_tec_cplx3
            else:
                if (short and not(deep)):
                    rep["url_mc_get_tec_cplx3"] = self.url_mc_get_tec_cplx3.type_rep()
                else:
                    rep["url_mc_get_tec_cplx3"] = self.url_mc_get_tec_cplx3.prop_rep(short, deep)
        if self.is_set("url_mc_get_general"):
            if isinstance(self.url_mc_get_general,dict):
                if (short and not(deep)):
                    rep["url_mc_get_general"] = "dict"
                else:
                    rep["url_mc_get_general"] = self.url_mc_get_general
            else:
                if (short and not(deep)):
                    rep["url_mc_get_general"] = self.url_mc_get_general.type_rep()
                else:
                    rep["url_mc_get_general"] = self.url_mc_get_general.prop_rep(short, deep)
        if self.is_set("url_mc_get_dimensions"):
            if isinstance(self.url_mc_get_dimensions,dict):
                if (short and not(deep)):
                    rep["url_mc_get_dimensions"] = "dict"
                else:
                    rep["url_mc_get_dimensions"] = self.url_mc_get_dimensions
            else:
                if (short and not(deep)):
                    rep["url_mc_get_dimensions"] = self.url_mc_get_dimensions.type_rep()
                else:
                    rep["url_mc_get_dimensions"] = self.url_mc_get_dimensions.prop_rep(short, deep)
        if self.is_set("url_mc_get_machine"):
            if isinstance(self.url_mc_get_machine,dict):
                if (short and not(deep)):
                    rep["url_mc_get_machine"] = "dict"
                else:
                    rep["url_mc_get_machine"] = self.url_mc_get_machine
            else:
                if (short and not(deep)):
                    rep["url_mc_get_machine"] = self.url_mc_get_machine.type_rep()
                else:
                    rep["url_mc_get_machine"] = self.url_mc_get_machine.prop_rep(short, deep)
        if self.is_set("url_sc_get_device_statistics"):
            if isinstance(self.url_sc_get_device_statistics,dict):
                if (short and not(deep)):
                    rep["url_sc_get_device_statistics"] = "dict"
                else:
                    rep["url_sc_get_device_statistics"] = self.url_sc_get_device_statistics
            else:
                if (short and not(deep)):
                    rep["url_sc_get_device_statistics"] = self.url_sc_get_device_statistics.type_rep()
                else:
                    rep["url_sc_get_device_statistics"] = self.url_sc_get_device_statistics.prop_rep(short, deep)
        if self.is_set("url_sc_get_farm_info"):
            if isinstance(self.url_sc_get_farm_info,dict):
                if (short and not(deep)):
                    rep["url_sc_get_farm_info"] = "dict"
                else:
                    rep["url_sc_get_farm_info"] = self.url_sc_get_farm_info
            else:
                if (short and not(deep)):
                    rep["url_sc_get_farm_info"] = self.url_sc_get_farm_info.type_rep()
                else:
                    rep["url_sc_get_farm_info"] = self.url_sc_get_farm_info.prop_rep(short, deep)
        if self.is_set("url_sc_get_farm_bathymetry"):
            if isinstance(self.url_sc_get_farm_bathymetry,dict):
                if (short and not(deep)):
                    rep["url_sc_get_farm_bathymetry"] = "dict"
                else:
                    rep["url_sc_get_farm_bathymetry"] = self.url_sc_get_farm_bathymetry
            else:
                if (short and not(deep)):
                    rep["url_sc_get_farm_bathymetry"] = self.url_sc_get_farm_bathymetry.type_rep()
                else:
                    rep["url_sc_get_farm_bathymetry"] = self.url_sc_get_farm_bathymetry.prop_rep(short, deep)
        if self.is_set("url_sc_get_farm_seabed_type"):
            if isinstance(self.url_sc_get_farm_seabed_type,dict):
                if (short and not(deep)):
                    rep["url_sc_get_farm_seabed_type"] = "dict"
                else:
                    rep["url_sc_get_farm_seabed_type"] = self.url_sc_get_farm_seabed_type
            else:
                if (short and not(deep)):
                    rep["url_sc_get_farm_seabed_type"] = self.url_sc_get_farm_seabed_type.type_rep()
                else:
                    rep["url_sc_get_farm_seabed_type"] = self.url_sc_get_farm_seabed_type.prop_rep(short, deep)
        if self.is_set("url_ed_get_study_results"):
            if isinstance(self.url_ed_get_study_results,dict):
                if (short and not(deep)):
                    rep["url_ed_get_study_results"] = "dict"
                else:
                    rep["url_ed_get_study_results"] = self.url_ed_get_study_results
            else:
                if (short and not(deep)):
                    rep["url_ed_get_study_results"] = self.url_ed_get_study_results.type_rep()
                else:
                    rep["url_ed_get_study_results"] = self.url_ed_get_study_results.prop_rep(short, deep)
        if self.is_set("url_cm_drag_anchor"):
            if isinstance(self.url_cm_drag_anchor,dict):
                if (short and not(deep)):
                    rep["url_cm_drag_anchor"] = "dict"
                else:
                    rep["url_cm_drag_anchor"] = self.url_cm_drag_anchor
            else:
                if (short and not(deep)):
                    rep["url_cm_drag_anchor"] = self.url_cm_drag_anchor.type_rep()
                else:
                    rep["url_cm_drag_anchor"] = self.url_cm_drag_anchor.prop_rep(short, deep)
        if self.is_set("url_cm_mooring_line"):
            if isinstance(self.url_cm_mooring_line,dict):
                if (short and not(deep)):
                    rep["url_cm_mooring_line"] = "dict"
                else:
                    rep["url_cm_mooring_line"] = self.url_cm_mooring_line
            else:
                if (short and not(deep)):
                    rep["url_cm_mooring_line"] = self.url_cm_mooring_line.type_rep()
                else:
                    rep["url_cm_mooring_line"] = self.url_cm_mooring_line.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        subgroup = handle.create_group("sk_inputs")
        self.sk_inputs.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("catalogue_lines")
        self.catalogue_lines.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("catalogue_anchors")
        self.catalogue_anchors.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.run_mode.encode("ascii"))
        handle["run_mode"] = np.asarray(ar)
        subgroup = handle.create_group("url_ec_get_farm")
        self.url_ec_get_farm.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("url_ec_get_farm_inputs")
        self.url_ec_get_farm_inputs.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("url_mc_get_single_machine_hydrodynamic")
        self.url_mc_get_single_machine_hydrodynamic.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("url_mc_get_wec_cplx1")
        self.url_mc_get_wec_cplx1.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("url_mc_get_wec_cplx2")
        self.url_mc_get_wec_cplx2.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("url_mc_get_wec_cplx3")
        self.url_mc_get_wec_cplx3.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("url_mc_get_tec_cplx1")
        self.url_mc_get_tec_cplx1.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("url_mc_get_tec_cplx2")
        self.url_mc_get_tec_cplx2.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("url_mc_get_tec_cplx3")
        self.url_mc_get_tec_cplx3.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("url_mc_get_general")
        self.url_mc_get_general.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("url_mc_get_dimensions")
        self.url_mc_get_dimensions.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("url_mc_get_machine")
        self.url_mc_get_machine.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("url_sc_get_device_statistics")
        self.url_sc_get_device_statistics.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("url_sc_get_farm_info")
        self.url_sc_get_farm_info.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("url_sc_get_farm_bathymetry")
        self.url_sc_get_farm_bathymetry.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("url_sc_get_farm_seabed_type")
        self.url_sc_get_farm_seabed_type.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("url_ed_get_study_results")
        self.url_ed_get_study_results.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("url_cm_drag_anchor")
        self.url_cm_drag_anchor.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        subgroup = handle.create_group("url_cm_mooring_line")
        self.url_cm_mooring_line.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "sk_inputs"
        try :
            if data[varName] != None:
                self.sk_inputs=Cplx.Cplx()
                self.sk_inputs.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "catalogue_lines"
        try :
            if data[varName] != None:
                self.catalogue_lines=Lines.Lines()
                self.catalogue_lines.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "catalogue_anchors"
        try :
            if data[varName] != None:
                self.catalogue_anchors=AnchorCatalogue.AnchorCatalogue()
                self.catalogue_anchors.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "run_mode"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "url_ec_get_farm"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_ec_get_farm=URL.URL()
                        self.url_ec_get_farm.loadFromJSONDict(data[varName])
                else:
                    self.url_ec_get_farm=data[varName]
        except :
            pass
        varName = "url_ec_get_farm_inputs"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_ec_get_farm_inputs=URL.URL()
                        self.url_ec_get_farm_inputs.loadFromJSONDict(data[varName])
                else:
                    self.url_ec_get_farm_inputs=data[varName]
        except :
            pass
        varName = "url_mc_get_single_machine_hydrodynamic"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_mc_get_single_machine_hydrodynamic=URL.URL()
                        self.url_mc_get_single_machine_hydrodynamic.loadFromJSONDict(data[varName])
                else:
                    self.url_mc_get_single_machine_hydrodynamic=data[varName]
        except :
            pass
        varName = "url_mc_get_wec_cplx1"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_mc_get_wec_cplx1=URL.URL()
                        self.url_mc_get_wec_cplx1.loadFromJSONDict(data[varName])
                else:
                    self.url_mc_get_wec_cplx1=data[varName]
        except :
            pass
        varName = "url_mc_get_wec_cplx2"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_mc_get_wec_cplx2=URL.URL()
                        self.url_mc_get_wec_cplx2.loadFromJSONDict(data[varName])
                else:
                    self.url_mc_get_wec_cplx2=data[varName]
        except :
            pass
        varName = "url_mc_get_wec_cplx3"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_mc_get_wec_cplx3=URL.URL()
                        self.url_mc_get_wec_cplx3.loadFromJSONDict(data[varName])
                else:
                    self.url_mc_get_wec_cplx3=data[varName]
        except :
            pass
        varName = "url_mc_get_tec_cplx1"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_mc_get_tec_cplx1=URL.URL()
                        self.url_mc_get_tec_cplx1.loadFromJSONDict(data[varName])
                else:
                    self.url_mc_get_tec_cplx1=data[varName]
        except :
            pass
        varName = "url_mc_get_tec_cplx2"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_mc_get_tec_cplx2=URL.URL()
                        self.url_mc_get_tec_cplx2.loadFromJSONDict(data[varName])
                else:
                    self.url_mc_get_tec_cplx2=data[varName]
        except :
            pass
        varName = "url_mc_get_tec_cplx3"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_mc_get_tec_cplx3=URL.URL()
                        self.url_mc_get_tec_cplx3.loadFromJSONDict(data[varName])
                else:
                    self.url_mc_get_tec_cplx3=data[varName]
        except :
            pass
        varName = "url_mc_get_general"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_mc_get_general=URL.URL()
                        self.url_mc_get_general.loadFromJSONDict(data[varName])
                else:
                    self.url_mc_get_general=data[varName]
        except :
            pass
        varName = "url_mc_get_dimensions"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_mc_get_dimensions=URL.URL()
                        self.url_mc_get_dimensions.loadFromJSONDict(data[varName])
                else:
                    self.url_mc_get_dimensions=data[varName]
        except :
            pass
        varName = "url_mc_get_machine"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_mc_get_machine=URL.URL()
                        self.url_mc_get_machine.loadFromJSONDict(data[varName])
                else:
                    self.url_mc_get_machine=data[varName]
        except :
            pass
        varName = "url_sc_get_device_statistics"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_sc_get_device_statistics=URL.URL()
                        self.url_sc_get_device_statistics.loadFromJSONDict(data[varName])
                else:
                    self.url_sc_get_device_statistics=data[varName]
        except :
            pass
        varName = "url_sc_get_farm_info"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_sc_get_farm_info=URL.URL()
                        self.url_sc_get_farm_info.loadFromJSONDict(data[varName])
                else:
                    self.url_sc_get_farm_info=data[varName]
        except :
            pass
        varName = "url_sc_get_farm_bathymetry"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_sc_get_farm_bathymetry=URL.URL()
                        self.url_sc_get_farm_bathymetry.loadFromJSONDict(data[varName])
                else:
                    self.url_sc_get_farm_bathymetry=data[varName]
        except :
            pass
        varName = "url_sc_get_farm_seabed_type"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_sc_get_farm_seabed_type=URL.URL()
                        self.url_sc_get_farm_seabed_type.loadFromJSONDict(data[varName])
                else:
                    self.url_sc_get_farm_seabed_type=data[varName]
        except :
            pass
        varName = "url_ed_get_study_results"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_ed_get_study_results=URL.URL()
                        self.url_ed_get_study_results.loadFromJSONDict(data[varName])
                else:
                    self.url_ed_get_study_results=data[varName]
        except :
            pass
        varName = "url_cm_drag_anchor"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_cm_drag_anchor=URL.URL()
                        self.url_cm_drag_anchor.loadFromJSONDict(data[varName])
                else:
                    self.url_cm_drag_anchor=data[varName]
        except :
            pass
        varName = "url_cm_mooring_line"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtosk:inputs:URL":
                        self.url_cm_mooring_line=URL.URL()
                        self.url_cm_mooring_line.loadFromJSONDict(data[varName])
                else:
                    self.url_cm_mooring_line=data[varName]
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("sk_inputs" in list(gr.keys())):
            subgroup = gr["sk_inputs"]
            self.sk_inputs.loadFromHDF5Handle(subgroup)
        if ("catalogue_lines" in list(gr.keys())):
            subgroup = gr["catalogue_lines"]
            self.catalogue_lines.loadFromHDF5Handle(subgroup)
        if ("catalogue_anchors" in list(gr.keys())):
            subgroup = gr["catalogue_anchors"]
            self.catalogue_anchors.loadFromHDF5Handle(subgroup)
        if ("run_mode" in list(gr.keys())):
            self.run_mode = gr["run_mode"][0].decode("ascii")
        if ("url_ec_get_farm" in list(gr.keys())):
            subgroup = gr["url_ec_get_farm"]
            self.url_ec_get_farm.loadFromHDF5Handle(subgroup)
        if ("url_ec_get_farm_inputs" in list(gr.keys())):
            subgroup = gr["url_ec_get_farm_inputs"]
            self.url_ec_get_farm_inputs.loadFromHDF5Handle(subgroup)
        if ("url_mc_get_single_machine_hydrodynamic" in list(gr.keys())):
            subgroup = gr["url_mc_get_single_machine_hydrodynamic"]
            self.url_mc_get_single_machine_hydrodynamic.loadFromHDF5Handle(subgroup)
        if ("url_mc_get_wec_cplx1" in list(gr.keys())):
            subgroup = gr["url_mc_get_wec_cplx1"]
            self.url_mc_get_wec_cplx1.loadFromHDF5Handle(subgroup)
        if ("url_mc_get_wec_cplx2" in list(gr.keys())):
            subgroup = gr["url_mc_get_wec_cplx2"]
            self.url_mc_get_wec_cplx2.loadFromHDF5Handle(subgroup)
        if ("url_mc_get_wec_cplx3" in list(gr.keys())):
            subgroup = gr["url_mc_get_wec_cplx3"]
            self.url_mc_get_wec_cplx3.loadFromHDF5Handle(subgroup)
        if ("url_mc_get_tec_cplx1" in list(gr.keys())):
            subgroup = gr["url_mc_get_tec_cplx1"]
            self.url_mc_get_tec_cplx1.loadFromHDF5Handle(subgroup)
        if ("url_mc_get_tec_cplx2" in list(gr.keys())):
            subgroup = gr["url_mc_get_tec_cplx2"]
            self.url_mc_get_tec_cplx2.loadFromHDF5Handle(subgroup)
        if ("url_mc_get_tec_cplx3" in list(gr.keys())):
            subgroup = gr["url_mc_get_tec_cplx3"]
            self.url_mc_get_tec_cplx3.loadFromHDF5Handle(subgroup)
        if ("url_mc_get_general" in list(gr.keys())):
            subgroup = gr["url_mc_get_general"]
            self.url_mc_get_general.loadFromHDF5Handle(subgroup)
        if ("url_mc_get_dimensions" in list(gr.keys())):
            subgroup = gr["url_mc_get_dimensions"]
            self.url_mc_get_dimensions.loadFromHDF5Handle(subgroup)
        if ("url_mc_get_machine" in list(gr.keys())):
            subgroup = gr["url_mc_get_machine"]
            self.url_mc_get_machine.loadFromHDF5Handle(subgroup)
        if ("url_sc_get_device_statistics" in list(gr.keys())):
            subgroup = gr["url_sc_get_device_statistics"]
            self.url_sc_get_device_statistics.loadFromHDF5Handle(subgroup)
        if ("url_sc_get_farm_info" in list(gr.keys())):
            subgroup = gr["url_sc_get_farm_info"]
            self.url_sc_get_farm_info.loadFromHDF5Handle(subgroup)
        if ("url_sc_get_farm_bathymetry" in list(gr.keys())):
            subgroup = gr["url_sc_get_farm_bathymetry"]
            self.url_sc_get_farm_bathymetry.loadFromHDF5Handle(subgroup)
        if ("url_sc_get_farm_seabed_type" in list(gr.keys())):
            subgroup = gr["url_sc_get_farm_seabed_type"]
            self.url_sc_get_farm_seabed_type.loadFromHDF5Handle(subgroup)
        if ("url_ed_get_study_results" in list(gr.keys())):
            subgroup = gr["url_ed_get_study_results"]
            self.url_ed_get_study_results.loadFromHDF5Handle(subgroup)
        if ("url_cm_drag_anchor" in list(gr.keys())):
            subgroup = gr["url_cm_drag_anchor"]
            self.url_cm_drag_anchor.loadFromHDF5Handle(subgroup)
        if ("url_cm_mooring_line" in list(gr.keys())):
            subgroup = gr["url_cm_mooring_line"]
            self.url_cm_mooring_line.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
