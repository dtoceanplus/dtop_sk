import collections
import numpy as np
import os
import json
import h5py
from . import SteadyForceModel
from . import HydroDataMCFormat
from . import FoundationDesignParameters
from ..mooring import MooringDesignCriteria
from . import CustomMooringInput
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class DeviceProperties():

    """data model representing device properties
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._steady_force_model=SteadyForceModel.SteadyForceModel()
        self._steady_force_model.description = 'geometry description for steady force models: wind, current, mean wave drift'
        self._hdb_source='nemoh_run'
        self._hydro_data_MC_module=HydroDataMCFormat.HydroDataMCFormat()
        self._hydro_data_MC_module.description = 'Hydrodynamic data in format of MC module output'
        self._path_to_nemoh_result_folder=''
        self._nemoh_scale_factor=1.0
        self._critical_damping_coefficient=np.zeros(shape=(6), dtype=float)
        self._submerged_volume=0.0
        self._mass=0.0
        self._mass_matrix=np.zeros(shape=(6,6), dtype=float)
        self._hydrostatic_matrix=np.zeros(shape=(6,6), dtype=float)
        self._cog=np.zeros(shape=(3), dtype=float)
        self._machine_type='wec'
        self._positioning_type='moored'
        self._positioning_reference='seabed'
        self._rotation_center_location='origin'
        self._thrust_coeff_curve_file=''
        self._thrust_curve_ct=np.zeros(shape=(1), dtype=float)
        self._thrust_curve_v=np.zeros(shape=(1), dtype=float)
        self._rotor_diameter=0.0
        self._hub_position=np.zeros(shape=(1,1), dtype=float)
        self._include_orbital_velocity=False
        self._device_foundation_profile='cylinder'
        self._foundation_design_parameters=FoundationDesignParameters.FoundationDesignParameters()
        self._foundation_design_parameters.description = 'Parameter inputs for foundation design'
        self._mooring_input_flag=''
        self._mooring_design_criteria=MooringDesignCriteria.MooringDesignCriteria()
        self._mooring_design_criteria.description = 'Criteria for mooring design, safety_factor is always necessary, the rest is required only if design is chosen'
        self._custom_mooring_input=[]
        self._file_mooring_input=''
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
        self.hub_position=[[]]
        self.critical_damping_coefficient = np.array([0.1,0.1,0.1,0.1,0.1,0.1],dtype=float)
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def steady_force_model(self): # pragma: no cover
        """:obj:`~.SteadyForceModel.SteadyForceModel`: geometry description for steady force models: wind, current, mean wave drift
        """
        return self._steady_force_model
    #------------
    @ property
    def hdb_source(self): # pragma: no cover
        """str: flag that indicates how to fetch the hydrodynamic data. nemoh_run or mc_module
        """
        return self._hdb_source
    #------------
    @ property
    def hydro_data_MC_module(self): # pragma: no cover
        """:obj:`~.HydroDataMCFormat.HydroDataMCFormat`: Hydrodynamic data in format of MC module output
        """
        return self._hydro_data_MC_module
    #------------
    @ property
    def path_to_nemoh_result_folder(self): # pragma: no cover
        """str: path to folder that contains nemoh results. Required only if hdb_source is nemoh_run
        """
        return self._path_to_nemoh_result_folder
    #------------
    @ property
    def nemoh_scale_factor(self): # pragma: no cover
        """float: Scale ratio to apply for the classical geometries defined in the hydro database catalogue []
        """
        return self._nemoh_scale_factor
    #------------
    @ property
    def critical_damping_coefficient(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Additional damping in terms of coefficient of critical damping. (1.0 is critical damping) dim(6) []
        """
        return self._critical_damping_coefficient
    #------------
    @ property
    def submerged_volume(self): # pragma: no cover
        """float: device submerged volume. Only used for bottom fixed devices to compute the weight in water. [m3]
        """
        return self._submerged_volume
    #------------
    @ property
    def mass(self): # pragma: no cover
        """float: device mass [kg]
        """
        return self._mass
    #------------
    @ property
    def mass_matrix(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:device mass matrix dim(6,6) [kg]
        """
        return self._mass_matrix
    #------------
    @ property
    def hydrostatic_matrix(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:device hydrostatic and gravitational restoring matrix dim(6,6) []
        """
        return self._hydrostatic_matrix
    #------------
    @ property
    def cog(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:position of cog in body-fixed coordinate system dim(3) [kg]
        """
        return self._cog
    #------------
    @ property
    def machine_type(self): # pragma: no cover
        """str: tec or wec
        """
        return self._machine_type
    #------------
    @ property
    def positioning_type(self): # pragma: no cover
        """str: moored or fixed
        """
        return self._positioning_type
    #------------
    @ property
    def positioning_reference(self): # pragma: no cover
        """str: seabed or masterstructure
        """
        return self._positioning_reference
    #------------
    @ property
    def rotation_center_location(self): # pragma: no cover
        """str: Origin or center of rotation defined by user
        """
        return self._rotation_center_location
    #------------
    @ property
    def thrust_coeff_curve_file(self): # pragma: no cover
        """str: Path to file containing rotor thrust coefficient curve. Required only if machine is a tec
        """
        return self._thrust_coeff_curve_file
    #------------
    @ property
    def thrust_curve_ct(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Ct values of the thurst coefficient curve. Required if thurst_coeff_curve_file is not given. dim(*) []
        """
        return self._thrust_curve_ct
    #------------
    @ property
    def thrust_curve_v(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Velocity values of the thurst coefficient curve. Required if thurst_coeff_curve_file is not given. dim(*) []
        """
        return self._thrust_curve_v
    #------------
    @ property
    def rotor_diameter(self): # pragma: no cover
        """float: Rotor diameter. Required only if machine is a tec []
        """
        return self._rotor_diameter
    #------------
    @ property
    def hub_position(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Hub position. Required only if machine is a tec. dim =(3,number of rotor) dim(*,*) []
        """
        return self._hub_position
    #------------
    @ property
    def include_orbital_velocity(self): # pragma: no cover
        """bool: Include orbital velocity []
        """
        return self._include_orbital_velocity
    #------------
    @ property
    def device_foundation_profile(self): # pragma: no cover
        """str: device foundation profile: cylinder or rectangular. Required if positionning_type is fixed.
        """
        return self._device_foundation_profile
    #------------
    @ property
    def foundation_design_parameters(self): # pragma: no cover
        """:obj:`~.FoundationDesignParameters.FoundationDesignParameters`: Parameter inputs for foundation design
        """
        return self._foundation_design_parameters
    #------------
    @ property
    def mooring_input_flag(self): # pragma: no cover
        """str: Flag describing how the mooring system is defined: design, custom, input_file
        """
        return self._mooring_input_flag
    #------------
    @ property
    def mooring_design_criteria(self): # pragma: no cover
        """:obj:`~.MooringDesignCriteria.MooringDesignCriteria`: Criteria for mooring design, safety_factor is always necessary, the rest is required only if design is chosen
        """
        return self._mooring_design_criteria
    #------------
    @ property
    def custom_mooring_input(self): # pragma: no cover
        """:obj:`list` of :obj:`~.CustomMooringInput.CustomMooringInput`: Mooring system input of the device, required if mooring_input_flag is custom. If one is given, will be used on all devices. If several, should be as many as devices
        """
        return self._custom_mooring_input
    #------------
    @ property
    def file_mooring_input(self): # pragma: no cover
        """str: Path to JSON file that describes a custom_mooring_input_flag, required if mooring_input_flag is input_file
        """
        return self._file_mooring_input
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ steady_force_model.setter
    def steady_force_model(self,val): # pragma: no cover
        self._steady_force_model=val
    #------------
    @ hdb_source.setter
    def hdb_source(self,val): # pragma: no cover
        self._hdb_source=str(val)
    #------------
    @ hydro_data_MC_module.setter
    def hydro_data_MC_module(self,val): # pragma: no cover
        self._hydro_data_MC_module=val
    #------------
    @ path_to_nemoh_result_folder.setter
    def path_to_nemoh_result_folder(self,val): # pragma: no cover
        self._path_to_nemoh_result_folder=str(val)
    #------------
    @ nemoh_scale_factor.setter
    def nemoh_scale_factor(self,val): # pragma: no cover
        self._nemoh_scale_factor=float(val)
    #------------
    @ critical_damping_coefficient.setter
    def critical_damping_coefficient(self,val): # pragma: no cover
        self._critical_damping_coefficient=val
    #------------
    @ submerged_volume.setter
    def submerged_volume(self,val): # pragma: no cover
        self._submerged_volume=float(val)
    #------------
    @ mass.setter
    def mass(self,val): # pragma: no cover
        self._mass=float(val)
    #------------
    @ mass_matrix.setter
    def mass_matrix(self,val): # pragma: no cover
        self._mass_matrix=val
    #------------
    @ hydrostatic_matrix.setter
    def hydrostatic_matrix(self,val): # pragma: no cover
        self._hydrostatic_matrix=val
    #------------
    @ cog.setter
    def cog(self,val): # pragma: no cover
        self._cog=val
    #------------
    @ machine_type.setter
    def machine_type(self,val): # pragma: no cover
        self._machine_type=str(val)
    #------------
    @ positioning_type.setter
    def positioning_type(self,val): # pragma: no cover
        self._positioning_type=str(val)
    #------------
    @ positioning_reference.setter
    def positioning_reference(self,val): # pragma: no cover
        self._positioning_reference=str(val)
    #------------
    @ rotation_center_location.setter
    def rotation_center_location(self,val): # pragma: no cover
        self._rotation_center_location=str(val)
    #------------
    @ thrust_coeff_curve_file.setter
    def thrust_coeff_curve_file(self,val): # pragma: no cover
        self._thrust_coeff_curve_file=str(val)
    #------------
    @ thrust_curve_ct.setter
    def thrust_curve_ct(self,val): # pragma: no cover
        self._thrust_curve_ct=val
    #------------
    @ thrust_curve_v.setter
    def thrust_curve_v(self,val): # pragma: no cover
        self._thrust_curve_v=val
    #------------
    @ rotor_diameter.setter
    def rotor_diameter(self,val): # pragma: no cover
        self._rotor_diameter=float(val)
    #------------
    @ hub_position.setter
    def hub_position(self,val): # pragma: no cover
        self._hub_position=val
    #------------
    @ include_orbital_velocity.setter
    def include_orbital_velocity(self,val): # pragma: no cover
        self._include_orbital_velocity=val
    #------------
    @ device_foundation_profile.setter
    def device_foundation_profile(self,val): # pragma: no cover
        self._device_foundation_profile=str(val)
    #------------
    @ foundation_design_parameters.setter
    def foundation_design_parameters(self,val): # pragma: no cover
        self._foundation_design_parameters=val
    #------------
    @ mooring_input_flag.setter
    def mooring_input_flag(self,val): # pragma: no cover
        self._mooring_input_flag=str(val)
    #------------
    @ mooring_design_criteria.setter
    def mooring_design_criteria(self,val): # pragma: no cover
        self._mooring_design_criteria=val
    #------------
    @ custom_mooring_input.setter
    def custom_mooring_input(self,val): # pragma: no cover
        self._custom_mooring_input=val
    #------------
    @ file_mooring_input.setter
    def file_mooring_input(self,val): # pragma: no cover
        self._file_mooring_input=str(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:DeviceProperties"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:DeviceProperties"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("steady_force_model"):
            if (short and not(deep)):
                rep["steady_force_model"] = self.steady_force_model.type_rep()
            else:
                rep["steady_force_model"] = self.steady_force_model.prop_rep(short, deep)
        if self.is_set("hdb_source"):
            rep["hdb_source"] = self.hdb_source
        if self.is_set("hydro_data_MC_module"):
            if (short and not(deep)):
                rep["hydro_data_MC_module"] = self.hydro_data_MC_module.type_rep()
            else:
                rep["hydro_data_MC_module"] = self.hydro_data_MC_module.prop_rep(short, deep)
        if self.is_set("path_to_nemoh_result_folder"):
            rep["path_to_nemoh_result_folder"] = self.path_to_nemoh_result_folder
        if self.is_set("nemoh_scale_factor"):
            rep["nemoh_scale_factor"] = self.nemoh_scale_factor
        if self.is_set("critical_damping_coefficient"):
            if (short):
                rep["critical_damping_coefficient"] = str(self.critical_damping_coefficient.shape)
            else:
                try:
                    rep["critical_damping_coefficient"] = self.critical_damping_coefficient.tolist()
                except:
                    rep["critical_damping_coefficient"] = self.critical_damping_coefficient
        if self.is_set("submerged_volume"):
            rep["submerged_volume"] = self.submerged_volume
        if self.is_set("mass"):
            rep["mass"] = self.mass
        if self.is_set("mass_matrix"):
            if (short):
                rep["mass_matrix"] = str(self.mass_matrix.shape)
            else:
                try:
                    rep["mass_matrix"] = self.mass_matrix.tolist()
                except:
                    rep["mass_matrix"] = self.mass_matrix
        if self.is_set("hydrostatic_matrix"):
            if (short):
                rep["hydrostatic_matrix"] = str(self.hydrostatic_matrix.shape)
            else:
                try:
                    rep["hydrostatic_matrix"] = self.hydrostatic_matrix.tolist()
                except:
                    rep["hydrostatic_matrix"] = self.hydrostatic_matrix
        if self.is_set("cog"):
            if (short):
                rep["cog"] = str(self.cog.shape)
            else:
                try:
                    rep["cog"] = self.cog.tolist()
                except:
                    rep["cog"] = self.cog
        if self.is_set("machine_type"):
            rep["machine_type"] = self.machine_type
        if self.is_set("positioning_type"):
            rep["positioning_type"] = self.positioning_type
        if self.is_set("positioning_reference"):
            rep["positioning_reference"] = self.positioning_reference
        if self.is_set("rotation_center_location"):
            rep["rotation_center_location"] = self.rotation_center_location
        if self.is_set("thrust_coeff_curve_file"):
            rep["thrust_coeff_curve_file"] = self.thrust_coeff_curve_file
        if self.is_set("thrust_curve_ct"):
            if (short):
                rep["thrust_curve_ct"] = str(self.thrust_curve_ct.shape)
            else:
                try:
                    rep["thrust_curve_ct"] = self.thrust_curve_ct.tolist()
                except:
                    rep["thrust_curve_ct"] = self.thrust_curve_ct
        if self.is_set("thrust_curve_v"):
            if (short):
                rep["thrust_curve_v"] = str(self.thrust_curve_v.shape)
            else:
                try:
                    rep["thrust_curve_v"] = self.thrust_curve_v.tolist()
                except:
                    rep["thrust_curve_v"] = self.thrust_curve_v
        if self.is_set("rotor_diameter"):
            rep["rotor_diameter"] = self.rotor_diameter
        if self.is_set("hub_position"):
            if (short):
                rep["hub_position"] = str(self.hub_position.shape)
            else:
                try:
                    rep["hub_position"] = self.hub_position.tolist()
                except:
                    rep["hub_position"] = self.hub_position
        if self.is_set("include_orbital_velocity"):
            rep["include_orbital_velocity"] = self.include_orbital_velocity
        if self.is_set("device_foundation_profile"):
            rep["device_foundation_profile"] = self.device_foundation_profile
        if self.is_set("foundation_design_parameters"):
            if (short and not(deep)):
                rep["foundation_design_parameters"] = self.foundation_design_parameters.type_rep()
            else:
                rep["foundation_design_parameters"] = self.foundation_design_parameters.prop_rep(short, deep)
        if self.is_set("mooring_input_flag"):
            rep["mooring_input_flag"] = self.mooring_input_flag
        if self.is_set("mooring_design_criteria"):
            if (short and not(deep)):
                rep["mooring_design_criteria"] = self.mooring_design_criteria.type_rep()
            else:
                rep["mooring_design_criteria"] = self.mooring_design_criteria.prop_rep(short, deep)
        if self.is_set("custom_mooring_input"):
            rep["custom_mooring_input"] = []
            for i in range(0,len(self.custom_mooring_input)):
                if (short and not(deep)):
                    itemType = self.custom_mooring_input[i].type_rep()
                    rep["custom_mooring_input"].append( itemType )
                else:
                    rep["custom_mooring_input"].append( self.custom_mooring_input[i].prop_rep(short, deep) )
        else:
            rep["custom_mooring_input"] = []
        if self.is_set("file_mooring_input"):
            rep["file_mooring_input"] = self.file_mooring_input
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        subgroup = handle.create_group("steady_force_model")
        self.steady_force_model.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.hdb_source.encode("ascii"))
        handle["hdb_source"] = np.asarray(ar)
        subgroup = handle.create_group("hydro_data_MC_module")
        self.hydro_data_MC_module.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.path_to_nemoh_result_folder.encode("ascii"))
        handle["path_to_nemoh_result_folder"] = np.asarray(ar)
        handle["nemoh_scale_factor"] = np.array([self.nemoh_scale_factor],dtype=float)
        handle["critical_damping_coefficient"] = np.array(self.critical_damping_coefficient,dtype=float)
        handle["submerged_volume"] = np.array([self.submerged_volume],dtype=float)
        handle["mass"] = np.array([self.mass],dtype=float)
        handle["mass_matrix"] = np.array(self.mass_matrix,dtype=float)
        handle["hydrostatic_matrix"] = np.array(self.hydrostatic_matrix,dtype=float)
        handle["cog"] = np.array(self.cog,dtype=float)
        ar = []
        ar.append(self.machine_type.encode("ascii"))
        handle["machine_type"] = np.asarray(ar)
        ar = []
        ar.append(self.positioning_type.encode("ascii"))
        handle["positioning_type"] = np.asarray(ar)
        ar = []
        ar.append(self.positioning_reference.encode("ascii"))
        handle["positioning_reference"] = np.asarray(ar)
        ar = []
        ar.append(self.rotation_center_location.encode("ascii"))
        handle["rotation_center_location"] = np.asarray(ar)
        ar = []
        ar.append(self.thrust_coeff_curve_file.encode("ascii"))
        handle["thrust_coeff_curve_file"] = np.asarray(ar)
        handle["thrust_curve_ct"] = np.array(self.thrust_curve_ct,dtype=float)
        handle["thrust_curve_v"] = np.array(self.thrust_curve_v,dtype=float)
        handle["rotor_diameter"] = np.array([self.rotor_diameter],dtype=float)
        handle["hub_position"] = np.array(self.hub_position,dtype=float)
        handle["include_orbital_velocity"] = np.array([self.include_orbital_velocity],dtype=bool)
        ar = []
        ar.append(self.device_foundation_profile.encode("ascii"))
        handle["device_foundation_profile"] = np.asarray(ar)
        subgroup = handle.create_group("foundation_design_parameters")
        self.foundation_design_parameters.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.mooring_input_flag.encode("ascii"))
        handle["mooring_input_flag"] = np.asarray(ar)
        subgroup = handle.create_group("mooring_design_criteria")
        self.mooring_design_criteria.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("custom_mooring_input")
        for idx in range(0,len(self.custom_mooring_input)):
            subgroup1 = subgroup.create_group("custom_mooring_input" + "_" + str(idx))
            self.custom_mooring_input[idx].saveToHDF5Handle(subgroup1)
        ar = []
        ar.append(self.file_mooring_input.encode("ascii"))
        handle["file_mooring_input"] = np.asarray(ar)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "steady_force_model"
        try :
            if data[varName] != None:
                self.steady_force_model=SteadyForceModel.SteadyForceModel()
                self.steady_force_model.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "hdb_source"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "hydro_data_MC_module"
        try :
            if data[varName] != None:
                self.hydro_data_MC_module=HydroDataMCFormat.HydroDataMCFormat()
                self.hydro_data_MC_module.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "path_to_nemoh_result_folder"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "nemoh_scale_factor"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "critical_damping_coefficient"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "submerged_volume"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mass"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mass_matrix"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "hydrostatic_matrix"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "cog"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "machine_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "positioning_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "positioning_reference"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "rotation_center_location"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "thrust_coeff_curve_file"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "thrust_curve_ct"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "thrust_curve_v"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "rotor_diameter"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "hub_position"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "include_orbital_velocity"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "device_foundation_profile"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "foundation_design_parameters"
        try :
            if data[varName] != None:
                self.foundation_design_parameters=FoundationDesignParameters.FoundationDesignParameters()
                self.foundation_design_parameters.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "mooring_input_flag"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mooring_design_criteria"
        try :
            if data[varName] != None:
                self.mooring_design_criteria=MooringDesignCriteria.MooringDesignCriteria()
                self.mooring_design_criteria.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "custom_mooring_input"
        try :
            if data[varName] != None:
                self.custom_mooring_input=[]
                for i in range(0,len(data[varName])):
                    self.custom_mooring_input.append(CustomMooringInput.CustomMooringInput())
                    self.custom_mooring_input[i].loadFromJSONDict(data[varName][i])
            else:
                self.custom_mooring_input = []
        except :
            pass
        varName = "file_mooring_input"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("steady_force_model" in list(gr.keys())):
            subgroup = gr["steady_force_model"]
            self.steady_force_model.loadFromHDF5Handle(subgroup)
        if ("hdb_source" in list(gr.keys())):
            self.hdb_source = gr["hdb_source"][0].decode("ascii")
        if ("hydro_data_MC_module" in list(gr.keys())):
            subgroup = gr["hydro_data_MC_module"]
            self.hydro_data_MC_module.loadFromHDF5Handle(subgroup)
        if ("path_to_nemoh_result_folder" in list(gr.keys())):
            self.path_to_nemoh_result_folder = gr["path_to_nemoh_result_folder"][0].decode("ascii")
        if ("nemoh_scale_factor" in list(gr.keys())):
            self.nemoh_scale_factor = gr["nemoh_scale_factor"][0]
        if ("critical_damping_coefficient" in list(gr.keys())):
            self.critical_damping_coefficient = gr["critical_damping_coefficient"][:]
        if ("submerged_volume" in list(gr.keys())):
            self.submerged_volume = gr["submerged_volume"][0]
        if ("mass" in list(gr.keys())):
            self.mass = gr["mass"][0]
        if ("mass_matrix" in list(gr.keys())):
            self.mass_matrix = gr["mass_matrix"][:][:]
        if ("hydrostatic_matrix" in list(gr.keys())):
            self.hydrostatic_matrix = gr["hydrostatic_matrix"][:][:]
        if ("cog" in list(gr.keys())):
            self.cog = gr["cog"][:]
        if ("machine_type" in list(gr.keys())):
            self.machine_type = gr["machine_type"][0].decode("ascii")
        if ("positioning_type" in list(gr.keys())):
            self.positioning_type = gr["positioning_type"][0].decode("ascii")
        if ("positioning_reference" in list(gr.keys())):
            self.positioning_reference = gr["positioning_reference"][0].decode("ascii")
        if ("rotation_center_location" in list(gr.keys())):
            self.rotation_center_location = gr["rotation_center_location"][0].decode("ascii")
        if ("thrust_coeff_curve_file" in list(gr.keys())):
            self.thrust_coeff_curve_file = gr["thrust_coeff_curve_file"][0].decode("ascii")
        if ("thrust_curve_ct" in list(gr.keys())):
            self.thrust_curve_ct = gr["thrust_curve_ct"][:]
        if ("thrust_curve_v" in list(gr.keys())):
            self.thrust_curve_v = gr["thrust_curve_v"][:]
        if ("rotor_diameter" in list(gr.keys())):
            self.rotor_diameter = gr["rotor_diameter"][0]
        if ("hub_position" in list(gr.keys())):
            self.hub_position = gr["hub_position"][:][:]
        if ("include_orbital_velocity" in list(gr.keys())):
            self.include_orbital_velocity = gr["include_orbital_velocity"][0]
        if ("device_foundation_profile" in list(gr.keys())):
            self.device_foundation_profile = gr["device_foundation_profile"][0].decode("ascii")
        if ("foundation_design_parameters" in list(gr.keys())):
            subgroup = gr["foundation_design_parameters"]
            self.foundation_design_parameters.loadFromHDF5Handle(subgroup)
        if ("mooring_input_flag" in list(gr.keys())):
            self.mooring_input_flag = gr["mooring_input_flag"][0].decode("ascii")
        if ("mooring_design_criteria" in list(gr.keys())):
            subgroup = gr["mooring_design_criteria"]
            self.mooring_design_criteria.loadFromHDF5Handle(subgroup)
        if ("custom_mooring_input" in list(gr.keys())):
            subgroup = gr["custom_mooring_input"]
            self.custom_mooring_input=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["custom_mooring_input"+"_"+str(idx)]
                self.custom_mooring_input.append(CustomMooringInput.CustomMooringInput())
                self.custom_mooring_input[idx].loadFromHDF5Handle(ssubgroup)
        if ("file_mooring_input" in list(gr.keys())):
            self.file_mooring_input = gr["file_mooring_input"][0].decode("ascii")
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
