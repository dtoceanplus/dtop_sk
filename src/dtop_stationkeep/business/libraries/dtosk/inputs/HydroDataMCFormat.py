# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class HydroDataMCFormat():

    """data model representing device hydrodynamic data from MC module
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._frequency=np.zeros(shape=(1), dtype=float)
        self._direction=np.zeros(shape=(1), dtype=float)
        self._added_mass=np.zeros(shape=(1,1,1), dtype=float)
        self._radiation_damping=np.zeros(shape=(1,1,1), dtype=float)
        self._excitation_force_real=np.zeros(shape=(1,1,1), dtype=float)
        self._excitation_force_imag=np.zeros(shape=(1,1,1), dtype=float)
        self._fitting_damping=np.zeros(shape=(1,1,1,1,1), dtype=float)
        self._fitting_stiffness=np.zeros(shape=(1,1,1,1,1), dtype=float)
        self._velocity_transformation_matrix=np.zeros(shape=(1,1), dtype=float)
        self._tp_capture_width=np.zeros(shape=(1), dtype=float)
        self._hs_capture_width=np.zeros(shape=(1), dtype=float)
        self._wave_angle_capture_width=np.zeros(shape=(1), dtype=float)
        self._pto_damping=np.zeros(shape=(1,1,1,1,1), dtype=float)
        self._mooring_stiffness=np.zeros(shape=(1,1,1,1,1), dtype=float)
        self._additional_damping=np.zeros(shape=(1,1,1,1,1), dtype=float)
        self._additional_stiffness=np.zeros(shape=(1,1,1,1,1), dtype=float)
        self._shared_dof=np.zeros(shape=(6), dtype=int)
        self._hydrostatic_matrix=np.zeros(shape=(1,1), dtype=float)
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def frequency(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Wave frequencies for which the hydrodamic data are given (omega). [rad/s] dim(*) []
        """
        return self._frequency
    #------------
    @ property
    def direction(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Wave directions for which the hydrodamic data are given (beta). [rad] dim(*) []
        """
        return self._direction
    #------------
    @ property
    def added_mass(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Added mass matrix dim(*,*,*) []
        """
        return self._added_mass
    #------------
    @ property
    def radiation_damping(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Radiation damping matrix dim(*,*,*) []
        """
        return self._radiation_damping
    #------------
    @ property
    def excitation_force_real(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Excitation force transfer function, real part dim(*,*,*) []
        """
        return self._excitation_force_real
    #------------
    @ property
    def excitation_force_imag(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Excitation force transfer function, imaginary part dim(*,*,*) []
        """
        return self._excitation_force_imag
    #------------
    @ property
    def fitting_damping(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:fitting_damping (tp,hs,dir,dof,dof) dim(*,*,*,*,*) []
        """
        return self._fitting_damping
    #------------
    @ property
    def fitting_stiffness(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:fitting_stiffness (tp,hs,dir,dof,dof) dim(*,*,*,*,*) []
        """
        return self._fitting_stiffness
    #------------
    @ property
    def velocity_transformation_matrix(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:velocity transformation matrix dim(*,*) []
        """
        return self._velocity_transformation_matrix
    #------------
    @ property
    def tp_capture_width(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:tp array used for fitting damping dim(*) []
        """
        return self._tp_capture_width
    #------------
    @ property
    def hs_capture_width(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:hs array used for fitting damping dim(*) []
        """
        return self._hs_capture_width
    #------------
    @ property
    def wave_angle_capture_width(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:direction array used for fitting damping dim(*) []
        """
        return self._wave_angle_capture_width
    #------------
    @ property
    def pto_damping(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:pto_damping (tp,hs,dir,dof,dof) dim(*,*,*,*,*) []
        """
        return self._pto_damping
    #------------
    @ property
    def mooring_stiffness(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:mooring stiffness desired by the user (tp,hs,dir,dof,dof) dim(*,*,*,*,*) []
        """
        return self._mooring_stiffness
    #------------
    @ property
    def additional_damping(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:additional damping (tp,hs,dir,dof,dof) dim(*,*,*,*,*) []
        """
        return self._additional_damping
    #------------
    @ property
    def additional_stiffness(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:additional stiffness (tp,hs,dir,dof,dof) dim(*,*,*,*,*) []
        """
        return self._additional_stiffness
    #------------
    @ property
    def shared_dof(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`int`:shared degrees of freedom []
        """
        return self._shared_dof
    #------------
    @ property
    def hydrostatic_matrix(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:hydrostatic stiffness matrix (dof,dof) dim(*,*) []
        """
        return self._hydrostatic_matrix
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ frequency.setter
    def frequency(self,val): # pragma: no cover
        self._frequency=val
    #------------
    @ direction.setter
    def direction(self,val): # pragma: no cover
        self._direction=val
    #------------
    @ added_mass.setter
    def added_mass(self,val): # pragma: no cover
        self._added_mass=val
    #------------
    @ radiation_damping.setter
    def radiation_damping(self,val): # pragma: no cover
        self._radiation_damping=val
    #------------
    @ excitation_force_real.setter
    def excitation_force_real(self,val): # pragma: no cover
        self._excitation_force_real=val
    #------------
    @ excitation_force_imag.setter
    def excitation_force_imag(self,val): # pragma: no cover
        self._excitation_force_imag=val
    #------------
    @ fitting_damping.setter
    def fitting_damping(self,val): # pragma: no cover
        self._fitting_damping=val
    #------------
    @ fitting_stiffness.setter
    def fitting_stiffness(self,val): # pragma: no cover
        self._fitting_stiffness=val
    #------------
    @ velocity_transformation_matrix.setter
    def velocity_transformation_matrix(self,val): # pragma: no cover
        self._velocity_transformation_matrix=val
    #------------
    @ tp_capture_width.setter
    def tp_capture_width(self,val): # pragma: no cover
        self._tp_capture_width=val
    #------------
    @ hs_capture_width.setter
    def hs_capture_width(self,val): # pragma: no cover
        self._hs_capture_width=val
    #------------
    @ wave_angle_capture_width.setter
    def wave_angle_capture_width(self,val): # pragma: no cover
        self._wave_angle_capture_width=val
    #------------
    @ pto_damping.setter
    def pto_damping(self,val): # pragma: no cover
        self._pto_damping=val
    #------------
    @ mooring_stiffness.setter
    def mooring_stiffness(self,val): # pragma: no cover
        self._mooring_stiffness=val
    #------------
    @ additional_damping.setter
    def additional_damping(self,val): # pragma: no cover
        self._additional_damping=val
    #------------
    @ additional_stiffness.setter
    def additional_stiffness(self,val): # pragma: no cover
        self._additional_stiffness=val
    #------------
    @ shared_dof.setter
    def shared_dof(self,val): # pragma: no cover
        self._shared_dof=val
    #------------
    @ hydrostatic_matrix.setter
    def hydrostatic_matrix(self,val): # pragma: no cover
        self._hydrostatic_matrix=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:HydroDataMCFormat"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:HydroDataMCFormat"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("frequency"):
            if (short):
                rep["frequency"] = str(self.frequency.shape)
            else:
                try:
                    rep["frequency"] = self.frequency.tolist()
                except:
                    rep["frequency"] = self.frequency
        if self.is_set("direction"):
            if (short):
                rep["direction"] = str(self.direction.shape)
            else:
                try:
                    rep["direction"] = self.direction.tolist()
                except:
                    rep["direction"] = self.direction
        if self.is_set("added_mass"):
            if (short):
                rep["added_mass"] = str(self.added_mass.shape)
            else:
                try:
                    rep["added_mass"] = self.added_mass.tolist()
                except:
                    rep["added_mass"] = self.added_mass
        if self.is_set("radiation_damping"):
            if (short):
                rep["radiation_damping"] = str(self.radiation_damping.shape)
            else:
                try:
                    rep["radiation_damping"] = self.radiation_damping.tolist()
                except:
                    rep["radiation_damping"] = self.radiation_damping
        if self.is_set("excitation_force_real"):
            if (short):
                rep["excitation_force_real"] = str(self.excitation_force_real.shape)
            else:
                try:
                    rep["excitation_force_real"] = self.excitation_force_real.tolist()
                except:
                    rep["excitation_force_real"] = self.excitation_force_real
        if self.is_set("excitation_force_imag"):
            if (short):
                rep["excitation_force_imag"] = str(self.excitation_force_imag.shape)
            else:
                try:
                    rep["excitation_force_imag"] = self.excitation_force_imag.tolist()
                except:
                    rep["excitation_force_imag"] = self.excitation_force_imag
        if self.is_set("fitting_damping"):
            if (short):
                rep["fitting_damping"] = str(self.fitting_damping.shape)
            else:
                try:
                    rep["fitting_damping"] = self.fitting_damping.tolist()
                except:
                    rep["fitting_damping"] = self.fitting_damping
        if self.is_set("fitting_stiffness"):
            if (short):
                rep["fitting_stiffness"] = str(self.fitting_stiffness.shape)
            else:
                try:
                    rep["fitting_stiffness"] = self.fitting_stiffness.tolist()
                except:
                    rep["fitting_stiffness"] = self.fitting_stiffness
        if self.is_set("velocity_transformation_matrix"):
            if (short):
                rep["velocity_transformation_matrix"] = str(self.velocity_transformation_matrix.shape)
            else:
                try:
                    rep["velocity_transformation_matrix"] = self.velocity_transformation_matrix.tolist()
                except:
                    rep["velocity_transformation_matrix"] = self.velocity_transformation_matrix
        if self.is_set("tp_capture_width"):
            if (short):
                rep["tp_capture_width"] = str(self.tp_capture_width.shape)
            else:
                try:
                    rep["tp_capture_width"] = self.tp_capture_width.tolist()
                except:
                    rep["tp_capture_width"] = self.tp_capture_width
        if self.is_set("hs_capture_width"):
            if (short):
                rep["hs_capture_width"] = str(self.hs_capture_width.shape)
            else:
                try:
                    rep["hs_capture_width"] = self.hs_capture_width.tolist()
                except:
                    rep["hs_capture_width"] = self.hs_capture_width
        if self.is_set("wave_angle_capture_width"):
            if (short):
                rep["wave_angle_capture_width"] = str(self.wave_angle_capture_width.shape)
            else:
                try:
                    rep["wave_angle_capture_width"] = self.wave_angle_capture_width.tolist()
                except:
                    rep["wave_angle_capture_width"] = self.wave_angle_capture_width
        if self.is_set("pto_damping"):
            if (short):
                rep["pto_damping"] = str(self.pto_damping.shape)
            else:
                try:
                    rep["pto_damping"] = self.pto_damping.tolist()
                except:
                    rep["pto_damping"] = self.pto_damping
        if self.is_set("mooring_stiffness"):
            if (short):
                rep["mooring_stiffness"] = str(self.mooring_stiffness.shape)
            else:
                try:
                    rep["mooring_stiffness"] = self.mooring_stiffness.tolist()
                except:
                    rep["mooring_stiffness"] = self.mooring_stiffness
        if self.is_set("additional_damping"):
            if (short):
                rep["additional_damping"] = str(self.additional_damping.shape)
            else:
                try:
                    rep["additional_damping"] = self.additional_damping.tolist()
                except:
                    rep["additional_damping"] = self.additional_damping
        if self.is_set("additional_stiffness"):
            if (short):
                rep["additional_stiffness"] = str(self.additional_stiffness.shape)
            else:
                try:
                    rep["additional_stiffness"] = self.additional_stiffness.tolist()
                except:
                    rep["additional_stiffness"] = self.additional_stiffness
        if self.is_set("shared_dof"):
            if (short):
                rep["shared_dof"] = str(self.shared_dof.shape)
            else:
                try:
                    rep["shared_dof"] = self.shared_dof.tolist()
                except:
                    rep["shared_dof"] = self.shared_dof
        if self.is_set("hydrostatic_matrix"):
            if (short):
                rep["hydrostatic_matrix"] = str(self.hydrostatic_matrix.shape)
            else:
                try:
                    rep["hydrostatic_matrix"] = self.hydrostatic_matrix.tolist()
                except:
                    rep["hydrostatic_matrix"] = self.hydrostatic_matrix
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["frequency"] = np.array(self.frequency,dtype=float)
        handle["direction"] = np.array(self.direction,dtype=float)
        handle["added_mass"] = np.array(self.added_mass,dtype=float)
        handle["radiation_damping"] = np.array(self.radiation_damping,dtype=float)
        handle["excitation_force_real"] = np.array(self.excitation_force_real,dtype=float)
        handle["excitation_force_imag"] = np.array(self.excitation_force_imag,dtype=float)
        handle["fitting_damping"] = np.array(self.fitting_damping,dtype=float)
        handle["fitting_stiffness"] = np.array(self.fitting_stiffness,dtype=float)
        handle["velocity_transformation_matrix"] = np.array(self.velocity_transformation_matrix,dtype=float)
        handle["tp_capture_width"] = np.array(self.tp_capture_width,dtype=float)
        handle["hs_capture_width"] = np.array(self.hs_capture_width,dtype=float)
        handle["wave_angle_capture_width"] = np.array(self.wave_angle_capture_width,dtype=float)
        handle["pto_damping"] = np.array(self.pto_damping,dtype=float)
        handle["mooring_stiffness"] = np.array(self.mooring_stiffness,dtype=float)
        handle["additional_damping"] = np.array(self.additional_damping,dtype=float)
        handle["additional_stiffness"] = np.array(self.additional_stiffness,dtype=float)
        handle["shared_dof"] = np.array(self.shared_dof,dtype=int)
        handle["hydrostatic_matrix"] = np.array(self.hydrostatic_matrix,dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "frequency"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "direction"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "added_mass"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "radiation_damping"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "excitation_force_real"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "excitation_force_imag"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "fitting_damping"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "fitting_stiffness"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "velocity_transformation_matrix"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "tp_capture_width"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "hs_capture_width"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "wave_angle_capture_width"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "pto_damping"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "mooring_stiffness"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "additional_damping"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "additional_stiffness"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "shared_dof"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "hydrostatic_matrix"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("frequency" in list(gr.keys())):
            self.frequency = gr["frequency"][:]
        if ("direction" in list(gr.keys())):
            self.direction = gr["direction"][:]
        if ("added_mass" in list(gr.keys())):
            self.added_mass = gr["added_mass"][:][:][:]
        if ("radiation_damping" in list(gr.keys())):
            self.radiation_damping = gr["radiation_damping"][:][:][:]
        if ("excitation_force_real" in list(gr.keys())):
            self.excitation_force_real = gr["excitation_force_real"][:][:][:]
        if ("excitation_force_imag" in list(gr.keys())):
            self.excitation_force_imag = gr["excitation_force_imag"][:][:][:]
        if ("fitting_damping" in list(gr.keys())):
            self.fitting_damping = gr["fitting_damping"][:][:][:][:][:]
        if ("fitting_stiffness" in list(gr.keys())):
            self.fitting_stiffness = gr["fitting_stiffness"][:][:][:][:][:]
        if ("velocity_transformation_matrix" in list(gr.keys())):
            self.velocity_transformation_matrix = gr["velocity_transformation_matrix"][:][:]
        if ("tp_capture_width" in list(gr.keys())):
            self.tp_capture_width = gr["tp_capture_width"][:]
        if ("hs_capture_width" in list(gr.keys())):
            self.hs_capture_width = gr["hs_capture_width"][:]
        if ("wave_angle_capture_width" in list(gr.keys())):
            self.wave_angle_capture_width = gr["wave_angle_capture_width"][:]
        if ("pto_damping" in list(gr.keys())):
            self.pto_damping = gr["pto_damping"][:][:][:][:][:]
        if ("mooring_stiffness" in list(gr.keys())):
            self.mooring_stiffness = gr["mooring_stiffness"][:][:][:][:][:]
        if ("additional_damping" in list(gr.keys())):
            self.additional_damping = gr["additional_damping"][:][:][:][:][:]
        if ("additional_stiffness" in list(gr.keys())):
            self.additional_stiffness = gr["additional_stiffness"][:][:][:][:][:]
        if ("shared_dof" in list(gr.keys())):
            self.shared_dof = gr["shared_dof"][:]
        if ("hydrostatic_matrix" in list(gr.keys())):
            self.hydrostatic_matrix = gr["hydrostatic_matrix"][:][:]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
