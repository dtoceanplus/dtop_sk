# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from . import TaggedString
from . import TaggedScalar
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class DRMooringLineSegmentProperties():

    """Mooring line segment property digital representation
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._line_type_name=TaggedString.TaggedString()
        self._line_type_name.description = 'Line type name'
        self._material=TaggedString.TaggedString()
        self._material.description = 'Material of the line'
        self._type=TaggedString.TaggedString()
        self._type.description = 'Type of line: chain, rope, polyester'
        self._quality=TaggedString.TaggedString()
        self._quality.description = 'line quality (used for chain) : R3,R4'
        self._catalogue_id=TaggedString.TaggedString()
        self._catalogue_id.description = 'Catalogue id'
        self._diameter=TaggedScalar.TaggedScalar()
        self._diameter.description = 'Line diameter [m]. For chains: nominal diameter.'
        self._diameter_fls=TaggedScalar.TaggedScalar()
        self._diameter_fls.description = 'Line diameter used for stress computation in FLS analysis [m]. Used to take into account the effect of corrosion. For chains: nominal diameter.'
        self._weight_in_air=TaggedScalar.TaggedScalar()
        self._weight_in_air.description = 'Mass density in air [kg/m]'
        self._weight_in_water=TaggedScalar.TaggedScalar()
        self._weight_in_water.description = 'Mass density in water [kg/m]'
        self._ea=TaggedScalar.TaggedScalar()
        self._ea.description = 'Element axial stiffness [N]'
        self._cb=TaggedScalar.TaggedScalar()
        self._cb.description = 'Cable/seabed friction coefficient [-]'
        self._mbl=TaggedScalar.TaggedScalar()
        self._mbl.description = 'Breaking load [N]'
        self._mbl_uls=TaggedScalar.TaggedScalar()
        self._mbl_uls.description = 'Minimum Breaking load used for ULS analysis[N]. Used to take into account the effect of corrosion.'
        self._cost_per_meter=TaggedScalar.TaggedScalar()
        self._cost_per_meter.description = 'Cost per meter in euros'
        self._ad=TaggedScalar.TaggedScalar()
        self._ad.description = 'intercept parameter of the S-N curve, where the stress is given in [MPa]'
        self._m=TaggedScalar.TaggedScalar()
        self._m.description = 'slope of the S-N curve'
        self._unstretched_length=TaggedScalar.TaggedScalar()
        self._unstretched_length.description = 'Unstretched line length. [m]'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def populate(self,line,line_type):

        self.line_type_name.value=line_type.name
        self.material.value=line_type.material
        self.type.value=line_type.type
        self.quality.value=line_type.quality
        self.catalogue_id.value=line_type.catalogue_id
        self.diameter.value=line_type.diameter
        self.diameter.unit='m'
        self.diameter_fls.value=line_type.diameter_fls
        self.diameter_fls.unit='m'
        self.weight_in_air.value=line_type.weight_in_air
        self.weight_in_air.unit='kg/m'
        self.weight_in_water.value=line_type.weight_in_water
        self.weight_in_water.unit='kg/m'
        self.ea.value=line_type.ea
        self.ea.unit='N'
        self.cb.value=line_type.cb
        self.cb.unit='-'
        self.mbl.value=line_type.mbl
        self.mbl.unit='N'
        self.mbl_uls.value=line_type.mbl_uls
        self.mbl_uls.unit='N'
        self.cost_per_meter.value=line_type.cost_per_meter
        self.cost_per_meter.unit='euros/m'
        self.ad.value=line_type.ad
        self.ad.unit='-'
        self.m.value=line_type.m
        self.m.unit='-'
        self.unstretched_length.value=line.unstretched_length
        self.unstretched_length.unit='m'

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def line_type_name(self): # pragma: no cover
        """:obj:`~.TaggedString.TaggedString`: Line type name
        """
        return self._line_type_name
    #------------
    @ property
    def material(self): # pragma: no cover
        """:obj:`~.TaggedString.TaggedString`: Material of the line
        """
        return self._material
    #------------
    @ property
    def type(self): # pragma: no cover
        """:obj:`~.TaggedString.TaggedString`: Type of line: chain, rope, polyester
        """
        return self._type
    #------------
    @ property
    def quality(self): # pragma: no cover
        """:obj:`~.TaggedString.TaggedString`: line quality (used for chain) : R3,R4
        """
        return self._quality
    #------------
    @ property
    def catalogue_id(self): # pragma: no cover
        """:obj:`~.TaggedString.TaggedString`: Catalogue id
        """
        return self._catalogue_id
    #------------
    @ property
    def diameter(self): # pragma: no cover
        """:obj:`~.TaggedScalar.TaggedScalar`: Line diameter [m]. For chains: nominal diameter.
        """
        return self._diameter
    #------------
    @ property
    def diameter_fls(self): # pragma: no cover
        """:obj:`~.TaggedScalar.TaggedScalar`: Line diameter used for stress computation in FLS analysis [m]. Used to take into account the effect of corrosion. For chains: nominal diameter.
        """
        return self._diameter_fls
    #------------
    @ property
    def weight_in_air(self): # pragma: no cover
        """:obj:`~.TaggedScalar.TaggedScalar`: Mass density in air [kg/m]
        """
        return self._weight_in_air
    #------------
    @ property
    def weight_in_water(self): # pragma: no cover
        """:obj:`~.TaggedScalar.TaggedScalar`: Mass density in water [kg/m]
        """
        return self._weight_in_water
    #------------
    @ property
    def ea(self): # pragma: no cover
        """:obj:`~.TaggedScalar.TaggedScalar`: Element axial stiffness [N]
        """
        return self._ea
    #------------
    @ property
    def cb(self): # pragma: no cover
        """:obj:`~.TaggedScalar.TaggedScalar`: Cable/seabed friction coefficient [-]
        """
        return self._cb
    #------------
    @ property
    def mbl(self): # pragma: no cover
        """:obj:`~.TaggedScalar.TaggedScalar`: Breaking load [N]
        """
        return self._mbl
    #------------
    @ property
    def mbl_uls(self): # pragma: no cover
        """:obj:`~.TaggedScalar.TaggedScalar`: Minimum Breaking load used for ULS analysis[N]. Used to take into account the effect of corrosion.
        """
        return self._mbl_uls
    #------------
    @ property
    def cost_per_meter(self): # pragma: no cover
        """:obj:`~.TaggedScalar.TaggedScalar`: Cost per meter in euros
        """
        return self._cost_per_meter
    #------------
    @ property
    def ad(self): # pragma: no cover
        """:obj:`~.TaggedScalar.TaggedScalar`: intercept parameter of the S-N curve, where the stress is given in [MPa]
        """
        return self._ad
    #------------
    @ property
    def m(self): # pragma: no cover
        """:obj:`~.TaggedScalar.TaggedScalar`: slope of the S-N curve
        """
        return self._m
    #------------
    @ property
    def unstretched_length(self): # pragma: no cover
        """:obj:`~.TaggedScalar.TaggedScalar`: Unstretched line length. [m]
        """
        return self._unstretched_length
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ line_type_name.setter
    def line_type_name(self,val): # pragma: no cover
        self._line_type_name=val
    #------------
    @ material.setter
    def material(self,val): # pragma: no cover
        self._material=val
    #------------
    @ type.setter
    def type(self,val): # pragma: no cover
        self._type=val
    #------------
    @ quality.setter
    def quality(self,val): # pragma: no cover
        self._quality=val
    #------------
    @ catalogue_id.setter
    def catalogue_id(self,val): # pragma: no cover
        self._catalogue_id=val
    #------------
    @ diameter.setter
    def diameter(self,val): # pragma: no cover
        self._diameter=val
    #------------
    @ diameter_fls.setter
    def diameter_fls(self,val): # pragma: no cover
        self._diameter_fls=val
    #------------
    @ weight_in_air.setter
    def weight_in_air(self,val): # pragma: no cover
        self._weight_in_air=val
    #------------
    @ weight_in_water.setter
    def weight_in_water(self,val): # pragma: no cover
        self._weight_in_water=val
    #------------
    @ ea.setter
    def ea(self,val): # pragma: no cover
        self._ea=val
    #------------
    @ cb.setter
    def cb(self,val): # pragma: no cover
        self._cb=val
    #------------
    @ mbl.setter
    def mbl(self,val): # pragma: no cover
        self._mbl=val
    #------------
    @ mbl_uls.setter
    def mbl_uls(self,val): # pragma: no cover
        self._mbl_uls=val
    #------------
    @ cost_per_meter.setter
    def cost_per_meter(self,val): # pragma: no cover
        self._cost_per_meter=val
    #------------
    @ ad.setter
    def ad(self,val): # pragma: no cover
        self._ad=val
    #------------
    @ m.setter
    def m(self,val): # pragma: no cover
        self._m=val
    #------------
    @ unstretched_length.setter
    def unstretched_length(self,val): # pragma: no cover
        self._unstretched_length=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:dr:DRMooringLineSegmentProperties"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:dr:DRMooringLineSegmentProperties"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("line_type_name"):
            if (short and not(deep)):
                rep["line_type_name"] = self.line_type_name.type_rep()
            else:
                rep["line_type_name"] = self.line_type_name.prop_rep(short, deep)
        if self.is_set("material"):
            if (short and not(deep)):
                rep["material"] = self.material.type_rep()
            else:
                rep["material"] = self.material.prop_rep(short, deep)
        if self.is_set("type"):
            if (short and not(deep)):
                rep["type"] = self.type.type_rep()
            else:
                rep["type"] = self.type.prop_rep(short, deep)
        if self.is_set("quality"):
            if (short and not(deep)):
                rep["quality"] = self.quality.type_rep()
            else:
                rep["quality"] = self.quality.prop_rep(short, deep)
        if self.is_set("catalogue_id"):
            if (short and not(deep)):
                rep["catalogue_id"] = self.catalogue_id.type_rep()
            else:
                rep["catalogue_id"] = self.catalogue_id.prop_rep(short, deep)
        if self.is_set("diameter"):
            if (short and not(deep)):
                rep["diameter"] = self.diameter.type_rep()
            else:
                rep["diameter"] = self.diameter.prop_rep(short, deep)
        if self.is_set("diameter_fls"):
            if (short and not(deep)):
                rep["diameter_fls"] = self.diameter_fls.type_rep()
            else:
                rep["diameter_fls"] = self.diameter_fls.prop_rep(short, deep)
        if self.is_set("weight_in_air"):
            if (short and not(deep)):
                rep["weight_in_air"] = self.weight_in_air.type_rep()
            else:
                rep["weight_in_air"] = self.weight_in_air.prop_rep(short, deep)
        if self.is_set("weight_in_water"):
            if (short and not(deep)):
                rep["weight_in_water"] = self.weight_in_water.type_rep()
            else:
                rep["weight_in_water"] = self.weight_in_water.prop_rep(short, deep)
        if self.is_set("ea"):
            if (short and not(deep)):
                rep["ea"] = self.ea.type_rep()
            else:
                rep["ea"] = self.ea.prop_rep(short, deep)
        if self.is_set("cb"):
            if (short and not(deep)):
                rep["cb"] = self.cb.type_rep()
            else:
                rep["cb"] = self.cb.prop_rep(short, deep)
        if self.is_set("mbl"):
            if (short and not(deep)):
                rep["mbl"] = self.mbl.type_rep()
            else:
                rep["mbl"] = self.mbl.prop_rep(short, deep)
        if self.is_set("mbl_uls"):
            if (short and not(deep)):
                rep["mbl_uls"] = self.mbl_uls.type_rep()
            else:
                rep["mbl_uls"] = self.mbl_uls.prop_rep(short, deep)
        if self.is_set("cost_per_meter"):
            if (short and not(deep)):
                rep["cost_per_meter"] = self.cost_per_meter.type_rep()
            else:
                rep["cost_per_meter"] = self.cost_per_meter.prop_rep(short, deep)
        if self.is_set("ad"):
            if (short and not(deep)):
                rep["ad"] = self.ad.type_rep()
            else:
                rep["ad"] = self.ad.prop_rep(short, deep)
        if self.is_set("m"):
            if (short and not(deep)):
                rep["m"] = self.m.type_rep()
            else:
                rep["m"] = self.m.prop_rep(short, deep)
        if self.is_set("unstretched_length"):
            if (short and not(deep)):
                rep["unstretched_length"] = self.unstretched_length.type_rep()
            else:
                rep["unstretched_length"] = self.unstretched_length.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        subgroup = handle.create_group("line_type_name")
        self.line_type_name.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("material")
        self.material.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("type")
        self.type.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("quality")
        self.quality.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("catalogue_id")
        self.catalogue_id.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("diameter")
        self.diameter.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("diameter_fls")
        self.diameter_fls.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("weight_in_air")
        self.weight_in_air.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("weight_in_water")
        self.weight_in_water.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("ea")
        self.ea.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("cb")
        self.cb.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("mbl")
        self.mbl.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("mbl_uls")
        self.mbl_uls.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("cost_per_meter")
        self.cost_per_meter.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("ad")
        self.ad.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("m")
        self.m.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("unstretched_length")
        self.unstretched_length.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "line_type_name"
        try :
            if data[varName] != None:
                self.line_type_name=TaggedString.TaggedString()
                self.line_type_name.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "material"
        try :
            if data[varName] != None:
                self.material=TaggedString.TaggedString()
                self.material.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "type"
        try :
            if data[varName] != None:
                self.type=TaggedString.TaggedString()
                self.type.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "quality"
        try :
            if data[varName] != None:
                self.quality=TaggedString.TaggedString()
                self.quality.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "catalogue_id"
        try :
            if data[varName] != None:
                self.catalogue_id=TaggedString.TaggedString()
                self.catalogue_id.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "diameter"
        try :
            if data[varName] != None:
                self.diameter=TaggedScalar.TaggedScalar()
                self.diameter.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "diameter_fls"
        try :
            if data[varName] != None:
                self.diameter_fls=TaggedScalar.TaggedScalar()
                self.diameter_fls.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "weight_in_air"
        try :
            if data[varName] != None:
                self.weight_in_air=TaggedScalar.TaggedScalar()
                self.weight_in_air.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "weight_in_water"
        try :
            if data[varName] != None:
                self.weight_in_water=TaggedScalar.TaggedScalar()
                self.weight_in_water.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "ea"
        try :
            if data[varName] != None:
                self.ea=TaggedScalar.TaggedScalar()
                self.ea.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "cb"
        try :
            if data[varName] != None:
                self.cb=TaggedScalar.TaggedScalar()
                self.cb.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "mbl"
        try :
            if data[varName] != None:
                self.mbl=TaggedScalar.TaggedScalar()
                self.mbl.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "mbl_uls"
        try :
            if data[varName] != None:
                self.mbl_uls=TaggedScalar.TaggedScalar()
                self.mbl_uls.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "cost_per_meter"
        try :
            if data[varName] != None:
                self.cost_per_meter=TaggedScalar.TaggedScalar()
                self.cost_per_meter.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "ad"
        try :
            if data[varName] != None:
                self.ad=TaggedScalar.TaggedScalar()
                self.ad.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "m"
        try :
            if data[varName] != None:
                self.m=TaggedScalar.TaggedScalar()
                self.m.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "unstretched_length"
        try :
            if data[varName] != None:
                self.unstretched_length=TaggedScalar.TaggedScalar()
                self.unstretched_length.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("line_type_name" in list(gr.keys())):
            subgroup = gr["line_type_name"]
            self.line_type_name.loadFromHDF5Handle(subgroup)
        if ("material" in list(gr.keys())):
            subgroup = gr["material"]
            self.material.loadFromHDF5Handle(subgroup)
        if ("type" in list(gr.keys())):
            subgroup = gr["type"]
            self.type.loadFromHDF5Handle(subgroup)
        if ("quality" in list(gr.keys())):
            subgroup = gr["quality"]
            self.quality.loadFromHDF5Handle(subgroup)
        if ("catalogue_id" in list(gr.keys())):
            subgroup = gr["catalogue_id"]
            self.catalogue_id.loadFromHDF5Handle(subgroup)
        if ("diameter" in list(gr.keys())):
            subgroup = gr["diameter"]
            self.diameter.loadFromHDF5Handle(subgroup)
        if ("diameter_fls" in list(gr.keys())):
            subgroup = gr["diameter_fls"]
            self.diameter_fls.loadFromHDF5Handle(subgroup)
        if ("weight_in_air" in list(gr.keys())):
            subgroup = gr["weight_in_air"]
            self.weight_in_air.loadFromHDF5Handle(subgroup)
        if ("weight_in_water" in list(gr.keys())):
            subgroup = gr["weight_in_water"]
            self.weight_in_water.loadFromHDF5Handle(subgroup)
        if ("ea" in list(gr.keys())):
            subgroup = gr["ea"]
            self.ea.loadFromHDF5Handle(subgroup)
        if ("cb" in list(gr.keys())):
            subgroup = gr["cb"]
            self.cb.loadFromHDF5Handle(subgroup)
        if ("mbl" in list(gr.keys())):
            subgroup = gr["mbl"]
            self.mbl.loadFromHDF5Handle(subgroup)
        if ("mbl_uls" in list(gr.keys())):
            subgroup = gr["mbl_uls"]
            self.mbl_uls.loadFromHDF5Handle(subgroup)
        if ("cost_per_meter" in list(gr.keys())):
            subgroup = gr["cost_per_meter"]
            self.cost_per_meter.loadFromHDF5Handle(subgroup)
        if ("ad" in list(gr.keys())):
            subgroup = gr["ad"]
            self.ad.loadFromHDF5Handle(subgroup)
        if ("m" in list(gr.keys())):
            subgroup = gr["m"]
            self.m.loadFromHDF5Handle(subgroup)
        if ("unstretched_length" in list(gr.keys())):
            subgroup = gr["unstretched_length"]
            self.unstretched_length.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
