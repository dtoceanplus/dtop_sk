# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from . import TaggedString
from . import TaggedScalar
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class DRFoundationProperties():

    """Foundation property digital representation
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._type=TaggedString.TaggedString()
        self._type.description = 'Type of foundation: gravity, suction caisson, drag anchor, pile'
        self._cost=TaggedScalar.TaggedScalar()
        self._cost.description = 'Cost in euros'
        self._mass=TaggedScalar.TaggedScalar()
        self._mass.description = 'Mass in [kg]'
        self._height=TaggedScalar.TaggedScalar()
        self._height.description = 'Height in [m]'
        self._width=TaggedScalar.TaggedScalar()
        self._width.description = 'Width in [m]'
        self._length=TaggedScalar.TaggedScalar()
        self._length.description = 'Width in [m]'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def populate(self,anchor_and_foundation): # input anchor_and_foundation is of type FoundationOutput

        self.type.value = anchor_and_foundation.foundation_type
        self.cost.value = anchor_and_foundation.unit_cost

        if  anchor_and_foundation.foundation_type == 'pile':
            self.height.value= anchor_and_foundation.pile_foundation_design.total_length
            self.width.value= anchor_and_foundation.pile_foundation_design.d_ext
            self.length.value= anchor_and_foundation.pile_foundation_design.d_ext
            self.mass.value= anchor_and_foundation.pile_foundation_design.pile_weight/9.81

        elif  anchor_and_foundation.foundation_type == 'shallow' or anchor_and_foundation.foundation_type =='gravity_anchor':
            self.height.value =  anchor_and_foundation.gravity_foundation_design.thickness
            self.mass.value =  anchor_and_foundation.gravity_foundation_design.found_weight/9.81
            if  anchor_and_foundation.gravity_foundation_design.geometry_pref == 'rectangular':
                self.width.value =  anchor_and_foundation.gravity_foundation_design.base_ly
                self.length.value =  anchor_and_foundation.gravity_foundation_design.base_lx
            elif  anchor_and_foundation.gravity_foundation_design.geometry_pref == 'cylinder':
                self.width.value =  anchor_and_foundation.gravity_foundation_design.base_radius
                self.length.value =  anchor_and_foundation.gravity_foundation_design.base_radius
            elif  anchor_and_foundation.gravity_foundation_design.geometry_pref == 'contact_points':
                r1 =  anchor_and_foundation.gravity_foundation_design.base_r1
                r2 =  anchor_and_foundation.gravity_foundation_design.base_r2
                r3 =  anchor_and_foundation.gravity_foundation_design.base_r3
                self.width.value = 2*max([r1,r2,r3])
                self.length.value = self.width.value
        elif  anchor_and_foundation.foundation_type == 'drag_anchor':
            self.height.value=anchor_and_foundation.drag_anchor_design.dim_ef
            self.width.value=anchor_and_foundation.drag_anchor_design.dim_b
            self.length.value=anchor_and_foundation.drag_anchor_design.dim_a
            self.mass.value=anchor_and_foundation.drag_anchor_design.weight # drag_anchor_design.weight is the mass, not the weight
        elif  anchor_and_foundation.foundation_type == 'suction_caisson':
            self.height.value=anchor_and_foundation.suction_caisson_design.length
            self.width.value=anchor_and_foundation.suction_caisson_design.d_ext
            self.length.value=anchor_and_foundation.suction_caisson_design.d_ext
            self.mass.value=anchor_and_foundation.suction_caisson_design.anchor_mass

        pass

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def type(self): # pragma: no cover
        """:obj:`~.TaggedString.TaggedString`: Type of foundation: gravity, suction caisson, drag anchor, pile
        """
        return self._type
    #------------
    @ property
    def cost(self): # pragma: no cover
        """:obj:`~.TaggedScalar.TaggedScalar`: Cost in euros
        """
        return self._cost
    #------------
    @ property
    def mass(self): # pragma: no cover
        """:obj:`~.TaggedScalar.TaggedScalar`: Mass in [kg]
        """
        return self._mass
    #------------
    @ property
    def height(self): # pragma: no cover
        """:obj:`~.TaggedScalar.TaggedScalar`: Height in [m]
        """
        return self._height
    #------------
    @ property
    def width(self): # pragma: no cover
        """:obj:`~.TaggedScalar.TaggedScalar`: Width in [m]
        """
        return self._width
    #------------
    @ property
    def length(self): # pragma: no cover
        """:obj:`~.TaggedScalar.TaggedScalar`: Width in [m]
        """
        return self._length
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ type.setter
    def type(self,val): # pragma: no cover
        self._type=val
    #------------
    @ cost.setter
    def cost(self,val): # pragma: no cover
        self._cost=val
    #------------
    @ mass.setter
    def mass(self,val): # pragma: no cover
        self._mass=val
    #------------
    @ height.setter
    def height(self,val): # pragma: no cover
        self._height=val
    #------------
    @ width.setter
    def width(self,val): # pragma: no cover
        self._width=val
    #------------
    @ length.setter
    def length(self,val): # pragma: no cover
        self._length=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:dr:DRFoundationProperties"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:dr:DRFoundationProperties"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("type"):
            if (short and not(deep)):
                rep["type"] = self.type.type_rep()
            else:
                rep["type"] = self.type.prop_rep(short, deep)
        if self.is_set("cost"):
            if (short and not(deep)):
                rep["cost"] = self.cost.type_rep()
            else:
                rep["cost"] = self.cost.prop_rep(short, deep)
        if self.is_set("mass"):
            if (short and not(deep)):
                rep["mass"] = self.mass.type_rep()
            else:
                rep["mass"] = self.mass.prop_rep(short, deep)
        if self.is_set("height"):
            if (short and not(deep)):
                rep["height"] = self.height.type_rep()
            else:
                rep["height"] = self.height.prop_rep(short, deep)
        if self.is_set("width"):
            if (short and not(deep)):
                rep["width"] = self.width.type_rep()
            else:
                rep["width"] = self.width.prop_rep(short, deep)
        if self.is_set("length"):
            if (short and not(deep)):
                rep["length"] = self.length.type_rep()
            else:
                rep["length"] = self.length.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        subgroup = handle.create_group("type")
        self.type.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("cost")
        self.cost.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("mass")
        self.mass.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("height")
        self.height.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("width")
        self.width.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("length")
        self.length.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "type"
        try :
            if data[varName] != None:
                self.type=TaggedString.TaggedString()
                self.type.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "cost"
        try :
            if data[varName] != None:
                self.cost=TaggedScalar.TaggedScalar()
                self.cost.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "mass"
        try :
            if data[varName] != None:
                self.mass=TaggedScalar.TaggedScalar()
                self.mass.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "height"
        try :
            if data[varName] != None:
                self.height=TaggedScalar.TaggedScalar()
                self.height.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "width"
        try :
            if data[varName] != None:
                self.width=TaggedScalar.TaggedScalar()
                self.width.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "length"
        try :
            if data[varName] != None:
                self.length=TaggedScalar.TaggedScalar()
                self.length.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("type" in list(gr.keys())):
            subgroup = gr["type"]
            self.type.loadFromHDF5Handle(subgroup)
        if ("cost" in list(gr.keys())):
            subgroup = gr["cost"]
            self.cost.loadFromHDF5Handle(subgroup)
        if ("mass" in list(gr.keys())):
            subgroup = gr["mass"]
            self.mass.loadFromHDF5Handle(subgroup)
        if ("height" in list(gr.keys())):
            subgroup = gr["height"]
            self.height.loadFromHDF5Handle(subgroup)
        if ("width" in list(gr.keys())):
            subgroup = gr["width"]
            self.width.loadFromHDF5Handle(subgroup)
        if ("length" in list(gr.keys())):
            subgroup = gr["length"]
            self.length.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
