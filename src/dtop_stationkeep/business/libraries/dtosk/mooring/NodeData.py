# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class NodeData():

    """data model representing a node in Map++
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._node_number=0.0
        self._type=''
        self._position=np.zeros(shape=(3), dtype=float)
        self._point_mass=0.0
        self._point_volume=0.0
        self._force=np.zeros(shape=(3), dtype=float)
        self._anchor_required=False
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def write_to_string_list(self):

        # Init
        finput = []

        # Populate
        if self.type=='Connect' or self.type=='CONNECT':
            finput.append('{}  {}  {}  {}  {}  {: >8.2f}  {: >8.2f}  {: >8.2f}  {: >8.2f}  {: >8.2f}'.format(int(self.node_number),self.type,str('#')+str(self.position[0]),str('#')+str(self.position[1]),str('#')+str(self.position[2]),self.point_mass,self.point_volume,self.force[0],self.force[1],self.force[2]))
        else:
            finput.append('{}  {}  {: >8.2f}  {: >8.2f}  {: >8.2f}  {: >8.2f}  {: >8.2f}  #  #  #'.format(int(self.node_number),self.type,self.position[0],self.position[1],self.position[2],self.point_mass,self.point_volume))

        return finput
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def node_number(self): # pragma: no cover
        """float: Node number [-] []
        """
        return self._node_number
    #------------
    @ property
    def type(self): # pragma: no cover
        """str: Type of node: Fix, Connect or Vessel. Vessel implies the node motion is prescribed.
        """
        return self._type
    #------------
    @ property
    def position(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Global coordinate if type is fix or connect. [m] Local coordinate if type is vessel. [m] dim(3) []
        """
        return self._position
    #------------
    @ property
    def point_mass(self): # pragma: no cover
        """float: Point mass applied to the node. [kg] []
        """
        return self._point_mass
    #------------
    @ property
    def point_volume(self): # pragma: no cover
        """float: Displaced volume applied to the node. [m^3] []
        """
        return self._point_volume
    #------------
    @ property
    def force(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:If type is connect: external force applied to node. If type is vessel or fix: initial guess to speed convergenc. [N] dim(3) []
        """
        return self._force
    #------------
    @ property
    def anchor_required(self): # pragma: no cover
        """bool: Indicate if the node represents an anchor on the seabed. If true, an anchor will be designed. []
        """
        return self._anchor_required
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ node_number.setter
    def node_number(self,val): # pragma: no cover
        self._node_number=float(val)
    #------------
    @ type.setter
    def type(self,val): # pragma: no cover
        self._type=str(val)
    #------------
    @ position.setter
    def position(self,val): # pragma: no cover
        self._position=val
    #------------
    @ point_mass.setter
    def point_mass(self,val): # pragma: no cover
        self._point_mass=float(val)
    #------------
    @ point_volume.setter
    def point_volume(self,val): # pragma: no cover
        self._point_volume=float(val)
    #------------
    @ force.setter
    def force(self,val): # pragma: no cover
        self._force=val
    #------------
    @ anchor_required.setter
    def anchor_required(self,val): # pragma: no cover
        self._anchor_required=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:mooring:NodeData"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:mooring:NodeData"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("node_number"):
            rep["node_number"] = self.node_number
        if self.is_set("type"):
            rep["type"] = self.type
        if self.is_set("position"):
            if (short):
                rep["position"] = str(self.position.shape)
            else:
                try:
                    rep["position"] = self.position.tolist()
                except:
                    rep["position"] = self.position
        if self.is_set("point_mass"):
            rep["point_mass"] = self.point_mass
        if self.is_set("point_volume"):
            rep["point_volume"] = self.point_volume
        if self.is_set("force"):
            if (short):
                rep["force"] = str(self.force.shape)
            else:
                try:
                    rep["force"] = self.force.tolist()
                except:
                    rep["force"] = self.force
        if self.is_set("anchor_required"):
            rep["anchor_required"] = self.anchor_required
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["node_number"] = np.array([self.node_number],dtype=float)
        ar = []
        ar.append(self.type.encode("ascii"))
        handle["type"] = np.asarray(ar)
        handle["position"] = np.array(self.position,dtype=float)
        handle["point_mass"] = np.array([self.point_mass],dtype=float)
        handle["point_volume"] = np.array([self.point_volume],dtype=float)
        handle["force"] = np.array(self.force,dtype=float)
        handle["anchor_required"] = np.array([self.anchor_required],dtype=bool)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "node_number"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "position"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "point_mass"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "point_volume"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "force"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "anchor_required"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("node_number" in list(gr.keys())):
            self.node_number = gr["node_number"][0]
        if ("type" in list(gr.keys())):
            self.type = gr["type"][0].decode("ascii")
        if ("position" in list(gr.keys())):
            self.position = gr["position"][:]
        if ("point_mass" in list(gr.keys())):
            self.point_mass = gr["point_mass"][0]
        if ("point_volume" in list(gr.keys())):
            self.point_volume = gr["point_volume"][0]
        if ("force" in list(gr.keys())):
            self.force = gr["force"][:]
        if ("anchor_required" in list(gr.keys())):
            self.anchor_required = gr["anchor_required"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
