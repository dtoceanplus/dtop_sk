import collections
import numpy as np
import os
import json
import h5py
from . import LineTypeData
from . import NodeData
from . import LineData
from . import SolverOptions
from . import Ancillaries
#------------------------------------
# @ USER DEFINED IMPORTS START
from ..utilities import PlotlyPlot
from ..utilities import Scatter3D
from ..rotation_module import rotation_about_z
from ..rotation_module import Rab
from ..mooring import SolverOptions
from ..mooring import LineTypeData
from ..mooring import NodeData
from ..mooring import LineData
from  ..inputs.CustomMooringInput import CustomMooringInput
from ..object_list_module import get_object_from_list
import pymap # MAP++ library for python
from ..global_parameters import acceleration_of_gravity
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import copy
import plotly.graph_objects as go
# @ USER DEFINED IMPORTS END
#------------------------------------

class Mooring():

    """data model representing a mooring system in Map++
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._anchor_position_in_n=np.zeros(shape=(1,1), dtype=float)
        self._fairlead_position_in_b=np.zeros(shape=(1,1), dtype=float)
        self._vessel_position=np.zeros(shape=(6), dtype=float)
        self._line_dictionnary=[]
        self._node_properties=[]
        self._line_properties=[]
        self._solver_options=SolverOptions.SolverOptions()
        self._solver_options.description = 'solver options'
        self._check_convergence=False
        self._mooring_radius=0.0
        self._ancillaries=Ancillaries.Ancillaries()
        self._ancillaries.description = 'List of ancillaries'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
        if hasattr(self, 'is_initiated'):
            if self.is_initiated:
                self.map_model.end()
        self.map_model = []
        self.is_initiated=False
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START

    def save_as_custom_mooring_input(self,reference_system,device_position,filePath):

        # Save mooring sytsem as a custom_mooring_input file

        # Translate coordinates if asked by the user
        if reference_system=='device':
            self.translate(-device_position)

        # Populate custom_mooring_input object
        custom_mooring_input = CustomMooringInput()
        custom_mooring_input.name = self.name
        custom_mooring_input.reference_system = reference_system
        custom_mooring_input.line_dictionnary=copy.deepcopy(self.line_dictionnary)
        custom_mooring_input.node_properties=copy.deepcopy(self.node_properties)
        custom_mooring_input.line_properties=copy.deepcopy(self.line_properties)
        custom_mooring_input.solver_options=copy.deepcopy(self.solver_options)

        # Save file
        custom_mooring_input.saveJSON(fileName = filePath)

    def create_from_custom_input_file(self,input_file_path,device_position):

        custom_mooring_input = CustomMooringInput()
        custom_mooring_input.loadJSON(filePath = input_file_path)
        self.create_from_custom_input(custom_mooring_input,device_position)

    def create_from_custom_input(self,custom_mooring_input,device_position):

        # Reset model
        self.__init__()

        # Copy the existing data
        self.line_dictionnary=copy.deepcopy(custom_mooring_input.line_dictionnary)
        self.node_properties=copy.deepcopy(custom_mooring_input.node_properties)
        self.line_properties=copy.deepcopy(custom_mooring_input.line_properties)
        self.solver_options=copy.deepcopy(custom_mooring_input.solver_options)

        # Count anchor points (all nodes of type Fix, although they do not require an anchor)
        n_anchor = 0
        for idx in range(0,len(self.node_properties)):
            if self.node_properties[idx].type == 'Fix':
                n_anchor = n_anchor+1
        self.anchor_position_in_n=np.zeros(shape=(n_anchor,3), dtype=float)
        ianchor = 0
        for idx in range(0,len(self.node_properties)):
            if self.node_properties[idx].type == 'Fix':
                self.anchor_position_in_n[ianchor,0:3] = self.node_properties[idx].position
                ianchor=ianchor+1

        # Count vessels points
        n_fairlead = 0
        for idx in range(0,len(self.node_properties)):
            if self.node_properties[idx].type=='Vessel':
                n_fairlead = n_fairlead+1
        self.fairlead_position_in_b=np.zeros(shape=(n_fairlead,3), dtype=float)
        ifairlead = 0
        for idx in range(0,len(self.node_properties)):
            if self.node_properties[idx].type=='Vessel':
                self.fairlead_position_in_b[ifairlead,0:3] = self.node_properties[idx].position
                ifairlead=ifairlead+1

        # Translate mooring system, if necessary
        if custom_mooring_input.reference_system == 'device':
            self.translate(device_position[0:3])

    def translate(self,vect_in):

        # We force the third component to be zero, so that we translate horizontally
        vect = copy.deepcopy(vect_in)
        vect[2] = 0.0

        # Translate anchor position
        ls = self.anchor_position_in_n.shape
        for idx in range (0,ls[0]):
            self.anchor_position_in_n[idx,0:3] = self.anchor_position_in_n[idx,0:3] + vect

        # Translate nodes that are defined globally (FIX and CONNECT)
        for idx in range(0,len(self.node_properties)):
            if not self.node_properties[idx].type == 'Vessel':
                self.node_properties[idx].position = self.node_properties[idx].position + vect


    def create_from_simple_input_line_type(self,line_type,body_position_in_n,n_bundle,n_line_per_batch,mooring_radius,mooring_ref_angle,mooring_line_length,water_depth,batch_angle,fairlead_radius,fairlead_depth,sea_water_density,vessel_position = None,tag_name = None):

        # Reset model
        self.__init__()

        # Register vessel_position
        if vessel_position!= None:
            self.vessel_position=vessel_position
        else:
            self.vessel_position=body_position_in_n

        # Set name
        if tag_name != None:
            self.name='simple_input' + tag_name
        else:
            self.name='simple_input'

        # Set mooring_radius
        self.mooring_radius = copy.deepcopy(mooring_radius)

        # Register some generic data
        self.n_bundle=n_bundle
        self.n_line_per_bundle=n_line_per_batch

        # Fairlead position and anchor position
        fair_pos_ref = np.array([fairlead_radius, 0., fairlead_depth])     # m, m, m
        anchor_pos_ref = np.array([mooring_radius, 0., -water_depth])      # m, m, m

        # Register line type
        self.line_dictionnary.append(line_type)

        # Construct nodes
        azimuth_bundle_start = 0.0
        counter=0
        line_counter=0
        self.anchor_position_in_n=np.zeros(shape=(n_bundle*n_line_per_batch,3), dtype=float)
        self.fairlead_position_in_b=np.zeros(shape=(n_bundle*n_line_per_batch,3), dtype=float)
        for ibundle in range (0,n_bundle):
            azimuth_bundle_start = mooring_ref_angle + ibundle*2*np.pi/n_bundle - (n_line_per_batch-1)*0.5*batch_angle
            for iline in range(0,n_line_per_batch): # number of lines per batch (2 nodes per line)
                    azimuth_line = azimuth_bundle_start + iline*batch_angle
                    line_counter=line_counter+1

                    # Anchor and fairlead
                    anchor_pos = rotation_about_z(anchor_pos_ref,azimuth_line)
                    fair_pos = rotation_about_z(fair_pos_ref,azimuth_line)
                    idx=line_counter-1
                    self.anchor_position_in_n[idx,0:3]=anchor_pos + body_position_in_n[0:3]
                    self.fairlead_position_in_b[idx,0:3]=fair_pos

                    #Construct anchor node
                    counter=counter+1
                    node=NodeData.NodeData()
                    node.node_number=counter
                    node.type='Fix'
                    node.position=copy.deepcopy(self.anchor_position_in_n[idx,0:3])
                    node.point_mass=0.0
                    node.point_volume=0.0
                    node.force=np.zeros(shape=(3), dtype=float)
                    node.anchor_required = True
                    node.name='node'+str(counter)
                    node.description='node number ' + str(counter)
                    # Register
                    self.node_properties.append(node)

                    #Construct fairlead node
                    counter=counter+1
                    node=NodeData.NodeData()
                    node.node_number=counter
                    node.type='Vessel'
                    node.position=copy.deepcopy(fair_pos)
                    node.point_mass=0.0
                    node.point_volume=0.0
                    node.force=np.zeros(shape=(3), dtype=float)
                    node.anchor_required = False
                    node.name='node'+str(counter)
                    node.description='node number ' + str(counter)
                    # Register
                    self.node_properties.append(node)

         # Construct lines
        counter=0
        for ibundle in range (0,n_bundle):
            for iline in range(0,n_line_per_batch): # number of lines per batch (2 nodes per line)
                counter=counter+1
                line=LineData.LineData()
                line.line_number=counter
                line.line_type_name=line_type.name
                line.line_type_index=0
                line.unstretched_length=mooring_line_length
                line.anchor_node_number=2*counter-1
                line.fairlead_node_number=2*counter
                line.flags=['GX_POS','GY_POS','GZ_POS']
                # Register
                self.line_properties.append(line)

        # Initiate calculation
        self.initiate_calculation(water_depth,sea_water_density,self.vessel_position)

    def create_from_simple_input(self,body_position_in_n,n_bundle,n_line_per_batch,mooring_radius,mooring_ref_angle,mooring_line_length,water_depth,batch_angle,fairlead_radius,fairlead_depth,line_diameter,line_density,line_ea,seabed_friction_coeff,sea_water_density,vessel_position = None,tag_name = None, material_name = None, catalogue_id = None, type_of_line = None, mbl = None):

        # Reset model
        self.__init__()

        # Register vessel_position
        if vessel_position!= None:
            self.vessel_position=vessel_position
        else:
            self.vessel_position=body_position_in_n

        # Set name
        if tag_name != None:
            self.name='simple_input' + tag_name
        else:
            self.name='simple_input'

        # Set mooring_radius
        self.mooring_radius = copy.deepcopy(mooring_radius)

        # Register some generic data
        self.n_bundle=n_bundle
        self.n_line_per_bundle=n_line_per_batch

        # Fairlead position and anchor position
        fair_pos_ref = np.array([fairlead_radius, 0., fairlead_depth])     # m, m, m
        anchor_pos_ref = np.array([mooring_radius, 0., -water_depth])      # m, m, m

        # Construct line type
        line_type=LineTypeData.LineTypeData()
        line_type.line_type_name='line_type1'
        line_type.diameter=line_diameter
        line_type.weight_in_air=line_density
        line_type.weight_in_water=line_density-sea_water_density*np.pi*line_diameter**2/4
        line_type.ea=line_ea
        line_type.cb=seabed_friction_coeff
        line_type.c_int_damp=1.0E8
        line_type.ca=0.6
        line_type.cdn=-1.0
        line_type.cdt=0.05
        line_type.name='line_type1'
        line_type.description=''
        if material_name != None:
            line_type.material = material_name
        if catalogue_id != None:
            line_type.catalogue_id = catalogue_id
        self.line_dictionnary.append(line_type)
        if type_of_line != None:
            line_type.type = type_of_line
        if mbl != None:
            line_type.mbl = mbl

        # Construct nodes
        azimuth_bundle_start = 0.0
        counter=0
        line_counter=0
        self.anchor_position_in_n=np.zeros(shape=(n_bundle*n_line_per_batch,3), dtype=float)
        self.fairlead_position_in_b=np.zeros(shape=(n_bundle*n_line_per_batch,3), dtype=float)
        for ibundle in range (0,n_bundle):
            azimuth_bundle_start = mooring_ref_angle + ibundle*2*np.pi/n_bundle - (n_line_per_batch-1)*0.5*batch_angle
            for iline in range(0,n_line_per_batch): # number of lines per batch (2 nodes per line)
                    azimuth_line = azimuth_bundle_start + iline*batch_angle
                    line_counter=line_counter+1

                    # Anchor and fairlead
                    anchor_pos = rotation_about_z(anchor_pos_ref,azimuth_line)
                    fair_pos = rotation_about_z(fair_pos_ref,azimuth_line)
                    idx=line_counter-1
                    self.anchor_position_in_n[idx,0:3]=anchor_pos + body_position_in_n[0:3]
                    self.fairlead_position_in_b[idx,0:3]=fair_pos

                    #Construct anchor node
                    counter=counter+1
                    node=NodeData.NodeData()
                    node.node_number=counter
                    node.type='Fix'
                    node.position=copy.deepcopy(self.anchor_position_in_n[idx,0:3])
                    node.point_mass=0.0
                    node.point_volume=0.0
                    node.force=np.zeros(shape=(3), dtype=float)
                    node.anchor_required = True
                    node.name='node'+str(counter)
                    node.description='node number ' + str(counter)
                    # Register
                    self.node_properties.append(node)

                    #Construct fairlead node
                    counter=counter+1
                    node=NodeData.NodeData()
                    node.node_number=counter
                    node.type='Vessel'
                    node.position=copy.deepcopy(fair_pos)
                    node.point_mass=0.0
                    node.point_volume=0.0
                    node.force=np.zeros(shape=(3), dtype=float)
                    node.anchor_required = False
                    node.name='node'+str(counter)
                    node.description='node number ' + str(counter)
                    # Register
                    self.node_properties.append(node)

         # Construct lines
        counter=0
        for ibundle in range (0,n_bundle):
            for iline in range(0,n_line_per_batch): # number of lines per batch (2 nodes per line)
                counter=counter+1
                line=LineData.LineData()
                line.line_number=counter
                line.line_type_name='line_type1'
                line.line_type_index=0
                line.unstretched_length=mooring_line_length
                line.anchor_node_number=2*counter-1
                line.fairlead_node_number=2*counter
                line.flags=['GX_POS','GY_POS','GZ_POS']
                # Register
                self.line_properties.append(line)

        # Initiate calculation
        self.initiate_calculation(water_depth,sea_water_density,self.vessel_position)

    def initiate_calculation(self,water_depth,sea_water_density,vessel_position):

        # Initiate calculation
        self.vessel_position=copy.deepcopy(vessel_position)

        # Destroy the instance of pyMap if it exists
        if self.is_initiated:
            self.map_model.end()
            self.map_model = pymap.pyMAP()
            self.is_initiated=False
        else:
            self.map_model = pymap.pyMAP()

        # Write pymap input file to tmp folder
        this_dir = os.path.dirname(os.path.realpath(__file__))
        tmp_dir = os.path.join(this_dir,'..','..','..','..','..','..','tmp')
        if not os.path.exists(tmp_dir):
            try:
                os.mkdir(tmp_dir)
            except OSError:
                raise Exception("Creation of the directory %s failed" % tmp_dir)

        # Write to string list (stored in self.finput)
        self.write_to_string_list()

        # Path to summary file
        summary_file = os.path.join(tmp_dir,'summary_file_' + self.name + '.txt')
        if os.path.exists(summary_file):
            os.remove(summary_file)

        # Read pymap input file
        self.map_model.read_list_input(self.finput)
        self.map_model.summary_file(summary_file)
        self.map_model.map_set_sea_depth(water_depth)     # m
        self.map_model.map_set_gravity(acceleration_of_gravity())        # m/s^2
        self.map_model.map_set_sea_density(sea_water_density)   # kg/m^3
        self.map_model.init()
        pos=self.vessel_position*np.array([1.0,1.0,1.0,180.0/np.pi,180.0/np.pi,180.0/np.pi]) # convert angles from rad to deg
        self.map_model.displace_vessel(*pos)
        self.map_model.update_states(0.0, 0)

        # Update states
        for idx in range(0,self.map_model.size_lines()):
            self.map_model.displace_vessel(*pos)
            self.map_model.update_states(0.0, 0) # I still don't understand why we need to repeat this the first time. If we don't, the mooring system is not correctly initialised in map++

        # Register initiation
        self.is_initiated=True

    def write_to_string_list(self):

        # Init string list
        self.finput = []

        # Line dictionnary
        self.finput.append('---------------------- LINE DICTIONARY -----------------------------------------------------')
        self.finput.append('LineType  Diam   MassDenInAir     EA         CB   CIntDamp           Ca        Cdn        Cdt')
        self.finput.append('(-)       (m)      (kg/m)         (N)        (-)   (Pa-s)            (-)       (-)        (-)')
        for idx in range(0,len(self.line_dictionnary)):
            self.finput.extend(self.line_dictionnary[idx].write_to_string_list())
        # Node properties
        self.finput.append('---------------------- NODE PROPERTIES -----------------------------------------------------')
        self.finput.append('Node      Type       X        Y        Z        M        V        FX       FY      FZ')
        self.finput.append('(-)       (-)       (m)      (m)      (m)      (kg)     (m^3)    (kN)     (kN)    (kN)')
        for idx in range(0,len(self.node_properties)):
            self.finput.extend(self.node_properties[idx].write_to_string_list())
        # Lines
        self.finput.append('---------------------- LINE PROPERTIES -----------------------------------------------------')
        self.finput.append('Line     LineType  UnstrLen   NodeAnch  NodeFair  Flags')
        self.finput.append('(-)      (-)       (m)        (-)       (-)       (-)')
        for idx in range(0,len(self.line_properties)):
            self.finput.extend(self.line_properties[idx].write_to_string_list())
        # Solver options
        self.finput.append('---------------------- SOLVER OPTIONS -----------------------------------------------------')
        self.finput.append('Option')
        self.finput.append('(-)')
        self.finput.extend(self.solver_options.write_to_string_list())

    def write_to_map_input_file(self,filepath_in):

        filepath=os.path.abspath(filepath_in)
        f= open(filepath,mode='+w', encoding='utf-8', newline='\n')
        self.write_to_string_list()
        for item in self.finput:
            f.write("%s\n" % item)
        f.close()


    def identify_mooring_line_hierarchy(self):
        # Identify a mooring line as a set of segment connected together by nodes that are not fairlead
        # Outputs:
        # - ml_of_segment : mooring line number of each line segment
        # - ml_of_node : mooring line number of each node
        # - nml_out : number of mooring line

        # Init
        ml_of_node = np.zeros(shape=(len(self.node_properties)), dtype=int) # Describe to which mooring line a node belongs to
        nml = 0 # number of identified mooring line

        for idx in range(0,len(self.node_properties)):

            if self.node_properties[idx].type == 'Connect':

                if (ml_of_node[idx] == 0):
                    nml = nml + 1
                    ml_of_node[idx] = nml

                for iseg in range(0,len(self.line_properties)):

                    # Check if segment is connected to the current node, if so, select the other node
                    if (self.line_properties[iseg].anchor_node_number == idx+1):
                        idx_node2 = self.line_properties[iseg].fairlead_node_number-1
                        if self.node_properties[idx_node2].type != 'Connect':
                            idx_node2 = None
                    elif self.line_properties[iseg].fairlead_node_number == idx+1:
                        idx_node2 = self.line_properties[iseg].anchor_node_number-1
                        if self.node_properties[idx_node2].type != 'Connect':
                            idx_node2 = None
                    else:
                        idx_node2 = None

                    # If the current segment is connected to the node, set the mooring line number
                    if idx_node2 != None:
                        if ml_of_node[idx_node2] == 0: # node2 does not have an associated mooring line yet
                            ml_of_node[idx_node2] = ml_of_node[idx]
                        else:
                            if ml_of_node[idx_node2] != ml_of_node[idx]:# node2 already has an associated mooring line
                                ml_of_node[ml_of_node == ml_of_node[idx]] = ml_of_node[idx_node2]

        # Identify the mooring line of each line segment
        ml_of_segment = np.zeros(shape=(len(self.line_properties)), dtype=int) # Describe to which mooring line a segment belongs to
        for idx in range(0,len(ml_of_segment)):
            if ml_of_node[self.line_properties[idx].fairlead_node_number-1]!=0:
                ml_of_segment[idx] = ml_of_node[self.line_properties[idx].fairlead_node_number-1]
            elif ml_of_node[self.line_properties[idx].anchor_node_number-1]!=0:
                ml_of_segment[idx] = ml_of_node[self.line_properties[idx].anchor_node_number-1]
            else:
                # If both ends are Vessel and/or Anchor, identify the line segment as a mooringline itself
                nml = nml + 1
                ml_of_segment[idx] = nml
                ml_of_node[self.line_properties[idx].fairlead_node_number-1] = nml
                ml_of_node[self.line_properties[idx].anchor_node_number-1] = nml

        # Unicity of the mooring line number for each segment
        ml_list = np.unique(ml_of_segment)
        nml_out = len(ml_list)
        for idx in range(0,len(ml_list)):
            ml_of_segment[ml_of_segment == ml_list[idx]] = idx+1

        # Unicity of the mooring line number for each node
        ml_list = np.unique(ml_of_node)
        nml_out = len(ml_list)
        for idx in range(0,len(ml_list)):
            ml_of_node[ml_of_node == ml_list[idx]] = idx+1

        return ml_of_segment, ml_of_node, nml_out

    def plot_mooring_system(self,fig_idx = None):
        # Apply new position in MAP++
        # Plot the mooring profile
        if fig_idx == None:
            fig_idx = 1
        fig = plt.figure(fig_idx)
        ax = Axes3D(fig)

        # Plot mooring lines
        [xlma, xlmi, ylma, ylmi, zlma, zlmi] = self.plot_mooring_lines(ax)

        # Create cubic bounding box to simulate equal aspect ratio
        max_range = np.array([xlma.max()-xlmi.min(), ylma.max()-ylmi.min(), zlma.max()-zlmi.min()]).max()
        Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() + 0.5*(xlma.max()+xlmi.min())
        Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten() + 0.5*(ylma.max()+ylmi.min())
        Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten() + 0.5*(zlma.max()+zlmi.min())
        # Comment or uncomment following both lines to test the fake bounding box:
        for xb, yb, zb in zip(Xb, Yb, Zb):
            ax.plot([xb], [yb], [zb], 'w')

        # Plot free water surface
        x = np.linspace(xlmi.min(),xlma.max(),10)
        y = np.linspace(ylmi.min(),ylma.max(),10)
        X,Y = np.meshgrid(x,y)
        Z = 0.0*X
        ax.plot_surface( X, Y, Z, rstride=10, cstride=10,cmap='jet',alpha=0.5,linewidth=0,antialiased=True,vmin=0)

        plt.show()

    def plot_html(self,filename = None,show=True,logo=None):

        # Configure plot object
        scene=dict(camera=dict(eye=dict(x=1.25, y=1.25, z=1.25)), #the default values are 1.25, 1.25, 1.25
           aspectmode='data', #this string can be 'data', 'cube', 'auto', 'manual'
           #a custom aspectratio is defined as follows:
           #aspectratio=dict(x=1, y=1, z=0.95)
           )
        layout = go.Layout(scene = scene)
        fig = go.Figure(layout=layout)

        # Plot mooring lines
        for i in range( self.map_model.size_lines() ):
            x = self.map_model.plot_x( i, 20 ) # i is the the line number
            y = self.map_model.plot_y( i, 20)  # 20 is the number of points plotted on the line
            z = self.map_model.plot_z( i, 20)
            fig.add_trace(go.Scatter3d(x=x, y=y, z=z, mode='lines',name=('Line no. ' + str(self.line_properties[i].line_number))))


        # Plot fairleads
        lshape = self.fairlead_position_in_b.shape
        fairlead_pos_in_n=np.zeros(shape=(lshape[0],3), dtype=float)
        for idx in range(0,lshape[0]):
            fairlead_pos_in_n[idx,0:3]= self.vessel_position[0:3] + Rab(self.vessel_position[3:6]).dot(self.fairlead_position_in_b[idx,0:3].T).T
        fig.add_trace(go.Scatter3d(x=fairlead_pos_in_n[:,0], y=fairlead_pos_in_n[:,1], z=fairlead_pos_in_n[:,2] ,mode='markers',name='Fairleads'))

        # Add image
        from PIL import Image
        if not logo is None:
            img = Image.open(logo)
            fig.add_layout_image(
                dict(
                    source=img,
                    xref="paper", yref="paper",
                    x=0, y=0.95,
                    sizex=0.5, sizey=0.2,
                    xanchor="left", yanchor="bottom"
                )
            )

        # Update layout properties
        fig.update_layout(
            autosize=False,
            height=800,
            width=1500,
            bargap=0.15,
            bargroupgap=0.1,
            barmode="stack",
            hovermode="x",
            margin=dict(r=20, l=300, b=75, t=125),
            title=("Mooring system visualization<br>"),
            title_x=0.6,
            title_y=0.85
        )

        # Show/save plot
        if show:
            fig.show()
        if not filename is None:
            fig.write_html(filename)

    def generate_plotly_plot_representation(self):

        # Init
        my_plot = PlotlyPlot.PlotlyPlot()
        my_plot.title = 'Mooring system'

        # Plot of mooring lines
        for i in range( self.map_model.size_lines() ):
            scat = Scatter3D.Scatter3D()
            scat.x = self.map_model.plot_x( i, 20 ) # i is the the line number
            scat.y = self.map_model.plot_y( i, 20)  # 20 is the number of points plotted on the line
            scat.z = self.map_model.plot_z( i, 20)
            scat.mode = 'lines'
            scat.name = 'Line no. ' + str(self.line_properties[i].line_number)
            my_plot.scatter3d.append(scat)

        # Plot fairleads
        scat = Scatter3D.Scatter3D()
        lshape = self.fairlead_position_in_b.shape
        fairlead_pos_in_n=np.zeros(shape=(lshape[0],3), dtype=float)
        for idx in range(0,lshape[0]):
            fairlead_pos_in_n[idx,0:3]= self.vessel_position[0:3] + Rab(self.vessel_position[3:6]).dot(self.fairlead_position_in_b[idx,0:3].T).T
        scat.x = fairlead_pos_in_n[:,0]
        scat.y = fairlead_pos_in_n[:,1]
        scat.z = fairlead_pos_in_n[:,2]
        scat.mode = 'markers'
        scat.name = 'Fairleads'
        my_plot.scatter3d.append(scat)

        return my_plot

    def get_nlines(self):

        nlines = self.map_model.size_lines()

        return nlines

    def compare_fairlead_and_topline_position(self):

        fairlead_pos_in_n=np.zeros(shape=(self.map_model.size_lines(),3), dtype=float)
        lshape = self.fairlead_position_in_b.shape

        # Compute fairlead position
        for idx in range(0,lshape[0]):
            fairlead_pos_in_n[idx,0:3]= self.vessel_position[0:3] + Rab(self.vessel_position[3:6]).dot(self.fairlead_position_in_b[idx,0:3].T).T

        # Compute topline position and check against fairlead
        n=20
        idx = -1
        for i in range( self.map_model.size_lines() ):
            success_flag = True
            if self.node_properties[self.line_properties[i].fairlead_node_number-1].type == 'Vessel' or self.node_properties[self.line_properties[i].anchor_node_number-1].type == 'Vessel' :
                idx = idx+1
                x = self.map_model.plot_x( i, n) # i is the the line number
                y = self.map_model.plot_y( i, n)  # 20 is the number of points plotted on the line
                z = self.map_model.plot_z( i, n)
                p1 = [x[n-1],y[n-1],z[n-1]]
                p2 = [x[0],y[0],z[0]]
                eps = min(np.linalg.norm(p1-fairlead_pos_in_n[idx,0:3]),np.linalg.norm(p2-fairlead_pos_in_n[idx,0:3]))
                if eps<1.0:
                    success_flag = True
                else:
                    success_flag = False

        return success_flag



    def plot_mooring_lines(self,ax):
        # Fairleads
        fairlead_pos_in_n=np.zeros(shape=(self.map_model.size_lines(),3), dtype=float)
        lshape = self.fairlead_position_in_b.shape
        for idx in range(0,lshape[0]):
            fairlead_pos_in_n[idx,0:3]= self.vessel_position[0:3] + Rab(self.vessel_position[3:6]).dot(self.fairlead_position_in_b[idx,0:3].T).T
        if lshape[0]>0:
            ax.plot( *np.vstack((fairlead_pos_in_n,fairlead_pos_in_n)).T, 'k-', lw=3 )
        colors = ['b','g','r','c','y','m']*1000
        xlma=np.zeros(shape=(self.map_model.size_lines()),dtype=float)
        ylma=np.zeros(shape=(self.map_model.size_lines()),dtype=float)
        zlma=np.zeros(shape=(self.map_model.size_lines()),dtype=float)
        xlmi=np.zeros(shape=(self.map_model.size_lines()),dtype=float)
        ylmi=np.zeros(shape=(self.map_model.size_lines()),dtype=float)
        zlmi=np.zeros(shape=(self.map_model.size_lines()),dtype=float)
        for i in range( self.map_model.size_lines() ):
            x = self.map_model.plot_x( i, 20 ) # i is the the line number
            y = self.map_model.plot_y( i, 20)  # 20 is the number of points plotted on the line
            z = self.map_model.plot_z( i, 20)
            ax.plot( x, y, z, colors[i]+'-')
            ax.plot( fairlead_pos_in_n[i:i+1,0], fairlead_pos_in_n[i:i+1,1], fairlead_pos_in_n[i:i+1,2], colors[i]+'o', ms=3 )
            xlma[i]=(np.array(x)).max()
            ylma[i]=(np.array(y)).max()
            zlma[i]=(np.array(z)).max()
            xlmi[i]=(np.array(x)).min()
            ylmi[i]=(np.array(y)).min()
            zlmi[i]=(np.array(z)).min()
        ax.set_xlabel('X [m]')
        ax.set_ylabel('Y [m]')
        ax.set_zlabel('Z [m]')

        return xlma, xlmi, ylma, ylmi, zlma, zlmi

    def get_line_segment_data_for_hierarchy(self,iseg):

        design_id = self.line_properties[iseg].name
        length = self.line_properties[iseg].unstretched_length

        # From line type data
        idxt = self.line_properties[iseg].line_type_index
        line_type = self.line_dictionnary[idxt]
        material = line_type.material
        catalogue_id = line_type.catalogue_id
        total_mass = line_type.weight_in_air * length
        diameter = line_type.diameter

        # From logic in mooring:
        upstream_id=[]
        downstream_id=[]
        for idx in range(0,len(self.line_properties)):
            if idx != iseg:
                if ((self.line_properties[idx].anchor_node_number == self.line_properties[iseg].anchor_node_number) or (self.line_properties[idx].fairlead_node_number == self.line_properties[iseg].anchor_node_number)):
                    downstream_id.append(self.line_properties[idx].name)
                elif ((self.line_properties[idx].anchor_node_number == self.line_properties[iseg].fairlead_node_number) or (self.line_properties[idx].fairlead_node_number == self.line_properties[iseg].fairlead_node_number)):
                    upstream_id.append(self.line_properties[idx].name)

        # If upstream or downstream is empty, write 'NA'
        if not upstream_id:
            upstream_id=['NA']
        if not downstream_id:
            downstream_id=['NA']

        # Cost
        line_type = self.line_dictionnary[self.line_properties[iseg].line_type_index]
        cost = self.line_properties[iseg].unstretched_length*line_type.cost_per_meter

        return design_id,catalogue_id,material,length,total_mass,diameter,cost,upstream_id,downstream_id

    def solve_mooring_system(self,vessel_position):

        # Move the vessel and update mooring system
        self.vessel_position=copy.deepcopy(vessel_position)
        pos=self.vessel_position*np.array([1,1,1,180/np.pi,180/np.pi,180/np.pi]) # convert angles from rad to deg
        self.map_model.displace_vessel(*pos)
        self.map_model.update_states(0.0, 0)


    def compute_force(self,vessel_position,rotation_center_in_b):
        """
        Move the floater according to position;
        Compute the resultant mooring forces.
        """

        self.solve_mooring_system(vessel_position)

        # Retrieve 3D tensions at fairlead for each lines
        mooring_force_in_b = np.array([0.0,0.0,0.0,0.0,0.0,0.0])
        Rbn=Rab(self.vessel_position[3:6])

        for idx in range(self.map_model.size_lines()):

            if self.node_properties[self.line_properties[idx].anchor_node_number-1].type == 'Vessel':
                fx, fy, fz = self.map_model.get_anchor_force_3d(idx)
                forces_in_b= ((Rbn.T).dot(-np.array([fx, fy, fz]).T)).T
                moments_in_b = np.cross(self.node_properties[self.line_properties[idx].anchor_node_number-1].position - rotation_center_in_b,forces_in_b)
                mooring_force_in_b[0:3]=mooring_force_in_b[0:3]+forces_in_b[0:3]
                mooring_force_in_b[3:6]=mooring_force_in_b[3:6]+moments_in_b[0:3]

            if self.node_properties[self.line_properties[idx].fairlead_node_number-1].type == 'Vessel':
                fx, fy, fz = self.map_model.get_fairlead_force_3d(idx)
                forces_in_b= ((Rbn.T).dot(-np.array([fx, fy, fz]).T)).T
                moments_in_b = np.cross(self.node_properties[self.line_properties[idx].fairlead_node_number-1].position - rotation_center_in_b,forces_in_b)
                mooring_force_in_b[0:3]=mooring_force_in_b[0:3]+forces_in_b[0:3]
                mooring_force_in_b[3:6]=mooring_force_in_b[3:6]+moments_in_b[0:3]

            flag = self.check_converge_reason(idx)
            if not flag:
                print('**************WARNING23***********')
                fx = 0.0
                fy = 0.0
                fz = 0.0

        #self.compare_fairlead_and_topline_position()

        # Return total loads
        return mooring_force_in_b

    def get_tension_at_fairlead(self,line_index):

        if line_index<self.map_model.size_lines():
            fx, fy, fz = self.map_model.get_fairlead_force_3d(line_index)
            flag = self.check_converge_reason(line_index)
            if flag:
                tension = np.linalg.norm(np.array([fx,fy,fz]))
            else:
                print('**********WARNING*******')
                tension = 0.0
        else:
            raise Exception("Error in get_tension_at_fairlead: line_index cannot be larger than the number of lines: " + str(line_index))

        # Return total loads
        return tension

    def get_tension_at_anchor(self,line_index):

        if line_index<self.map_model.size_lines():
            fx, fy, fz = self.map_model.get_anchor_force_3d(line_index)
            flag = self.check_converge_reason(line_index)
            if flag:
                tension = np.linalg.norm(np.array([fx,fy,fz]))
            else:
                print('**********WARNING*******')
                tension = 0.0
        else:
            raise Exception("Error in get_tension_at_fairlead: line_index cannot be larger than the number of lines: " + str(line_index))

        # Return total loads
        return tension

    def get_fairlead_force_in_n(self,line_index):

        force_in_n = np.zeros(shape=(3), dtype=float)

        if line_index<self.map_model.size_lines():
            fx, fy, fz = self.map_model.get_fairlead_force_3d(line_index)
            flag = self.check_converge_reason(line_index)
            if flag:
                force_in_n[0] = fx
                force_in_n[1] = fy
                force_in_n[2] = fz
            else:
                print('**********WARNING*******')

                force_in_n[0] = 0.0
                force_in_n[1] = 0.0
                force_in_n[2] = 0.0
        else:
            raise Exception("Error in get_anchor_force_in_n: line_index cannot be larger than the number of lines: " + str(line_index))

        # Return total loads
        return force_in_n

    def get_anchor_force_in_n(self,line_index):

        force_in_n = np.zeros(shape=(3), dtype=float)

        if line_index<self.map_model.size_lines():
            fx, fy, fz = self.map_model.get_anchor_force_3d(line_index)
            flag = self.check_converge_reason(line_index)
            if flag:
                force_in_n[0] = fx
                force_in_n[1] = fy
                force_in_n[2] = fz
            else:
                print('**********WARNING*******')
                force_in_n[0] = 0.0
                force_in_n[1] = 0.0
                force_in_n[2] = 0.0
        else:
            raise Exception("Error in get_anchor_force_in_n: line_index cannot be larger than the number of lines: " + str(line_index))

        # Return total loads
        return force_in_n

    def check_converge_reason(self,line_index):
        flag = True
        info = self.get_line_converge_reason(line_index)
        #print('info' + str(info))
        if info in [5,6,7,8]:
            flag = False
            print('info ' + str(info))
            if self.check_convergence:
                raise Exception("Error from map++ module: solver calculation did not converge for line segment no. " + str(line_index) + ". Error code = " + str(info))
        return flag


    def get_line_geometry_data(self,line_index):

        psi = 0.0
        Lb = 0.0
        alpha = 0.0
        alpha_anchor = 0.0

        if line_index<self.map_model.size_lines():
            [psi,Lb,alpha,alpha_anchor] = self.map_model.get_line_geometry_data(line_index)
        else:
            raise Exception("Error in get_line_geometry_data: line_index cannot be larger than the number of lines: " + str(line_index))

        return psi,Lb,alpha,alpha_anchor

    def get_line_converge_reason(self,line_index):

        info = 0

        if line_index<self.map_model.size_lines():
            info = int(self.map_model.get_line_converge_reason(line_index))
        else:
            raise Exception("Error in get_line_converge_reason: line_index cannot be larger than the number of lines: " + str(line_index))

        if not info in [0,1,2,3,4,5,6,7,8]:
            info = 0

        return info

    def compute_linear_stiffness(self,vessel_position):

        # Move the vessel and update mooring system
        self.vessel_position=copy.deepcopy(vessel_position)
        pos=self.vessel_position*np.array([1,1,1,180/np.pi,180/np.pi,180/np.pi]) # convert angles from rad to deg
        self.map_model.displace_vessel(*pos)
        self.map_model.update_states(0.0, 0)

        # Compute Mooring stiffness
        epsilon = 1e-1 # finite difference epsilon
        mooring_stiffness_in_n = np.array( self.map_model.linear( epsilon ) )

        # Return Mooring stiffness
        return mooring_stiffness_in_n

    def get_type_of_line(self,iseg):

        idx = self.line_properties[iseg].line_type_index
        type_of_line = self.line_dictionnary[idx]

        return type_of_line

    def get_tension(self):

        # Init
        mooring_tension = np.zeros(shape=(len(self.line_properties),2), dtype=float)

        # Populate the tension in each line segment, for anchor and fairlead point
        for idx in range(0,len(self.line_properties)):
            mooring_tension[idx,0] = self.get_tension_at_anchor(idx)
            mooring_tension[idx,1] = self.get_tension_at_fairlead(idx)

        return mooring_tension

    def get_SN_curve_parameters(self,iline):

        # Note: for definition of ad and m, see DNVGL-OS-E301, section 6.2.1
        idtyp = self.line_properties[iline].line_type_index
        if self.line_dictionnary[idtyp].material == 'steel':
            # ad and m are already representing the SN-curve
            ad = self.line_dictionnary[idtyp].ad
            m = self.line_dictionnary[idtyp].m
        else:
            # ad and m represents the RN curve. We need to transform them to SN-curve parameters
            m = self.line_dictionnary[idtyp].m
            mbl = self.line_dictionnary[idtyp].mbl
            [cross_section_area_fls, diameter_fls] = self.line_dictionnary[idtyp].compute_fls_area()
            mbl_in_mpa = mbl/1000000
            ad = self.line_dictionnary[idtyp].ad * (mbl_in_mpa/cross_section_area_fls)**m

        return ad,m


# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def anchor_position_in_n(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:anchor positions in n , dim(number_line_anchored_on_the_seabed,3) dim(*,*) []
        """
        return self._anchor_position_in_n
    #------------
    @ property
    def fairlead_position_in_b(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:fairlead position in b, dim(number_line_fixed_on_the_vessel,3) dim(*,*) []
        """
        return self._fairlead_position_in_b
    #------------
    @ property
    def vessel_position(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Position of the vessel in n dim(6) []
        """
        return self._vessel_position
    #------------
    @ property
    def line_dictionnary(self): # pragma: no cover
        """:obj:`list` of :obj:`~.LineTypeData.LineTypeData`: dictionnary documenting line type
        """
        return self._line_dictionnary
    #------------
    @ property
    def node_properties(self): # pragma: no cover
        """:obj:`list` of :obj:`~.NodeData.NodeData`: list of nodes
        """
        return self._node_properties
    #------------
    @ property
    def line_properties(self): # pragma: no cover
        """:obj:`list` of :obj:`~.LineData.LineData`: list of lines
        """
        return self._line_properties
    #------------
    @ property
    def solver_options(self): # pragma: no cover
        """:obj:`~.SolverOptions.SolverOptions`: solver options
        """
        return self._solver_options
    #------------
    @ property
    def check_convergence(self): # pragma: no cover
        """bool: Flag for checking convergence of line segment calculation []
        """
        return self._check_convergence
    #------------
    @ property
    def mooring_radius(self): # pragma: no cover
        """float: Mooring radius [m] []
        """
        return self._mooring_radius
    #------------
    @ property
    def ancillaries(self): # pragma: no cover
        """:obj:`~.Ancillaries.Ancillaries`: List of ancillaries
        """
        return self._ancillaries
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ anchor_position_in_n.setter
    def anchor_position_in_n(self,val): # pragma: no cover
        self._anchor_position_in_n=val
    #------------
    @ fairlead_position_in_b.setter
    def fairlead_position_in_b(self,val): # pragma: no cover
        self._fairlead_position_in_b=val
    #------------
    @ vessel_position.setter
    def vessel_position(self,val): # pragma: no cover
        self._vessel_position=val
    #------------
    @ line_dictionnary.setter
    def line_dictionnary(self,val): # pragma: no cover
        self._line_dictionnary=val
    #------------
    @ node_properties.setter
    def node_properties(self,val): # pragma: no cover
        self._node_properties=val
    #------------
    @ line_properties.setter
    def line_properties(self,val): # pragma: no cover
        self._line_properties=val
    #------------
    @ solver_options.setter
    def solver_options(self,val): # pragma: no cover
        self._solver_options=val
    #------------
    @ check_convergence.setter
    def check_convergence(self,val): # pragma: no cover
        self._check_convergence=val
    #------------
    @ mooring_radius.setter
    def mooring_radius(self,val): # pragma: no cover
        self._mooring_radius=float(val)
    #------------
    @ ancillaries.setter
    def ancillaries(self,val): # pragma: no cover
        self._ancillaries=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:mooring:Mooring"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:mooring:Mooring"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("anchor_position_in_n"):
            if (short):
                rep["anchor_position_in_n"] = str(self.anchor_position_in_n.shape)
            else:
                try:
                    rep["anchor_position_in_n"] = self.anchor_position_in_n.tolist()
                except:
                    rep["anchor_position_in_n"] = self.anchor_position_in_n
        if self.is_set("fairlead_position_in_b"):
            if (short):
                rep["fairlead_position_in_b"] = str(self.fairlead_position_in_b.shape)
            else:
                try:
                    rep["fairlead_position_in_b"] = self.fairlead_position_in_b.tolist()
                except:
                    rep["fairlead_position_in_b"] = self.fairlead_position_in_b
        if self.is_set("vessel_position"):
            if (short):
                rep["vessel_position"] = str(self.vessel_position.shape)
            else:
                try:
                    rep["vessel_position"] = self.vessel_position.tolist()
                except:
                    rep["vessel_position"] = self.vessel_position
        if self.is_set("line_dictionnary"):
            rep["line_dictionnary"] = []
            for i in range(0,len(self.line_dictionnary)):
                if (short and not(deep)):
                    itemType = self.line_dictionnary[i].type_rep()
                    rep["line_dictionnary"].append( itemType )
                else:
                    rep["line_dictionnary"].append( self.line_dictionnary[i].prop_rep(short, deep) )
        else:
            rep["line_dictionnary"] = []
        if self.is_set("node_properties"):
            rep["node_properties"] = []
            for i in range(0,len(self.node_properties)):
                if (short and not(deep)):
                    itemType = self.node_properties[i].type_rep()
                    rep["node_properties"].append( itemType )
                else:
                    rep["node_properties"].append( self.node_properties[i].prop_rep(short, deep) )
        else:
            rep["node_properties"] = []
        if self.is_set("line_properties"):
            rep["line_properties"] = []
            for i in range(0,len(self.line_properties)):
                if (short and not(deep)):
                    itemType = self.line_properties[i].type_rep()
                    rep["line_properties"].append( itemType )
                else:
                    rep["line_properties"].append( self.line_properties[i].prop_rep(short, deep) )
        else:
            rep["line_properties"] = []
        if self.is_set("solver_options"):
            if (short and not(deep)):
                rep["solver_options"] = self.solver_options.type_rep()
            else:
                rep["solver_options"] = self.solver_options.prop_rep(short, deep)
        if self.is_set("check_convergence"):
            rep["check_convergence"] = self.check_convergence
        if self.is_set("mooring_radius"):
            rep["mooring_radius"] = self.mooring_radius
        if self.is_set("ancillaries"):
            if (short and not(deep)):
                rep["ancillaries"] = self.ancillaries.type_rep()
            else:
                rep["ancillaries"] = self.ancillaries.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["anchor_position_in_n"] = np.array(self.anchor_position_in_n,dtype=float)
        handle["fairlead_position_in_b"] = np.array(self.fairlead_position_in_b,dtype=float)
        handle["vessel_position"] = np.array(self.vessel_position,dtype=float)
        subgroup = handle.create_group("line_dictionnary")
        for idx in range(0,len(self.line_dictionnary)):
            subgroup1 = subgroup.create_group("line_dictionnary" + "_" + str(idx))
            self.line_dictionnary[idx].saveToHDF5Handle(subgroup1)
        subgroup = handle.create_group("node_properties")
        for idx in range(0,len(self.node_properties)):
            subgroup1 = subgroup.create_group("node_properties" + "_" + str(idx))
            self.node_properties[idx].saveToHDF5Handle(subgroup1)
        subgroup = handle.create_group("line_properties")
        for idx in range(0,len(self.line_properties)):
            subgroup1 = subgroup.create_group("line_properties" + "_" + str(idx))
            self.line_properties[idx].saveToHDF5Handle(subgroup1)
        subgroup = handle.create_group("solver_options")
        self.solver_options.saveToHDF5Handle(subgroup)
        handle["check_convergence"] = np.array([self.check_convergence],dtype=bool)
        handle["mooring_radius"] = np.array([self.mooring_radius],dtype=float)
        subgroup = handle.create_group("ancillaries")
        self.ancillaries.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "anchor_position_in_n"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "fairlead_position_in_b"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "vessel_position"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "line_dictionnary"
        try :
            if data[varName] != None:
                self.line_dictionnary=[]
                for i in range(0,len(data[varName])):
                    self.line_dictionnary.append(LineTypeData.LineTypeData())
                    self.line_dictionnary[i].loadFromJSONDict(data[varName][i])
            else:
                self.line_dictionnary = []
        except :
            pass
        varName = "node_properties"
        try :
            if data[varName] != None:
                self.node_properties=[]
                for i in range(0,len(data[varName])):
                    self.node_properties.append(NodeData.NodeData())
                    self.node_properties[i].loadFromJSONDict(data[varName][i])
            else:
                self.node_properties = []
        except :
            pass
        varName = "line_properties"
        try :
            if data[varName] != None:
                self.line_properties=[]
                for i in range(0,len(data[varName])):
                    self.line_properties.append(LineData.LineData())
                    self.line_properties[i].loadFromJSONDict(data[varName][i])
            else:
                self.line_properties = []
        except :
            pass
        varName = "solver_options"
        try :
            if data[varName] != None:
                self.solver_options=SolverOptions.SolverOptions()
                self.solver_options.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "check_convergence"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mooring_radius"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "ancillaries"
        try :
            if data[varName] != None:
                self.ancillaries=Ancillaries.Ancillaries()
                self.ancillaries.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("anchor_position_in_n" in list(gr.keys())):
            self.anchor_position_in_n = gr["anchor_position_in_n"][:][:]
        if ("fairlead_position_in_b" in list(gr.keys())):
            self.fairlead_position_in_b = gr["fairlead_position_in_b"][:][:]
        if ("vessel_position" in list(gr.keys())):
            self.vessel_position = gr["vessel_position"][:]
        if ("line_dictionnary" in list(gr.keys())):
            subgroup = gr["line_dictionnary"]
            self.line_dictionnary=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["line_dictionnary"+"_"+str(idx)]
                self.line_dictionnary.append(LineTypeData.LineTypeData())
                self.line_dictionnary[idx].loadFromHDF5Handle(ssubgroup)
        if ("node_properties" in list(gr.keys())):
            subgroup = gr["node_properties"]
            self.node_properties=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["node_properties"+"_"+str(idx)]
                self.node_properties.append(NodeData.NodeData())
                self.node_properties[idx].loadFromHDF5Handle(ssubgroup)
        if ("line_properties" in list(gr.keys())):
            subgroup = gr["line_properties"]
            self.line_properties=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["line_properties"+"_"+str(idx)]
                self.line_properties.append(LineData.LineData())
                self.line_properties[idx].loadFromHDF5Handle(ssubgroup)
        if ("solver_options" in list(gr.keys())):
            subgroup = gr["solver_options"]
            self.solver_options.loadFromHDF5Handle(subgroup)
        if ("check_convergence" in list(gr.keys())):
            self.check_convergence = gr["check_convergence"][0]
        if ("mooring_radius" in list(gr.keys())):
            self.mooring_radius = gr["mooring_radius"][0]
        if ("ancillaries" in list(gr.keys())):
            subgroup = gr["ancillaries"]
            self.ancillaries.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
