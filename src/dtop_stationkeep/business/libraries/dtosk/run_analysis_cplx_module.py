# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np 
from .entity.Farm import Farm
from .entity.Body import Body
from .environment.Environment import Environment
from .mooring.Designer import Designer
from .outputs.Hierarchy import Hierarchy
from .outputs.EnvironmentalImpactResults import EnvironmentalImpactResults
from .outputs.BoM import BoM
from .design_catenary_module import design_catenary
from .design_foundation_module import run_foundation_design_wrapper
from .frequency_analysis_module import run_frequency_analysis,run_fls_frequency_analysis
from .forceModel.RotorForce import RotorForce
from .static_analysis_module import run_static_analysis_for_master_structure,run_static_analysis_for_fixed_master_structure,compute_forces_on_fixed_structure
from .dr.DigitalRepresentation import DigitalRepresentation
import copy
import logging
logger = logging.getLogger() # grabs root logger

def run_analysis_cplx(input_cplx,catalogue_lines,catalogue_anchors):

	# Parse inputs
	logger.info('Parsing inputs ...')
	[farm,environment_array,fls_environment_array,mooring_input_flag,mooring_input, ms_mooring_input_flag, ms_mooring_input] = parse_inputs_cplx(input_cplx)

	# Run analysis
	designer = Designer()
	one_device_has_been_analyzed = False
	idx_run = 0
	sub_idx = -1
	for idx in range(0,len(farm.body_array)):
		is_substation = False
		if len(farm.body_array[idx].name)>=10:
			if farm.body_array[idx].name[0:10]=='substation':
				is_substation = True
				sub_idx = sub_idx + 1 
		if is_substation:
			if farm.body_array[idx].positioning_type != 'fixed':
				raise Exception('A substation is necessarily fixed in DTO+. Found: ' + str(farm.body_array[idx].positioning_type))
			logger.info('Start of substation analysis ...')
			run_mooring_design_analysis(farm.body_array[idx],mooring_input_device,mooring_input_flag,environment_array,fls_environment_array,farm.body_array[idx].water_depth,input_cplx,catalogue_lines)
			run_foundation_design_wrapper(input_cplx.substation_properties[sub_idx].foundation_design_parameters,farm.body_array[idx],input_cplx.substation_properties[sub_idx].steady_force_model.device_wet_profile,input_cplx.substation_properties[sub_idx].steady_force_model.device_wet_width,farm.body_array[idx].water_depth,environment_array,catalogue_anchors)
			logger.info('End of substation analysis.')
		else:
			if not one_device_has_been_analyzed:
				if mooring_input_flag == 'custom':
					mooring_input_device = mooring_input[0]
				else:
					mooring_input_device = mooring_input
				# Run analysis for the device
				logger.info('Start of device analysis ...')
				run_mooring_design_analysis(farm.body_array[idx],mooring_input_device,mooring_input_flag,environment_array,fls_environment_array,farm.body_array[idx].water_depth,input_cplx,catalogue_lines)
				run_foundation_design_wrapper(input_cplx.device_properties.foundation_design_parameters,farm.body_array[idx],input_cplx.device_properties.steady_force_model.device_wet_profile,input_cplx.device_properties.steady_force_model.device_wet_width,farm.body_array[idx].water_depth,environment_array,catalogue_anchors)
				one_device_has_been_analyzed = True
				idx_run = copy.deepcopy(idx)
				logger.info('End of device analysis.')
			else:
				if mooring_input_flag == 'custom' and len(mooring_input) > 1:
					mooring_input_device = mooring_input[idx]
					logger.info('Start of device analysis ...')
					run_mooring_design_analysis(farm.body_array[idx],mooring_input_device,mooring_input_flag,environment_array,fls_environment_array,farm.body_array[idx].water_depth,input_cplx,catalogue_lines)
					run_foundation_design_wrapper(input_cplx.device_properties.foundation_design_parameters,farm.body_array[idx],input_cplx.device_properties.steady_force_model.device_wet_profile,input_cplx.device_properties.steady_force_model.device_wet_width,farm.body_array[idx].water_depth,environment_array,catalogue_anchors)
					logger.info('End of device analysis.')
				else:
					# Copy results from the device that has been analyzed
					if farm.body_array[idx_run].positioning_type == 'moored':
						designer.duplicate_mooring_design(farm.body_array[idx_run],farm.body_array[idx],input_cplx.water_density)
					farm.body_array[idx].water_depth = copy.deepcopy(farm.body_array[idx_run].water_depth)
					farm.body_array[idx].results = copy.deepcopy(farm.body_array[idx_run].results)

	# Analysis of the master structure
	if farm.master_structure_is_present:
		logger.info('Start of masterstructure analysis ...')
		run_master_structure_analysis(input_cplx.master_structure_properties.foundation_design_parameters,farm.master_structure,farm.body_array,ms_mooring_input_flag, ms_mooring_input,input_cplx.water_density,environment_array,input_cplx.uls_analysis_parameters.weather_direction,catalogue_anchors)
		logger.info('End of masterstructure analysis.')
	# Populate outputs
	[hierarchy, bom, env_impact, representation] = populate_outputs(farm,'component')

	return farm, hierarchy, bom, env_impact, environment_array, input_cplx.uls_analysis_parameters.weather_direction, fls_environment_array, representation 

def run_master_structure_analysis(foundation_design_parameters,master_structure,body_array,ms_mooring_input_flag, ms_mooring_input,water_density,environment_array,weather_direction,catalogue_anchors):

	if master_structure.positioning_type == 'moored':

		# Build mooring system
		if ms_mooring_input_flag == 'design':

			raise Exception('Mooring system of master structure cannot be design automatically.')

		else:

			if ms_mooring_input_flag == 'input_file':

				master_structure.mooring_system_force.create_from_custom_input_file(ms_mooring_input,master_structure.initial_position)

			elif ms_mooring_input_flag == 'custom':

				master_structure.mooring_system_force.create_from_custom_input(ms_mooring_input,master_structure.initial_position)
			
			else:
			
				raise Exception("Error: unknown value of mooring_input_flag : " + str(ms_mooring_input_flag))


		# Initiate the mooring system module
		master_structure.mooring_system_force.initiate_calculation(master_structure.water_depth,water_density,master_structure.initial_position)

		# Run static analysis
		run_static_analysis_for_master_structure(master_structure,body_array,weather_direction,environment_array,master_structure.water_depth)

		# Populate mooring design in body.results
		master_structure.populate_mooring_design() 

	else:

		run_static_analysis_for_fixed_master_structure(master_structure,body_array,weather_direction,environment_array,master_structure.water_depth)

	# Run foundation design
	run_foundation_design_wrapper(foundation_design_parameters,master_structure,'cylinder',0.1,master_structure.water_depth,environment_array,catalogue_anchors)

	pass

def run_mooring_design_analysis(body,mooring_input,mooring_input_flag,environment_array,fls_environment_array,water_depth,input_cplx,catalogue_lines):

	if body.positioning_type == 'moored':

		# Mooring system design and/or analysis
		designer = Designer()
		if mooring_input_flag == 'design':

			if body.mooring_design_criteria.mooring_type == 'catenary':

				# Extract chain catalogue corresponding to selected quality
				catalogue_chains = catalogue_lines.extract('chain',body.mooring_design_criteria.line_quality)

				# Find a correct catenary system, and run frequency analysis, results are stored in body.results
				design_catenary(body,environment_array,water_depth,input_cplx.uls_analysis_parameters.weather_direction,catalogue_chains)
			
			else:
				
				raise Exception("Error: design procedure only available for mooring_type = catenary")
		else:

			if mooring_input_flag == 'input_file':

				body.mooring_system_force.create_from_custom_input_file(mooring_input,body.initial_position)

			elif mooring_input_flag == 'custom':

				body.mooring_system_force.create_from_custom_input(mooring_input,body.initial_position)
			
			else:
				
				raise Exception("Error: unknown value of mooring_input_flag : " + str(mooring_input_flag))

			# Initiate the mooring system module
			body.mooring_system_force.initiate_calculation(water_depth,environment_array[0].current.water_density,body.initial_position)

			# Run ULS frequency analysis, results stored in body.results
			run_frequency_analysis(body,input_cplx.uls_analysis_parameters.weather_direction,environment_array,water_depth)

		# Run FLS frequency analysis, results stored in body.fls_results
		if len(fls_environment_array)>0:
			run_fls_frequency_analysis(body,fls_environment_array,water_depth,input_cplx.path_to_figure_folder)

		# Populate mooring design in body.results
		body.populate_mooring_design() 

	else:

		# Compute forces on fixed structure
		compute_forces_on_fixed_structure(body,input_cplx.uls_analysis_parameters.weather_direction,environment_array,water_depth)

def parse_inputs_cplx(input_cplx):
	# Init
	mooring_input_flag = None
	mooring_input = None

	# Create the environments for the uls analysis
	environment_array=[]
	if len(input_cplx.uls_analysis_parameters.hs_array)==0:
		environment_array.append(Environment())
		environment_array[idx].set_water_density(input_cplx.water_density)
		environment_array[idx].current.velocity = input_cplx.uls_analysis_parameters.current_vel
		environment_array[idx].wind.velocity = input_cplx.uls_analysis_parameters.wind_vel
		environment_array[idx].wave.hs = 0.0
		environment_array[idx].wave.tp = 0.0
	else:
		for idx in range(0,len(input_cplx.uls_analysis_parameters.hs_array)):
			environment_array.append(Environment())
			environment_array[idx].set_water_density(input_cplx.water_density)
			environment_array[idx].current.velocity = input_cplx.uls_analysis_parameters.current_vel
			environment_array[idx].wind.velocity = input_cplx.uls_analysis_parameters.wind_vel
			environment_array[idx].wave.hs = input_cplx.uls_analysis_parameters.hs_array[idx]
			environment_array[idx].wave.tp = input_cplx.uls_analysis_parameters.tp_array[idx]
	# Convert weather directions from DEG to RAD
	input_cplx.uls_analysis_parameters.weather_direction = input_cplx.uls_analysis_parameters.weather_direction*np.pi/180.0

	# Create the environments for the fls analysis
	fls_environment_array=[]
	counter = -1
	for idx in range(0,len(input_cplx.fls_analysis_parameters.hs_dp_proba)):
		if input_cplx.fls_analysis_parameters.hs_dp_proba[idx]>0:
			counter = counter +1
			fls_environment_array.append(Environment())
			fls_environment_array[counter].proba = input_cplx.fls_analysis_parameters.hs_dp_proba[idx] / 100.0 # Switch from percentage to unit reference
			fls_environment_array[counter].set_water_density(input_cplx.water_density)
			fls_environment_array[counter].current.velocity = input_cplx.fls_analysis_parameters.associated_cur[idx]
			fls_environment_array[counter].current.direction= -input_cplx.fls_analysis_parameters.associated_dircur[idx]*np.pi/180.0
			fls_environment_array[counter].wind.velocity = input_cplx.fls_analysis_parameters.associated_wind[idx]
			fls_environment_array[counter].wind.direction = np.pi - input_cplx.fls_analysis_parameters.associated_dirwind[idx]*np.pi/180.0
			fls_environment_array[counter].wave.hs = (input_cplx.fls_analysis_parameters.intervals_hs_min[idx]+input_cplx.fls_analysis_parameters.intervals_hs_max[idx])/2
			fls_environment_array[counter].wave.tp = input_cplx.fls_analysis_parameters.associated_tp[idx]
			fls_environment_array[counter].wave.direction = np.pi - (input_cplx.fls_analysis_parameters.intervals_dp_min[idx]+input_cplx.fls_analysis_parameters.intervals_dp_max[idx])/2*np.pi/180.0

	# Create each device of the farm
	farm = Farm()
	for idx in range(0,len(input_cplx.deviceId)):

		# Create body
		farm.body_array.append(Body())

		# Set body name
		farm.body_array[idx].name=str(input_cplx.deviceId[idx])
		farm.body_array[idx].water_depth = input_cplx.water_depth[idx]

		# Set device type
		farm.body_array[idx].positioning_type=input_cplx.device_properties.positioning_type
		farm.body_array[idx].positioning_reference=input_cplx.device_properties.positioning_reference # seabed or masterstructure
		if farm.body_array[idx].positioning_type == 'moored':

			# Set design criteria for mooring system
			farm.body_array[idx].mooring_design_criteria = copy.deepcopy(input_cplx.device_properties.mooring_design_criteria)

			# Some physical properties: mass matrix, cog, hydrostatics, added mass, damping and excitation force
			if input_cplx.device_properties.hdb_source=='nemoh_run':
				# Mass matrix, cog, hydrostatics, added mass, damping and excitation force (only one body permitted)
				farm.body_array[idx].populate_from_nemoh_run(input_cplx.device_properties.path_to_nemoh_result_folder,input_cplx.device_properties.nemoh_scale_factor) 
				farm.body_array[idx].critical_damping_coefficient=copy.deepcopy(input_cplx.device_properties.critical_damping_coefficient)
			elif input_cplx.device_properties.hdb_source=='mc_module':
				# Mass matrix, cog, hydrostatics read directly
				farm.body_array[idx].mass_matrix=copy.deepcopy(input_cplx.device_properties.mass_matrix)
				farm.body_array[idx].cog_position=copy.deepcopy(input_cplx.device_properties.cog)
				farm.body_array[idx].hydrostatic_force.stiffness_matrix=copy.deepcopy(input_cplx.device_properties.hydrostatic_matrix)
				# Added mass, damping and excitation force read from MC module outputs
				farm.body_array[idx].populate_from_mc_module_outputs(input_cplx.device_properties.hydro_data_MC_module)
				pass
		
		elif farm.body_array[idx].positioning_type == 'fixed':
				
				farm.body_array[idx].mass = copy.deepcopy(input_cplx.device_properties.mass)
				farm.body_array[idx].submerged_volume = copy.deepcopy(input_cplx.device_properties.submerged_volume)
				farm.body_array[idx].populate_simple_buoyancy_force(input_cplx.water_density)
				farm.body_array[idx].mass_matrix=copy.deepcopy(input_cplx.device_properties.mass_matrix)
				farm.body_array[idx].cog_position=copy.deepcopy(input_cplx.device_properties.cog)
			
		else:
			raise Exception('Error : unknown positioning type : ' + str(farm.body_array[idx].positioning_type))

		# Set body position
		farm.body_array[idx].initial_position[0]=input_cplx.north[idx]-input_cplx.farm_north
		farm.body_array[idx].initial_position[1]=-(input_cplx.east[idx]-input_cplx.farm_east)
		farm.body_array[idx].initial_position[5]=copy.deepcopy(input_cplx.yaw[idx])*np.pi/180.0
		if farm.body_array[idx].positioning_type == 'moored':
			logger.info('Floating device positionned at free water surface')
			farm.body_array[idx].initial_position[2]=0.0 # Body is placed at free water level
		elif farm.body_array[idx].positioning_type =='fixed':
			logger.info('Fixed device positionned at seabed level (water depth is ' +str(farm.body_array[idx].water_depth) + 'm)')
			farm.body_array[idx].initial_position[2]= - farm.body_array[idx].water_depth # Body is placed at the seabed
		farm.body_array[idx].position=copy.deepcopy(farm.body_array[idx].initial_position)

		# Some other physical properties: wind force model and current force model
		farm.body_array[idx].populate_steady_force_model(input_cplx.device_properties.steady_force_model,farm.body_array[idx].water_depth)
		
		# If tidal energy converter (tec): rotor inputs (assume only 1 rotor is needed)
		farm.body_array[idx].machine_type=input_cplx.device_properties.machine_type
		if farm.body_array[idx].machine_type == 'tec':
			lshape = np.shape(input_cplx.device_properties.hub_position)
			n_rotor = lshape[1]
			for idrot in range(0,n_rotor):
				farm.body_array[idx].rotor_force.append(RotorForce())
				farm.body_array[idx].rotor_force[idrot].hub_position[0:3] = input_cplx.device_properties.hub_position[0:3,idrot]
				farm.body_array[idx].rotor_force[idrot].rotor_diameter = input_cplx.device_properties.rotor_diameter
				farm.body_array[idx].rotor_force[idrot].include_orbital_velocity = input_cplx.device_properties.include_orbital_velocity
				if not input_cplx.device_properties.thrust_coeff_curve_file == '':
					farm.body_array[idx].rotor_force[idrot].read_thrust_coeff_curve_from_file(input_cplx.device_properties.thrust_coeff_curve_file)
				else:
					npoints = len(input_cplx.device_properties.thrust_curve_v)
					farm.body_array[idx].rotor_force[idrot].thrust_coeff_curve = np.zeros(shape=(npoints,2), dtype=float)
					farm.body_array[idx].rotor_force[idrot].thrust_coeff_curve[:,0] = input_cplx.device_properties.thrust_curve_v
					farm.body_array[idx].rotor_force[idrot].thrust_coeff_curve[:,1] = input_cplx.device_properties.thrust_curve_ct


	# Mooring system of device
	mooring_input_flag = input_cplx.device_properties.mooring_input_flag
	if mooring_input_flag == 'custom':
		mooring_input = input_cplx.device_properties.custom_mooring_input # Note: this is a list of objects. If only one element is present, it will be used for all the devices 
	elif mooring_input_flag == 'input_file':
		mooring_input = input_cplx.device_properties.file_mooring_input
	elif mooring_input_flag == 'design':
		mooring_input = input_cplx.device_properties.mooring_design_criteria # Note: for convenience, mooring_design_criteria is copied in the body structure (see above)	

	# Substation, if present
	if input_cplx.substation_is_present:
		farm.substation_is_present = True
		for ids in range(0,len(input_cplx.substation_properties)):
			substation = create_substation(input_cplx.substation_properties[ids],ids)
			farm.body_array.append(copy.deepcopy(substation))

	# Masterstructure, if present
	if input_cplx.device_properties.positioning_reference == 'masterstructure':
		farm.master_structure_is_present = True
		[farm.master_structure,ms_mooring_input_flag,ms_mooring_input] = create_master_structure(input_cplx.master_structure_properties)
	else:
		ms_mooring_input_flag = None
		ms_mooring_input = None

	return farm,environment_array, fls_environment_array, mooring_input_flag, mooring_input, ms_mooring_input_flag, ms_mooring_input

def create_substation(substation_properties,ids):

	# add cost of support structure here?

	# Init
	logger.info('Create substation')
	substation = Body()

	# Poulate inputs
	substation.name = 'substation_' + str(ids)
	substation.positioning_type = substation_properties.positioning_type #fixed
	substation.positioning_reference = substation_properties.positioning_reference #seabed
	substation.cog_position = substation_properties.cog
	substation.mass = substation_properties.mass
	substation.water_depth = substation_properties.water_depth

	# Set body position
	substation.initial_position[0]=copy.deepcopy(substation_properties.north)
	substation.initial_position[1]=copy.deepcopy(-substation_properties.east)
	if substation.positioning_type == 'moored':
		substation.initial_position[2]=0.0 # Body is placed at free water level
	elif substation.positioning_type =='fixed':
		substation.initial_position[2]= - substation.water_depth # Body is placed at the seabed
	substation.position=copy.deepcopy(substation.initial_position)

	# Some other physical properties: wind force model and current force model
	substation.populate_steady_force_model(substation_properties.steady_force_model,substation.water_depth)
	
	return substation

def create_master_structure(master_structure_properties):

	# Init
	master_structure = Body()

	# Populate inputs
	master_structure.name = 'master_structure'
	master_structure.positioning_type = master_structure_properties.positioning_type
	master_structure.mooring_type = master_structure_properties.mooring_type
	master_structure.water_depth = master_structure_properties.water_depth
	master_structure.mooring_design_criteria = copy.deepcopy(master_structure_properties.mooring_design_criteria)


	# Mooring system of masterstructure
	ms_mooring_input_flag = master_structure_properties.mooring_input_flag
	if ms_mooring_input_flag == 'custom':
		ms_mooring_input = master_structure_properties.custom_mooring_input
	elif ms_mooring_input_flag == 'input_file':
		ms_mooring_input = master_structure_properties.file_mooring_input
	elif ms_mooring_input_flag == 'design':
		raise Exception('Design is not an available option for the mooring system of a master structure')

	# Add mass if masterstructure is fixed
	if master_structure_properties.positioning_type == 'fixed':
		master_structure.mass = copy.deepcopy(master_structure_properties.mass)

	# Set active dofs to surge,sway,yaw only
	master_structure.active_dof_static=np.array([1,1,0,0,0,1],dtype=int)
	master_structure.active_dof_dynamic=np.array([1,1,0,0,0,1],dtype=int)


	return master_structure,ms_mooring_input_flag,ms_mooring_input

def populate_outputs(farm,failure_rate_level):

	# Populate output - hierarchy
	hierarchy = Hierarchy()
	hierarchy.populate(farm,failure_rate_level)

	# Populate output - BoM
	bom = BoM()
	bom.populate(farm)

	# Populate output - environmental_impact
	env_impact = EnvironmentalImpactResults()
	env_impact.populate(farm)

	# Populate output - representation (digital representation as in deliverable D7.1)
	representation = DigitalRepresentation()
	representation.populate(farm)

	return hierarchy, bom, env_impact, representation

        
