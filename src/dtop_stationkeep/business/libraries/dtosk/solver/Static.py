# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
from scipy.optimize import minimize
from ..entity.Body import Body
import copy
# @ USER DEFINED IMPORTS END
#------------------------------------

class Static():

    """data model representing a static solver for equilibrium calculation of a body
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._tolerance=1.0E-6
        self._max_inner_iter=50
        self._max_outer_iter=10
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
        self.P=[]
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START

    def first_guess_equilibrium(self,body,current=None,wind=None,wave=None,water_depth = 100.0):

        if body.active_dof_static[0]==1 and body.active_dof_static[1]==1:
            env_force = body.compute_static_force(current = current, wind = wind, wave = wave, water_depth = water_depth, include_mooring = False, include_hydrostatic = False)
            if np.linalg.norm(env_force[0:2])>0:
                cos_b = env_force[0]/np.linalg.norm(env_force[0:2])
                sin_b = env_force[1]/np.linalg.norm(env_force[0:2])
            else:
                cos_b = 1.0
                sin_b = 0.0

            delta=0.0
            approx_is_found = False
            while not approx_is_found and delta < 0.5*body.water_depth:
                delta = delta + 1.0 # Increment 1 meter
                body.position[0] = body.initial_position[0] + delta*cos_b
                body.position[1] = body.initial_position[1] + delta*sin_b
                f_moor = body.compute_static_force()
                if np.linalg.norm(f_moor[0:2])>np.linalg.norm(env_force[0:2]):
                    approx_is_found = True

            if not approx_is_found:
                body.position = copy.deepcopy(body.initial_position)

    def compute_equilibrium(self,body, k=0, current = None, wind = None, wave = None, water_depth = 100.0):
        """
        Take a body with starting postion.
        Solve static equilibrium equation through a gradient descent optimisation algorithm.
        Return the equilirium position.
        """

        # Static analysis only if at least one dof is free
        equilibrium_found = True
        if sum(body.active_dof_static)>0:

            # Init
            equilibrium_found = False
            x_0=body.get_position_relevant_dof()

            # Define the conditionning matrix (sort of diagonalized mooring stiffness matrix to ease the optimisation)
            jacobian = -(body.compute_linear_stiffness_by_difference_for_relevant_dof(current = current, wind = wind, wave = wave, water_depth = water_depth))

            # Find if the jacobian is somehow zero on the diagonal (this can happen for example when no stiffness is present in yaw)
            diago =  np.diag( np.abs(jacobian))
            counter = -1
            for idx in range(0,len(body.active_dof_static)):
                if body.active_dof_static[idx] == 1:
                    counter = counter +1
                    if diago[counter] < 10E-6:
                        body.active_dof_static[idx] = 0
            
            # Recompute the jacobian with possibly reduced dofs
            x_0=body.get_position_relevant_dof()
            jacobian = -(body.compute_linear_stiffness_by_difference_for_relevant_dof(current = current, wind = wind, wave = wave, water_depth = water_depth))


            # Conditionning matrix
            self.P = np.sqrt( 1/np.diag( np.abs(jacobian) ) ) # same matrix applied left and right (inverse)

            # Define cost function
            def cost_function( z, norm=2 ):
                """
                Evaluate mooring and external forces;
                Compute the error on forces equilibrium;
                Then evaluate mooring and hydrostatic stiffnesses;
                Compute the jacobian matrix of the error vector;
                Compute the gradient of the error modulus;
                Return the error modulus and its gradient.
                """
                x = self.P *z  # deconditionning
                body.set_position_relevant_dof(x)

                # Evaluate error
                error = body.compute_static_force_for_relevant_dof(current = current, wind = wind, wave = wave, water_depth = water_depth)
                error = self.P *error # conditionning
                if norm==2:
                    cost = np.sqrt( error.dot(error) )
                elif norm=='inf':
                    cost = np.max( np.abs(error) )
                    index = np.argmax( np.abs(error) )
                # Evaluate gradient
                if cost < 1e-10:
                    gradient = np.zeros(sum(body.active_dof_static))
                else:
                    jacobian = -(body.compute_linear_stiffness_by_difference_for_relevant_dof(current = current, wind = wind, wave = wave, water_depth = water_depth))
                    jacobian = np.diag(self.P).dot( jacobian ).dot( np.diag(self.P) ) # conditionning
                    if norm==2:
                        gradient = error.dot(jacobian) /cost
                    elif norm=='inf':
                        gradient = jacobian[index] *np.sign(error)
                # Return
                return cost, gradient

            # optimisation
            z_0 = x_0 /self.P # conditionning
            result = minimize( cost_function, z_0, jac=True, method='CG', tol=self.tolerance, options={'disp':False, 'maxiter':self.max_inner_iter} )
            z_opt = result.x
            x_opt = self.P *z_opt  # deconditionning
            #print('--> {} iter. / {} fun. eval.'.format(result.nit, result.nfev))

            # Check if equilibirum is reached
            if result.fun < self.tolerance:
                equilibrium_found = True
            else:
                equilibrium_found = False

            # If the optimisation fails, run a new one starting from x_opt --> update the conditionning
            if (not equilibrium_found) and k<self.max_outer_iter:
                body.set_position_relevant_dof(x_opt)
                [x_dummy,equilibrium_found] = self.compute_equilibrium(body, k=k+1,current = current, wind = wind, wave = wave, water_depth = water_depth)


        # Get body position result
        x_result = copy.deepcopy(body.position)

        # Return solution
        return x_result,equilibrium_found
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def tolerance(self): # pragma: no cover
        """float: Tolerance for defining equilibrium []
        """
        return self._tolerance
    #------------
    @ property
    def max_inner_iter(self): # pragma: no cover
        """float: Maximum number of iterations (when determining equilibrium) []
        """
        return self._max_inner_iter
    #------------
    @ property
    def max_outer_iter(self): # pragma: no cover
        """float: Maximum numlber of iterations (when conditionning the matrix system) []
        """
        return self._max_outer_iter
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ tolerance.setter
    def tolerance(self,val): # pragma: no cover
        self._tolerance=float(val)
    #------------
    @ max_inner_iter.setter
    def max_inner_iter(self,val): # pragma: no cover
        self._max_inner_iter=float(val)
    #------------
    @ max_outer_iter.setter
    def max_outer_iter(self,val): # pragma: no cover
        self._max_outer_iter=float(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:solver:Static"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:solver:Static"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("tolerance"):
            rep["tolerance"] = self.tolerance
        if self.is_set("max_inner_iter"):
            rep["max_inner_iter"] = self.max_inner_iter
        if self.is_set("max_outer_iter"):
            rep["max_outer_iter"] = self.max_outer_iter
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["tolerance"] = np.array([self.tolerance],dtype=float)
        handle["max_inner_iter"] = np.array([self.max_inner_iter],dtype=float)
        handle["max_outer_iter"] = np.array([self.max_outer_iter],dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "tolerance"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "max_inner_iter"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "max_outer_iter"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("tolerance" in list(gr.keys())):
            self.tolerance = gr["tolerance"][0]
        if ("max_inner_iter" in list(gr.keys())):
            self.max_inner_iter = gr["max_inner_iter"][0]
        if ("max_outer_iter" in list(gr.keys())):
            self.max_outer_iter = gr["max_outer_iter"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
