import collections
import numpy as np
import os
import json
import h5py
from . import WaveDriftCoeffRect
#------------------------------------
# @ USER DEFINED IMPORTS START
import copy
from ..rotation_module import Rab
from ..global_parameters import acceleration_of_gravity
from ..geometry_module import get_cylinder_intersection
# @ USER DEFINED IMPORTS END
#------------------------------------

class MeanWaveDriftForce():

    """data model representing steady force from mean wave drift force
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._wave_drift_coef_rect=WaveDriftCoeffRect.WaveDriftCoeffRect()
        self._wave_drift_coef_rect.description = 'Mean wave drift coefficients. (Remery, Hermans, 1971, valid for a barge with draft/water depth ratio of 0.175)'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def compute_force(self,positioning_type,body_position_in_n,body_parts,rotation_center_in_b,wave,water_depth):

        # Init
        force_in_b=np.zeros(shape=(6), dtype=float)

        # Rotation matrix of body in {n}
        Rbn=Rab(body_position_in_n[3:])

        # Consider zero roll and pitch
        mean_body_position_in_n=copy.deepcopy(body_position_in_n)
        mean_body_position_in_n[3]=0.0
        mean_body_position_in_n[4]=0.0

        # Loop over all the body parts
        for idx in range(0,len(body_parts)):

            if body_parts[idx].mean_wave_drift_force_applied:

                if body_parts[idx].geometry_type=='cylinder':

                    if positioning_type=='fixed':

                        # Fixed Cylinder case
                        [force_in_n, pa_n] = self.compute_force_on_fixed_cylinder(mean_body_position_in_n,body_parts[idx].position_in_b,body_parts[idx].cylinder,wave,water_depth)

                    elif positioning_type=='moored':

                        # Floating Cylinder case
                        [force_in_n, pa_n] = self.compute_force_on_floating_cylinder(mean_body_position_in_n,body_parts[idx].position_in_b,body_parts[idx].cylinder,wave,water_depth)

                    else:
                        raise Exception("unknown positioning_type: " + positioning_type)

                elif body_parts[idx].geometry_type=='rectangle':

                    # Fixed or floating rectangular box case
                    [force_in_n, pa_n] = self.compute_force_on_rectangular_box(Rbn,mean_body_position_in_n,body_parts[idx].position_in_b,body_parts[idx].rectangle,wave,water_depth)

                else:
                    raise Exception('Unknown geometry type : ' + body_parts[idx].geometry_type)

                # Compute force in {b}
                force_in_b_temp=np.zeros(shape=(6), dtype=float)
                force_in_b_temp[0:3]=(Rbn.T).dot(force_in_n[0:3].T).T

                # Compute moment in {b}
                pa_b = (Rbn.T).dot((pa_n-body_position_in_n[0:3]).T).T
                force_in_b_temp[3:6]=np.cross(pa_b - rotation_center_in_b,force_in_b_temp[0:3])

                # Add contribution from bodypart
                force_in_b = force_in_b + force_in_b_temp

        return force_in_b

    def compute_force_on_rectangular_box(self,Rbn,mean_body_position_in_n,rectangle_position_in_b,rectangle,wave,water_depth):

        # Init
        k = wave.wave_number(water_depth)
        rho = wave.water_density
        g = acceleration_of_gravity()
        A = 0.5 * wave.hmax(water_depth)
        W = rectangle.dy
        L = rectangle.dx
        omega = wave.omega_peak()

        # Set application point to z=0
        # Application point is always at free surface
        pa_n = copy.deepcopy(mean_body_position_in_n[0:3])
        pa_n[2] = 0.0

        # Relative wave direction
        beta_r=np.mod(wave.direction - (mean_body_position_in_n[5] + rectangle_position_in_b[5]) + 2.0*np.pi,2.0*np.pi)

        # Reflexion coefficient
        Cr = self.wave_drift_coef_rect.get_wave_drift_coefficient(omega)

        # Remery and Herman, 1971, "The slow drift oscillations of a moored object in random seas".
        mean_wave_drift_load = 0.5 * rho * g * A**2 * Cr**2

        # Compute wave drift force in n
        force_in_bp=np.zeros(shape=(3), dtype=float)
        force_in_bp[0] = mean_wave_drift_load * np.cos(beta_r) * W
        force_in_bp[1] = mean_wave_drift_load * np.sin(beta_r) * L

        # Convert force in bp to force in n (assume small roll and pitch)
        angles=np.zeros(shape=(3), dtype=float)
        angles[2] = mean_body_position_in_n[5] + rectangle_position_in_b[5] # Yaw of the body part in n
        Rbpn = Rab(angles)
        force_in_n = Rbpn.dot(force_in_bp)

        return force_in_n, pa_n

    def compute_force_on_fixed_cylinder(self,mean_body_position_in_n,cylinder_position_in_b,cylinder,wave,water_depth):

        # Init
        k = wave.wave_number(water_depth)
        rho = wave.water_density
        g = acceleration_of_gravity()
        A = 0.5 * wave.hmax(water_depth)
        r = 0.5 * cylinder.diameter
        h = water_depth

        # Application point is always at free surface
        pa_n = copy.deepcopy(mean_body_position_in_n[0:3])
        pa_n[2] = 0.0

        # McIver 1987 fixed cylinder
        mean_wave_drift_load = 5.0 / 16.0 * rho * g * A**2 * r * np.pi**2 * (k*r)**3 * ( 1 + (2*k*h)/(np.sinh(2*k*h)) )
        if k * r > 1.0:
            #print('WARNING: ka > 1.0. McIver assumption is violated. Horizontal mean drift load may be inaccurate')
            pass

        # Compute wind force in n
        force_in_n=np.zeros(shape=(3), dtype=float)
        force_in_n[0] = mean_wave_drift_load*np.cos(wave.direction)
        force_in_n[1] = mean_wave_drift_load*np.sin(wave.direction)

        return force_in_n, pa_n

    def compute_force_on_floating_cylinder(self,mean_body_position_in_n,cylinder_position_in_b,cylinder,wave,water_depth):

        # Init
        k = wave.wave_number(water_depth)
        rho = wave.water_density
        g = acceleration_of_gravity()
        A = 0.5 * wave.hmax(water_depth)
        r = 0.5 * cylinder.diameter
        h = water_depth

        # Application point is always at free surface
        pa_n = copy.deepcopy(mean_body_position_in_n[0:3])
        pa_n[2] = 0.0

        # Eatock Taylor, Hu and Nielsen 1990 floating cylinder
        mean_wave_drift_load = 5.0 / 16.0 * rho * g * A**2 * r * np.pi**2 * (k*r)**3

        if k * r > 0.4:
            #print('WARNING: ka > 0.4. Eatock Taylor, Hu and Nielsen assumption is violated. Horizontal mean drift load may be inaccurate')
            pass

        # Compute wind force in n
        force_in_n=np.zeros(shape=(3), dtype=float)
        force_in_n[0] = mean_wave_drift_load*np.cos(wave.direction)
        force_in_n[1] = mean_wave_drift_load*np.sin(wave.direction)

        return force_in_n, pa_n

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def wave_drift_coef_rect(self): # pragma: no cover
        """:obj:`~.WaveDriftCoeffRect.WaveDriftCoeffRect`: Mean wave drift coefficients. (Remery, Hermans, 1971, valid for a barge with draft/water depth ratio of 0.175)
        """
        return self._wave_drift_coef_rect
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ wave_drift_coef_rect.setter
    def wave_drift_coef_rect(self,val): # pragma: no cover
        self._wave_drift_coef_rect=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:forceModel:MeanWaveDriftForce"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:forceModel:MeanWaveDriftForce"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("wave_drift_coef_rect"):
            if (short and not(deep)):
                rep["wave_drift_coef_rect"] = self.wave_drift_coef_rect.type_rep()
            else:
                rep["wave_drift_coef_rect"] = self.wave_drift_coef_rect.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        subgroup = handle.create_group("wave_drift_coef_rect")
        self.wave_drift_coef_rect.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "wave_drift_coef_rect"
        try :
            if data[varName] != None:
                self.wave_drift_coef_rect=WaveDriftCoeffRect.WaveDriftCoeffRect()
                self.wave_drift_coef_rect.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("wave_drift_coef_rect" in list(gr.keys())):
            subgroup = gr["wave_drift_coef_rect"]
            self.wave_drift_coef_rect.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
