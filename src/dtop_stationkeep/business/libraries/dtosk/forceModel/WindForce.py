import collections
import numpy as np
import os
import json
import h5py
from . import DragCoeffCyl
from . import WindCoeffRect
#------------------------------------
# @ USER DEFINED IMPORTS START
from ..rotation_module import Rab
from .. geometry_module import compute_cylinder_projected_area, get_dry_cylinder_properties,get_dry_rectangle_properties
# @ USER DEFINED IMPORTS END
#------------------------------------

class WindForce():

    """data model representing steady force from wind on dry part of the body
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._drag_coef_cyl=DragCoeffCyl.DragCoeffCyl()
        self._drag_coef_cyl.description = 'Wind coefficients for a cylinder.'
        self._drag_coef_rect=WindCoeffRect.WindCoeffRect()
        self._drag_coef_rect.description = 'Wind coefficients for a rectangle. (DNV-RP-C205 Table 5-5, October 2010)'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def compute_force(self,body_position_in_n,body_parts,rotation_center_in_b,wind):

        # Init
        force_in_b=np.zeros(shape=(6), dtype=float)
        force_in_n=np.zeros(shape=(6), dtype=float)
        pa_n=np.zeros(shape=(3), dtype=float)

        # Rotation matrix of body in {n}
        Rbn=Rab(body_position_in_n[3:])

        # Loop over all the body parts
        for idx in range(0,len(body_parts)):

            if body_parts[idx].geometry_type=='cylinder':

                # Cylinder case
                if body_parts[idx].cylinder.length*body_parts[idx].cylinder.diameter > 0.0:
                    [force_in_n, pa_n] = self.compute_force_on_cylinder(body_position_in_n,body_parts[idx].position_in_b,body_parts[idx].cylinder,body_parts[idx].surface_roughness,wind)

            elif body_parts[idx].geometry_type=='rectangle':

                # Rectangular box case
                if body_parts[idx].rectangle.dx*body_parts[idx].rectangle.dy*body_parts[idx].rectangle.dz > 0.0:
                    [force_in_n, pa_n] = self.compute_force_on_rectangular_box(body_position_in_n,body_parts[idx].position_in_b,body_parts[idx].rectangle,wind)

            # Compute force in {b}
            force_in_b_temp=np.zeros(shape=(6), dtype=float)
            force_in_b_temp[0:3]=(Rbn.T).dot(force_in_n[0:3].T).T

            # Compute moment in {b}
            pa_b = (Rbn.T).dot((pa_n-body_position_in_n[0:3]).T).T
            force_in_b_temp[3:6]=np.cross(pa_b - rotation_center_in_b,force_in_b_temp[0:3])

            # Add contribution from bodypart
            force_in_b = force_in_b + force_in_b_temp

        return force_in_b

    def compute_force_on_cylinder(self,body_position_in_n,cylinder_position_in_b,cylinder,surface_roughness,wind):

        # Init
        wind_load = 0.0

        # Find the intersection between the cylinder axis and z=0, and get the properties
        # l: dry cylinder length
        # pa_n: position of the cylinder center
        # c1_n,c2_n: position of the top and bottom of the cylinder
        [l,pa_n,z,c1_n,c2_n] = get_dry_cylinder_properties(body_position_in_n,cylinder_position_in_b,cylinder)

        if l>0:
            # Compute velocity at highest point of the cylinder
            u = wind.get_wind_at_height(z)

            # Compute projected area of the dry cylinder on the plane perpendicular to the wind direction
            proj_area = compute_cylinder_projected_area(c1_n,c2_n,cylinder.diameter,wind.direction)

            # Get drag coefficient
            drag_coeff=self.drag_coef_cyl.get_drag_coefficient(cylinder.diameter,u,wind.kinematic_viscosity,surface_roughness)

            # Compute wind load
            wind_load = 0.5 * wind.air_density * drag_coeff * (u**2) * proj_area

        # Compute wind force in n
        force_in_n=np.zeros(shape=(3), dtype=float)
        force_in_n[0] = wind_load*np.cos(wind.direction)
        force_in_n[1] = wind_load*np.sin(wind.direction)

        return force_in_n, pa_n

    def compute_force_on_rectangular_box(self,body_position_in_n,rectangle_position_in_b,rectangle,wind):

        # Get exposed height, and application point
        [zmax,height,width,length,pa_n] = get_dry_rectangle_properties(body_position_in_n,rectangle_position_in_b,rectangle)

        # Compute velocity at highest point of the box
        if zmax>0.0:
            u = wind.get_wind_at_height(zmax)
        else:
            u = 0.0

        # Relative wind direction
        beta_r=np.mod(wind.direction - (body_position_in_n[5] + rectangle_position_in_b[5]) + 2.0*np.pi,2.0*np.pi)

        # Get drag coefficient
        drag_coeff_x=self.drag_coef_rect.get_drag_coefficient(height,width,length)
        drag_coeff_y=self.drag_coef_rect.get_drag_coefficient(height,length,width)

        # Compute wind force in body part coordinate system
        force_in_bp=np.zeros(shape=(3), dtype=float)
        force_in_bp[0] = 0.5 * wind.air_density * drag_coeff_x * (u**2) * width*height * np.cos(beta_r)
        force_in_bp[1] = 0.5 * wind.air_density * drag_coeff_y * (u**2) * length*height * np.sin(beta_r)

        # Convert force in bp to force in n (assume small roll and pitch)
        angles=np.zeros(shape=(3), dtype=float)
        angles[2] = body_position_in_n[5] + rectangle_position_in_b[5] # Yaw of the body part in n
        Rbpn = Rab(angles)
        force_in_n = Rbpn.dot(force_in_bp)

        return force_in_n, pa_n

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def drag_coef_cyl(self): # pragma: no cover
        """:obj:`~.DragCoeffCyl.DragCoeffCyl`: Wind coefficients for a cylinder.
        """
        return self._drag_coef_cyl
    #------------
    @ property
    def drag_coef_rect(self): # pragma: no cover
        """:obj:`~.WindCoeffRect.WindCoeffRect`: Wind coefficients for a rectangle. (DNV-RP-C205 Table 5-5, October 2010)
        """
        return self._drag_coef_rect
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ drag_coef_cyl.setter
    def drag_coef_cyl(self,val): # pragma: no cover
        self._drag_coef_cyl=val
    #------------
    @ drag_coef_rect.setter
    def drag_coef_rect(self,val): # pragma: no cover
        self._drag_coef_rect=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:forceModel:WindForce"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:forceModel:WindForce"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("drag_coef_cyl"):
            if (short and not(deep)):
                rep["drag_coef_cyl"] = self.drag_coef_cyl.type_rep()
            else:
                rep["drag_coef_cyl"] = self.drag_coef_cyl.prop_rep(short, deep)
        if self.is_set("drag_coef_rect"):
            if (short and not(deep)):
                rep["drag_coef_rect"] = self.drag_coef_rect.type_rep()
            else:
                rep["drag_coef_rect"] = self.drag_coef_rect.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        subgroup = handle.create_group("drag_coef_cyl")
        self.drag_coef_cyl.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("drag_coef_rect")
        self.drag_coef_rect.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "drag_coef_cyl"
        try :
            if data[varName] != None:
                self.drag_coef_cyl=DragCoeffCyl.DragCoeffCyl()
                self.drag_coef_cyl.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "drag_coef_rect"
        try :
            if data[varName] != None:
                self.drag_coef_rect=WindCoeffRect.WindCoeffRect()
                self.drag_coef_rect.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("drag_coef_cyl" in list(gr.keys())):
            subgroup = gr["drag_coef_cyl"]
            self.drag_coef_cyl.loadFromHDF5Handle(subgroup)
        if ("drag_coef_rect" in list(gr.keys())):
            subgroup = gr["drag_coef_rect"]
            self.drag_coef_rect.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
