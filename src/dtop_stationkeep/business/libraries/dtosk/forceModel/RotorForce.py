import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
from ..rotation_module import Rab
from scipy import interpolate
from ..entity.BodyPart import BodyPart
# @ USER DEFINED IMPORTS END
#------------------------------------

class RotorForce():

    """data model representing steady force from a rotor
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._hub_position=np.zeros(shape=(3), dtype=float)
        self._rotor_diameter=0.0
        self._thrust_coeff_curve=np.zeros(shape=(1,1), dtype=float)
        self._include_orbital_velocity=False
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def compute_force(self,body_position_in_n,rotation_center_in_b,current,water_depth,wave):

        # Init
        vmin=min(self.thrust_coeff_curve[:,0])
        vmax=max(self.thrust_coeff_curve[:,0])

        # Compute vertical position of the hub in n
        Rbn = Rab(body_position_in_n[3:])
        OH_n = body_position_in_n[0:3] + Rbn.dot(self.hub_position.T).T
        z = OH_n[2]

        # Get current velocity at hub position
        c_vel = current.get_current_velocity(z,water_depth)
        """ It is assumed that the nacelle always faces the flow direction. (Assumption from DTOcean 1.0)"""

        # Add orbital velocity if needed
        if self.include_orbital_velocity & (wave is not None):
            # Compute orbital velocity
            c_orb = wave.vmax(water_depth,z)
            # Add it to the current velocity
            c_vel = c_vel + c_orb

        # Get thrust coefficient
        thrustcoef = 0.0
        thrustcoefint = interpolate.interp1d(self.thrust_coeff_curve[:,0],self.thrust_coeff_curve[:,1],fill_value="extrapolate")
        if (c_vel >= vmin and c_vel <= vmax):
            thrustcoef = thrustcoefint(c_vel)
        else:
            raise Exception("Current velocity outside tabulated range: " + str(c_vel))

        # Compute rotorload
        rotor_area = self.compute_rotor_area()
        rotor_load = 0.5 * current.water_density * rotor_area * thrustcoef * c_vel ** 2.0

        # Compute force in {n}
        force_in_n=np.zeros(shape=(6), dtype=float)
        force_in_n[0]=rotor_load*np.cos(current.direction)
        force_in_n[1]=rotor_load*np.sin(current.direction)

        # Compute force in {b}
        force_in_b=np.zeros(shape=(6), dtype=float)
        force_in_b[0:3]=(Rbn.T).dot(force_in_n[0:3].T).T

        # Compute moment in {b}
        force_in_b[3:6]=np.cross(self.hub_position - rotation_center_in_b,force_in_b[0:3])

        return force_in_b

    def compute_rotor_area(self):

        rotor_area = (np.pi / 4.0) * self.rotor_diameter ** 2.0

        return rotor_area

    def read_thrust_coeff_curve_from_file(self, filePath):

        self.thrust_coeff_curve=np.loadtxt(filePath, delimiter="\t")

        pass



    def plot(self,body_position_in_n,ax):

        # Use BodyPart to represent graphically the rotor
        blade = BodyPart()
        blade.geometry_type='cylinder'
        blade.cylinder.length = self.rotor_diameter
        blade.cylinder.diameter = 0.1
        blade.position_in_b[0:3] = self.hub_position

        # Init
        xlma=np.array([0.0,0.0])
        xlmi=np.array([0.0,0.0])
        ylma=np.array([0.0,0.0])
        ylmi=np.array([0.0,0.0])
        zlma=np.array([0.0,0.0])
        zlmi=np.array([0.0,0.0])

        # Plot 2 blades at 45deg
        blade.position_in_b[3] = np.pi*45/180.0
        [xlma_t, xlmi_t, ylma_t, ylmi_t, zlma_t, zlmi_t] = blade.plot(body_position_in_n,ax)
        xlma[0] = xlma_t.max()
        xlmi[0] = xlmi_t.min()
        ylma[0] = ylma_t.max()
        ylmi[0] = ylmi_t.min()
        zlma[0] = zlma_t.max()
        zlmi[0] = zlmi_t.min()

        # Plot 2 blades at -45deg
        blade.position_in_b[3] = -np.pi*45/180.0
        [xlma_t, xlmi_t, ylma_t, ylmi_t, zlma_t, zlmi_t] = blade.plot(body_position_in_n,ax)
        xlma[1] = xlma_t.max()
        xlmi[1] = xlmi_t.min()
        ylma[1] = ylma_t.max()
        ylmi[1] = ylmi_t.min()
        zlma[1] = zlma_t.max()
        zlmi[1] = zlmi_t.min()

        return xlma, xlmi, ylma, ylmi, zlma, zlmi

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def hub_position(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Rotor hub position in {b} dim(3) []
        """
        return self._hub_position
    #------------
    @ property
    def rotor_diameter(self): # pragma: no cover
        """float: Rotor diameter [m]
        """
        return self._rotor_diameter
    #------------
    @ property
    def thrust_coeff_curve(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Thrust coefficient as function of flow velocity. [:,1]: current velocities. [:,2]: thrust coefficient. dim(*,*) []
        """
        return self._thrust_coeff_curve
    #------------
    @ property
    def include_orbital_velocity(self): # pragma: no cover
        """bool: Include orbital velocity []
        """
        return self._include_orbital_velocity
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ hub_position.setter
    def hub_position(self,val): # pragma: no cover
        self._hub_position=val
    #------------
    @ rotor_diameter.setter
    def rotor_diameter(self,val): # pragma: no cover
        self._rotor_diameter=float(val)
    #------------
    @ thrust_coeff_curve.setter
    def thrust_coeff_curve(self,val): # pragma: no cover
        self._thrust_coeff_curve=val
    #------------
    @ include_orbital_velocity.setter
    def include_orbital_velocity(self,val): # pragma: no cover
        self._include_orbital_velocity=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:forceModel:RotorForce"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:forceModel:RotorForce"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("hub_position"):
            if (short):
                rep["hub_position"] = str(self.hub_position.shape)
            else:
                try:
                    rep["hub_position"] = self.hub_position.tolist()
                except:
                    rep["hub_position"] = self.hub_position
        if self.is_set("rotor_diameter"):
            rep["rotor_diameter"] = self.rotor_diameter
        if self.is_set("thrust_coeff_curve"):
            if (short):
                rep["thrust_coeff_curve"] = str(self.thrust_coeff_curve.shape)
            else:
                try:
                    rep["thrust_coeff_curve"] = self.thrust_coeff_curve.tolist()
                except:
                    rep["thrust_coeff_curve"] = self.thrust_coeff_curve
        if self.is_set("include_orbital_velocity"):
            rep["include_orbital_velocity"] = self.include_orbital_velocity
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["hub_position"] = np.array(self.hub_position,dtype=float)
        handle["rotor_diameter"] = np.array([self.rotor_diameter],dtype=float)
        handle["thrust_coeff_curve"] = np.array(self.thrust_coeff_curve,dtype=float)
        handle["include_orbital_velocity"] = np.array([self.include_orbital_velocity],dtype=bool)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "hub_position"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "rotor_diameter"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "thrust_coeff_curve"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "include_orbital_velocity"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("hub_position" in list(gr.keys())):
            self.hub_position = gr["hub_position"][:]
        if ("rotor_diameter" in list(gr.keys())):
            self.rotor_diameter = gr["rotor_diameter"][0]
        if ("thrust_coeff_curve" in list(gr.keys())):
            self.thrust_coeff_curve = gr["thrust_coeff_curve"][:][:]
        if ("include_orbital_velocity" in list(gr.keys())):
            self.include_orbital_velocity = gr["include_orbital_velocity"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
