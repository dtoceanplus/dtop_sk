# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np 
import os
from scipy import optimize
from .global_parameters import acceleration_of_gravity
from .solver.Static import Static
from .linear_wave_module import compute_jonswap_spectrum
from .forceModel.FirstOrderWaveForce import FirstOrderWaveForce
from scipy import interpolate
from .rotation_module import rotation_about_z
import copy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import math
from dtop_shared_library.response_amplitude_operator import response_amplitude_operator
import logging
import sys
logger = logging.getLogger()

def compute_eigenperiod(body):
	
	# Compute eigen period at initial position (asymptotic value of added mass is used)
	body.position = copy.deepcopy(body.initial_position)
	K_mooring = body.compute_mooring_stiffness_by_difference()

	ls = body.A.shape
	eigenperiod_red_dof = np.zeros(shape=(ls[2]),dtype=float)

	if  body.hdb_source == 'mc_module':
		ndof=ls[1]
		shared_dof = body.active_dof_dynamic
		n_red_dof = sum(shared_dof)
		P = body.velocity_transformation_matrix

		# Transform  K_mooring from [6,6] matrix to a [n_active_dof,n_active_dof] matrix for the platform body (by convention, it is the first body)
		K_m_shared_main_body = from_dof_to_reduced_dof(K_mooring,shared_dof)

		# Add other degrees of freedom (terms are equal to zero since only the first body is connected to the mooring)
		lp = P.shape
		ndof_shared_all = lp[0]
		K_m_shared_all = np.zeros(shape=(ndof_shared_all,ndof_shared_all),dtype=float)
		K_m_shared_all[0:n_red_dof,0:n_red_dof] = K_m_shared_main_body

		# Compute the mooring stiffness matrix in the generalized dofs coordinate system
		K_m_gendof = (P.T).dot(K_m_shared_all).dot(P)
		Km = K_m_gendof

	else:
		Km = K_mooring

	for idof in range(0,ls[2]):

		if body.hdb_source == 'nemoh_run':
			Kh = body.hydrostatic_force.stiffness_matrix[idof,idof]	
		elif  body.hdb_source == 'mc_module':
			Kh = body.hydrostatic_matrix_gendof[idof,idof]

		om_eg = np.sqrt((Km[idof,idof]+Kh)/(body.mass_matrix[idof,idof] + body.A[0,idof,idof]))

		if om_eg>0.0:
			eigenperiod_red_dof[idof] = 2*np.pi/om_eg
		else:
			eigenperiod_red_dof[idof] = float('inf')
	body.position = copy.deepcopy(body.initial_position)

	# Rebuild 6dofs
	eigenperiod = np.zeros(shape=(6),dtype=float)
	idl=-1
	for idof in range(0,6):
		if body.active_dof_dynamic[idof]==1:
			idl=idl+1
			eigenperiod[idof] = eigenperiod_red_dof[idl]

	return eigenperiod

def run_fls_frequency_analysis(body,environment_array,water_depth,path_to_figure_folder):

	if len(environment_array)>0:
		logger.info('-- run FLS frequency domain analysis')

	# Init
	static_solver = Static()

	# Init the variables to store the results
	# Results from initial position
	mooring_pretension=np.zeros(shape=(len(body.mooring_system_force.line_properties),2), dtype=float)
	# Results from equilibrium calculation (with applied steady forces from wind, current and mean wave drift)
	steady_position=np.zeros(shape=(len(environment_array),6), dtype=float)
	steady_force=np.zeros(shape=(len(environment_array),6), dtype=float)
	steady_mooring_force=np.zeros(shape=(len(environment_array),len(body.mooring_system_force.line_properties),2,3), dtype=float)
	steady_mooring_tension=np.zeros(shape=(len(environment_array),len(body.mooring_system_force.line_properties),2), dtype=float)
	steady_offset = np.zeros(shape=(len(environment_array)),dtype=float)
	# Results from motion frequency analysis (due to 1st order wave force)
	motion_spectrum=np.zeros(shape=(len(environment_array),len(body.w),6), dtype=float)
	omega=copy.deepcopy(body.w)
	damping_matrix=np.zeros(shape=(len(environment_array),6), dtype=float)
	# Results from uls postprocessing (estimating the maxima)
	dynamic_offset = np.zeros(shape=(len(environment_array)),dtype=float)
	mooring_force = np.zeros(shape=(len(environment_array),len(body.mooring_system_force.line_properties),2,3),dtype=float)
	mooring_tension = np.zeros(shape=(len(environment_array),len(body.mooring_system_force.line_properties),2),dtype=float)
	mooring_touchdown = np.zeros(shape=(len(environment_array),len(body.mooring_system_force.line_properties)),dtype=float)
	tension_versus_mbl = np.zeros(shape=(len(environment_array),len(body.mooring_system_force.line_properties)),dtype=float)
	total_position = np.zeros(shape=(len(environment_array),6),dtype=float)
	total_offset = np.zeros(shape=(len(environment_array)),dtype=float)
	# Results from fls postprocessing
	env_damage_lifetime=np.zeros(shape=(len(environment_array),len(body.mooring_system_force.line_properties)),dtype=float)
	env_damage_one_year=np.zeros(shape=(len(environment_array),len(body.mooring_system_force.line_properties)),dtype=float)
	damage_lifetime=np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype=float)
	damage_one_year=np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype=float)
	fls_criteria_1=np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype=float)
	fls_criteria_2=np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype=float)
	env_proba=np.zeros(shape=(len(environment_array)),dtype=float)
	env_stress_std=np.zeros(shape=(len(environment_array),len(body.mooring_system_force.line_properties)),dtype=float)
	env_stress_range=np.zeros(shape=(len(environment_array),len(body.mooring_system_force.line_properties)),dtype=float)
	ad=np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype=float)
	m=np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype=float)
	env_n_cycles_lifetime=np.zeros(shape=(len(environment_array),len(body.mooring_system_force.line_properties)),dtype=float)
	n_cycles_lifetime=np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype=float)

	# Compute equilibrium without excitation forces (initial position)
	[xmean,eq_found] = static_solver.compute_equilibrium(body)
	if not eq_found:
		raise Exception("Error in run_fls_frequency_analysis: initial equilibrium not found")
	mooring_pretension = body.mooring_system_force.get_tension()

	# Compute response for each environment
	for idx in range(0,len(environment_array)):
		
		# Compute wave spectrum
		Seta = compute_jonswap_spectrum(environment_array[idx].wave.hs,environment_array[idx].wave.tp,body.w)

		# Read the direction of the waves
		beta = environment_array[idx].wave.direction

		#######################
		# Steady analysis     #
		#######################
		# Note1: this equilibrium is conservative regarding the offset, because the mean wave drift is computed based on hmax
		# Note2: in practice we should compute the equilibrium with the correct mean wave drift, and run the low frequency analysis in addition (we don't do that due to lack of input data: we don't have access to second order wave drift forces)
		
		# First guess of equilibrium position (approximation of the offset). This makes the equilibrium calculation faster and more robust
		static_solver.first_guess_equilibrium(body,current = environment_array[idx].current, wind = environment_array[idx].wind, wave = environment_array[idx].wave, water_depth = water_depth)

		# Equilibrium calculation
		[xmean,eq_found] = static_solver.compute_equilibrium(body, current = environment_array[idx].current, wind = environment_array[idx].wind, wave = environment_array[idx].wave, water_depth = water_depth)
		steady_position[idx,0:6] = copy.deepcopy(xmean)
		steady_offset[idx] = np.linalg.norm(xmean[0:2]-body.initial_position[0:2])
		if not eq_found:
			raise Exception("Error in run_frequency_analysis: equilibrium not found")

		# Register the total steady force (from the environment)
		steady_force[idx,0:6] = body.compute_static_force(current = environment_array[idx].current, wind = environment_array[idx].wind, wave = environment_array[idx].wave, water_depth = water_depth, include_mooring = False, include_hydrostatic = False)

		# Register tension in each line segment
		body.position = copy.deepcopy(xmean)
		[steady_mooring_force[idx,:,:,:],steady_mooring_tension[idx,:,:],dum1,dum2] = body.compute_tension_in_all_lines()
		body.position = copy.deepcopy(body.initial_position)

		#######################
		# Dynamic analysis    #
		#######################

		# Compute mooring stiffness at equilibrium position
		body.position = copy.deepcopy(xmean)
		K_mooring = body.compute_mooring_stiffness_by_difference()

		# Select the direction closest to the real one
		b1 = (body.dir  + 2 * np.pi) % (2 * np.pi)
		b2 = (beta - body.position[5] +  2*np.pi) % (2*np.pi)
		beta_rel_dir = (abs(b1 - b2)+np.pi+2*np.pi)%(2*np.pi) - np.pi
		index_beta = abs(beta_rel_dir).tolist().index(min(abs(beta_rel_dir)))

		# Compute damping matrix if nemoh results is used as database (else a damping matrix is already given)
		B_add = np.zeros(shape=(6,6),dtype=float)
		if body.hdb_source == 'nemoh_run':
			for idof in range(0,6):
				B_add[idof,idof] = body.critical_damping_coefficient[idof]*2*((body.hydrostatic_force.stiffness_matrix[idof,idof]+K_mooring[idof,idof])*(body.mass_matrix[idof,idof]+max(body.A[idof,idof,:])))**0.5
				damping_matrix[idx,idof] = copy.deepcopy(B_add[idof,idof])
		elif body.hdb_source == 'mc_module':
			# Fitting damping and stiffness (using closest neighbour)
			ndof = sum(body.active_dof_static)
			body.K_fitting_closest = find_closest_neighbours_fitting_matrix(body.K_fitting,body.tp_fitting_array,body.hs_fitting_array,body.dir_fitting_array,environment_array[idx].wave.tp,environment_array[idx].wave.hs,b2,ndof)
			body.B_fitting_closest = find_closest_neighbours_fitting_matrix(body.B_fitting,body.tp_fitting_array,body.hs_fitting_array,body.dir_fitting_array,environment_array[idx].wave.tp,environment_array[idx].wave.hs,b2,ndof)
			body.K_add_closest = find_closest_neighbours_fitting_matrix(body.K_add,body.tp_fitting_array,body.hs_fitting_array,body.dir_fitting_array,environment_array[idx].wave.tp,environment_array[idx].wave.hs,b2,ndof)
			body.B_add_closest = find_closest_neighbours_fitting_matrix(body.B_add,body.tp_fitting_array,body.hs_fitting_array,body.dir_fitting_array,environment_array[idx].wave.tp,environment_array[idx].wave.hs,b2,ndof)
			body.pto_damping_closest = find_closest_neighbours_fitting_matrix(body.pto_damping,body.tp_fitting_array,body.hs_fitting_array,body.dir_fitting_array,environment_array[idx].wave.tp,environment_array[idx].wave.hs,b2,ndof)


		# Construct RAO (depends on K_mooring)
		rao = compute_rao(body, K_mooring = K_mooring, B_add = B_add)

		# Compute response spectrum
		Syy = compute_response_spectrum(rao[:,index_beta,:],Seta,body.active_dof_dynamic)
		for iomega in range(0,len(body.w)):
			for idof in range(0,6):
				motion_spectrum[idx,iomega,idof] = np.real(Syy[iomega,idof,idof])

		#######################
		# Postprocessing      #
		#######################

		# Compute most probable largest position
		Xmax = compute_maximum_response_from_spectrum(body.w,Syy)

		# Switch back to the initial coordinate system {n}
		Xmax_n = copy.deepcopy(Xmax)
		Xmax_n[0] = Xmax[0] * np.cos(xmean[5]) - Xmax[1]* np.sin(xmean[5])
		Xmax_n[1] = Xmax[0] * np.sin(xmean[5]) + Xmax[1]* np.cos(xmean[5])

		# Compute most probable largest offset
		dynamic_offset[idx] = np.linalg.norm(Xmax_n[0:2])
		total_offset[idx] = dynamic_offset[idx] + steady_offset[idx]

		# Compute extreme positions
		total_position[idx,0] = xmean[0] + np.sign(xmean[0])*Xmax_n[0]
		total_position[idx,1] = xmean[1] + np.sign(xmean[1])*Xmax_n[1]
		total_position[idx,2:6] = xmean[2:6] + Xmax_n[2:6]

		# Compute maximum tension in lines
		body.position = copy.deepcopy(total_position[idx,:])
		[mooring_force[idx,:,:,:],mooring_tension[idx,:,:],mooring_touchdown[idx,:],dum] = body.compute_tension_in_all_lines()
		body.position = copy.deepcopy(body.initial_position)

		# Perform fatigue analysis for environment no.idx (compute stress range and damage)
		[env_damage_lifetime[idx,:],env_damage_one_year[idx,:],env_stress_std[idx,:],env_stress_range[idx,:],env_proba[idx],ad[:],m[:],env_n_cycles_lifetime[idx,:]] = fls_postprocessing(body,Syy,steady_position[idx,:],steady_mooring_tension[idx,:,:],environment_array[idx],Seta)
		n_cycles_lifetime[:] = n_cycles_lifetime[:] + env_n_cycles_lifetime[idx,:]
		damage_one_year[:] = damage_one_year[:] + env_damage_one_year[idx,:]
		damage_lifetime[:] = damage_lifetime[:] + env_damage_lifetime[idx,:]

	
	# Compute long term CDF of stress range 
	[cdf_stress_range,cdf] = compute_stress_range_cdf(env_proba,env_stress_range)

	# Generate plot
	generate_cdf_stress_range_plot(body.name,body.mooring_system_force.line_properties,cdf_stress_range,cdf,path_to_figure_folder)
	
	# Compute criteria (DNVGL-OS-E301 section 6.4.1)
	# criteria_1 is 1-d*gamma (>0)
	# criteria_2 is 1/d (>gamma)
	gamma_f_a = np.zeros(shape=(len(body.mooring_system_force.line_properties)))
	for iline in range(0,len(body.mooring_system_force.line_properties)):
		# Fetch correct fatigue safety factor
		iline_index = body.mooring_system_force.line_properties[iline].line_type_index
		if body.mooring_system_force.line_dictionnary[iline_index].material == 'steel':
			gamma_f_a[iline] = body.mooring_design_criteria.gamma_f	
		else:
			gamma_f_a[iline] = body.mooring_design_criteria.gamma_f_fiber_rope
	[fls_criteria_1,fls_criteria_2] = compute_fls_criteria(damage_lifetime,gamma_f_a)

	# Register results in body.results
	body.results.fls_results.total_position=copy.deepcopy(total_position)
	body.results.fls_results.total_offset=copy.deepcopy(total_offset)
	body.results.fls_results.steady_offset=copy.deepcopy(steady_offset)
	body.results.fls_results.dynamic_offset=copy.deepcopy(dynamic_offset)
	body.results.fls_results.mooring_pretension=copy.deepcopy(mooring_pretension)
	body.results.fls_results.steady_position=copy.deepcopy(steady_position)
	body.results.fls_results.steady_force=copy.deepcopy(steady_force)
	body.results.fls_results.steady_mooring_force=copy.deepcopy(steady_mooring_force)
	body.results.fls_results.steady_mooring_tension=copy.deepcopy(steady_mooring_tension)
	body.results.fls_results.damping_matrix=copy.deepcopy(damping_matrix)
	body.results.fls_results.omega=copy.deepcopy(omega)
	body.results.fls_results.motion_spectrum=copy.deepcopy(motion_spectrum)

	# Compute max tension in mooring line segment
	body.results.fls_results.mooring_force=copy.deepcopy(mooring_force)
	body.results.fls_results.mooring_tension=copy.deepcopy(mooring_tension)
	body.results.fls_results.mooring_touchdown=copy.deepcopy(mooring_touchdown)

	# Register the results from FLS analysis
	body.results.fls_results.env_damage_lifetime=copy.deepcopy(env_damage_lifetime)
	body.results.fls_results.env_damage_one_year=copy.deepcopy(env_damage_one_year)
	body.results.fls_results.damage_lifetime=copy.deepcopy(damage_lifetime)
	body.results.fls_results.damage_one_year=copy.deepcopy(damage_one_year)
	body.results.fls_results.fls_criteria_1=copy.deepcopy(fls_criteria_1)
	body.results.fls_results.fls_criteria_2=copy.deepcopy(fls_criteria_2)
	body.results.fls_results.env_proba=copy.deepcopy(env_proba)
	body.results.fls_results.env_stress_std=copy.deepcopy(env_stress_std)
	body.results.fls_results.env_stress_range=copy.deepcopy(env_stress_range)
	body.results.fls_results.cdf_stress_range=copy.deepcopy(cdf_stress_range)
	body.results.fls_results.cdf=copy.deepcopy(cdf)
	body.results.fls_results.ad=copy.deepcopy(ad)
	body.results.fls_results.m=copy.deepcopy(m)
	body.results.fls_results.n_cycles_lifetime=copy.deepcopy(n_cycles_lifetime)

	# Mirror some inputs used for the analysis
	body.results.fls_results.gamma_f = copy.deepcopy(gamma_f_a)
	body.results.fls_results.n_years_lifetime = copy.deepcopy(body.mooring_design_criteria.n_years_lifetime)

	# Put the mooring system back to the initial position
	body.position = copy.deepcopy(body.initial_position)
	body.mooring_system_force.solve_mooring_system(body.position)

	return total_offset,steady_offset,dynamic_offset,mooring_force,mooring_tension,mooring_touchdown

def generate_cdf_stress_range_plot(body_name,line_properties,cdf_stress_range,cdf,path_to_figure_folder):

	filename = os.path.join(path_to_figure_folder,'fls_cdf_' + body_name + '.png')

	fig, ax = plt.subplots()
	nlines = len(line_properties)
	for iline in range(0,nlines):
		ax.plot(cdf_stress_range[:,iline],cdf[:,iline],label=line_properties[iline].name,marker='o')
	ax.set_xlabel('Stress range [MPa]')
	ax.set_ylabel('CDF [-]')
	ax.legend()
	plt.savefig(filename)
	plt.close()

def compute_fls_criteria(damage_lifetime,gamma_f_a):

	# Init
	nlines = len(damage_lifetime)
	fls_criteria_1 = np.zeros(shape=(nlines),dtype=float)
	fls_criteria_2 = np.zeros(shape=(nlines),dtype=float)

	# Compute fatigue criteria
	fls_criteria_1 = 1 - damage_lifetime[:]*gamma_f_a[:]
	fls_criteria_2 = 1/damage_lifetime

	return 	fls_criteria_1,fls_criteria_2

def compute_stress_range_cdf(env_proba,env_stress_std):

	# Init
	lshape = env_stress_std.shape
	n_env = lshape[0]
	nlines = lshape[1]
	cdf_stress_range=np.zeros(shape=(n_env,nlines),dtype=float)
	cdf=np.zeros(shape=(n_env,nlines),dtype=float)

	# Build CDF for each line segment
	for iline in range(0,nlines):
		# cdf_stress_range[:,iline] = np.linspace(min(env_stress_std[:,iline]), max(env_stress_std[:,iline]), num=n_points)
		indices = np.argsort(env_stress_std[:,iline])
		cdf_stress_range[:,iline] = env_stress_std[indices,iline]
		for ipoint in range(0,n_env):
			cdf[ipoint,iline]=0.0
			for ienv in range(0,n_env):
				if env_stress_std[ienv,iline]<=cdf_stress_range[ipoint,iline]:
					cdf[ipoint,iline]=cdf[ipoint,iline]+env_proba[ienv]	

	# fig, ax = plt.subplots()
	# for iline in range(0,nlines):
	# 	ax.plot(cdf_stress_range[:,iline],cdf[:,iline],label='Line no. '+str(iline),marker='o')
	# ax.set_xlabel('Stress range [MPa]')
	# ax.set_ylabel('CDF [-]')
	# leg = ax.legend()
	# plt.show()

	return cdf_stress_range,cdf

def fls_postprocessing(body,Syy,steady_position,steady_mooring_tension,environment,Seta):

	# Init
	env_damage_lifetime=np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype=float)
	env_damage_one_year=np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype=float)
	env_stress_std=np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype=float)
	env_stress_range=np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype=float)
	ad=np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype=float)
	m=np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype=float)
	env_n_cycles_lifetime=np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype=float)
	env_proba = environment.proba
	T_one_year = 365*24*3600 # Seconds in one year

	# Compute standard deviation of motion
	std_motion = np.zeros(shape=(6),dtype=float)
	for idof in range(0,6):
		std_motion[idof] = np.sqrt(compute_spectrum_moment(body.w,Syy[:,idof,idof],k=0))

	# Compute mean-up-crossing rate of tension in hertz voi
	# draft version: we take the same period as the period given by the wave period, while we need the period of the tension process!!! To be improved
	T1 = compute_t1(body.w,Seta)
	v0i = 1/T1

	# Compute stress range and damage (narrow-banded assumption DNVGL-OS-E301 section 6.3.5)
	std_motion_n = np.zeros(shape=(6),dtype=float)
	std_motion_n[0] = np.abs(std_motion[0] * np.cos(steady_position[5]) - std_motion[1]* np.sin(steady_position[5]))
	std_motion_n[1] = np.abs(std_motion[0] * np.sin(steady_position[5]) + std_motion[1]* np.cos(steady_position[5]))
	body.position = copy.deepcopy(steady_position[0:6] + std_motion_n[0:6])

	# Compute mean + std positions
	body.position[0] = steady_position[0] + np.sign(steady_position[0])*std_motion_n[0]
	body.position[1] = steady_position[1] + np.sign(steady_position[1])*std_motion_n[1]
	body.position[3] = steady_position[3] + np.sign(steady_position[1])*std_motion_n[3] # If y>0, it is assumed that a positive roll gives the largest tension
	body.position[4] = steady_position[4] - np.sign(steady_position[0])*std_motion_n[4] # If x>0, it is assumed that a negative pitch gives the largest tension

	[mooring_force,mooring_tension,mooring_touchdown,dum] = body.compute_tension_in_all_lines()
	env_tension_both_ends = np.absolute(mooring_tension - steady_mooring_tension)

	# Get cross section area
	[diameter_fls,cross_section_area_fls] = body.get_diameter_fls_all_lines()


	for iline in range(0,len(body.mooring_system_force.line_properties)):

		# Cross section are
		cross_section_area = cross_section_area_fls[iline]
		
		# Get SN curve parameters (RN parameters of non-steel components are transformed to SN curve parameters by this routine)
		[ad[iline],m[iline]] = body.mooring_system_force.get_SN_curve_parameters(iline)

		# Compute stress std and stress range
		iline_index = body.mooring_system_force.line_properties[iline].line_type_index
		env_stress_std[iline] = max(env_tension_both_ends[iline,0],env_tension_both_ends[iline,1]) / cross_section_area / 1000000.0 # Get it in [MPa] (SN-curve is used for steel material)
		env_stress_range[iline] = (2*np.sqrt(2)*env_stress_std[iline]) * math.gamma(1/2+1) # This is the expected value of the nominal stress range process E[S]

		# Compute number of cycles in this environment for the lifetime
		env_n_cycles_lifetime[iline] = v0i * T_one_year * environment.proba * body.mooring_design_criteria.n_years_lifetime

		# Compute damage
		env_damage_one_year[iline] = v0i * T_one_year * environment.proba /ad[iline] * (2*np.sqrt(2)*env_stress_std[iline])**m[iline] * math.gamma(m[iline]/2+1)
		env_damage_lifetime[iline] = env_damage_one_year[iline] * body.mooring_design_criteria.n_years_lifetime

	# Restore body position to its initial position
	body.position = copy.deepcopy(body.initial_position)

	return env_damage_lifetime,env_damage_one_year,env_stress_std,env_stress_range,env_proba,ad,m,env_n_cycles_lifetime

def run_frequency_analysis(body,beta,environment_array,water_depth):

	logger.info('-- run ULS frequency domain analysis')

	# Init
	static_solver = Static()

	# Init the variables to store the results
	# Results from initial position
	mooring_pretension=np.zeros(shape=(len(body.mooring_system_force.line_properties),2), dtype=float)
	mooring_stiffness =np.zeros(shape=(6,6), dtype=float)
	eigenperiod = np.zeros(shape=(6), dtype=float)
	# Results from equilibrium calculation (with applied steady forces from wind, current and mean wave drift)
	steady_position=np.zeros(shape=(len(beta),len(environment_array),6), dtype=float)
	steady_force=np.zeros(shape=(len(beta),len(environment_array),6), dtype=float)
	steady_mooring_force=np.zeros(shape=(len(beta),len(environment_array),len(body.mooring_system_force.line_properties),2,3), dtype=float)
	steady_mooring_tension=np.zeros(shape=(len(beta),len(environment_array),len(body.mooring_system_force.line_properties),2), dtype=float)
	steady_offset = np.zeros(shape=(len(beta),len(environment_array)),dtype=float)
	# Results from motion frequency analysis (due to 1st order wave force)
	motion_spectrum=np.zeros(shape=(len(beta),len(environment_array),len(body.w),6), dtype=float)
	omega=copy.deepcopy(body.w)
	damping_matrix=np.zeros(shape=(len(beta),len(environment_array),6), dtype=float)
	# Results from postprocessing (estimating the maxima)
	dynamic_offset = np.zeros(shape=(len(beta),len(environment_array)),dtype=float)
	mooring_force = np.zeros(shape=(len(beta),len(environment_array),len(body.mooring_system_force.line_properties),2,3),dtype=float)
	mooring_tension = np.zeros(shape=(len(beta),len(environment_array),len(body.mooring_system_force.line_properties),2),dtype=float)
	mooring_touchdown = np.zeros(shape=(len(beta),len(environment_array),len(body.mooring_system_force.line_properties)),dtype=float)
	total_position = np.zeros(shape=(len(beta),len(environment_array),6),dtype=float)
	total_offset = np.zeros(shape=(len(beta),len(environment_array)),dtype=float)

	# Compute equilibrium without excitation forces (initial position)
	[xmean,eq_found] = static_solver.compute_equilibrium(body)
	if not eq_found:
		raise Exception("Error in run_frequency_analysis: initial equilibrium not found")

	mooring_pretension = body.mooring_system_force.get_tension()
	#print(xmean)
	#sys.exit()
	# Compute stiffness
	mooring_stiffness = body.compute_mooring_stiffness_by_difference()

	# Compute eigenperiods
	eigenperiod = compute_eigenperiod(body)
	
	# Compute response for each direction and aeach environment
	for idx in range(0,len(environment_array)):
		
		# Compute wave spectrum
		Seta = compute_jonswap_spectrum(environment_array[idx].wave.hs,environment_array[idx].wave.tp,body.w)

		for idir in range(0,len(beta)):

			# Set colinear direction for current/wind/wave
			environment_array[idx].set_colinear_direction(beta[idir])

			#######################
			# Steady analysis     #
			#######################
			# Note1: this equilibrium is conservative regarding the offset, because the mean wave drift is computed based on hmax
			# Note2: in practice we should compute the equilibrium with the correct mean wave drift, and run the low frequency analysis in addition (we don't do that due to lack of input data: we don't have access to second order wave drift forces)
			
			# First guess of equilibrium position (approximation of the offset). This makes the equilibrium calculation faster and more robust
			static_solver.first_guess_equilibrium(body,current = environment_array[idx].current, wind = environment_array[idx].wind, wave = environment_array[idx].wave, water_depth = water_depth)

			# Equilibrium calculation
			[xmean,eq_found] = static_solver.compute_equilibrium(body, current = environment_array[idx].current, wind = environment_array[idx].wind, wave = environment_array[idx].wave, water_depth = water_depth )
			steady_position[idir,idx,0:6] = copy.deepcopy(xmean)
			steady_offset[idir,idx] = np.linalg.norm(xmean[0:2]-body.initial_position[0:2])
			if not eq_found:
				raise Exception("Error in run_frequency_analysis: equilibrium not found")

			# Register the total steady force from the environment
			steady_force[idir,idx,0:6] = body.compute_static_force(current = environment_array[idx].current, wind = environment_array[idx].wind, wave = environment_array[idx].wave, water_depth = water_depth, include_mooring = False, include_hydrostatic = False)
			# Register tension in each line segment
			body.position = copy.deepcopy(xmean)
			[steady_mooring_force[idir,idx,:,:,:],steady_mooring_tension[idir,idx,:,:],dum1,dum2] = body.compute_tension_in_all_lines()
			body.position = copy.deepcopy(body.initial_position)

			#######################
			# Dynamic analysis    #
			#######################

			# Compute mooring stiffness at equilibrium position
			body.position = copy.deepcopy(xmean)
			K_mooring = body.compute_mooring_stiffness_by_difference()

			# Select the direction closest to the real one
			b1 = (body.dir  + 2 * np.pi) % (2 * np.pi)
			b2 = (beta[idir] - body.position[5] +  2*np.pi) % (2*np.pi)
			beta_rel_dir = (abs(b1 - b2)+np.pi+2*np.pi)%(2*np.pi) - np.pi
			index_beta = abs(beta_rel_dir).tolist().index(min(abs(beta_rel_dir)))

			# Compute damping matrix
			B_add = np.zeros(shape=(6,6),dtype=float)
			if body.hdb_source == 'nemoh_run':
				for idof in range(0,6):
					B_add[idof,idof] = body.critical_damping_coefficient[idof]*2*((body.hydrostatic_force.stiffness_matrix[idof,idof]+K_mooring[idof,idof])*(body.mass_matrix[idof,idof]+max(body.A[idof,idof,:])))**0.5
					damping_matrix[idir,idx,idof] = copy.deepcopy(B_add[idof,idof])
			elif body.hdb_source == 'mc_module':
				# Fitting damping and stiffness (using closest neighbour)
				ndof = sum(body.active_dof_static)

				body.K_fitting_closest = find_closest_neighbours_fitting_matrix(body.K_fitting,body.tp_fitting_array,body.hs_fitting_array,body.dir_fitting_array,environment_array[idx].wave.tp,environment_array[idx].wave.hs,b2,ndof)
				body.B_fitting_closest = find_closest_neighbours_fitting_matrix(body.B_fitting,body.tp_fitting_array,body.hs_fitting_array,body.dir_fitting_array,environment_array[idx].wave.tp,environment_array[idx].wave.hs,b2,ndof)
				body.K_add_closest = find_closest_neighbours_fitting_matrix(body.K_add,body.tp_fitting_array,body.hs_fitting_array,body.dir_fitting_array,environment_array[idx].wave.tp,environment_array[idx].wave.hs,b2,ndof)
				body.B_add_closest = find_closest_neighbours_fitting_matrix(body.B_add,body.tp_fitting_array,body.hs_fitting_array,body.dir_fitting_array,environment_array[idx].wave.tp,environment_array[idx].wave.hs,b2,ndof)
				body.pto_damping_closest = find_closest_neighbours_fitting_matrix(body.pto_damping,body.tp_fitting_array,body.hs_fitting_array,body.dir_fitting_array,environment_array[idx].wave.tp,environment_array[idx].wave.hs,b2,ndof)


			# Construct RAO (depends on K_mooring)
			rao = compute_rao(body, K_mooring = K_mooring, B_add = B_add)

			# Compute response spectrum
			Syy = compute_response_spectrum(rao[:,index_beta,:],Seta,body.active_dof_dynamic)
			for iomega in range(0,len(body.w)):
				for idof in range(0,6):
					motion_spectrum[idir,idx,iomega,idof] = np.real(Syy[iomega,idof,idof])

			#######################
			# Postprocessing      #
			#######################

			# Compute most probable largest position
			Xmax = compute_maximum_response_from_spectrum(body.w,Syy)

			# Switch back to the initial coordinate system {n}
			Xmax_n = copy.deepcopy(Xmax)
			Xmax_n[0] = Xmax[0] * np.cos(xmean[5]) - Xmax[1]* np.sin(xmean[5])
			Xmax_n[1] = Xmax[0] * np.sin(xmean[5]) + Xmax[1]* np.cos(xmean[5])

			# Compute most probable largest offset
			dynamic_offset[idir,idx] = np.linalg.norm(Xmax_n[0:2])
			total_offset[idir,idx] = dynamic_offset[idir,idx] + steady_offset[idir,idx]

			# Compute extreme positions
			total_position[idir,idx,0] = xmean[0] + np.sign(xmean[0])*Xmax_n[0]
			total_position[idir,idx,1] = xmean[1] + np.sign(xmean[1])*Xmax_n[1]
			total_position[idir,idx,3] = xmean[3] + np.sign(xmean[1])*Xmax_n[3] # If y>0, it is assumed that a positive roll gives the largest tension
			total_position[idir,idx,4] = xmean[4] - np.sign(xmean[0])*Xmax_n[4] # If x>0, it is assumed that a negative pitch gives the largest tension
			total_position[idir,idx,2] = xmean[2] + Xmax_n[2]
			total_position[idir,idx,5] = xmean[5] + Xmax_n[5]
			logger.info('xmean:' + str(xmean))
			logger.info('xdyn:' + str(Xmax_n))
			logger.info('total position:' + str(total_position[idir,idx,:]))

	# Register results in body.results
	body.results.uls_results.total_position=copy.deepcopy(total_position)
	body.results.uls_results.total_offset=copy.deepcopy(total_offset)
	body.results.uls_results.steady_offset=copy.deepcopy(steady_offset)
	body.results.uls_results.dynamic_offset=copy.deepcopy(dynamic_offset)
	body.results.uls_results.mooring_pretension=copy.deepcopy(mooring_pretension)
	body.results.uls_results.eigenperiod=copy.deepcopy(eigenperiod)
	body.results.uls_results.mooring_stiffness=copy.deepcopy(mooring_stiffness)
	
	body.results.uls_results.steady_position=copy.deepcopy(steady_position)
	body.results.uls_results.steady_force=copy.deepcopy(steady_force)
	body.results.uls_results.steady_mooring_force=copy.deepcopy(steady_mooring_force)
	body.results.uls_results.steady_mooring_tension=copy.deepcopy(steady_mooring_tension)
	body.results.uls_results.damping_matrix=copy.deepcopy(damping_matrix)
	body.results.uls_results.omega=copy.deepcopy(omega)
	body.results.uls_results.motion_spectrum=copy.deepcopy(motion_spectrum)

	# Compute max tension in mooring line segment
	[mooring_force,mooring_tension,mooring_touchdown,tension_versus_mbl] = compute_tension_from_offset_position(body,total_position)
	body.results.uls_results.mooring_force=copy.deepcopy(mooring_force)
	body.results.uls_results.mooring_tension=copy.deepcopy(mooring_tension)
	body.results.uls_results.mooring_touchdown=copy.deepcopy(mooring_touchdown)
	body.results.uls_results.tension_versus_mbl=copy.deepcopy(tension_versus_mbl)

	# Get MBL used for ULS analysis
	body.results.uls_results.mbl_uls=body.get_mbl_uls_all_lines()

	# Mirror some inputs used for the analysis
	body.results.uls_results.safety_factor = copy.deepcopy(body.mooring_design_criteria.safety_factor)

	# Put the mooring system back to the initial position
	body.position = copy.deepcopy(body.initial_position)
	body.mooring_system_force.solve_mooring_system(body.position)

	return total_offset,steady_offset,dynamic_offset,mooring_force,mooring_tension,mooring_touchdown

def compute_tension_from_offset_position(body,total_position):

	# Init
	ls = total_position.shape
	ndir = ls[0]
	nenv = ls[1]
	mooring_force = np.zeros(shape=(ndir,nenv,len(body.mooring_system_force.line_properties),2,3),dtype=float)
	mooring_tension = np.zeros(shape=(ndir,nenv,len(body.mooring_system_force.line_properties),2),dtype=float)
	mooring_touchdown = np.zeros(shape=(ndir,nenv,len(body.mooring_system_force.line_properties)),dtype=float)
	tension_versus_mbl = np.zeros(shape=(ndir,nenv,len(body.mooring_system_force.line_properties)),dtype=float)

	for idx in range(0,nenv):
		for idir in range(0,ndir):
			
			# Compute max tension in each line segment
			body.position = copy.deepcopy(total_position[idir,idx,:])
			[mooring_force[idir,idx,:,:,:],mooring_tension[idir,idx,:,:],mooring_touchdown[idir,idx,:],tension_versus_mbl[idir,idx,:]] = body.compute_tension_in_all_lines()
			body.position = copy.deepcopy(body.initial_position)

	return mooring_force,mooring_tension,mooring_touchdown,tension_versus_mbl


def compute_rao(body,K_mooring = None,B_add = None):

	
	# Construct RAO
	if body.hdb_source == 'nemoh_run':
		rao = build_rao_from_nemoh_results(body.dir,body.w,body.A,body.B,body.Fe,body.mass_matrix,body.hydrostatic_force.stiffness_matrix,K_mooring = K_mooring,B_add = B_add)
	elif  body.hdb_source == 'mc_module':
		rao = build_rao_from_mc_results(body.dir,body.w,body.A,body.B,body.Fe,body.mass_matrix,body.hydrostatic_matrix_gendof,body.velocity_transformation_matrix,body.B_fitting_closest+body.B_add_closest+body.pto_damping_closest,body.K_fitting_closest+body.K_add_closest,body.active_dof_dynamic,K_mooring = K_mooring)
	else:
		raise Exception("Error from run_frequency_analysis: use of shared library compute_rao not implemented yet")
	return rao


def compute_max_dynamic_offset(body,environment_array,K_mooring):
	# This routine use used to get a rough estimate of the maximum dynamic offset. Therefore we voluntarily set the damping to zero.

	# Set damping matrix to zero (to be conservative in the first place)
	if body.hdb_source == 'mc_module':
		# Fitting damping and stiffness (using closest neighbour)
		ndof = sum(body.active_dof_static)

		body.K_fitting_closest = find_closest_neighbours_fitting_matrix(body.K_fitting,body.tp_fitting_array,body.hs_fitting_array,body.dir_fitting_array,environment_array[0].wave.tp,environment_array[0].wave.hs,0,ndof)
		body.B_fitting_closest = find_closest_neighbours_fitting_matrix(body.B_fitting,body.tp_fitting_array,body.hs_fitting_array,body.dir_fitting_array,environment_array[0].wave.tp,environment_array[0].wave.hs,0,ndof)*0
		body.K_add_closest = find_closest_neighbours_fitting_matrix(body.K_add,body.tp_fitting_array,body.hs_fitting_array,body.dir_fitting_array,environment_array[0].wave.tp,environment_array[0].wave.hs,0,ndof)
		body.B_add_closest = find_closest_neighbours_fitting_matrix(body.B_add,body.tp_fitting_array,body.hs_fitting_array,body.dir_fitting_array,environment_array[0].wave.tp,environment_array[0].wave.hs,0,ndof)*0
		body.pto_damping_closest = find_closest_neighbours_fitting_matrix(body.pto_damping,body.tp_fitting_array,body.hs_fitting_array,body.dir_fitting_array,environment_array[0].wave.tp,environment_array[0].wave.hs,0,ndof)*0


	# Construct RAO
	rao = compute_rao(body,K_mooring = K_mooring)

	# Compute max offset due to first order wave force
	offset = np.zeros(shape=(len(body.dir),len(environment_array)),dtype=float)

	for ide in range (0,len(environment_array)):

		# Compute wave spectrum
		Seta = compute_jonswap_spectrum(environment_array[ide].wave.hs,environment_array[ide].wave.tp,body.w)

		for idir in range(0,len(body.dir)):
		
			# Set weather direction
			environment_array[ide].set_colinear_direction(body.dir[idir])
			
			# Compute response spectrum
			Syy = compute_response_spectrum(rao[:,idir,:],Seta,body.active_dof_dynamic)
			
			# Compute most probable largest position
			Xmax = compute_maximum_response_from_spectrum(body.w,Syy)

			# Compute most probable largest offset
			offset[idir,ide] = np.linalg.norm(Xmax[0:2])

	# Extract maximum offset
	dynamic_offset_max = np.amax(offset)

	return dynamic_offset_max



def build_rao_from_nemoh_results(beta,omega,A,B,Fe,M,K,K_mooring = None,B_add = None):
	# Compute RAO from wave to motion
	# Inputs:
	# 	- beta: direction array relative to the body [rad]
	#	- omega: frequency array [rad/s]
	#	- A: added mass matrix dim(omega,ndof,ndof)
	#	- B: linear damping matrix dim(omega,ndof,ndof)
	#	- Fe: first order wave force transfer function dim(omega,dir,ndof)
	#	- M: mass matrix dim(ndof,ndof)
	#	- K: hydrostatic stiffness matrix dim(ndof,ndof)
	#	- K_mooring: additional stiffness matrix, usually used to represent mooring stiffness dim(ndof,ndof)
	#	- B_add: additional damping matrix, usually used to represent other sources of damping than the potential one dim(ndof,ndof)

	# Init
	ndir=len(beta)
	nomega=len(omega)
	la=A.shape
	ndof=la[1]
	rao = np.zeros(shape=(nomega,ndir,ndof),dtype=np.complex)

	# Check that the 6dofs of the main platform are available
	if ndof<6:
		raise Exception('Error: Hydrodynamics data must be known for at least the 6 degree of freedom of the platform')

	K_m = np.zeros(shape=(ndof,ndof),dtype=float)
	if not K_mooring is None:
		K_m[0:6,0:6] = K_mooring

	B_a = np.zeros(shape=(ndof,ndof),dtype=float)
	if not B_add is None:
		B_a[0:6,0:6] = B_add

	rao = response_amplitude_operator(omega, A + M, B + B_a , K + K_m, Fe)

	return rao

def build_rao_from_mc_results(beta,omega,A,B,Fe,M,K,P,B_add,K_add,shared_dof,K_mooring = None):
	# Compute RAO from wave to motion
	# Inputs:
	# 	- beta: direction array relative to the body [rad]
	#	- omega: frequency array [rad/s]
	#	- A: added mass matrix dim(omega,ndof,ndof)
	#	- B: linear damping matrix dim(omega,ndof,ndof)
	#	- Fe: first order wave force transfer function dim(omega,dir,ndof)
	#	- M: mass matrix dim(ndof,ndof)
	#	- K: hydrostatic stiffness matrix dim(ndof,ndof)
	#	- P: velocity transformation matrix
	#	- K_mooring: additional stiffness matrix, usually used to represent mooring stiffness dim(ndof,ndof)
	#	- B_add: additional damping matrix, usually used to represent other sources of damping than the potential one dim(ndof,ndof)

	# Init
	ndir=len(beta)
	nomega=len(omega)
	la=A.shape
	ndof=la[1]
	n_red_dof = sum(shared_dof)
	rao = np.zeros(shape=(nomega,ndir,ndof),dtype=np.complex)

	if K_mooring is None:
		K_mooring = np.zeros(shape=(6,6),dtype=float)

	# Transform  K_mooring from [6,6] matrix to a [n_active_dof,n_active_dof] matrix for the platform body (by convention, it is the first body)
	K_m_shared_main_body = from_dof_to_reduced_dof(K_mooring,shared_dof)

	# Add other degrees of freedom (terms are equal to zero since only the first body is connected to the mooring)
	lp = P.shape
	ndof_shared_all = lp[0]
	K_m_shared_all = np.zeros(shape=(ndof_shared_all,ndof_shared_all),dtype=float)
	K_m_shared_all[0:n_red_dof,0:n_red_dof] = K_m_shared_main_body

	# Compute the mooring stiffness matrix in the generalized dofs coordinate system
	K_m_gendof = (P.T).dot(K_m_shared_all).dot(P)

	rao = response_amplitude_operator(omega, A + M, B + B_add , K + K_m_gendof + K_add, Fe)

	return rao

def find_closest_neighbours_fitting_matrix(mat_in,tp_array,hs_array,dir_array,tp0,hs0,dir0,ndof):
	if np.shape(mat_in) != (len(tp_array),len(hs_array),len(dir_array),ndof,ndof):
		mat_in = np.zeros((len(tp_array),len(hs_array),len(dir_array),ndof,ndof))

	# Find closest direction index
	beta_rel_dir = (abs(dir_array - dir0)+np.pi+2*np.pi)%(2*np.pi) - np.pi
	idx_dir = abs(beta_rel_dir).tolist().index(min(abs(beta_rel_dir)))

	# Find closest hs,tp
	idx_tp = abs(tp_array-tp0).tolist().index(min(abs(tp_array-tp0)))
	idx_hs = abs(hs_array-hs0).tolist().index(min(abs(hs_array-hs0)))

	mat_out = mat_in[idx_tp, idx_hs, idx_dir, :, :]

	return mat_out

def compute_amplitude_from_spectrum(omega_array,S):

	# Compute amplitude from spectrum

	eta=np.zeros(shape=(len(omega_array)), dtype=float)

	for idx in range(0,len(omega_array)):
		if idx == 0:
			d_omega = (omega_array[idx+1]-omega_array[idx])
		elif idx == len(omega_array)-1:
			d_omega = (omega_array[idx]-omega_array[idx-1])
		else:
			d_omega = (omega_array[idx+1]-omega_array[idx-1])/2.0

		eta[idx] = np.sqrt(2*S[idx]*d_omega)
		
	return eta

def compute_excitation_force(Hf,eta):

	# Init
	#Hf = np.zeros((6,len(omega_array)),dtype=np.complex)
	lshape = Hf.shape
	n_dof = lshape[0]
	n_omega = lshape[1]
	Fext=np.zeros(shape=lshape, dtype=np.complex)


	for id_omega in range(0,n_omega):
		Fext[:,id_omega] = Hf[:,id_omega] * eta[id_omega]

	return Fext

def compute_response_spectrum(H,Sext,active_dof_dynamic):

	# Init
	#H = np.zeros(shape=(nomega,ndof),dtype=np.complex)
	lshape = H.shape
	if len(lshape)!=2:
		raise Exception("Error: H should be of rank 2")
	nomega = lshape[0]
	ndof = lshape[1]
	Sff_red_dof=np.zeros(shape=(nomega,ndof,ndof), dtype=np.complex)

	for id_omega in range(0,nomega):
		for idof1 in range(0,ndof):
			for idof2 in range(0,ndof):
				Sff_red_dof[id_omega,idof1,idof2] = H[id_omega,idof1] * Sext[id_omega] * np.conj(H[id_omega,idof2])

	Sff = np.zeros(shape=(nomega,6,6),dtype=np.complex)
	for id_omega in range(0,nomega):
		Sff[id_omega,0:6,0:6] = from_reduced_dof_to_dof(Sff_red_dof[id_omega,:,:],active_dof_dynamic) 

	return Sff

def compute_excitation_spectrum(Hf,Sext):

	# Init
	#Hf = np.zeros((6,len(omega_array),len(beta_array)),dtype=np.complex)
	lshape = Hf.shape
	ndof = lshape[0]
	nomega = lshape[1]
	Sff=np.zeros(shape=(ndof,ndof,nomega), dtype=np.complex)

	for id_omega in range(0,nomega):
		for idof1 in range(0,ndof):
			for idof2 in range(0,ndof):
				Sff[idof1,idof2,id_omega] = Hf[idof1,id_omega] * Sext[id_omega] * np.conj(Hf[idof2,id_omega])

	return Sff

def compute_spectrum_moment(omega_array,S,k=0):

	mk = np.trapz(np.real(S)*omega_array**k, x = omega_array)

	return mk

def compute_t2(omega_array,S):

	m0 = compute_spectrum_moment(omega_array,S,k=0)
	m2 = compute_spectrum_moment(omega_array,S,k=2)
	t2 = 2*np.pi*np.sqrt(m0/m2)

	return t2,m0

def compute_t1(omega_array,S):

	m0 = compute_spectrum_moment(omega_array,S,k=0)
	m1 = compute_spectrum_moment(omega_array,S,k=1)
	t1 = 2*np.pi*m0/m1

	return t1

def compute_maximum_response_from_spectrum(omega_array,Syy):
	lshape = Syy.shape
	ndof = lshape[1]
	Xmax = np.zeros(shape=(ndof), dtype=float)

	for idof in range(0,ndof):
		[t2,m0] = compute_t2(omega_array,Syy[:,idof,idof])
		Xmax[idof] = np.sqrt(2*m0*np.log(3600*3.0/t2)) # Sea state duration is taken as 3-hours

	return Xmax

# def interpolate_rank2_matrix(x,M_in,xp):
# 	# Inputs:
# 	# 	- x array of rank1
# 	# 	- M_in matrix of rank3 [:,:,len(x)]
# 	# 	- xp float
# 	# Ouputs:
# 	#	- Mp: matrix interpolated at xp. dim = [:,:]

# 	lshape = M_in.shape
# 	n1 = lshape[0]
# 	n2 = lshape[1]
# 	nx = lshape[2]
# 	Mp = np.zeros(shape=(n1,n2), dtype=np.complex)

# 	for idx1 in range(0,n1):
# 		for idx2 in range(0,n2):
# 			f_abs = interpolate.interp1d(x, abs(M_in[idx1,idx2,:]))
# 			f_phase = interpolate.interp1d(x, np.angle(M_in[idx1,idx2,:]))
# 			Mp[idx1,idx2] = f_abs(xp) + np.exp((1j)*f_phase(xp))

# 	return Mp

def from_reduced_dof_to_dof(K_red_dof,shared_dof):

    K_dof = np.zeros(shape=(len(shared_dof),len(shared_dof)),dtype=K_red_dof.dtype)
    idl1=-1
    for idx1 in range(0,len(shared_dof)):
        if shared_dof[idx1]==1:
            idl1 = idl1 +1
            idl2=-1
            for idx2 in range(0,len(shared_dof)):
                if shared_dof[idx2]==1:
                	idl2=idl2+1
                	K_dof[idx1,idx2] = K_red_dof[idl1,idl2]
    return K_dof

def from_dof_to_reduced_dof(K_dof,shared_dof):

    n_dof = len(shared_dof)
    n_red_dof = sum(shared_dof)
    K_red_dof = np.zeros(shape=(n_red_dof,n_red_dof),dtype=K_dof.dtype)
    idl1=-1
    for idx1 in range(0,n_dof):
        if shared_dof[idx1]==1:
            idl1 = idl1 +1
            idl2=-1
            for idx2 in range(0,n_dof):
                if shared_dof[idx2]==1:
                	idl2=idl2+1
                	K_red_dof[idl1,idl2] = K_dof[idx1,idx2]
    return K_red_dof


