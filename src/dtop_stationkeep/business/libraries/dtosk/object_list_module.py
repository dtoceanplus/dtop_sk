# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



def get_object_from_list(object_list,object_name):

    object_instance = None

    for idx in range(0,len(object_list)):
        if object_list[idx].name == object_name:
            object_instance = object_list[idx]

    return object_instance

def get_linetype_data_from_linetypename(line_dictionnary,line_type_name):

    line_type = None

    for idx in range(0,len(line_dictionnary)):
        if line_dictionnary[idx].line_type_name == line_type_name:
            line_type = line_dictionnary[idx]

    if line_type == None:
        raise Exception('Error in get_linetype_data_from_linetypename: line_type_name unknown: ' + str(line_type_name))

    return line_type

