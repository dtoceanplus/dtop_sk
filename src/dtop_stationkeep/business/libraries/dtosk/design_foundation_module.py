# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections 
import numpy as np 
import os
import pandas as pd
import pytest
import copy
try: 
    import json 
except: 
    print('WARNING: json is not installed.') 
from .foundations.foundation_main_module import run_foundation_design

def run_foundation_design_wrapper(foundation_design_parameters,body,device_wet_profile,device_wet_width,water_depth,uls_env,catalogue_anchors):

    '''loads_ap (array): loadsin absolute value and application point [V_min, V_max, H_x, H_y, M_x, M_y, z_ap] : 
    - V_min: Vertical minimal value (upwards) [N]
    - V_max: Vertical maximal value (downwards) [N]
    - H_x: horizonal maximal load on x axis [N]
    - H_y: horizonal maximal load on y axis [N]
    - M_x: Maximal moment on x axis [Nm]
    - M_y: Maximal moment on x axis [Nm]
    - z_ap: distance between soil and the application point of loads [m]
    '''

    '''Inputs from Stationkeeping analysis'''
    if foundation_design_parameters.design_load_flag == 'auto':
        if body.positioning_type == 'moored':

            [design_force_at_anchor,nanchors,d_chain] = body.compute_design_force_at_anchor()
            if nanchors>0:
                load_ap = np.array([design_force_at_anchor[2], 0.0, design_force_at_anchor[0], design_force_at_anchor[1] , 0.0 , 0.0 , 0.0])

        elif body.positioning_type == 'fixed':
            d_chain = 0.0
            V_min = max(max(body.results.uls_results.forces_on_fixed_structure[:,:,2]))
            V_max = min(min(body.results.uls_results.forces_on_fixed_structure[:,:,2]))
            H_x = max(max(body.results.uls_results.forces_on_fixed_structure[:,:,0]))
            H_y = max(max(body.results.uls_results.forces_on_fixed_structure[:,:,1]))
            M_x = max(max(body.results.uls_results.forces_on_fixed_structure[:,:,3]))
            M_y = max(max(body.results.uls_results.forces_on_fixed_structure[:,:,4]))
            z_ap = body.initial_position[2]
            load_ap = np.array([-V_min,-V_max,abs(H_x),abs(H_y),abs(M_x),abs(M_y),water_depth+z_ap])
            nanchors = 1

        else:

            raise Exception("Error: unknown positioning type : " + str(body.positioning_type))

    elif foundation_design_parameters.design_load_flag == 'manual':
        if body.positioning_type == 'moored':
            [dum1,nanchors,d_chain] = body.compute_design_force_at_anchor()
        elif body.positioning_type == 'fixed':
            d_chain = 0.0
            nanchors = 1
        else:
            raise Exception("Error: unknown positioning type : " + str(body.positioning_type))
        z_ap = 0.0
        V_min = foundation_design_parameters.design_load[0]
        V_max = foundation_design_parameters.design_load[1]
        H_x = foundation_design_parameters.design_load[2]
        H_y = foundation_design_parameters.design_load[3]
        M_x = foundation_design_parameters.design_load[4]
        M_y = foundation_design_parameters.design_load[5]
        load_ap = np.array([V_min,V_max,abs(H_x),abs(H_y),abs(M_x),abs(M_y),z_ap])

    else:

        raise Exception("Error: unknown design load flag : " + str(foundation_design_parameters.design_load_flag))

    if nanchors>0:
        seabed_connection_type  = copy.deepcopy(body.positioning_type)

        '''User inputs'''
        foundation_type_selection = copy.deepcopy(foundation_design_parameters.foundation_type_selection)
        catalogue_index = copy.deepcopy(foundation_design_parameters.catalogue_index)
        foundation_preference = copy.deepcopy(foundation_design_parameters.foundation_preference)
        dist_attachment_pa = foundation_design_parameters.dist_attachment_pa
        pile_tip = foundation_design_parameters.pile_tip
        pile_length_above_seabed = foundation_design_parameters.pile_length_above_seabed               
        deflection_criteria_for_pile = foundation_design_parameters.deflection_criteria_for_pile
        soil_properties_sf= foundation_design_parameters.soil_properties_sf # user must choose between: user or default (1.3 - DNV-OSJ103)
        user_soil_properties_sf = foundation_design_parameters.user_soil_properties_sf
        foundation_material = foundation_design_parameters.foundation_material
        foundation_material_density = foundation_design_parameters.foundation_material_density #for gravity kg/m3
        SF = foundation_design_parameters.SF
        																								  

        '''inputs from SC'''
        soil_type = foundation_design_parameters.soil_type
        # Check soil type value is not empty
        if soil_type == "":
            raise Exception('Soil type has not been defined! Read value : ' + soil_type)
        # variable needed only if soil_type_from_sc ='custom'
        soil_type_def = foundation_design_parameters.soil_type_def # then must choose between : cohesive or cohesionless
        undrained_shear_strength = foundation_design_parameters.undrained_shear_strength #soil cohesion requiered for soil_type = cohesive; in Pa
        internal_friction_angle =foundation_design_parameters.internal_friction_angle #data required for soil_type = cohesionless, in deg
        soil_bouyant_weight = foundation_design_parameters.soil_buoyant_weight # soil bouyant weight in N/m3
        relative_density = foundation_design_parameters.relative_density #data requiered for soil_type = cohesionless, no unit															 
        soil_slope = foundation_design_parameters.soil_slope

        ''' build device geometry '''
        device_geometry = {'geometry':device_wet_profile, 'lr':device_wet_width, 'lx':device_wet_width, 'ly':device_wet_width}
        [inputs,found_def,found_design] = run_foundation_design(foundation_preference,dist_attachment_pa,pile_tip,pile_length_above_seabed,deflection_criteria_for_pile,soil_type,soil_slope,seabed_connection_type,device_geometry,load_ap,SF, \
                        soil_type_def,foundation_material,foundation_material_density,internal_friction_angle,relative_density,undrained_shear_strength,soil_bouyant_weight,soil_properties_sf,user_soil_properties_sf, \
                        d_chain,foundation_design_parameters.foundation_design_flag,foundation_design_parameters.custom_foundation_input,uls_env,water_depth, foundation_type_selection, catalogue_anchors, catalogue_index)

        # Define default cost, if foundation was selected automatically
        if (foundation_design_parameters.foundation_preference == 'evaluation'):
            if (found_def.foundation_type == 'pile' or found_def.foundation_type == 'suction_caisson'):
                foundation_cost_per_kilo = 3.7 # Estimated cost of a steel pile €/kg (SANDIA report)
            elif found_def.foundation_type == 'drag_anchor':
                foundation_cost_per_kilo = 5.0 # Average cost of drag anchors for ORE systems €/kg (Manufacturer estimation)
            else:
                foundation_cost_per_kilo = 0.1 # Average cost of manufactured concrete €/kg
        else:
            foundation_cost_per_kilo = foundation_design_parameters.foundation_cost_per_kilo

        # Populate results 
        body.results.design_results.anchor_and_foundation.foundation_type = copy.deepcopy(found_def.foundation_type)
        body.results.design_results.anchor_and_foundation.cost_per_kilo = copy.deepcopy(foundation_cost_per_kilo)
        if body.positioning_type == 'moored':
            body.results.design_results.anchor_and_foundation.n_anchors = int(copy.deepcopy(nanchors))
        elif body.positioning_type == 'fixed':
            body.results.design_results.anchor_and_foundation.n_anchors = 1
        add_name = body.name
        if found_def.foundation_type == 'pile':
            if body.positioning_type == 'moored':
                found_design.name = 'pile_anchor'+add_name
            else:
                found_design.name = 'pile_foundation'+add_name
            body.results.design_results.anchor_and_foundation.pile_foundation_design = copy.deepcopy(found_design)
            body.results.design_results.anchor_and_foundation.unit_cost = foundation_cost_per_kilo*found_design.pile_weight/9.81
        elif found_def.foundation_type == 'gravity_anchor' or found_def.foundation_type == 'shallow':
            if body.positioning_type == 'moored':
                found_design.name = 'gravity_anchor'+add_name
            else:
                found_design.name = 'gravity_foundation'+add_name
            body.results.design_results.anchor_and_foundation.gravity_foundation_design = copy.deepcopy(found_design)
            body.results.design_results.anchor_and_foundation.unit_cost = foundation_cost_per_kilo*found_design.found_weight/9.81
            print('test3 ' + str(foundation_cost_per_kilo) + ' ' + str(found_design.found_weight/9.81))
        elif found_def.foundation_type == 'drag_anchor':
            found_design.name = 'drag_anchor'+add_name
            body.results.design_results.anchor_and_foundation.drag_anchor_design = copy.deepcopy(found_design)
            body.results.design_results.anchor_and_foundation.unit_cost = foundation_cost_per_kilo*found_design.weight # found.design.weight here is the mass, not the weight.
        elif found_def.foundation_type == 'suction_caisson':
            found_design.name = 'suction_caisson'+add_name
            body.results.design_results.anchor_and_foundation.suction_caisson_design = copy.deepcopy(found_design)
            body.results.design_results.anchor_and_foundation.unit_cost = foundation_cost_per_kilo*found_design.anchor_mass   
        else:
            raise Exception("Error: cannot populate results of foundation type " + str(found_def.foundation_type))
    else:
        body.results.design_results.anchor_and_foundation.n_anchors = 0
        body.results.design_results.anchor_and_foundation.unit_cost = 0.0
        inputs = None
        found_def = None
        found_design = None

    return inputs,found_def,found_design

