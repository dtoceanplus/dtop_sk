# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
from .rotation_module import Rab
from .intersection_module import intersection_segment_plane_z0


def compute_cylinder_projected_area(c1_n,c2_n,diameter,beta):
	# inputs:
	# c1_n, c2_n : coordinates of the ends of the cylinder
	# diameter: diameter of the cylinder
	# beta: wind direction [rad]. Projected area is computed on the plane perpendicular to the wind direction

	# Init
	proj_area = 0.0

	if np.linalg.norm(c1_n-c2_n) > 0.0:
		w1=np.array([np.cos(beta),np.sin(beta),0])
		w2=np.array([-np.sin(beta),np.cos(beta),0])
		w3=np.array([0,0,1])

		proj_vect=np.zeros(shape=(2),dtype=float)
		proj_vect[0]=np.dot((c2_n-c1_n),w2)
		proj_vect[1]=np.dot((c2_n-c1_n),w3)

		proj_l=np.linalg.norm(proj_vect)

		proj_area = proj_l*diameter

	return proj_area

def get_wet_cylinder_properties(body_position_in_n,cylinder_position_in_b,cylinder):

	# Ouputs
	# - l : length of the cut cylinder
	# - pa_n : position in n of the origin of the cut cylinder (placed at half-length)
	# - z : maximum vertical position of the cut cylinder (=0 if the cylinder is surface piercing)
	# - c1_n : position in n of end no.1 of the cut cylinder
	# - c2_n : position in n of end no.2 of the cut cylinder

	keep_z_sign = -1.0
	[l,pa_n,z,c1_n,c2_n] = get_cut_cylinder_properties(body_position_in_n,cylinder_position_in_b,cylinder,keep_z_sign)

	return l,pa_n,z,c1_n,c2_n

def get_dry_cylinder_properties(body_position_in_n,cylinder_position_in_b,cylinder):

	keep_z_sign = 1.0
	[l,pa_n,z,c1_n,c2_n] = get_cut_cylinder_properties(body_position_in_n,cylinder_position_in_b,cylinder,keep_z_sign)

	return l,pa_n,z,c1_n,c2_n

def get_cylinder_intersection(body_position_in_n,cylinder_position_in_b,cylinder):

	# Outputs:
	# - pi_n : position of the intersection point in n
	# - intersection_exists: True if the cylinder crosses the mean water level
	# - pp1_n : position of the end no.1 of the cylinder in n
	# - pp2_n : position of the enf no.2 of the cylinder in n
	# - pp_n : position of the cylinder origin in n

	# Rotation matrix of body in {n}
	Rbn=Rab(body_position_in_n[3:])

	# Rotation matrix of bodypart in {b}
	Rpb=Rab(cylinder_position_in_b[3:])				

	# Center of the cylinder
	pp_n = body_position_in_n[0:3] + Rbn.dot(cylinder_position_in_b[0:3].T).T

	# top of the cylinder
	pp1_p=np.zeros(shape=(3), dtype=float)
	pp1_p[2]=cylinder.length/2
	pp1_b=cylinder_position_in_b[0:3] + Rpb.dot(pp1_p.T).T
	pp1_n=body_position_in_n[0:3] + Rbn.dot(pp1_b.T).T

	# bottom of the cylinder
	pp2_p=np.zeros(shape=(3), dtype=float)
	pp2_p[2]=-cylinder.length/2
	pp2_b=cylinder_position_in_b[0:3] + Rpb.dot(pp2_p.T).T
	pp2_n=body_position_in_n[0:3] + Rbn.dot(pp2_b.T).T

	# compute intersection of the cylinder axis with the mean water level (plan z=0)
	[pi_n,intersection_exists] = intersection_segment_plane_z0(pp1_n,pp2_n)

	# Check if cylinder is "cut" exactly at one end (in that case, it should not be considered as cut)
	if ((intersection_exists) and (pi_n[2]==pp1_n[2] or pi_n[2]==pp2_n[2])):
		intersection_exists = False

	return pi_n, intersection_exists, pp1_n, pp2_n, pp_n


def get_cut_cylinder_properties(body_position_in_n,cylinder_position_in_b,cylinder,keep_z_sign):

	# inputs: 
	# keep_z_sign: =1 to keep the cylinder part that is above water level
	#			   =-1 to keep the cylinder part that is below water level

	# Ouputs
	# - l : length of the cut cylinder
	# - pa_n : position in n of the origin of the cut cylinder (placed at half-length)
	# - z : maximum vertical position of the cut cylinder
	# - c1_n : position in n of end no.1 of the cut cylinder
	# - c2_n : position in n of end no.2 of the cut cylinder

	# compute intersection of the cylinder axis with the mean water level (plan z=0)
	[pi_n,intersection_exists,pp1_n,pp2_n, pp_n] = get_cylinder_intersection(body_position_in_n,cylinder_position_in_b,cylinder)

	# Compute cut cylinder length (l), center of the cut cylinder (pa_n), maximum altitude of the cut cylinder and end positions of cut cylinder (c1_n and c2_n)
	if ((not intersection_exists) and ((pp2_n[2]*keep_z_sign>0.0)or(pp1_n[2]*keep_z_sign>0.0))):
		pa_n=pp_n
		l=cylinder.length
		z=max(pp2_n[2],pp1_n[2])
		c1_n=pp1_n 
		c2_n=pp2_n
	elif ((not intersection_exists) and ((pp2_n[2]*keep_z_sign<0.0)or(pp1_n[2]*keep_z_sign<0.0))):
		pa_n=pp2_n
		l=0.0
		z=0.0
		c1_n=0.0
		c2_n=0.0
	elif pp2_n[2]*keep_z_sign>0.0:
		c1_n=pi_n
		c2_n=pp2_n
		pa_n = c1_n + 0.5*(c2_n - c1_n)
		l=np.linalg.norm(c2_n-c1_n)
		z=max(c1_n[2],c2_n[2])
	elif pp1_n[2]*keep_z_sign>0.0:
		c1_n=pp1_n
		c2_n=pi_n
		pa_n = c1_n + 0.5*(c2_n - c1_n)
		l=np.linalg.norm(c2_n-c1_n)
		z=max(c1_n[2],c2_n[2])
	else:
		raise Exception("Error in intersection_module.get_dry_cylinder: this error should never happen. Intersection of segment and plane.")

	return l,pa_n,z,c1_n,c2_n

def get_dry_rectangle_properties(body_position_in_n,rect_position_in_b,rectangle):

	# Assumptions 1: we neglect roll and pitch of the body when computing wind direction for computing aspect ratio
	# Assumptions 2: we assume roll and pitch of rectangular cuboid in {b} to be zero

	keep_z_sign=1.0
	[zmax,height,width,length,pa_n] = get_cut_rectangle_properties(body_position_in_n,rect_position_in_b,rectangle,keep_z_sign)

	return zmax,height,width,length,pa_n

def get_wet_rectangle_properties(body_position_in_n,rect_position_in_b,rectangle):

	# Assumptions 1: we neglect roll and pitch of the body when computing wind direction for computing aspect ratio
	# Assumptions 2: we assume roll and pitch of rectangular cuboid in {b} to be zero

	keep_z_sign=-1.0
	[zmax,height,width,length,pa_n] = get_cut_rectangle_properties(body_position_in_n,rect_position_in_b,rectangle,keep_z_sign)

	return zmax,height,width,length,pa_n

def get_cut_rectangle_properties(body_position_in_n,rect_position_in_b,rectangle,keep_z_sign):

	# inputs: 
	# keep_z_sign: =1 to keep the rectangle part that is above water level
	#			   =-1 to keep the rectangle part that is below water level

	# Assumptions 1: we neglect roll and pitch of the body when computing wind direction for computing aspect ratio
	# Assumptions 2: we assume roll and pitch of rectangular cuboid in {b} to be zero

	# Rotation matrix of body in {n}
	Rbn=Rab(body_position_in_n[3:])

	# Compute dry height
	pa_n = body_position_in_n[0:3] + Rbn.dot(rect_position_in_b[0:3].T).T
	z_n  = pa_n[2]
	if z_n*keep_z_sign > rectangle.dz/2.0:
		height = rectangle.dz
		zmax = pa_n[2] + rectangle.dz/2.0
	elif z_n*keep_z_sign < -rectangle.dz/2.0:
		height = 0.0
		pa_n[:]=0.0
		zmax=0.0
	else:
		height = keep_z_sign*z_n + rectangle.dz/2.0
		pa_n[2] = keep_z_sign*height/2.0
		zmax = pa_n[2] + height/2.0
	
	# Compute width and length
	width = rectangle.dy
	length = rectangle.dx

	return zmax,height,width,length,pa_n

def plot_cylinder_in_n(p0,p1,R,ax):

	#vector in direction of axis
	v = p1 - p0
	#find magnitude of vector
	mag = np.linalg.norm(v)
	#unit vector in direction of axis
	if abs(mag)>0:
		v = v / mag
		#make some vector not in the same direction as v
		not_v = np.array([1, 0, 0])
		if (v == not_v).all():
			not_v = np.array([0, 1, 0])
		#make vector perpendicular to v
		n1 = np.cross(v, not_v)
		#normalize n1
		n1 /= np.linalg.norm(n1)
		#make unit vector perpendicular to v and n1
		n2 = np.cross(v, n1)
		#surface ranges over t from 0 to length of axis and 0 to 2*pi
		t = np.linspace(0, mag, 2)
		theta = np.linspace(0, 2 * np.pi, 10)
		#use meshgrid to make 2d arrays
		t, theta = np.meshgrid(t, theta)
		#generate coordinates for surface
		X, Y, Z = [p0[i] + v[i] * t + R * np.sin(theta) * n1[i] + R * np.cos(theta) * n2[i] for i in [0, 1, 2]]
		ax.plot_surface(X, Y, Z)

def plot_rectangle_in_n(pos,dx,dy,dz,ax):

	# Rotation matrix of body in {n}
	Rbn=Rab(pos[3:6])

	def x_y_edge(x_range, y_range, z_range):

		# Init
		xx_b, yy_b = np.meshgrid(x_range, y_range)
		xx, yy = np.meshgrid(x_range, y_range)
		zz = xx*0.0

		for value in [0,1]:
			for idx1 in range(0,2):
				for idx2 in range(0,2):
					pos_b = np.array([xx_b[idx1,idx2],yy_b[idx1,idx2],z_range[value]])
					pos_n = pos[0:3] + Rbn.dot(pos_b.T).T
					xx[idx1,idx2] = pos_n[0]
					yy[idx1,idx2] = pos_n[1]
					zz[idx1,idx2] = pos_n[2]
			ax.plot_wireframe(xx, yy, zz, color="g")
			ax.plot_surface(xx, yy, zz, color="g")


	def y_z_edge(x_range, y_range, z_range):

		# Init
		yy_b, zz_b = np.meshgrid(y_range, z_range)
		yy, zz = np.meshgrid(y_range, z_range)
		xx = yy*0.0

		for value in [0,1]:
			for idx1 in range(0,2):
				for idx2 in range(0,2):
					pos_b = np.array([x_range[value],yy_b[idx1,idx2],zz_b[idx1,idx2]])
					pos_n = pos[0:3] + Rbn.dot(pos_b.T).T
					xx[idx1,idx2] = pos_n[0]
					yy[idx1,idx2] = pos_n[1]
					zz[idx1,idx2] = pos_n[2]
			ax.plot_wireframe(xx, yy, zz, color="g")
			ax.plot_surface(xx, yy, zz, color="g")


	def x_z_edge(x_range, y_range, z_range):
		xx, zz = np.meshgrid(x_range, z_range)

		# Init
		xx_b, zz_b = np.meshgrid(x_range, z_range)
		xx, zz = np.meshgrid(x_range, z_range)
		yy = xx*0.0

		for value in [0,1]:
			for idx1 in range(0,2):
				for idx2 in range(0,2):
					pos_b = np.array([xx_b[idx1,idx2],y_range[value],zz_b[idx1,idx2]])
					pos_n = pos[0:3] + Rbn.dot(pos_b.T).T
					xx[idx1,idx2] = pos_n[0]
					yy[idx1,idx2] = pos_n[1]
					zz[idx1,idx2] = pos_n[2]
			ax.plot_wireframe(xx, yy, zz, color="g")
			ax.plot_surface(xx, yy, zz, color="g")

	def rect_prism(x_range, y_range, z_range):
		x_y_edge(x_range, y_range, z_range)
		y_z_edge(x_range, y_range, z_range)
		x_z_edge(x_range, y_range, z_range)

	x_range = np.array([-dx,dx])
	y_range = np.array([-dy,dy])
	z_range = np.array([-dz,dz])

	rect_prism(x_range, y_range, z_range)
	