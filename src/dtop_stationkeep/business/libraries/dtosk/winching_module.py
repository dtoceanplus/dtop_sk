# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
from .solver.Static import Static
import copy


def winch_line_for_pretension(body,pretension_coeff_min,length_increment,water_depth,water_density):

	# Set body position equal to initial position
	body.position = copy.deepcopy(body.initial_position)

	# Compute equilibrium at rest
	static_solver = Static()
	status = np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype = int)
	iterative_process = True
	line_length_increment =np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype = float)
	counter = 0

	if pretension_coeff_min>0.0:

		while (iterative_process and counter<100):

			counter = counter+1
			
			# Compute pretension
			tension = get_pretension_for_line_length_increment(body,line_length_increment,water_depth,water_density)

			# Compute tension in each line and compare to target pretension
			for idx in range(0,len(body.mooring_system_force.line_properties)):

				# Get mbl of the line
				idx_type = body.mooring_system_force.line_properties[idx].line_type_index
				mbl = body.mooring_system_force.line_dictionnary[idx_type].mbl
				if tension[idx] >= pretension_coeff_min*mbl:
					status[idx] = 0 # Ok, do nothing
				else:
					status[idx] = -1 # Pretension too low: reduce line length

			# Change length if needed
			if (status == -1).all():
				# We can try to decrease the length of every line
				line_length_increment[:] = -length_increment
				
			elif (status == 0).all():
				# Each line is ok: stop here
				iterative_process = False

def get_pretension_for_line_length_increment(body,line_length_increment,water_depth,water_density):
        
	# Init
	static_solver = Static()
	tension = np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype = float)

	if sum(line_length_increment)!=0.0:

		# Change line length
		for idx in range(0,len(body.mooring_system_force.line_properties)):
			body.mooring_system_force.line_properties[idx].unstretched_length = body.mooring_system_force.line_properties[idx].unstretched_length+line_length_increment[idx]
		# Re-initialise the mooring system
		body.mooring_system_force.initiate_calculation(water_depth,water_density,body.initial_position)

	# Compute equilibrium
	[xmean,eq_found] = static_solver.compute_equilibrium(body)

	# Compute tension in each line
	for idx in range(0,len(body.mooring_system_force.line_properties)):
		tension[idx] = body.mooring_system_force.get_tension_at_fairlead(idx)

	# Restore body position
	body.position = copy.deepcopy(body.initial_position)

	return tension

# def winch_line_for_pretension_old(body,pretension_coeff_max,pretension_coeff_min,length_increment,water_depth,water_density):

# 	# Set body position equal to initial position
# 	body.position = copy.deepcopy(body.initial_position)

# 	# Compute equilibrium at rest
# 	static_solver = Static()
# 	status = np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype = int)
# 	iterative_process = True
# 	line_length_increment =np.zeros(shape=(len(body.mooring_system_force.line_properties)),dtype = float)
# 	counter = 0

# 	while (iterative_process and counter<100):

# 		counter = counter+1
		
# 		# Compute pretension
# 		tension = get_pretension_for_line_length_increment(body,line_length_increment,water_depth,water_density)

# 		# Compute tension in each line and compare to target pretension
# 		for idx in range(0,len(body.mooring_system_force.line_properties)):

# 			# Get mbl of the line
# 			idx_type = body.mooring_system_force.line_properties[idx].line_type_index
# 			mbl = body.mooring_system_force.line_dictionnary[idx_type].mbl
# 			if tension[idx] < pretension_coeff_max*mbl and tension[idx] > pretension_coeff_min*mbl:
# 				status[idx] = 0 # Ok, do nothing
# 			elif tension[idx] > pretension_coeff_max*mbl:
# 				status[idx] = 1 # Pretension already too high: increase
# 			elif tension[idx] < pretension_coeff_min*mbl:
# 				status[idx] = -1 # Pretension too low: reduce length

# 		# Change length if needed
# 		if all(status) == -1:
# 			# We can try to decrease the length of every line
# 			line_length_increment[:] = -length_increment
			
# 		elif all(status) == 0:
# 			# Each line is ok: stop here
# 			iterative_process = False

# 		elif any(status) == 1:
# 			# At least one line has too high pretension: increase the length of all lines
# 			line_length_increment[:] = length_increment