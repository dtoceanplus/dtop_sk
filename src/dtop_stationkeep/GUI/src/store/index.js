// This is the Station Keeping module of the DTOceanPlus suite
// Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import axios from 'axios'
import { DTOP_DOMAIN, DTOP_PROTOCOL } from "@/../dtop/dtopConsts"

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    Project: [],
    ProjectStatus: [],
    test_begin: false,
    define_solver_option: false,
    RunStatus: '',
    UseSameForAll: false,
    GoToRunModule: false,
    WFM_present: false,
    is_image: false,
    index_of_mooring_system: [],
    uls_direction_in_deg: [],
    data_plot: [],
    layout_plot: [],
    Results: [],
    project_running: false,
    is_saved: false,
    Name: '',
    device_selected: ''
  },
  modules: {
    app,
    settings,
    user
  },
  mutations: {
    set_Project: (state, Project) => {
      if (Project === 1) {
        Initiate_Project_ProjectStatus()
      } else {
        state.Project = Project
      }
    },
    set_ProjectStatus: (state, ProjectStatus) => {
      state.ProjectStatus = ProjectStatus
    },
    set_TestBegin: (state, test_begin) => {
      state.test_begin = test_begin
    },
    set_define_solver_option: (state, define_solver_option) => {
      state.define_solver_option = define_solver_option
    },
    set_RunStatus: (state, RunStatus) => {
      state.RunStatus = RunStatus
    },
    set_UseSameForAll: (state, UseSameForAll) => {
      state.UseSameForAll = UseSameForAll
    },
    set_GoToRunModule: (state, GoToRunModule) => {
      state.GoToRunModule = GoToRunModule
    },
    set_WFM_present: (state, WFM_present) => {
      state.WFM_present = WFM_present
    },
    set_is_image: (state, is_image) => {
      state.is_image = is_image
    },
    set_index_of_mooring_system: (state, index_of_mooring_system) => {
      state.index_of_mooring_system = index_of_mooring_system
    },
    set_data_plot: (state, data_plot) => {
      state.data_plot = data_plot
    },
    set_layout_plot: (state, layout_plot) => {
      state.layout_plot = layout_plot
    },
    set_Results: (state, Results) => {
      state.Results = Results
    },
    set_project_running: (state, project_running) => {
      state.project_running = project_running
    },
    set_is_saved: (state, is_saved) => {
      state.is_saved = is_saved
    },
    set_Name: (state, Name) => {
      state.Name = Name
    },
    set_device_selected: (state, device_selected) => {
      state.device_selected = device_selected
    }
  },
  actions: {
    set_ProjectAction: ({commit, state}, newValue) => {
      commit('set_Project', newValue)
      return state.Project
    },
    set_ProjectStatusAction: ({commit, state}, newValue) => {
      commit('set_ProjectStatus', newValue)
      return state.ProjectStatus
    },
    set_testbeginAction: ({commit, state}, newValue) => {
      commit('set_TestBegin', newValue)
      return state.test_begin
    },
    set_definesolveroptionAction: ({commit, state}, newValue) => {
      commit('set_define_solver_option', newValue)
      return state.define_solver_option
    },
    set_RunStatusAction: ({commit, state}, newValue) => {
      commit('set_RunStatus', newValue)
      return state.RunStatus
    },
    set_UseSameForAllAction: ({commit, state}, newValue) => {
      commit('set_UseSameForAll', newValue)
      return state.UseSameForAll
    },
    set_GoToRunModuleAction: ({commit, state}, newValue) => {
      commit('set_GoToRunModule', newValue)
      return state.GoToRunModule
    },
    set_WFM_presentAction: ({commit, state}, newValue) => {
      commit('set_WFM_present', newValue)
      return state.WFM_present
    },
    set_is_imageAction: ({commit, state}, newValue) => {
      commit('set_is_image', newValue)
      return state.is_image
    },
    set_index_of_mooring_systemAction: ({commit, state}, newValue) => {
      commit('set_index_of_mooring_system', newValue)
      return state.index_of_mooring_system
    },
    set_data_plotAction: ({commit, state}, newValue) => {
      commit('set_data_plot', newValue)
      return state.data_plot
    },
    set_layout_plotAction: ({commit, state}, newValue) => {
      commit('set_layout_plot', newValue)
      return state.layout_plot
    },
    set_ResultsAction: ({commit, state}, newValue) => {
      commit('set_Results', newValue)
      return state.Results
    },
    set_project_runningAction: ({commit, state}, newValue) => {
      commit('set_project_running', newValue)
      return state.project_running
    },
    set_is_savedAction: ({commit, state}, newValue) => {
      commit('set_is_saved', newValue)
      return state.is_saved
    },
    set_NameAction: ({commit, state}, newValue) => {
      commit('set_Name', newValue)
      return state.Name
    },
    set_device_selectedAction: ({commit, state}, newValue) => {
      commit('set_device_selected', newValue)
      return state.device_selected
    }
  },
  getters
})

const Initiate_Project_ProjectStatus = () => {
  const path = `${DTOP_PROTOCOL}//sk.${DTOP_DOMAIN}/sk/initialise_Project_ProjectStatus`
  axios
    .get(path)
    .then((res) => {
      store.commit('set_Project', res.data[0]);
      store.commit('set_ProjectStatus', res.data[1]);
    })
}

Initiate_Project_ProjectStatus();

export default store
