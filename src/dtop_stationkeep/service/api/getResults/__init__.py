# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from flask import Blueprint, render_template, url_for, jsonify, request, current_app, send_file, abort
from os import listdir
import os
import os.path
import sys
from dtop_stationkeep.business.libraries.dtosk.outputs.Project import Project
#------------------------------------
# @ USER DEFINED IMPORTS START
import json
from dtop_stationkeep.business.libraries.dtosk.inputs.InputStatus import InputStatus
# @ USER DEFINED IMPORTS END
#------------------------------------


bp = Blueprint('getResults', __name__)

#------------------------------------
# @ USER DEFINED FUNCTIONS START

@bp.route('/<ProjectId>/status', methods=['GET'])
def get_project_status(ProjectId):

    Entity_name = 'error404'    

    if (os.path.exists('./storage/' + ProjectId + '/status.json')):
        project_name = ProjectId
    else:
        with open('./storage/Entity_table.json') as json_file:
            List = json.load(json_file)
            for entity in List['List_of_entity']:
                if int(ProjectId) == entity['EntityId']:
                    Entity_name = entity['EntityName']
        project_name=Entity_name + '_' + ProjectId

    filename=os.path.join('./storage',project_name+'/status.json')
    if os.path.exists(filename):
        status=InputStatus()
        status.loadJSON(filePath = filename)
    else:
        abort(404)

    try:
        res=status.entity_status
    except:
        abort(404)
    return jsonify(res)

# @ USER DEFINED FUNCTIONS END
#------------------------------------

##############################
# Functions for project load #
##############################

# Load project file
def load_Project(ProjectId):

    Entity_name = 'error404'    

    if (os.path.exists('./storage/' + ProjectId + '/outputs.json')):
        project_name = ProjectId
    else:
        with open('./storage/Entity_table.json') as json_file:
            List = json.load(json_file)
            for entity in List['List_of_entity']:
                if int(ProjectId) == entity['EntityId']:
                    Entity_name = entity['EntityName']
        project_name=Entity_name + '_' + ProjectId

    filename=os.path.join('./storage',project_name+'/outputs.json')
    if os.path.exists(filename):
        project=Project()
        project.loadJSON(filePath = filename)
    else:
        abort(404)
    return project

# Find object index in array from its name
def find_index_from_name(name,object_array):
    index=-1
    for idx in range(0,len(object_array)):
        if name==object_array[idx].name:
            index=idx
    if index==-1:
        abort(404)
    return index

###########
# GET API #
###########

@bp.route('/<ProjectId>', methods=['GET'])
def get_Project(ProjectId):
    project=load_Project(ProjectId)
    return jsonify(project.prop_rep())


@bp.route('/<ProjectId>/hierarchy', methods=['GET'])
def get_project_hierarchy(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.hierarchy.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/bom', methods=['GET'])
def get_project_bom(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.bom.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/environmental_impact', methods=['GET'])
def get_project_environmental_impact(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.environmental_impact.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/design_assessment', methods=['GET'])
def get_project_design_assessment(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.design_assessment.prop_rep()
    except:
        abort(404)
    return jsonify(res)

#####################
# Utility functions #
#####################

def list_files(directory, extension):
    fl=listdir(directory)
    folder=[]
    folder_name=[]
    files=[]
    for i in range(0,len(fl)):
        path=os.path.join(directory, fl[i])
        path=os.path.abspath(path)
        if os.path.isdir(path):
            folder.append(path)
            folder_name.append(fl[i])
        elif path.endswith('.'+extension):
            files.append(path)
    return folder,folder_name,files
