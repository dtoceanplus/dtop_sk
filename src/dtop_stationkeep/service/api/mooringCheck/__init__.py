# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from flask import Blueprint, render_template, url_for, jsonify, request, current_app, send_file, abort
from os import listdir
import os
import os.path
import sys
import collections
from dtop_stationkeep.business.libraries.dtosk.inputs.MooringCheckInputs import MooringCheckInputs
from dtop_stationkeep.business.libraries.dtosk.run_mooring_check_module import run_mooring_check
from dtop_stationkeep.business.libraries.dtosk.outputs.MooringCheckResults import MooringCheckResults



bp = Blueprint('mooringCheck', __name__)


def load_inputs_from_request():

    # Create input main structure
    my_inputs = MooringCheckInputs()

    # Populate input data
    my_inputs_rep = request.get_json()
    my_inputs.loadFromJSONDict(my_inputs_rep)

    return my_inputs

# API that reads a MooringCheckInputs inputs from request, run mooring check and generate a plot if mooring has been defined correctly
@bp.route('/run_mooring_check', methods=['POST'])
def run():

    if not os.path.exists('./storage/Tmp/Mooring_system'):
        os.mkdir('./storage/Tmp/Mooring_system')
    if not os.path.exists('./storage/Tmp/Mooring_system/mooring_check_results.json'):
        my_data = MooringCheckResults()
        my_data.saveJSON('./storage/Tmp/Mooring_system/mooring_check_results.json')

    # Load inputs from request
    my_inputs = load_inputs_from_request()

    # Run analysis
    log = collections.OrderedDict()

    try:

        # Run analysis
        run_mooring_check(my_inputs,show=False)

        # Report success
        log["analysis_status"] = "success"

    except Exception as e:

        # Report fail and error message
        log["analysis_status"] = "failed"
        if hasattr(e, 'message'):
            log["error_message"] =str(e.message)
        else:
            log["error_message"] =str(e)

    return jsonify(log)

# API that reads a MooringCheckResults
@bp.route('/load_mooring_check', methods=['GET'])
def load_data():

    my_data = MooringCheckResults()

    filename = './storage/Tmp/Mooring_system/mooring_check_results.json'
    my_data.loadJSON(filePath = filename)

    return jsonify(my_data.prop_rep())