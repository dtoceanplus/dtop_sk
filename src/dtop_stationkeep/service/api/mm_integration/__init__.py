# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from flask import Blueprint, render_template, url_for, jsonify, request, current_app, send_file, abort
from os import listdir
import os
import os.path
import sys
from dtop_stationkeep.business.libraries.dtosk.inputs.Inputs import Inputs
from dtop_stationkeep.business.libraries.dtosk.inputs.InputStatus import InputStatus
from dtop_stationkeep.service.api.postInputs import save_inputs_to_storage
import json
import requests as reqs

bp = Blueprint('mm_integration', __name__)

# API that create an entity defined by the name defined in the Maine Module
@bp.route('/', methods=['POST'], strict_slashes=False)
def create_entity():

    if not(os.path.exists('./storage/Entity_table.json')):
        f = open("./storage/Entity_table.json",'w')
        f.write('{"List_of_entity": []}')
        f.close()

    request_body = request.get_json()
    if not request_body:
        return ('No request body provided'), 400

    EntityName = request_body.get('name')
    Complexity_level = request_body.get('cplx')
    eid = save_inputstatus(EntityName, Complexity_level)

    # Create input structure
    my_input = Inputs(EntityName)
    my_input.name = EntityName

    # Save the input structure
    save_inputs_to_storage(my_input,EntityName+'_'+str(eid))

    return ({"EntityId": eid, "EntityStatus": 0}),201

def save_inputstatus(ProjectId, cplx):

    # Create status structure from request
    my_inputstatus = InputStatus()

    my_inputstatus.name = ProjectId
    my_inputstatus.complexity_level = cplx

    cpt = 1
    testlist = []

    # Get the ID
    with open('./storage/Entity_table.json') as json_file:
        List = json.load(json_file)
        for entity in List['List_of_entity']:
            testlist.append(entity['EntityId'])

    while cpt in testlist:
        cpt = cpt+1

    List['List_of_entity'].append({"EntityName": ProjectId, "EntityId": cpt})
    my_inputstatus.entity_id = cpt

    ListtoWrite = json.dumps(List)
    f=open('./storage/Entity_table.json', "w")
    f.write(ListtoWrite)
    f.close()

    foldername = ProjectId + '_' + str(cpt)
    
    # Create the hierarchy in the storage directory

    if not os.path.exists('./storage/'+ foldername):
        os.mkdir('./storage/'+ foldername)
    if not os.path.exists('./storage/'+ foldername +'/figures'):
        os.mkdir('./storage/'+ foldername +'/figures')
    if not os.path.exists('./storage/'+ foldername +'/Nemoh'):
        os.mkdir('./storage/'+ foldername +'/Nemoh')
    if not os.path.exists('./storage/'+ foldername +'/Nemoh/results'):
        os.mkdir('./storage/'+ foldername +'/Nemoh/results')
    if not os.path.exists('./storage/'+ foldername +'/Nemoh/mesh'):
        os.mkdir('./storage/'+ foldername +'/Nemoh/mesh')
    if not os.path.exists('./storage/'+ foldername +'/Mooring_system'):
        os.mkdir('./storage/'+ foldername +'/Mooring_system')
    
    # Save the input status structure
    storagePath = './storage'
    fileName=os.path.join(storagePath,foldername + '/status.json')
    my_inputstatus.saveJSON(fileName)

    # Copy file from mooring check results (if any) into the project folder
    from shutil import copyfile
    
    # Save the project input
    storagePath = './storage'

    if os.path.exists('./storage/Tmp/Mooring_system/mooring_check_results.json'):
        copyfile('./storage/Tmp/Mooring_system/mooring_check_results.json', './storage/'+foldername+'/Mooring_system/mooring_check_results.json')

    if os.path.exists('./storage/Tmp/Mooring_system/mooring_check_plot.html'):
        copyfile('./storage/Tmp/Mooring_system/mooring_check_plot.html', './storage/'+foldername+'/figures/mooring_check_plot.html')

    return cpt

def delete_entity_by_name(ProjectId):

    src_files = os.listdir('./storage/'+ProjectId)
    for file_name in src_files:
        full_file_name = os.path.join('./storage/', ProjectId+'/'+file_name)
        if os.path.isfile(full_file_name):
            os.remove(full_file_name)
    
    src_files = os.listdir('./storage/'+ProjectId+'/figures')
    for file_name in src_files:
        full_file_name = os.path.join('./storage/', ProjectId+'/figures/'+file_name)
        if os.path.isfile(full_file_name):
            os.remove(full_file_name)
    os.rmdir(os.path.join('./storage/', ProjectId+'/figures'))
    
    src_files = os.listdir('./storage/'+ProjectId+'/Mooring_system')
    for file_name in src_files:
        full_file_name = os.path.join('./storage/', ProjectId+'/Mooring_system/'+file_name)
        if os.path.isfile(full_file_name):
            os.remove(full_file_name)
    os.rmdir(os.path.join('./storage/', ProjectId+'/Mooring_system'))
    
    src_files = os.listdir('./storage/'+ProjectId+'/Nemoh')
    for file_name in src_files:
        full_file_name = os.path.join('./storage/', ProjectId+'/Nemoh/'+file_name)
        if os.path.isfile(full_file_name):
            os.remove(full_file_name)
    
    src_files = os.listdir('./storage/'+ProjectId+'/Nemoh/results')
    for file_name in src_files:
        full_file_name = os.path.join('./storage/', ProjectId+'/Nemoh/results/'+file_name)
        if os.path.isfile(full_file_name):
            os.remove(full_file_name)
    os.rmdir(os.path.join('./storage/', ProjectId+'/Nemoh/results'))
    
    src_files = os.listdir('./storage/'+ProjectId+'/Nemoh/mesh')
    for file_name in src_files:
        full_file_name = os.path.join('./storage/', ProjectId+'/Nemoh/mesh/'+file_name)
        if os.path.isfile(full_file_name):
            os.remove(full_file_name)
    os.rmdir(os.path.join('./storage/', ProjectId+'/Nemoh/mesh'))

    os.rmdir(os.path.join('./storage/', ProjectId+'/Nemoh'))

    os.rmdir(os.path.join('./storage/', ProjectId))



# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/<Entity_id>/', methods=['DELETE'], strict_slashes=False)
def delete_entity(Entity_id):

    Entity_name = 'error'
    testlist = []

    # Get the Name
    with open('./storage/Entity_table.json') as json_file:
        List = json.load(json_file)
        for entity in List['List_of_entity']:
            if int(Entity_id) == entity['EntityId']:
                Entity_name = entity['EntityName']
            else:
                testlist.append(entity)

    List['List_of_entity'] = testlist
    ListtoWrite = json.dumps(List)
    f=open('./storage/Entity_table.json', "w")
    f.write(ListtoWrite)
    f.close()

    if (Entity_name == 'error'):
        return "Error", 500
    else:
        delete_entity_by_name(Entity_name + '_' + str(Entity_id))
        return ("Entity deleted"),200

@bp.route('/<eid>/Get_Entity_Name', methods=['GET'])
def get_entity_name(eid):
    Entity_name = 'error'
    testlist = []

    # Get the Name
    with open('./storage/Entity_table.json') as json_file:
        List = json.load(json_file)
        for entity in List['List_of_entity']:
            if int(eid) == entity['EntityId']:
                Entity_name = entity['EntityName']

    if (Entity_name == 'error'):
        return "This entity does not exist", 500
    else:
        return (Entity_name + '_' + str(eid)),200

# @bp.route('/integration/list_of_entity', methods=['GET'])
# def list_of_entity():
#     storagePath = './storage'
#     entity_list = []
#     fl = listdir(storagePath)
#     for i, fli in enumerate(fl):
#         if (fli[-12:] == '_inputs.json'):
#             my_input_temp = Inputs()
#             my_input_temp.loadJSON(filePath = './storage/'+fli)
#             temp = {
#                 "entityId": int(my_input_temp.entity_id),
#                 "entityName": my_input_temp.name,
#                 "entityStatus": my_input_temp.entity_status
#             }
#             entity_list.append(temp)

#     return jsonify(entity_list)

# @bp.route('/integration/entity_info/<eid>', methods=['GET'])
# def entity_info(eid):
#     storagePath = './storage'
#     entityinfo = []
#     fl = listdir(storagePath)
#     for i, fli in enumerate(fl):
#         if (fli[-12:] == '_inputs.json'):
#             my_input_temp = Inputs()
#             my_input_temp.loadJSON(filePath = './storage/'+fli)
#             if (my_input_temp.entity_id == eid):
#                 entityinfo = {
#                     "entityId": int(my_input_temp.entity_id),
#                     "entityName": my_input_temp.name,
#                     "entityStatus": my_input_temp.entity_status
#                 }
#     if (entityinfo == []):
#         entityinfo = 'No entity with Id ' + eid
#     return jsonify(entityinfo)

