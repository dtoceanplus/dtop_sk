# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
import os
import copy


import plotly.graph_objects as go
import pandas as pd
import plotly.express as px
from dtop_stationkeep.business.libraries.dtosk.inputs.MooringCheckInputs import MooringCheckInputs
from dtop_stationkeep.business.libraries.dtosk.run_mooring_check_module import run_mooring_check

    # storage_dir = os.path.join(this_dir, "..","..","..","storage")
    # mooring_file = os.path.join(data_dir,'mooring_input_example.json')
    # mooring_check_inputs.custom_mooring_input.loadJSON(filePath=mooring_file)
    # mooring_check_inputs.water_depth = 100.0
    # mooring_check_inputs.water_density = 1025.0
    # mooring_check_inputs.device_position = np.array([0.0,0.0,0.0,0.0,0.0,0.0])
    # mooring_check_inputs.html_plot_file_name = os.path.join(storage_dir,'mooring_check_plot.html') 
    # mooring_check_inputs.logo_file_name = os.path.join(data_dir,'dto_logo.png') 
    # mooring_check_inputs.saveJSON(fileName=os.path.join(data_dir,'mooring_check_input_example.json'))

def test_plot_html_mooring():

    # Path definition
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    storage_dir = os.path.join(this_dir, "..","..","..","storage")
    mooring_file = os.path.join(data_dir,'mooring_input_example.json')

    # Load input
    mooring_check_inputs = MooringCheckInputs()
    mooring_check_inputs.custom_mooring_input.loadJSON(filePath=mooring_file)
    mooring_check_inputs.water_depth = 100.0
    mooring_check_inputs.water_density = 1025.0
    mooring_check_inputs.device_position = np.array([0.0,0.0,0.0,0.0,0.0,0.0])
    mooring_check_inputs.html_plot_file_name = os.path.join(storage_dir,'mooring_check_plot.html')
    mooring_check_inputs.results_file_name = os.path.join(storage_dir,'mooring_check_results.json') 
    mooring_check_inputs.logo_file_name = os.path.join(data_dir,'dto_logo.png') 

    # Run mooring check
    run_mooring_check(mooring_check_inputs,show=False)

    assert True


def test_plot_html_8lines_mooring():

    # Path definition
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    storage_dir = os.path.join(this_dir, "..","..","..","storage")
    mooring_file = os.path.join(data_dir,'mooring_input_masterstructure.json') 

    # Load input
    mooring_check_inputs = MooringCheckInputs()
    mooring_check_inputs.custom_mooring_input.loadJSON(filePath=mooring_file)
    mooring_check_inputs.water_depth = 100.0
    mooring_check_inputs.water_density = 1025.0
    mooring_check_inputs.device_position = np.array([0.0,0.0,0.0,0.0,0.0,0.0])
    mooring_check_inputs.html_plot_file_name = os.path.join(storage_dir,'mooring_8lines_check_plot.html')
    mooring_check_inputs.results_file_name = os.path.join(storage_dir,'mooring_8lines_check_results.json') 
    mooring_check_inputs.logo_file_name = os.path.join(data_dir,'dto_logo.png') 

    # Run mooring check
    run_mooring_check(mooring_check_inputs,show=False)

    assert True



