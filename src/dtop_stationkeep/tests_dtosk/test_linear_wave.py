# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
import os
import matplotlib.pyplot as plt

from dtop_stationkeep.business.libraries.dtosk.linear_wave_module import compute_cplx_acceleration
from dtop_stationkeep.business.libraries.dtosk.linear_wave_module import compute_cplx_elevation
from dtop_stationkeep.business.libraries.dtosk.linear_wave_module import compute_cplx_velocity
from dtop_stationkeep.business.libraries.dtosk.linear_wave_module import compute_cplx_pressure
from dtop_stationkeep.business.libraries.dtosk.linear_wave_module import compute_jonswap_spectrum
from dtop_stationkeep.business.libraries.dtosk.frequency_analysis_module import compute_amplitude_from_spectrum,compute_t2,compute_t1,compute_spectrum_moment

def test_cplx_velocity():

    omega = 2*np.pi/10.0
    water_depth = 30.0
    x0=0
    x=0
    z=0
    amp=5

    [ux,uz] = compute_cplx_velocity(omega,amp,water_depth,x0,x,z)

    assert ((abs(ux) == pytest.approx(3.5726019, 0.0001)) and (abs(uz) == pytest.approx(3.141592, 0.0001)))


def test_cplx_acceleration():

    omega = 2*np.pi/10.0
    water_depth = 30.0
    x0=0
    x=0
    z=0
    amp=5
    rho = 1025.0

    [ax,az] = compute_cplx_acceleration(omega,amp,water_depth,x0,x,z)

    zeta = compute_cplx_elevation(omega,amp,water_depth,x0,x)

    pressure = compute_cplx_pressure(rho,omega,amp,water_depth,x0,x,z)

    assert ((abs(zeta) == pytest.approx(5.0, 0.0001)) and (abs(ax) == pytest.approx(2.244731997687, 0.0001)) and (abs(az) == pytest.approx(1.9739208, 0.0001)))


def test_std():

    t = np.linspace(0, 360, num=1000) * np.pi / 180.0

    y = 5.5*np.cos(t)

    assert (y.std() == pytest.approx(5.5/(2**0.5), 0.01))

def test_jonswap():

    hs = 5.0
    tp = 12.8657
    omega_array = np.linspace(2*np.pi/50, 2*np.pi/3, num=1000)
    t_array = 2*np.pi/omega_array

    S = compute_jonswap_spectrum(hs,tp,omega_array)

    # Fig.2.6 in Sea Loads, Faltinsen
    #plt.plot(omega_array*tp/1.2865707/(2*np.pi),S/(hs**2*tp/1.2865707))
    #plt.show()

    eta = compute_amplitude_from_spectrum(omega_array,S)

    #plt.plot(t_array,eta)
    #plt.show()

    assert (max(S) == pytest.approx(9.92949352, 0.0001) and (max(eta) == pytest.approx(0.19782549684258963, 0.0001)))

def test_spectrum_integration():

    hs = 5.0
    tp = 12.8657
    omega_array = np.linspace(2*np.pi/50, 2*np.pi/3, num=1000)
    t_array = 2*np.pi/omega_array

    S = compute_jonswap_spectrum(hs,tp,omega_array)

    # Compute T2
    [T2,m0] = compute_t2(omega_array,S)

    # Compute T1
    T1 = compute_t1(omega_array,S)

    # Re-compute Hs from spectrum
    m0 = compute_spectrum_moment(omega_array,S,k=0)
    hs_num = 4*np.sqrt(m0)

    assert (hs_num == pytest.approx(4.99676, 0.001) and (T1 == pytest.approx(10.83217, 0.001)) and (T2 == pytest.approx(10.26792, 0.001)))


