# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
import os
import copy
from dtop_stationkeep.business.libraries.dtosk.inputs.CustomMooringInput import CustomMooringInput

def test_mooring_write_input_file():

    from dtop_stationkeep.business.libraries.dtosk.mooring import (
        Mooring, LineTypeData, NodeData, LineData
    )
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body

    
    # Init two line type data
    line_type=LineTypeData.LineTypeData()
    line_type.line_type_name='line_steel1' 
    line_type.diameter=0.0766
    line_type.mass_density_in_air=113.35  
    line_type.ea=7.536e8 
    line_type.cb=1.0
    line_type.c_int_damp=1.0E8  
    line_type.ca=0.6 
    line_type.cdn=-1.0 
    line_type.cdt=0.05 
    line_type.name='line_type1' 
    line_type.description='line type example'
    line_type2=LineTypeData.LineTypeData()
    line_type2.line_type_name='line_steel2' 
    line_type2.diameter=0.09
    line_type2.mass_density_in_air=115 
    line_type2.ea=7.536e8 
    line_type2.cb=1.0
    line_type2.c_int_damp=1.0E8  
    line_type2.ca=0.6 
    line_type2.cdn=-1.0 
    line_type2.cdt=0.05 
    line_type2.name='line_type2' 
    line_type2.description='line type example'

    # Init node properties
    node1=NodeData.NodeData()
    node1.node_number=1.0 
    node1.type='Fix' 
    node1.position=np.array([-800.00,0.00,-200.00])
    node1.point_mass=0.1 
    node1.point_volume=0.2 
    node1.force=np.zeros(shape=(3), dtype=float) 
    node1.name='node1' 
    node1.description='node number 1'
    node2=NodeData.NodeData()
    node2.node_number=2.0 
    node2.type='Vessel' 
    node2.position=np.array([-40.00,0.00,14.00])
    node2.point_mass=0.1 
    node2.point_volume=0.2 
    node2.force=np.zeros(shape=(3), dtype=float) 
    node2.name='node2' 
    node2.description='node number 2'

    # Init line properties 
    line1=LineData.LineData()
    line1.line_number=1.0 
    line1.line_type_name='line_steel1' 
    line1.unstretched_length=835.35 
    line1.anchor_node_number=1.0 
    line1.fairlead_node_number=2.0 
    line1.flags=['GX_POS','GY_POS','GZ_POS'] 
    line1.name='line1' 
    line1.description='line number 1'
    line2=LineData.LineData()
    line2.line_number=2.0 
    line2.line_type_name='line_steel1' 
    line2.unstretched_length=835.35 
    line2.anchor_node_number=1.0 
    line2.fairlead_node_number=2.0 
    line2.flags=['GX_POS','GY_POS','GZ_POS'] 
    line2.name='line2' 
    line2.description='line number 2' 

    # Init mooring
    mooring=Mooring.Mooring()
    mooring.line_dictionnary.append(line_type)
    mooring.line_dictionnary.append(line_type2)
    mooring.node_properties.append(node1)
    mooring.node_properties.append(node2)
    mooring.line_properties.append(line1)
    mooring.line_properties.append(line2)
    
    # Write mooring system to map++ input file
    mooring.write_to_map_input_file('map_pp_input_file.map')

    assert True

def test_mooring_force_from_simple_input():
    from dtop_stationkeep.business.libraries.dtosk.mooring import Mooring

    # Init
    n_bundle=4
    n_line_per_batch=3
    mooring_radius=400.0
    mooring_ref_angle=180*np.pi/180.0
    mooring_line_length=300.0
    water_depth=200.0
    batch_angle=10.0*np.pi/180.0
    fairlead_radius= 40.
    fairlead_depth= 14.
    line_diameter=0.0766
    line_density=11.35
    line_ea=7.536e5
    seabed_friction_coeff=1.0
    sea_water_density=1025.0
    rotation_center_in_b=np.array([0,0,0])

    # Populate mooring
    mooring=Mooring.Mooring()
    mooring.create_from_simple_input(np.array([0,0,0,0,0,0]),n_bundle,n_line_per_batch,mooring_radius,mooring_ref_angle,mooring_line_length,water_depth,batch_angle,fairlead_radius,fairlead_depth,line_diameter,line_density,line_ea,seabed_friction_coeff,sea_water_density)
    
    # Compute force
    force1=mooring.compute_force(np.array([50,0,0,0,0,0]),rotation_center_in_b)
    force2=mooring.compute_force(np.array([0,0,0,0,25*np.pi/180.0,0]),rotation_center_in_b)
    #mooring.plot_mooring_system()
    
    assert ((force1[0] == pytest.approx(-8.26850663e+05, 100.0)) and (force2[4] == pytest.approx( 7.54758764e+06,100.0)))

def test_mooring_force_at_anchor_from_simple_input():
    from dtop_stationkeep.business.libraries.dtosk.mooring import Mooring

    # Init
    n_bundle=4
    n_line_per_batch=3
    mooring_radius=400.0
    mooring_ref_angle=180*np.pi/180.0
    mooring_line_length=300
    water_depth=200.0
    batch_angle=10.0*np.pi/180.0
    fairlead_radius= 40.
    fairlead_depth= 14.
    line_diameter=0.0766
    line_density=11.35
    line_ea=7.536e5
    seabed_friction_coeff=1.0
    sea_water_density=1025.0
    rotation_center_in_b=np.array([0,0,0])

    # Populate mooring
    mooring=Mooring.Mooring()
    mooring.create_from_simple_input(np.array([0,0,0,0,0,0]),n_bundle,n_line_per_batch,mooring_radius,mooring_ref_angle,mooring_line_length,water_depth,batch_angle,fairlead_radius,fairlead_depth,line_diameter,line_density,line_ea,seabed_friction_coeff,sea_water_density)
    
    # Compute force
    force_dummy=mooring.compute_force(np.array([0,0,0,0,0*np.pi/180.0,0]),rotation_center_in_b)
    #mooring.plot_mooring_system()

    # Get force at anchor
    anchor_force_in_n = mooring.get_anchor_force_in_n(11)
    
    assert ((anchor_force_in_n[0] == pytest.approx(49786.08692299, 10.0)) and (anchor_force_in_n[2] == pytest.approx( 145694.04043456,10.0)))


def test_mooring_force_with_convergence():
    from dtop_stationkeep.business.libraries.dtosk.mooring import Mooring

    # Init
    n_bundle=4
    n_line_per_batch=3
    mooring_radius=400.0
    mooring_ref_angle=180*np.pi/180.0
    mooring_line_length=530
    water_depth=200.0
    batch_angle=10.0*np.pi/180.0
    fairlead_radius= 40.0
    fairlead_depth= 0.
    line_diameter=0.0766
    line_density=11.35
    line_ea=7.536e8
    seabed_friction_coeff=1.0
    sea_water_density=1025.0
    rotation_center_in_b=np.array([0,0,0])

    # Populate mooring
    mooring=Mooring.Mooring()
    mooring.create_from_simple_input(np.array([0,0,0,0,0,0]),n_bundle,n_line_per_batch,mooring_radius,mooring_ref_angle,mooring_line_length,water_depth,batch_angle,fairlead_radius,fairlead_depth,line_diameter,line_density,line_ea,seabed_friction_coeff,sea_water_density)
    
    # Compute force
    force_dummy=mooring.compute_force(np.array([0,0,0,0,0*np.pi/180.0,0]),rotation_center_in_b)
    #mooring.plot_mooring_system()

    # Check convergence criteria from map++
    info=mooring.get_line_converge_reason(11)

    assert (info == 2)

def test_mooring_force_no_convergence():
    from dtop_stationkeep.business.libraries.dtosk.mooring import Mooring

    # Init
    n_bundle=4
    n_line_per_batch=3
    mooring_radius=400.0
    mooring_ref_angle=180*np.pi/180.0
    mooring_line_length=600
    water_depth=200.0
    batch_angle=10.0*np.pi/180.0
    fairlead_radius= 40.0
    fairlead_depth= 0.
    line_diameter=0.0766
    line_density=11.35
    line_ea=7.536e8
    seabed_friction_coeff=1.0
    sea_water_density=1025.0
    rotation_center_in_b=np.array([0,0,0])

    # Populate mooring
    mooring=Mooring.Mooring()
    mooring.create_from_simple_input(np.array([0,0,0,0,0,0]),n_bundle,n_line_per_batch,mooring_radius,mooring_ref_angle,mooring_line_length,water_depth,batch_angle,fairlead_radius,fairlead_depth,line_diameter,line_density,line_ea,seabed_friction_coeff,sea_water_density)
    
    # Compute force
    force_dummy=mooring.compute_force(np.array([0,0,0,0,0*np.pi/180.0,0]),rotation_center_in_b)
    #mooring.plot_mooring_system()

    # Check convergence criteria from map++
    info=mooring.get_line_converge_reason(11)

    assert (info == 0)
    

def test_mooring_stiffness_from_simple_input():
    from dtop_stationkeep.business.libraries.dtosk.mooring.Mooring import Mooring
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body

    # Init
    n_bundle=4
    n_line_per_batch=3
    mooring_radius=400.0
    mooring_ref_angle=180*np.pi/180.0
    mooring_line_length=300.0
    water_depth=200.0
    batch_angle=10.0*np.pi/180.0
    fairlead_radius= 0.
    fairlead_depth= -14.
    line_diameter=0.0766
    line_density=11.35
    line_ea=7.536e5
    seabed_friction_coeff=1.0
    sea_water_density=1025.0

    # Populate mooring
    mooring=Mooring()
    mooring.create_from_simple_input(np.array([0,0,0,0,0,0]),n_bundle,n_line_per_batch,mooring_radius,mooring_ref_angle,mooring_line_length,water_depth,batch_angle,fairlead_radius,fairlead_depth,line_diameter,line_density,line_ea,seabed_friction_coeff,sea_water_density)
    
    # Compute local stiffness
    k1=mooring.compute_linear_stiffness(np.array([0,0,0,0,0,0.0]))
    k2=mooring.compute_linear_stiffness(np.array([50,0,0,0,0,0]))
    k3=mooring.compute_linear_stiffness(np.array([100,0,0,0,0,0]))
    assert ((k1[0,0] == pytest.approx(16524.67, 1.0)) and (k2[0,0] == pytest.approx(16559.15,1.0)) and (k3[0,0] == pytest.approx(16566.88, 1.0)) )

def test_body_stiffness_from_simple_input():
    from dtop_stationkeep.business.libraries.dtosk.mooring.Mooring import Mooring
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body

    # Init
    n_bundle=4
    n_line_per_batch=3
    mooring_radius=400.0
    mooring_ref_angle=180*np.pi/180.0
    mooring_line_length=300.0
    water_depth=200.0
    batch_angle=10.0*np.pi/180.0
    fairlead_radius= 5.
    fairlead_depth= -14.
    line_diameter=0.0766
    line_density=11.35
    line_ea=7.536e5
    seabed_friction_coeff=1.0
    sea_water_density=1025.0

    # Init
    body=Body()
    body.position=np.array([0.,0.,0.,0.,0.,0.])
    body.mooring_system_force.create_from_simple_input(body.position,n_bundle,n_line_per_batch,mooring_radius,mooring_ref_angle,mooring_line_length,water_depth,batch_angle,fairlead_radius,fairlead_depth,line_diameter,line_density,line_ea,seabed_friction_coeff,sea_water_density)
    k=body.compute_linear_stiffness_by_difference()
    k3=body.mooring_system_force.compute_linear_stiffness(np.array([0.,0.,0.,0.,0.,0.0]))
    
    assert ((k[0,0] == pytest.approx(17904.49, 1.0)))

def test_hierarchy_mooring_line():
    from dtop_stationkeep.business.libraries.dtosk.mooring.Mooring import Mooring
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body

    # Init
    n_bundle=4
    n_line_per_batch=3
    mooring_radius=400.0
    mooring_ref_angle=180*np.pi/180.0
    mooring_line_length=300.0
    water_depth=200.0
    batch_angle=10.0*np.pi/180.0
    fairlead_radius= 5.
    fairlead_depth= -14.
    line_diameter=0.0766
    line_density=11.35
    line_ea=7.536e5
    seabed_friction_coeff=1.0
    sea_water_density=1025.0

    # Init
    body=Body()
    body.position=np.array([0.,0.,0.,0.,0.,0.])
    body.mooring_system_force.create_from_simple_input(body.position,n_bundle,n_line_per_batch,mooring_radius,mooring_ref_angle,mooring_line_length,water_depth,batch_angle,fairlead_radius,fairlead_depth,line_diameter,line_density,line_ea,seabed_friction_coeff,sea_water_density,tag_name = 'hierarchy_test')
    
    # Modify the mooring configuration for the sake of testing the robustness
    body.mooring_system_force.line_properties[1].anchor_node_number = 1
    body.mooring_system_force.line_properties[10].fairlead_node_number = 1
    body.mooring_system_force.line_properties[5].fairlead_node_number = 10
    body.mooring_system_force.node_properties[9].type = 'Connect'

    [ml_of_segment,ml_of_node,nml_out] = body.mooring_system_force.identify_mooring_line_hierarchy()

    assert ((ml_of_segment[4] == 1) and (ml_of_segment[5] == 1))

def test_mooring_custom_input():

    from dtop_stationkeep.business.libraries.dtosk.mooring.Mooring import Mooring
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body

    # Init
    n_bundle=4
    n_line_per_batch=3
    mooring_radius=400.0
    mooring_ref_angle=180*np.pi/180.0
    mooring_line_length=300.0
    water_depth=200.0
    batch_angle=10.0*np.pi/180.0
    fairlead_radius= 5.
    fairlead_depth= -14.
    line_diameter=0.0766
    line_density=11.35
    line_ea=7.536e5
    seabed_friction_coeff=1.0
    sea_water_density=1025.0

    # Init
    body=Body()
    body.position=np.array([0.,0.,0.,0.,0.,0.])
    body.mooring_system_force.create_from_simple_input(body.position,n_bundle,n_line_per_batch,mooring_radius,mooring_ref_angle,mooring_line_length,water_depth,batch_angle,fairlead_radius,fairlead_depth,line_diameter,line_density,line_ea,seabed_friction_coeff,sea_water_density)
    k=body.compute_linear_stiffness_by_difference()
    k3=body.mooring_system_force.compute_linear_stiffness(np.array([0.,0.,0.,0.,0.,0.0]))    

    # Create input with custom iput
    mooring_input = CustomMooringInput()
    mooring_input.line_properties = copy.deepcopy(body.mooring_system_force.line_properties)
    mooring_input.node_properties = copy.deepcopy(body.mooring_system_force.node_properties)
    mooring_input.line_dictionnary = copy.deepcopy(body.mooring_system_force.line_dictionnary)
    mooring_input.solver_options = copy.deepcopy(body.mooring_system_force.solver_options)

    # Create a new body
    body2=Body()
    body2.position=np.array([0.,0.,0.,0.,0.,0.])
    body2.mooring_system_force.create_from_custom_input(mooring_input,body2.initial_position)
    vessel_position = np.zeros(shape=(6),dtype=float)
    body2.mooring_system_force.initiate_calculation(water_depth,sea_water_density,vessel_position)
    k=body2.compute_linear_stiffness_by_difference()

    assert ((k[0,0] == pytest.approx(17904.49, 1.0)))

def test_mooring_not_on_seabed():
    from dtop_stationkeep.business.libraries.dtosk.mooring.Mooring import Mooring
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    # Init
    n_bundle=4
    n_line_per_batch=3
    mooring_radius=400.0
    mooring_ref_angle=180*np.pi/180.0
    mooring_line_length=500.0
    water_depth=150.0
    batch_angle=10.0*np.pi/180.0
    fairlead_radius= 5.
    fairlead_depth= -14.
    line_diameter=0.0766
    line_density=11.35
    line_ea=7.536e5
    seabed_friction_coeff=1.0
    sea_water_density=1025.0

    # Init
    body=Body()
    body.position=np.array([0.,0.,0.,0.,0.,0.])
    body.mooring_system_force.create_from_simple_input(body.position,n_bundle,n_line_per_batch,mooring_radius,mooring_ref_angle,mooring_line_length,water_depth,batch_angle,fairlead_radius,fairlead_depth,line_diameter,line_density,line_ea,seabed_friction_coeff,sea_water_density)
    k=body.compute_linear_stiffness_by_difference()
    k3=body.mooring_system_force.compute_linear_stiffness(np.array([0.,0.,0.,0.,0.,0.0]))    

    # Create input with custom iput
    mooring_input = CustomMooringInput()
    mooring_input.line_properties = copy.deepcopy(body.mooring_system_force.line_properties)
    mooring_input.node_properties = copy.deepcopy(body.mooring_system_force.node_properties)
    mooring_input.line_dictionnary = copy.deepcopy(body.mooring_system_force.line_dictionnary)
    mooring_input.solver_options = copy.deepcopy(body.mooring_system_force.solver_options)

    # Modify the anchor node so that they are not at sea bed level but above
    for idx in range(0,len(mooring_input.node_properties)):
        if mooring_input.node_properties[idx].position[2] < -100.0:
            mooring_input.node_properties[idx].position[2] = -50.0
    for idx in range(0,len(mooring_input.line_properties)):
            print(mooring_input.line_properties[idx].flags)
            mooring_input.line_properties[idx].flags.append('OMIT_CONTACT')


    # Create a new body
    body2=Body()
    body2.position=np.array([0.,0.,0.,0.,0.,0.])
    body2.mooring_system_force.create_from_custom_input(mooring_input,body2.initial_position)
    vessel_position = np.zeros(shape=(6),dtype=float)
    body2.mooring_system_force.initiate_calculation(water_depth,sea_water_density,vessel_position)
    k=body2.compute_linear_stiffness_by_difference()
    #body2.mooring_system_force.plot_mooring_system()

    body2.position[0] = 50
    k=body2.compute_linear_stiffness_by_difference()
    #body2.mooring_system_force.plot_mooring_system()


    assert ((k[0,0] == pytest.approx(7.53277425e+02, 1.0)))

def test_mooring_connect_node_with_force():
    from dtop_stationkeep.business.libraries.dtosk.mooring.Mooring import Mooring
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    # Init
    n_bundle=4
    n_line_per_batch=3
    mooring_radius=400.0
    mooring_ref_angle=180*np.pi/180.0
    mooring_line_length=500.0
    water_depth=150.0
    batch_angle=10.0*np.pi/180.0
    fairlead_radius= 5.
    fairlead_depth= -14.
    line_diameter=0.0766
    line_density=11.35
    line_ea=7.536e5
    seabed_friction_coeff=1.0
    sea_water_density=1025.0

    # Init
    body=Body()
    body.position=np.array([0.,0.,0.,0.,0.,0.])
    body.mooring_system_force.create_from_simple_input(body.position,n_bundle,n_line_per_batch,mooring_radius,mooring_ref_angle,mooring_line_length,water_depth,batch_angle,fairlead_radius,fairlead_depth,line_diameter,line_density,line_ea,seabed_friction_coeff,sea_water_density)
    k=body.compute_linear_stiffness_by_difference()
    k3=body.mooring_system_force.compute_linear_stiffness(np.array([0.,0.,0.,0.,0.,0.0]))    

    # Create input with custom iput
    mooring_input = CustomMooringInput()
    mooring_input.line_properties = copy.deepcopy(body.mooring_system_force.line_properties)
    mooring_input.node_properties = copy.deepcopy(body.mooring_system_force.node_properties)
    mooring_input.line_dictionnary = copy.deepcopy(body.mooring_system_force.line_dictionnary)
    mooring_input.solver_options = copy.deepcopy(body.mooring_system_force.solver_options)

    # Modify the anchor node so that they are not at sea bed level but above
    for idx in range(0,len(mooring_input.node_properties)):
        if mooring_input.node_properties[idx].position[2] < -100.0:
            mooring_input.node_properties[idx].position[2] = -50.0
    for idx in range(0,len(mooring_input.line_properties)):
            print(mooring_input.line_properties[idx].flags)
            mooring_input.line_properties[idx].flags.append('OMIT_CONTACT')

    # Connect node
    mooring_input.node_properties[0].type = 'CONNECT'
    mooring_input.node_properties[0].force[0] = -9897.85820971 - 6000
    mooring_input.node_properties[0].force[1] = 1745.0037324
    mooring_input.node_properties[0].force[2] = 14908.64044893

    # Create a new body
    body2=Body()
    body2.position=np.array([0.,0.,0.,0.,0.,0.])
    body2.mooring_system_force.create_from_custom_input(mooring_input,body2.initial_position)
    vessel_position = np.zeros(shape=(6),dtype=float)
    body2.mooring_system_force.initiate_calculation(water_depth,sea_water_density,vessel_position)
    k=body2.compute_linear_stiffness_by_difference()
    #body2.mooring_system_force.plot_mooring_system()
    force = body2.mooring_system_force.get_anchor_force_in_n(0)

    assert True

def test_mooring_simple_grid():
    from dtop_stationkeep.business.libraries.dtosk.mooring.Mooring import Mooring
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.mooring import (
        Mooring, LineTypeData, NodeData, LineData
    )
    
    # Parameters
    mooring_radius = 250.0
    water_depth = 100.0
    grid_width = 50.0

    # Create input with custom iput
    mooring_input = CustomMooringInput()

    ###################################
    # ANCHORS node
    ###################################

    # Acnhor positions
    x=grid_width*np.array([1 , 1, 1 , 0, -1, -1, -1, 0]) + mooring_radius * np.array([np.cos(np.pi/4) , 1, np.cos(np.pi/4) , 0 , -np.cos(np.pi/4) , -1 , -np.cos(np.pi/4) , 0])
    y=grid_width*np.array([1 , 0, -1 , -1, -1, 0, 1, 1]) + mooring_radius * np.array([np.cos(np.pi/4) , 0, -np.cos(np.pi/4) , -1 , -np.cos(np.pi/4) , 0 , np.cos(np.pi/4) , 1])
    z = y*0.0 - water_depth

    # Anchor node template
    node_property = NodeData.NodeData()
    node_property.node_number=0
    node_property.type='Fix'
    node_property.position=np.zeros(shape=(3), dtype=float)
    node_property.anchor_required=True

    # Anchor nodes
    for idx in range(0,len(x)):
        node_property.node_number=idx+1.0
        node_property.position[0]=x[idx]
        node_property.position[1]=y[idx]
        node_property.position[2]=z[idx]
        mooring_input.node_properties.append(copy.deepcopy(node_property))
    
    ###################################
    # GRID node
    ###################################

    # Grid node positions
    x=grid_width*np.array([1 , 1, 1 , 0, -1, -1, -1, 0, 0]) 
    y=grid_width*np.array([1 , 0, -1 , -1, -1, 0, 1, 1, 0])
    z = y*0 - 10.0

    # Grid node template
    node_property = NodeData.NodeData()
    node_property.node_number=0
    node_property.type='Connect'
    node_property.position=np.zeros(shape=(3), dtype=float)
    node_property.anchor_required=False
    #node_property.point_volume=100
    node_property.force[2] = 270713

    # Grid nodes
    for idx in range(0,len(x)):
        if idx in [1,3,5,7]:
            node_property.type='Connect'
            node_property.force[2] = 300713
        elif idx in [0,2,4,6]:
            node_property.type='Connect'
            node_property.force[2] = 250713
        elif idx in [8]:
            node_property.type='Vessel'
            node_property.force[2] = 0.0
        node_property.node_number=idx+1.0 +8
        node_property.position[0]=x[idx]
        node_property.position[1]=y[idx]
        node_property.position[2]=z[idx]
        mooring_input.node_properties.append(copy.deepcopy(node_property))
        

    ###################################
    # Main mooring line type
    ###################################

    line_type_data = LineTypeData.LineTypeData()
    line_type_data.name = "line_type1"
    line_type_data.line_type_name = "line_type1"
    line_type_data.diameter = 0.1998 
    line_type_data.weight_in_air=246.00
    line_type_data.weight_in_water=246.00-np.pi*0.1998**2/4*1025.0 
    line_type_data.ea= 3870756308.49 
    line_type_data.cb= 1.0
    line_type_data.c_int_damp= 100000000.0
    line_type_data.ca= 0.6
    line_type_data.cdn= -1.0
    line_type_data.cdt= 0.05
    mooring_input.line_dictionnary.append(copy.deepcopy(line_type_data))

    ###################################
    # Grid line type
    ###################################

    line_type_data = LineTypeData.LineTypeData()
    line_type_data.name = "line_type2"
    line_type_data.line_type_name = "line_type2"
    line_type_data.diameter=0.0766 
    line_type_data.weight_in_air=90.035
    line_type_data.weight_in_water=90.035 -np.pi*0.0766  **2/4*1025.0 
    line_type_data.ea=15.536e6
    line_type_data.cb= 1.0
    line_type_data.c_int_damp= 100000000.0
    line_type_data.ca= 0.6
    line_type_data.cdn= -1.0
    line_type_data.cdt= 0.05
    mooring_input.line_dictionnary.append(copy.deepcopy(line_type_data))

    ###################################
    # Main mooring lines
    ###################################

    line_property = LineData.LineData()
    line_property.line_number=0
    line_property.line_type_name="line_type1"
    line_property.line_type_index=0
    line_property.unstretched_length=300
    line_property.anchor_node_number=0
    line_property.fairlead_node_number=0


    for idx in range(0,8):
        line_property.line_number = idx +1
        line_property.anchor_node_number= idx +1
        line_property.fairlead_node_number= idx + 9
        mooring_input.line_properties.append(copy.deepcopy(line_property))

    ###################################
    # Grid lines
    ###################################

    line_property = LineData.LineData()
    line_property.line_number=0
    line_property.line_type_name="line_type2"
    line_property.line_type_index=0
    line_property.unstretched_length=grid_width*1.1
    line_property.anchor_node_number=0
    line_property.fairlead_node_number=0
    line_property.flags.append('OMIT_CONTACT')
    #line_property.flags.append('LINEAR_SPRING')
    #line_property.flags.append('DIAGNOSTIC')

    a = np.array([9 , 11 , 11 , 13 , 13 , 15 , 15, 9, 10 , 12 , 14 , 16])
    f = np.array([10 , 10 , 12 , 12 , 14 , 14 , 16 , 16, 17 , 17 , 17 , 17])
    

    for idx in range(0,len(f)):
        line_property.line_number = idx +1  +8
        line_property.anchor_node_number= a[idx]
        line_property.fairlead_node_number= f[idx]
        mooring_input.line_properties.append(copy.deepcopy(line_property))

    # TO do: grid node = Connect ,  Buoyancy Module => equilibrium, add fairlead nodes, add submooringlines 

    ###################################
    # Solver options
    ###################################
    # mooring_input.solver_options.inner_ftol=1.0E-2
    # mooring_input.solver_options.inner_gtol=1.0E-2
    # mooring_input.solver_options.inner_xtol=1.0E-2
    # mooring_input.solver_options.inner_max_its=500
    # mooring_input.solver_options.outer_max_its=500
    # mooring_input.solver_options.outer_tol=1.0E-2
    ###################################
    # Create model
    ###################################

    mooring_system_force = Mooring.Mooring()
    pos = np.zeros(shape=(6))
    mooring_system_force.create_from_custom_input(mooring_input,pos)
    mooring_system_force.check_convergence=False
    vessel_position = np.zeros(shape=(6),dtype=float)
    mooring_system_force.initiate_calculation(water_depth,1025.0,vessel_position)

    #mooring_system_force.plot_mooring_system()

    for idx in range(0,20):

        print(mooring_system_force.get_fairlead_force_in_n(idx))

    force = mooring_system_force.compute_force(np.array([0,0,0,0,0,0]),np.array([0,0,0]))
    

    print('force')
    print(force)

    force = mooring_system_force.compute_force(np.array([0,0,20,0,0,0]),np.array([0,0,0]))
    print('force')
    print(force)
    #mooring_system_force.plot_mooring_system()

    force = mooring_system_force.compute_force(np.array([10,0,30,0,0,0]),np.array([0,0,0]))
    print('force')
    print(force)
    #mooring_system_force.plot_mooring_system()

    assert True

def test_mooring_grid_catenary():
    from dtop_stationkeep.business.libraries.dtosk.mooring.Mooring import Mooring
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.mooring import (
        Mooring, LineTypeData, NodeData, LineData
    )
    
    # Parameters
    mooring_radius = 250.0
    water_depth = 100.0
    grid_width = 50.0

    # Create input with custom iput
    mooring_input = CustomMooringInput()

    ###################################
    # ANCHORS node
    ###################################

    # Acnhor positions
    x=grid_width*np.array([1 , 1, 1 , 0, -1, -1, -1, 0]) + mooring_radius * np.array([np.cos(np.pi/4) , 1, np.cos(np.pi/4) , 0 , -np.cos(np.pi/4) , -1 , -np.cos(np.pi/4) , 0])
    y=grid_width*np.array([1 , 0, -1 , -1, -1, 0, 1, 1]) + mooring_radius * np.array([np.cos(np.pi/4) , 0, -np.cos(np.pi/4) , -1 , -np.cos(np.pi/4) , 0 , np.cos(np.pi/4) , 1])
    z = y*0.0 - water_depth

    # Anchor node template
    node_property = NodeData.NodeData()
    node_property.node_number=0
    node_property.type='Fix'
    node_property.position=np.zeros(shape=(3), dtype=float)
    node_property.anchor_required=True

    # Anchor nodes
    for idx in range(0,len(x)):
        node_property.node_number=idx+1.0
        node_property.position[0]=x[idx]
        node_property.position[1]=y[idx]
        node_property.position[2]=z[idx]
        mooring_input.node_properties.append(copy.deepcopy(node_property))


    
    ###################################
    # GRID node
    ###################################

    # Grid node positions
    x=grid_width*np.array([1 , 1, 1 , 0, -1, -1, -1, 0, 0]) 
    y=grid_width*np.array([1 , 0, -1 , -1, -1, 0, 1, 1, 0])
    z = y*0 - 10.0

    # Grid node template
    node_property = NodeData.NodeData()
    node_property.node_number=0
    node_property.type='Connect'
    node_property.position=np.zeros(shape=(3), dtype=float)
    node_property.anchor_required=False
    #node_property.point_volume=100
    node_property.force[2] = 270713

    # Grid nodes
    for idx in range(0,len(x)):
        if idx in [1,3,5,7]:
            node_property.type='Connect'
            node_property.force[2] = 411859
        elif idx in [0,2,4,6]:
            node_property.type='Connect'
            node_property.force[2] = 305000
        elif idx in [8]:
            node_property.type='Connect'
            node_property.force[2] = 227894
        node_property.node_number=idx+1.0 +8
        node_property.position[0]=x[idx]
        node_property.position[1]=y[idx]
        node_property.position[2]=z[idx]
        mooring_input.node_properties.append(copy.deepcopy(node_property))
        
    ###################################
    # VESSEL nodes
    ###################################

    # Vessel positions
    fairlead_radius = 3
    x=0.5*grid_width*np.array([1,1,1,1,1,1,1,1,-1,-1,-1,-1,-1,-1,-1,-1]) + fairlead_radius * np.array([1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1])
    y=0.5*grid_width*np.array([1,1,1,1,-1,-1,-1,-1,-1,-1,-1,-1,1,1,1,1]) + fairlead_radius * np.array([1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1])
    z = y*0.0

    # Vessel node template
    node_property = NodeData.NodeData()
    node_property.node_number=0
    node_property.type='Fix'
    node_property.position=np.zeros(shape=(3), dtype=float)
    node_property.anchor_required=True

    # Vessel nodes
    for idx in range(0,len(x)):
        node_property.node_number=idx+1.0+16
        node_property.position[0]=x[idx]
        node_property.position[1]=y[idx]
        node_property.position[2]=z[idx]
        mooring_input.node_properties.append(copy.deepcopy(node_property))

    ###################################
    # Main mooring line type
    ###################################

    line_type_data = LineTypeData.LineTypeData()
    line_type_data.name = "line_type1"
    line_type_data.line_type_name = "line_type1"
    line_type_data.diameter = 0.1998
    line_type_data.weight_in_air=246.00 
    line_type_data.weight_in_water=246.00 -np.pi*0.1998 **2/4*1025.0  
    line_type_data.ea= 3870756308.49 
    line_type_data.cb= 1.0
    line_type_data.c_int_damp= 100000000.0
    line_type_data.ca= 0.6
    line_type_data.cdn= -1.0
    line_type_data.cdt= 0.05
    mooring_input.line_dictionnary.append(copy.deepcopy(line_type_data))

    ###################################
    # Grid line type
    ###################################

    line_type_data = LineTypeData.LineTypeData()
    line_type_data.name = "line_type2"
    line_type_data.line_type_name = "line_type2"
    line_type_data.diameter=0.0766
    line_type_data.weight_in_air=150.035 
    line_type_data.weight_in_water=150.035 -np.pi*0.0766**2/4*1025.0   
    line_type_data.ea=1.536e8
    line_type_data.cb= 1.0
    line_type_data.c_int_damp= 100000000.0
    line_type_data.ca= 0.6
    line_type_data.cdn= -1.0
    line_type_data.cdt= 0.05
    mooring_input.line_dictionnary.append(copy.deepcopy(line_type_data))

    ###################################
    # Vessel line type
    ###################################

    line_type_data = LineTypeData.LineTypeData()
    line_type_data.name = "line_type3"
    line_type_data.line_type_name = "line_type3"
    line_type_data.diameter=0.0766
    line_type_data.weight_in_air=150.035 
    line_type_data.weight_in_water=150.035 -np.pi*0.0766**2/4*1025.0  
    line_type_data.ea=1.536e8
    line_type_data.cb= 1.0
    line_type_data.c_int_damp= 100000000.0
    line_type_data.ca= 0.6
    line_type_data.cdn= -1.0
    line_type_data.cdt= 0.05
    mooring_input.line_dictionnary.append(copy.deepcopy(line_type_data))

    ###################################
    # Main mooring lines
    ###################################

    line_property = LineData.LineData()
    line_property.line_number=0
    line_property.line_type_name="line_type1"
    line_property.line_type_index=0
    line_property.unstretched_length=300
    line_property.anchor_node_number=0
    line_property.fairlead_node_number=0


    for idx in range(0,8):
        line_property.line_number = idx +1
        line_property.anchor_node_number= idx +1
        line_property.fairlead_node_number= idx + 9
        mooring_input.line_properties.append(copy.deepcopy(line_property))

    ###################################
    # Grid lines
    ###################################

    line_property = LineData.LineData()
    line_property.line_number=0
    line_property.line_type_name="line_type2"
    line_property.line_type_index=0
    line_property.unstretched_length=grid_width*1.1
    line_property.anchor_node_number=0
    line_property.fairlead_node_number=0
    line_property.flags.append('OMIT_CONTACT')
    #line_property.flags.append('LINEAR_SPRING')
    #line_property.flags.append('DIAGNOSTIC')

    a = np.array([9 , 11 , 11 , 13 , 13 , 15 , 15, 9, 10 , 12 , 14 , 16])
    f = np.array([10 , 10 , 12 , 12 , 14 , 14 , 16 , 16, 17 , 17 , 17 , 17])
    

    for idx in range(0,len(f)):
        line_property.line_number = idx +1  +8
        line_property.anchor_node_number= a[idx]
        line_property.fairlead_node_number= f[idx]
        mooring_input.line_properties.append(copy.deepcopy(line_property))

    ###################################
    # Vessel lines
    ###################################

    line_property = LineData.LineData()
    line_property.line_number=0
    line_property.line_type_name="line_type3"
    line_property.line_type_index=0
    line_property.unstretched_length=38*1.0
    line_property.anchor_node_number=0
    line_property.fairlead_node_number=0
    line_property.flags.append('OMIT_CONTACT')
    #line_property.flags.append('LINEAR_SPRING')
    #line_property.flags.append('DIAGNOSTIC')

    a = np.array([17,17,17,17,9 , 10  , 16 , 10 , 11 , 12 , 12 , 13 , 14,16,14,15])
    f = np.array([20,25,26,31,    18, 19,  21,  22,  23,  24,27,28,29,30,32,33])
    

    for idx in range(0,len(f)):
        line_property.line_number = idx +1  +16
        line_property.anchor_node_number= a[idx]
        line_property.fairlead_node_number= f[idx]
        mooring_input.line_properties.append(copy.deepcopy(line_property))

    # TO do: grid node = Connect ,  Buoyancy Module => equilibrium, add fairlead nodes, add submooringlines 

    ###################################
    # Solver options
    ###################################
    # mooring_input.solver_options.inner_ftol=1.0E-2
    # mooring_input.solver_options.inner_gtol=1.0E-2
    # mooring_input.solver_options.inner_xtol=1.0E-2
    # mooring_input.solver_options.inner_max_its=500
    # mooring_input.solver_options.outer_max_its=500
    # mooring_input.solver_options.outer_tol=1.0E-2
    ###################################
    # Create model
    ###################################

    mooring_system_force = Mooring.Mooring()
    pos = np.zeros(shape=(6))
    mooring_system_force.create_from_custom_input(mooring_input,pos)
    mooring_system_force.check_convergence=False
    vessel_position = np.zeros(shape=(6),dtype=float)
    mooring_system_force.initiate_calculation(water_depth,1025.0,vessel_position)



    for idx in range(0,20):

        print(mooring_system_force.get_fairlead_force_in_n(idx))

    force = mooring_system_force.compute_force(np.array([0,0,0,0,0,0]),np.array([0,0,0]))
    

    # print('force')
    # print(force)
    # mooring_system_force.plot_mooring_system()

    assert True


def test_mooring_grid():
    from dtop_stationkeep.business.libraries.dtosk.mooring.Mooring import Mooring
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.mooring import (
        Mooring, LineTypeData, NodeData, LineData
    )
    
    # Parameters
    mooring_radius = 250.0
    water_depth = 100.0
    grid_width = 50.0

    # Create input with custom iput
    mooring_input = CustomMooringInput()

    ###################################
    # ANCHORS node
    ###################################

    # Acnhor positions
    x=grid_width*np.array([1 , 1, 1 , 0, -1, -1, -1, 0]) + mooring_radius * np.array([np.cos(np.pi/4) , 1, np.cos(np.pi/4) , 0 , -np.cos(np.pi/4) , -1 , -np.cos(np.pi/4) , 0])
    y=grid_width*np.array([1 , 0, -1 , -1, -1, 0, 1, 1]) + mooring_radius * np.array([np.cos(np.pi/4) , 0, -np.cos(np.pi/4) , -1 , -np.cos(np.pi/4) , 0 , np.cos(np.pi/4) , 1])
    z = y*0.0 - water_depth

    # Anchor node template
    node_property = NodeData.NodeData()
    node_property.node_number=0
    node_property.type='Fix'
    node_property.position=np.zeros(shape=(3), dtype=float)
    node_property.anchor_required=True

    # Anchor nodes
    for idx in range(0,len(x)):
        node_property.node_number=idx+1.0
        node_property.position[0]=x[idx]
        node_property.position[1]=y[idx]
        node_property.position[2]=z[idx]
        mooring_input.node_properties.append(copy.deepcopy(node_property))


    
    ###################################
    # GRID node
    ###################################

    # Grid node positions
    x=grid_width*np.array([1 , 1, 1 , 0, -1, -1, -1, 0, 0]) 
    y=grid_width*np.array([1 , 0, -1 , -1, -1, 0, 1, 1, 0])
    z = y*0 - 10.0

    # Grid node template
    node_property = NodeData.NodeData()
    node_property.node_number=0
    node_property.type='Connect'
    node_property.position=np.zeros(shape=(3), dtype=float)
    node_property.anchor_required=False
    #node_property.point_volume=100
    node_property.force[2] = 270713

    # Grid nodes
    for idx in range(0,len(x)):
        node_property.force=node_property.force*0.0
        if idx in [1,3,5,7]:
            node_property.type='Connect'
            if idx ==1 :
                node_property.force[2] = 294260
                node_property.force[0] = 1.18898785e+05
            elif idx==3 :
                node_property.force[2] = 294260
                node_property.force[1] = -1.18898785e+05
            elif idx==5 :
                node_property.force[2] = 294260
                node_property.force[0] = -1.18898785e+05  
            elif idx==7 :
                node_property.force[2] = 294260
                node_property.force[1] = 1.18898785e+05                  
        elif idx in [0,2,4,6]:
            if idx ==0 :
                node_property.type='Connect'
                node_property.force[0] = 3.71479025e+05
                node_property.force[1] = 3.71479025e+05
                node_property.force[2] = 2.85456585e+05
            elif idx ==2 :
                node_property.type='Connect'
                node_property.force[0] = 3.71479025e+05
                node_property.force[1] = -3.71479025e+05
                node_property.force[2] = 2.85456585e+05
            elif idx ==4 :
                node_property.type='Connect'
                node_property.force[0] = -3.71479025e+05
                node_property.force[1] = -3.71479025e+05
                node_property.force[2] = 2.85456585e+05
            elif idx ==6 :
                node_property.type='Connect'
                node_property.force[0] = -3.71479025e+05
                node_property.force[1] = 3.71479025e+05
                node_property.force[2] = 2.85456585e+05
        elif idx in [8]:
            node_property.type='Connect'
            node_property.force[2] = 7.29489600e+04
        node_property.node_number=idx+1.0 +8
        node_property.position[0]=x[idx]
        node_property.position[1]=y[idx]
        node_property.position[2]=z[idx]
        mooring_input.node_properties.append(copy.deepcopy(node_property))
        
    ###################################
    # VESSEL nodes
    ###################################

    # Vessel positions
    fairlead_radius = 3
    x=0.5*grid_width*np.array([1,1,1,1,1,1,1,1,-1,-1,-1,-1,-1,-1,-1,-1]) + fairlead_radius * np.array([1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1])
    y=0.5*grid_width*np.array([1,1,1,1,-1,-1,-1,-1,-1,-1,-1,-1,1,1,1,1]) + fairlead_radius * np.array([1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1])
    z = y*0.0

    # Vessel node template
    node_property = NodeData.NodeData()
    node_property.node_number=0
    node_property.type='Vessel'
    node_property.position=np.zeros(shape=(3), dtype=float)
    node_property.anchor_required=True

    # Vessel nodes
    for idx in range(0,len(x)):
        node_property.node_number=idx+1.0+16
        node_property.position[0]=x[idx]
        node_property.position[1]=y[idx]
        node_property.position[2]=z[idx]
        mooring_input.node_properties.append(copy.deepcopy(node_property))

    ###################################
    # Main mooring line type
    ###################################

    line_type_data = LineTypeData.LineTypeData()
    line_type_data.name = "line_type1"
    line_type_data.line_type_name = "line_type1"
    line_type_data.diameter = 0.1998 
    line_type_data.weight_in_air=246.00
    line_type_data.weight_in_water=246.00-np.pi*0.1998 **2/4*1025.0  
    line_type_data.ea= 3870756308.49 
    line_type_data.cb= 1.0
    line_type_data.c_int_damp= 100000000.0
    line_type_data.ca= 0.6
    line_type_data.cdn= -1.0
    line_type_data.cdt= 0.05
    mooring_input.line_dictionnary.append(copy.deepcopy(line_type_data))

    ###################################
    # Grid line type
    ###################################

    line_type_data = LineTypeData.LineTypeData()
    line_type_data.name = "line_type2"
    line_type_data.line_type_name = "line_type2"
    line_type_data.diameter=0.0766 
    line_type_data.weight_in_air=50.035
    line_type_data.weight_in_water=50.035-np.pi*0.0766**2/4*1025.0  
    line_type_data.ea=1.536e6
    line_type_data.cb= 1.0
    line_type_data.c_int_damp= 100000000.0
    line_type_data.ca= 0.6
    line_type_data.cdn= -1.0
    line_type_data.cdt= 0.05
    mooring_input.line_dictionnary.append(copy.deepcopy(line_type_data))

    ###################################
    # Vessel line type
    ###################################

    line_type_data = LineTypeData.LineTypeData()
    line_type_data.name = "line_type3"
    line_type_data.line_type_name = "line_type3"
    line_type_data.diameter=0.0766 
    line_type_data.weight_in_air=150.035
    line_type_data.weight_in_water=150.035-np.pi*0.0766**2/4*1025.0
    line_type_data.ea=1.536e8
    line_type_data.cb= 1.0
    line_type_data.c_int_damp= 100000000.0
    line_type_data.ca= 0.6
    line_type_data.cdn= -1.0
    line_type_data.cdt= 0.05
    mooring_input.line_dictionnary.append(copy.deepcopy(line_type_data))

    ###################################
    # Main mooring lines
    ###################################

    line_property = LineData.LineData()
    line_property.line_number=0
    line_property.line_type_name="line_type1"
    line_property.line_type_index=0
    line_property.unstretched_length=300
    line_property.anchor_node_number=0
    line_property.fairlead_node_number=0


    for idx in range(0,8):
        line_property.line_number = idx +1
        line_property.anchor_node_number= idx +1
        line_property.fairlead_node_number= idx + 9
        mooring_input.line_properties.append(copy.deepcopy(line_property))

    ###################################
    # Grid lines
    ###################################

    line_property = LineData.LineData()
    line_property.line_number=0
    line_property.line_type_name="line_type2"
    line_property.line_type_index=0
    line_property.unstretched_length=grid_width*0.8
    line_property.anchor_node_number=0
    line_property.fairlead_node_number=0
    line_property.flags.append('LINEAR_SPRING')
    #line_property.flags.append('OMIT_CONTACT')
    #line_property.flags.append('DIAGNOSTIC')

    a = np.array([10 , 10 , 12 , 12 , 14 , 14 , 16, 16, 10 , 12 , 14 , 16])
    f = np.array([9 , 11 , 11 , 13 , 13, 15 , 15 , 9, 17 , 17 , 17 , 17])
    

    for idx in range(0,len(f)):
        line_property.line_number = idx +1  +8
        line_property.anchor_node_number= a[idx]
        line_property.fairlead_node_number= f[idx]
        mooring_input.line_properties.append(copy.deepcopy(line_property))

    ###################################
    # Vessel lines
    ###################################

    line_property = LineData.LineData()
    line_property.line_number=0
    line_property.line_type_name="line_type3"
    line_property.line_type_index=0
    line_property.unstretched_length=38*1.0
    line_property.anchor_node_number=0
    line_property.fairlead_node_number=0
    line_property.flags.append('OMIT_CONTACT')
    #line_property.flags.append('LINEAR_SPRING')
    #line_property.flags.append('DIAGNOSTIC')

    a = np.array([17,17,17,17,9 , 10  , 16 , 10 , 11 , 12 , 12 , 13 , 14,16,14,15])
    f = np.array([20,25,26,31,    18, 19,  21,  22,  23,  24,27,28,29,30,32,33])
    

    for idx in range(0,len(f)):
        line_property.line_number = idx +1  +16
        line_property.anchor_node_number= a[idx]
        line_property.fairlead_node_number= f[idx]
        mooring_input.line_properties.append(copy.deepcopy(line_property))

    # TO do: grid node = Connect ,  Buoyancy Module => equilibrium, add fairlead nodes, add submooringlines 

    ###################################
    # Solver options
    ###################################
    # mooring_input.solver_options.inner_ftol=1.0E-2
    # mooring_input.solver_options.inner_gtol=1.0E-2
    # mooring_input.solver_options.inner_xtol=1.0E-2
    # mooring_input.solver_options.inner_max_its=500
    # mooring_input.solver_options.outer_max_its=500
    # mooring_input.solver_options.outer_tol=1.0E-2
    ###################################
    # Create model
    ###################################

    mooring_system_force = Mooring.Mooring()
    pos = np.zeros(shape=(6))
    mooring_system_force.create_from_custom_input(mooring_input,pos)
    mooring_system_force.check_convergence=False
    vessel_position = np.zeros(shape=(6),dtype=float)
    mooring_system_force.initiate_calculation(water_depth,1025.0,vessel_position)



    for idx in range(0,20):

        print(mooring_system_force.get_fairlead_force_in_n(idx))

    force = mooring_system_force.compute_force(np.array([0.0,0.0,0.0,0.0,0.0,0.0]),np.array([0.0,0.0,0.0]))
    

    # print('force')
    # print(force)
    # mooring_system_force.plot_mooring_system()

    # force = mooring_system_force.compute_force(np.array([10,0,0,0,0,0]))
    # print('force')
    # print(force)
    # mooring_system_force.plot_mooring_system()

    assert True

def test_single_spring():
    from dtop_stationkeep.business.libraries.dtosk.mooring.Mooring import Mooring
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.mooring import (
        Mooring, LineTypeData, NodeData, LineData
    )
    
    # Parameters
    mooring_radius = 250.0
    water_depth = 100.0
    grid_width = 50.0

    # Create input with custom iput
    mooring_input = CustomMooringInput()

    ###################################
    # ANCHORS node
    ###################################


    # Anchor node template
    node_property = NodeData.NodeData()
    node_property.node_number=1
    node_property.type='Fix'
    node_property.position=np.zeros(shape=(3), dtype=float)
    node_property.anchor_required=True

    mooring_input.node_properties.append(copy.deepcopy(node_property))
        
    ###################################
    # VESSEL nodes
    ###################################

    # Anchor node template
    node_property = NodeData.NodeData()
    node_property.node_number=2
    node_property.type='Vessel'
    node_property.position=np.zeros(shape=(3), dtype=float)
    node_property.position[0] = 10.0
    node_property.anchor_required=True

    mooring_input.node_properties.append(copy.deepcopy(node_property))

    ###################################
    # Main mooring line type
    ###################################

    line_type_data = LineTypeData.LineTypeData()
    line_type_data.name = "line_type1"
    line_type_data.line_type_name = "line_type1"
    line_type_data.diameter = 0.1998 
    line_type_data.weight_in_air=246.00
    line_type_data.weight_in_water=246.00-np.pi*0.1998 **2/4*1025.0 
    line_type_data.ea= 3870756308.49 
    line_type_data.cb= 1.0
    line_type_data.c_int_damp= 100000000.0
    line_type_data.ca= 0.6
    line_type_data.cdn= -1.0
    line_type_data.cdt= 0.05
    mooring_input.line_dictionnary.append(copy.deepcopy(line_type_data))

    ###################################
    # Grid line type
    ###################################

    line_type_data = LineTypeData.LineTypeData()
    line_type_data.name = "line_type2"
    line_type_data.line_type_name = "line_type2"
    line_type_data.diameter=0.0766 
    line_type_data.weight_in_air=150.035
    line_type_data.weight_in_water=150.035-np.pi*0.0766**2/4*1025.0 
    line_type_data.ea=1.536e6
    line_type_data.cb= 1.0
    line_type_data.c_int_damp= 100000000.0
    line_type_data.ca= 0.6
    line_type_data.cdn= -1.0
    line_type_data.cdt= 0.05
    mooring_input.line_dictionnary.append(copy.deepcopy(line_type_data))

    ###################################
    # Vessel line type
    ###################################

    line_type_data = LineTypeData.LineTypeData()
    line_type_data.name = "line_type3"
    line_type_data.line_type_name = "line_type3"
    line_type_data.diameter=0.0766 
    line_type_data.weight_in_air=150.035
    line_type_data.weight_in_water=150.035-np.pi*0.0766**2/4*1025.0 
    line_type_data.ea=1.536e8
    line_type_data.cb= 1.0
    line_type_data.c_int_damp= 100000000.0
    line_type_data.ca= 0.6
    line_type_data.cdn= -1.0
    line_type_data.cdt= 0.05
    mooring_input.line_dictionnary.append(copy.deepcopy(line_type_data))



    ###################################
    # Grid lines
    ###################################

    line_property = LineData.LineData()
    line_property.line_number=0
    line_property.line_type_name="line_type2"
    line_property.line_type_index=0
    line_property.unstretched_length=10*0.7
    line_property.anchor_node_number=0
    line_property.fairlead_node_number=0
    line_property.flags.append('LINEAR_SPRING')
    #line_property.flags.append('OMIT_CONTACT')
    #line_property.flags.append('DIAGNOSTIC')

    a = np.array([1 ])
    f = np.array([2 ])
    

    for idx in range(0,len(f)):
        line_property.line_number = 1
        line_property.anchor_node_number= a[idx]
        line_property.fairlead_node_number= f[idx]
        mooring_input.line_properties.append(copy.deepcopy(line_property))


    # TO do: grid node = Connect ,  Buoyancy Module => equilibrium, add fairlead nodes, add submooringlines 

    ###################################
    # Solver options
    ###################################
    # mooring_input.solver_options.inner_ftol=1.0E-2
    # mooring_input.solver_options.inner_gtol=1.0E-2
    # mooring_input.solver_options.inner_xtol=1.0E-2
    # mooring_input.solver_options.inner_max_its=500
    # mooring_input.solver_options.outer_max_its=500
    # mooring_input.solver_options.outer_tol=1.0E-2
    ###################################
    # Create model
    ###################################

    mooring_system_force = Mooring.Mooring()
    pos = np.zeros(shape=(6))
    mooring_system_force.create_from_custom_input(mooring_input,pos)
    mooring_system_force.check_convergence=False
    vessel_position = np.zeros(shape=(6),dtype=float)
    mooring_system_force.initiate_calculation(water_depth,1025.0,vessel_position)

    assert True

def test_masterstructure_mooring_example():
    from dtop_stationkeep.business.libraries.dtosk.mooring.Mooring import Mooring
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.mooring import (
        Mooring, LineTypeData, NodeData, LineData
    )
    
    # Parameters
    mooring_radius = 250.0
    water_depth = 100.0
    grid_width = 50.0

    # Create input with custom iput
    mooring_input = CustomMooringInput()

    ###################################
    # ANCHORS node
    ###################################

    # Acnhor positions
    x=grid_width*np.array([1 , 1, 1 , 0, -1, -1, -1, 0]) + mooring_radius * np.array([np.cos(np.pi/4) , 1, np.cos(np.pi/4) , 0 , -np.cos(np.pi/4) , -1 , -np.cos(np.pi/4) , 0])
    y=grid_width*np.array([1 , 0, -1 , -1, -1, 0, 1, 1]) + mooring_radius * np.array([np.cos(np.pi/4) , 0, -np.cos(np.pi/4) , -1 , -np.cos(np.pi/4) , 0 , np.cos(np.pi/4) , 1])
    z = y*0.0 - water_depth

    # Anchor node template
    node_property = NodeData.NodeData()
    node_property.node_number=0
    node_property.type='Fix'
    node_property.position=np.zeros(shape=(3), dtype=float)
    node_property.anchor_required=True

    # Anchor nodes
    for idx in range(0,len(x)):
        node_property.node_number=idx+1.0
        node_property.position[0]=x[idx]
        node_property.position[1]=y[idx]
        node_property.position[2]=z[idx]
        mooring_input.node_properties.append(copy.deepcopy(node_property))
    
    ###################################
    # Vessel node
    ###################################

    # Grid node positions
    x=grid_width*np.array([1 , 1, 1 , 0, -1, -1, -1, 0]) 
    y=grid_width*np.array([1 , 0, -1 , -1, -1, 0, 1, 1])
    z = y*0 - 10.0

    # Grid node template
    node_property = NodeData.NodeData()
    node_property.node_number=0
    node_property.type='Vessel'
    node_property.position=np.zeros(shape=(3), dtype=float)
    node_property.anchor_required=False

    # Grid nodes
    for idx in range(0,len(x)):
        node_property.node_number=idx+1.0 +8
        node_property.position[0]=x[idx]
        node_property.position[1]=y[idx]
        node_property.position[2]=z[idx]
        mooring_input.node_properties.append(copy.deepcopy(node_property))

    ###################################
    # Main mooring line type
    ###################################

    line_type_data = LineTypeData.LineTypeData()
    line_type_data.name = "line_type1"
    line_type_data.line_type_name = "line_type1"
    line_type_data.diameter = 0.1998 
    line_type_data.weight_in_air=246.00
    line_type_data.weight_in_water=246.00-np.pi*0.1998**2/4*1025.0
    line_type_data.mbl = 12993000
    line_type_data.ea= 3870756308.49 
    line_type_data.cb= 1.0
    line_type_data.c_int_damp= 100000000.0
    line_type_data.ca= 0.6
    line_type_data.cdn= -1.0
    line_type_data.cdt= 0.05
    mooring_input.line_dictionnary.append(copy.deepcopy(line_type_data))

    ###################################
    # Main mooring lines
    ###################################

    line_property = LineData.LineData()
    line_property.line_number=0
    line_property.line_type_name="line_type1"
    line_property.line_type_index=0
    line_property.unstretched_length=300
    line_property.anchor_node_number=0
    line_property.fairlead_node_number=0


    for idx in range(0,8):
        line_property.line_number = idx +1
        line_property.anchor_node_number= idx +1
        line_property.fairlead_node_number= idx + 9
        mooring_input.line_properties.append(copy.deepcopy(line_property))

    # TO do: grid node = Connect ,  Buoyancy Module => equilibrium, add fairlead nodes, add submooringlines 

    ###################################
    # Solver options
    ###################################
    # mooring_input.solver_options.inner_ftol=1.0E-2
    # mooring_input.solver_options.inner_gtol=1.0E-2
    # mooring_input.solver_options.inner_xtol=1.0E-2
    # mooring_input.solver_options.inner_max_its=500
    # mooring_input.solver_options.outer_max_its=500
    # mooring_input.solver_options.outer_tol=1.0E-2
    ###################################
    # Create model
    ###################################

    mooring_system_force = Mooring.Mooring()
    pos = np.zeros(shape=(6))
    mooring_system_force.create_from_custom_input(mooring_input,pos)
    mooring_system_force.check_convergence=False

    vessel_position = np.array([0,0,0,0,0,0],dtype=float)
    mooring_system_force.initiate_calculation(water_depth,1025.0,vessel_position)


    for idx in range(0,7):

        print(mooring_system_force.get_fairlead_force_in_n(idx))

    force = mooring_system_force.compute_force(np.array([0.0,0.0,0.0,0.0,0.0,0.0]),np.array([0.0,0.0,0.0]))
    

    # print('force')
    # print(force)
    # mooring_system_force.plot_mooring_system()

    # force = mooring_system_force.compute_force(np.array([10,0,0,0,0,0]))
    # print('force')
    # print(force)
    #mooring_system_force.plot_mooring_system()



    mooring_input.saveJSON('mooring_input_masterstructure.json')

    assert True

def test_masterstructure_slave_mooring_example():
    from dtop_stationkeep.business.libraries.dtosk.mooring.Mooring import Mooring
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.mooring import (
        Mooring, LineTypeData, NodeData, LineData
    )
    
    # Parameters
    mooring_radius = 250.0
    water_depth = 100.0
    grid_width = 50.0

    # Create input with custom iput
    mooring_input = CustomMooringInput()

    
    ###################################
    # Attached node
    ###################################

    # Grid node positions
    x=0.5*grid_width*np.array([1,1,-1,-1]) + 25.0
    y=0.5*grid_width*np.array([1,-1,-1,1]) + 25.0
    z = y*0 - 10.0

    # Grid node template
    node_property = NodeData.NodeData()
    node_property.node_number=0
    node_property.type='Fix'
    node_property.position=np.zeros(shape=(3), dtype=float)
    node_property.anchor_required=False

    # Grid nodes
    for idx in range(0,len(x)):
        node_property.node_number=idx+1.0
        node_property.position[0]=x[idx]
        node_property.position[1]=y[idx]
        node_property.position[2]=z[idx]
        mooring_input.node_properties.append(copy.deepcopy(node_property))
        
    ###################################
    # VESSEL nodes
    ###################################

    # Vessel positions
    fairlead_radius = 3
    x=fairlead_radius * np.array([1,1,-1,-1])
    y=fairlead_radius * np.array([1,-1,-1,1])
    z = y*0.0

    # Vessel node template
    node_property = NodeData.NodeData()
    node_property.node_number=0
    node_property.type='Vessel'
    node_property.position=np.zeros(shape=(3), dtype=float)
    node_property.anchor_required=False

    # Vessel nodes
    for idx in range(0,len(x)):
        node_property.node_number=idx+1.0+4
        node_property.position[0]=x[idx]
        node_property.position[1]=y[idx]
        node_property.position[2]=z[idx]
        mooring_input.node_properties.append(copy.deepcopy(node_property))


    ###################################
    # Vessel line type
    ###################################

    line_type_data = LineTypeData.LineTypeData()
    line_type_data.name = "line_type3"
    line_type_data.line_type_name = "line_type3"
    line_type_data.diameter=0.0766 
    line_type_data.weight_in_air=150.035
    line_type_data.weight_in_water=150.035-np.pi*0.0766**2/4*1025.0
    line_type_data.mbl = 6252000
    line_type_data.ea=1.536e8
    line_type_data.cb= 1.0
    line_type_data.c_int_damp= 10.0
    line_type_data.ca= 0.6
    line_type_data.cdn= -1.0
    line_type_data.cdt= 0.05
    mooring_input.line_dictionnary.append(copy.deepcopy(line_type_data))


    ###################################
    # Vessel lines
    ###################################

    line_property = LineData.LineData()
    line_property.line_number=0
    line_property.line_type_name="line_type3"
    line_property.line_type_index=0
    line_property.unstretched_length=71.0
    line_property.anchor_node_number=0
    line_property.fairlead_node_number=0
    line_property.flags.append('OMIT_CONTACT')
    #line_property.flags.append('LINEAR_SPRING')
    line_property.flags.append('DIAGNOSTIC')

    a = np.array([1,2,3,4])
    f = np.array([5,6,7,8])
    

    for idx in range(0,len(f)):
        line_property.line_number = idx +1 
        line_property.anchor_node_number= a[idx]
        line_property.fairlead_node_number= f[idx]
        mooring_input.line_properties.append(copy.deepcopy(line_property))

    # TO do: grid node = Connect ,  Buoyancy Module => equilibrium, add fairlead nodes, add submooringlines 

    ###################################
    # Solver options
    ###################################
    # mooring_input.solver_options.inner_ftol=1.0E-7
    # mooring_input.solver_options.inner_gtol=1.0E-7
    # mooring_input.solver_options.inner_xtol=1.0E-7
    # mooring_input.solver_options.inner_max_its=500
    # mooring_input.solver_options.outer_max_its=500
    # mooring_input.solver_options.outer_tol=1.0E-2
    ###################################
    # Create model
    ###################################

    mooring_system_force = Mooring.Mooring()
    pos = np.zeros(shape=(6))
    mooring_system_force.create_from_custom_input(mooring_input,pos)
    mooring_system_force.check_convergence=False

    pos = np.array([25.0,25.0,0.0,0.0,0.0,0.0])
    mooring_system_force.initiate_calculation(water_depth,1025.0,pos)

    #mooring_system_force.plot_mooring_system()

    for idx in range(0,4):

        print(mooring_system_force.get_fairlead_force_in_n(idx))

    force = mooring_system_force.compute_force(np.array([25.0,25.0,0.0,0.0,0.0,0.0]),np.array([0,0,0]))
    # print('force')
    # print(force)
    #mooring_system_force.plot_mooring_system()

    # force = mooring_system_force.compute_force(np.array([10,0,0,0,0,0]))
    # print('force')
    # print(force)
    #mooring_system_force.plot_mooring_system()

    mooring_input.saveJSON('mooring_input_masterstructure_slave.json')

    assert True