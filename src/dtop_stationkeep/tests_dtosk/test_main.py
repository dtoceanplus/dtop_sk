# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
import os
import copy
from dtop_stationkeep.business.libraries.dtosk.inputs.Inputs import Inputs
from dtop_stationkeep.business.libraries.dtosk.run_main_module import run_main
from dtop_stationkeep.business.libraries.dtosk.outputs.Project import Project

def test_main():

    # Get path to example files
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    input_file = os.path.join(data_dir,'multi_substation.json')

    # Load example file
    inp = Inputs()

    # Load data from file instead of calling API (for testing purpose)
    inp.loadJSON(filePath = input_file)
    inp.sk_inputs.device_properties.path_to_nemoh_result_folder = os.path.join(data_dir,'TEST-AXI')
    ProjectId = 'multi_substation_example'

    project = run_main(inp, ProjectId,log_to_file = False)

    assert True


# def test_main_mc_input():
#     # Get path to example files
#     this_dir = os.path.dirname(os.path.realpath(__file__))
#     data_dir = os.path.join(this_dir, "..", "sample_data")
#     input_file = os.path.join(data_dir,'inputs_mc_module_bug.json')

#     # Load example file
#     inp = Inputs()

#     # Load data from file instead of calling API (for testing purpose)
#     inp.loadJSON(filePath = input_file)
#     inp.sk_inputs.substation_is_present=False
#     ProjectId = 'inputs_mc_module_bug'
    
#     if not os.path.isdir('./storage/' + ProjectId):
#         os.mkdir('./storage/' + ProjectId)
#     inp.sk_inputs.path_to_figure_folder = os.path.join('./storage',ProjectId)
#     inp.sk_inputs.device_properties.mooring_design_criteria.mooring_output_folder_path = os.path.join('./storage',ProjectId)
    

#     project = run_main(inp, ProjectId,log_to_file = True, log_to_stream = True)

#     assert True


    
