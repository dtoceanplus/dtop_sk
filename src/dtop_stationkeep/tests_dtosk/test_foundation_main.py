# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections 
import numpy as np 
import os
import pandas as pd
import pytest
try: 
    import json 
except: 
    print('WARNING: json is not installed.') 
from dtop_stationkeep.business.libraries.dtosk.foundations.foundation_main_module import run_foundation_design
from dtop_stationkeep.business.libraries.dtosk.foundations.foundationdesign import GravityFoundation
from dtop_stationkeep.business.libraries.dtosk.inputs.CustomFoundationInputs import CustomFoundationInputs
from dtop_stationkeep.business.libraries.dtosk.catalogue.AnchorCatalogue import AnchorCatalogue

#------------------------------------------------------------------------------------------------
## Inputs : This main is devopped for CL1 and 2
''' OTHER MODULES:
    
    soil_type_from_sc (string) : soil type from sc - CL 1 and 2 from liste
    soil_slope_from_sc (float): 'steep' if slope >= 10°, 'moderate' of slope < 10° [degrees]
    
    
    FROM MOORING DESIGN:
    
    seabed_connection_type (string): 'fixed' or 'moored'
    loads_ap (array): loads in absolute value and application point [V_min, V_max, H_x, H_y, M_x, M_y, z_ap] :- V_min: Vertical minimal load value (upwards) [N]
                                                                                                              - V_max: Vertical maximal load value (downwards) [N]
                                                                                                              - H_x: horizonal maximal load on x axis [N]
                                                                                                              - H_y: horizonal maximal load on y axis [N]
                                                                                                              - M_x: Maximal moment on x axis [Nm]
                                                                                                              - M_y: Maximal moment on y axis [Nm]
                                                                                                              - z_ap: distance between soil and the application point of loads [m]

    device_geom_description (dic): { geometry : 'cylider' or 'rectangular' (string), length on x or radius [m], length on y [m]}(required if seabed connexion type = fixed)
   
    USER:
    found_preference (string) : 'none' or 'type_of_foundation available', second option only available for CL3
    attachment_distance_for_pile_anchors (float): distance between the pile load attachement point above seafloor and the seafloor :  default value 0 [m]

    '''
def test_foundation_main():
    
    '''User inputs'''
    dist_attachment_pa = 0
    pile_tip = "close_end"
    deflection_criteria_for_pile = 0.1
    pile_length_above_seabed = 10

    '''other modules'''
    soil_type = 'very_dense_sand'
    soil_slope= 11


    '''SK inputs'''
    seabed_connection_type = 'moored'
    device_geometry = {'geometry':'cylinder', 'Lr':2}
    load_ap = [100, 500, 30000, 4000000, 50, 10000, 0] 
    foundation_preference = 'evaluation'

    # Not used
    soil_type_def = ''
    foundation_material = ''
    foundation_material_density = 0.0
    internal_friction_angle = 0.0
    relative_density = 0.0
    undrained_shear_strength = 0.0
    soil_bouyant_weight = 0.0
    soil_properties_sf = 'default'
    user_soil_properties_sf = 0.0
    d_chain = 0.0
    foundation_design_flag = 'auto'
    foundation_custom_input = CustomFoundationInputs()
    SF = 1.3
    uls_env = []
    water_depth = 100.0
    catalogue_anchors = AnchorCatalogue()

    [inputs,found_def,found_design] = run_foundation_design(foundation_preference,dist_attachment_pa,pile_tip,pile_length_above_seabed,deflection_criteria_for_pile,soil_type,soil_slope,seabed_connection_type,device_geometry,load_ap,SF, \
                    soil_type_def,foundation_material,foundation_material_density,internal_friction_angle,relative_density,undrained_shear_strength,soil_bouyant_weight,soil_properties_sf,user_soil_properties_sf, \
                    d_chain,foundation_design_flag,foundation_custom_input,uls_env,water_depth, 'evaluation',catalogue_anchors, 0)

    found_design.saveJSON("foundation_design.json")

    assert((found_def.foundation_type == 'pile') and (inputs.int_friction_angle == 38) and (found_design.deflection_crit == 0.1))

def test_assess_foundation_contact_points():

    '''SK inputs'''
    my_object  = GravityFoundation.GravityFoundation()
    my_object.soil_slope = 0.0
    my_object.soil = 'rock'
    my_object.int_friction_angle_d = 30.0
    my_object.base_alpha01 = 0
    my_object.base_alpha12 = 120
    my_object.base_alpha23 = 120
    my_object.base_r1 = 12.0
    my_object.base_r2 = 12.0
    my_object.base_r3 = 12.0
    my_object.contact_points_mass = 250000.0
    my_object.geometry_pref = 'contact_points'
    my_object.loads_from_mooring = [1200000, 1200000, 740000, 740000, 0, 0, 12]
    my_object.SF = 1.35
    my_object.seabed_connection_type = 'fixed'
    ''
    
    my_object.res_loads_design()
    my_object.compute_sliding_resistance()
    my_object.compute_resist_moment()
    my_object.assess()

    
    assert((my_object.Fn_sr == pytest.approx(2605078.125,0.001)) and (my_object.sliding_resistance == pytest.approx(1504042.5567287554,0.1)) and (my_object.criteria_stability == True))

def test_design_foundation_contact_points():

    '''SK inputs'''
    my_object  = GravityFoundation.GravityFoundation()
    my_object.soil_slope = 0.0
    my_object.soil = 'rock'
    my_object.int_friction_angle_d = 30.0 # aux alentours de 30 pour du rock
    my_object.geometry_pref = 'contact_points'
    # Chargements, grosse inconnue à determiner pour le design auto
    # Dans le cas de la turbine D10 Sabella, point d'app à (17-(10/2)=12m), en effort de trainée (1000kN/1.35=740kN), Vmax à 1 pour passer le init_eccentricity_fixed
    my_object.loads_from_mooring = [1200000, 1200000, 740000, 740000, 0, 0, 12] # grosse inconnue à determiner
    my_object.SF = 1.35
    my_object.n_iterations_max = 10000
    my_object.seabed_connection_type = 'fixed'
    ''
    my_object.design()

    assert((my_object.Fn_sr == pytest.approx(2563569.7211,0.001)) and (my_object.sliding_resistance == pytest.approx(1480077.66,0.01)) and (my_object.criteria_stability == True))

def test_foundation_main_drag():
    
    '''User inputs'''
    dist_attachment_pa = 0
    pile_tip = "close_end"
    deflection_criteria_for_pile = 0.1
    pile_length_above_seabed = 10

    '''other modules'''
    soil_type = 'very_dense_sand'
    soil_slope= 11


    '''SK inputs'''
    seabed_connection_type = 'moored'
    device_geometry = {'geometry':'cylinder', 'Lr':2}
    load_ap = [100, 500, 30000, 4000000, 50, 10000, 0] 
    foundation_preference = 'drag_anchor'

    # Not used
    soil_type_def = ''
    foundation_material = ''
    foundation_material_density = 0.0
    internal_friction_angle = 0.0
    relative_density = 0.0
    undrained_shear_strength = 0.0
    soil_bouyant_weight = 0.0
    soil_properties_sf = 'default'
    user_soil_properties_sf = 0.0
    d_chain = 0.0
    foundation_design_flag = 'auto'
    foundation_custom_input = CustomFoundationInputs()
    SF = 1.3
    uls_env = []
    water_depth = 100.0
    catalogue_anchors = AnchorCatalogue()

    [inputs,found_def,found_design] = run_foundation_design(foundation_preference,dist_attachment_pa,pile_tip,pile_length_above_seabed,deflection_criteria_for_pile,soil_type,soil_slope,seabed_connection_type,device_geometry,load_ap,SF, \
                    soil_type_def,foundation_material,foundation_material_density,internal_friction_angle,relative_density,undrained_shear_strength,soil_bouyant_weight,soil_properties_sf,user_soil_properties_sf, \
                    d_chain,foundation_design_flag,foundation_custom_input,uls_env,water_depth, 'evaluation',catalogue_anchors, 0)

    found_design.saveJSON("foundation_design.json")

    assert((found_def.foundation_type == 'drag_anchor') and (inputs.int_friction_angle == 38))