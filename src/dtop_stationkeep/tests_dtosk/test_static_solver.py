# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys

def test_equilibrium():

    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.solver.Static import Static

    # Init
    n_bundle=4
    n_line_per_batch=3
    mooring_radius=400.0
    mooring_ref_angle=180*np.pi/180.0
    mooring_line_length=300.0
    water_depth=200.0
    batch_angle=10.0*np.pi/180.0
    fairlead_radius= 5.
    fairlead_depth= -14.
    line_diameter=0.0766
    line_density=11.35
    line_ea=7.536e5
    seabed_friction_coeff=1.0
    sea_water_density=1025.0

    # Init
    body=Body()
    body.mooring_system_force.create_from_simple_input(body.position,n_bundle,n_line_per_batch,mooring_radius,mooring_ref_angle,mooring_line_length,water_depth,batch_angle,fairlead_radius,fairlead_depth,line_diameter,line_density,line_ea,seabed_friction_coeff,sea_water_density)
    body.position=np.array([-10.,-10.,10.,25*np.pi/180,25*np.pi/180,25*np.pi/180])
    body.hydrostatic_force.stiffness_matrix[2,2] = 1e6 # N/m
    body.hydrostatic_force.stiffness_matrix[3,3] = 1e9 # N/m
    body.hydrostatic_force.stiffness_matrix[4,4] = 1e9 # N/m
    body.constant_force.force_in_n=np.array([1e6, 1e6, 1895700., -1e7, 1e7, 1e6]) 

    # Init solver
    solver=Static()
    [xopt,eq_found]=solver.compute_equilibrium(body)
    body.position=xopt

    assert ((body.position[0] == pytest.approx(5.67106686e+01, 0.1)) and eq_found)

def test_equilibrium():

    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.solver.Static import Static

    # Init
    n_bundle=4
    n_line_per_batch=3
    mooring_radius=400.0
    mooring_ref_angle=180*np.pi/180.0
    mooring_line_length=300.0
    water_depth=200.0
    batch_angle=10.0*np.pi/180.0
    fairlead_radius= 5.
    fairlead_depth= -14.
    line_diameter=0.0766
    line_density=11.35
    line_ea=7.536e5
    seabed_friction_coeff=1.0
    sea_water_density=1025.0

    # Init
    body=Body()
    body.mooring_system_force.create_from_simple_input(body.position,n_bundle,n_line_per_batch,mooring_radius,mooring_ref_angle,mooring_line_length,water_depth,batch_angle,fairlead_radius,fairlead_depth,line_diameter,line_density,line_ea,seabed_friction_coeff,sea_water_density)
    body.position=np.array([-10.,-10.,10.,25*np.pi/180,25*np.pi/180,25*np.pi/180])
    body.hydrostatic_force.stiffness_matrix[2,2] = 1e6 # N/m
    body.hydrostatic_force.stiffness_matrix[3,3] = 1e9 # N/m
    body.hydrostatic_force.stiffness_matrix[4,4] = 1e9 # N/m
    Fext=np.array([1e6, 1e6, 1895700., -1e7, 1e7, 1e6]) 

    # Init solver
    solver=Static()
    body.set_constant_force(Fext)
    [xopt,eq_found]=solver.compute_equilibrium(body)
    body.position=xopt

    assert ((body.position[0] == pytest.approx(5.67106686e+01, 0.1)) and eq_found)


