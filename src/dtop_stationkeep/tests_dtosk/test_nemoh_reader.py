# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
import os
import copy
import matplotlib.pyplot as plt
from dtop_stationkeep.business.libraries.dtosk.utilities.Python_for_Nemoh.NemohReader import NemohReader
from dtop_stationkeep.business.libraries.dtosk.frequency_analysis_module import build_rao_from_nemoh_results

def test_run_nemoh_reader():

    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    result_path = os.path.join(data_dir,'TEST-AXI')

    ### Load Nemoh mesh and results
    nemoh = NemohReader( result_path )

    la=nemoh.A.shape
    lb=nemoh.B.shape
    lfe=nemoh.Fe.shape

    assert ((la[0]==6) and (lfe[1]==39))

def test_build_rao_from_nemoh_hdb():

    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    result_path = os.path.join(data_dir,'TEST2-SYM')

    ### Load Nemoh mesh and results
    nemoh = NemohReader( result_path )

    la=nemoh.A.shape
    lb=nemoh.B.shape
    lfe=nemoh.Fe.shape
    K_mooring = nemoh.mesh.inertia*0.0

    A = copy.deepcopy(np.transpose(nemoh.A,(2,0,1)))
    B = copy.deepcopy(np.transpose(nemoh.B,(2,0,1)))
    Fe = copy.deepcopy(np.transpose(nemoh.Fe,(1,0,2)))

    rao = build_rao_from_nemoh_results(nemoh.dir,nemoh.w,A,B,Fe,nemoh.mesh.inertia,nemoh.mesh.KH,K_mooring)
    # plt.plot(2*np.pi/nemoh.w,abs(rao[0,:,0]))
    # plt.plot(2*np.pi/nemoh.w,abs(rao[0,:,1]))
    # plt.plot(2*np.pi/nemoh.w,abs(rao[0,:,2]))
    # plt.show()

    assert (abs(rao[5,0,0]) == pytest.approx(1.029591460015115, 0.001))
