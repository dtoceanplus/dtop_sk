# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
import os
import copy
import pandas
from dtop_stationkeep.business.libraries.dtosk.run_analysis_cplx_module import run_analysis_cplx,populate_outputs
from dtop_stationkeep.business.libraries.dtosk.catalogue.Lines import Lines
from dtop_stationkeep.business.libraries.dtosk.catalogue.AnchorCatalogue import AnchorCatalogue
from dtop_stationkeep.business.libraries.dtosk.mooring.Designer import Designer
from dtop_stationkeep.business.libraries.dtosk.inputs.Cplx import Cplx
from dtop_stationkeep.business.libraries.dtosk.inputs.DeviceProperties import DeviceProperties
from dtop_stationkeep.business.libraries.dtosk.inputs.SteadyForceModel import SteadyForceModel
from dtop_stationkeep.business.libraries.dtosk.winching_module import winch_line_for_pretension
from dtop_stationkeep.business.libraries.dtosk.frequency_analysis_module import compute_eigenperiod,compute_max_dynamic_offset,run_frequency_analysis
from dtop_stationkeep.business.libraries.dtosk.inputs.CustomMooringInput import CustomMooringInput
from dtop_stationkeep.business.libraries.dtosk.inputs.MasterStructureProperties import MasterStructureProperties
from dtop_stationkeep.business.libraries.dtosk.inputs.FoundationDesignParameters import FoundationDesignParameters



def define_test_inputs_with_masterstructure():

    # Get path to nemoh file
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    result_path = os.path.join(data_dir,'TEST-AXI')
    mooring_input_path = os.path.join(data_dir,'mooring_input_example.json')
    mooring_input_slave_path = os.path.join(data_dir,'mooring_input_masterstructure_slave.json')
    mooring_input_master_path = os.path.join(data_dir,'mooring_input_masterstructure.json')
    # Create input structure for cplx
    input_cplx = Cplx()

    # Project name
    input_cplx.name='project_test2'

    # Device properties
    input_cplx.device_properties=DeviceProperties()

    # User inputs for steady force model
    input_cplx.device_properties.steady_force_model=SteadyForceModel()
    input_cplx.device_properties.steady_force_model.method='main_dimensions'
    input_cplx.device_properties.steady_force_model.device_dry_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_wet_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_dry_width=3.0
    input_cplx.device_properties.steady_force_model.device_wet_width=3.0
    input_cplx.device_properties.steady_force_model.device_dry_height=4
    input_cplx.device_properties.steady_force_model.device_wet_height=4

    
    # User inputs if nemoh run is used (standalone)
    input_cplx.device_properties.hdb_source='nemoh_run'
    input_cplx.device_properties.path_to_nemoh_result_folder=result_path
    input_cplx.device_properties.critical_damping_coefficient = np.array([0.1,0.1,0.1,0.1,0.1,0.1],dtype=float)
    
    # User inputs if mc_module is used
    #input_cplx.device_properties.hdb_source='mc_module'
    #input_cplx.device_properties.mass_matrix=np.zeros(shape=(6,6), dtype=float)
    #input_cplx.device_properties.hydrostatic_matrix=np.zeros(shape=(6,6), dtype=float)
    #input_cplx.device_properties.cog=np.zeros(shape=(3), dtype=float)
    input_cplx.device_properties.machine_type='wec'

    # Input for the tec devices
    #input_cplx.device_properties.thrust_coeff_curve_file=''
    #input_cplx.device_properties.rotor_diameter=0.0
    #input_cplx.device_properties.hub_position=np.zeros(shape=(3), dtype=float)
    
    # User input for mooring design
    input_cplx.device_properties.positioning_type  ='moored'
    input_cplx.device_properties.positioning_reference = 'masterstructure'
    input_cplx.device_properties.mooring_input_flag = 'custom'
    input_cplx.device_properties.mooring_design_criteria.mooring_type = 'catenary' # Important even if 'custom' is used because this drives the foundation choice as well (we will use the proofload to design the anchor)
    input_cplx.device_properties.mooring_design_criteria.nlines = 3
    input_cplx.device_properties.mooring_design_criteria.nlines_max = 10

    ########################################################################################
    ########################################################################################
    # Inputs required if mooring_input_flag = 'custom'
    ########################################################################################
    ########################################################################################
    input_cplx.device_properties.custom_mooring_input.append(CustomMooringInput())
    input_cplx.device_properties.custom_mooring_input[0].name='mooring_input_masterstructure_slave'
    input_cplx.device_properties.custom_mooring_input[0].loadJSON(filePath = mooring_input_slave_path)

    # Inputs required if mooring_input_flag = 'design'
    # # Constrains for pretension
    # input_cplx.device_properties.mooring_design_criteria.pretension_coeff_max=0.03 # 6% of MBL
    # input_cplx.device_properties.mooring_design_criteria.pretension_coeff_min=0.02 # 3% of MBL
    # input_cplx.device_properties.mooring_design_criteria.length_increment=1.0 # Winch 1.0m at a time (+or-)
    this_dir = os.path.dirname(os.path.realpath(__file__))
    storage_dir = os.path.join(this_dir, "..","..","..","storage")
    input_cplx.device_properties.mooring_design_criteria.mooring_output_folder_path = storage_dir


    # # Minimum horizontal egenperiod target [s]
    # input_cplx.device_properties.mooring_design_criteria.Tmin = 30
    
    # # Constrains for total offset
    # input_cplx.device_properties.mooring_design_criteria.offset_max = 30

    # Proof load for anchor of catenary line installation
    input_cplx.device_properties.mooring_design_criteria.proof_load_coeff = 0.8
    input_cplx.device_properties.mooring_design_criteria.safety_factor = 1.7

    # Inputs for motion analysis
    input_cplx.uls_analysis_parameters.weather_direction = np.array([0])

    # Inputs for the masterstructure, required here since input_cplx.device_properties.positioning_reference == 'masterstructure'
    input_cplx.master_structure_properties=MasterStructureProperties()
    input_cplx.master_structure_properties.positioning_type='moored'
    input_cplx.master_structure_properties.mooring_type='catenary'
    input_cplx.master_structure_properties.mooring_input_flag='custom'
    input_cplx.master_structure_properties.mass = 481751*1.5/9.81
    input_cplx.master_structure_properties.custom_mooring_input=CustomMooringInput()
    input_cplx.master_structure_properties.custom_mooring_input.name='mooring_input_masterstructure'
    input_cplx.master_structure_properties.custom_mooring_input.loadJSON(filePath = mooring_input_master_path)

    # Foundation parameters for the masterstructure
    input_cplx.master_structure_properties.foundation_design_parameters=FoundationDesignParameters()
    input_cplx.master_structure_properties.foundation_design_parameters.foundation_preference = 'none'
    input_cplx.master_structure_properties.foundation_design_parameters.dist_attachment_pa = 0
    input_cplx.master_structure_properties.foundation_design_parameters.pile_tip = 'close_end'
    input_cplx.master_structure_properties.foundation_design_parameters.deflection_criteria_for_pile = 0   
    input_cplx.master_structure_properties.foundation_design_parameters.soil_type = 'very_dense_sand'
    input_cplx.master_structure_properties.foundation_design_parameters.soil_slope = 1
    input_cplx.master_structure_properties.water_depth = 100.0  
    input_cplx.master_structure_properties.mooring_design_criteria.proof_load_coeff = 0.8
    input_cplx.master_structure_properties.mooring_design_criteria.safety_factor = 1.7
    this_dir = os.path.dirname(os.path.realpath(__file__))
    storage_dir = os.path.join(this_dir, "..","..","..","storage")
    input_cplx.master_structure_properties.mooring_design_criteria.mooring_output_folder_path = storage_dir
    input_cplx.master_structure_properties.mooring_design_criteria.ancillaries.price_method = 'percentage'
    input_cplx.master_structure_properties.mooring_design_criteria.ancillaries.percentage = 20 # [% of mooring line cost]



    # Read from SC
    input_cplx.water_density=1025.0
    input_cplx.uls_analysis_parameters.hs_array=np.array([12,13,14])
    input_cplx.uls_analysis_parameters.tp_array=np.array([14,15,16])
    input_cplx.uls_analysis_parameters.current_vel=2.0
    input_cplx.uls_analysis_parameters.wind_vel=30.0

    input_cplx.device_properties.foundation_design_parameters.soil_type = 'very_dense_sand'
    input_cplx.device_properties.foundation_design_parameters.soil_slope = 1

    # Read from EC
    input_cplx.water_depth=np.array([100,100,100,100])
    input_cplx.east=np.array([-25, 25, 25, -25])
    input_cplx.north=np.array([25, -25, 25, -25])
    input_cplx.yaw=np.array([0, 0, 0, 0])
    input_cplx.deviceId=np.array([0,1,2,3])

    return input_cplx


def test_cplx_masterstructure_cplx3():

    from dtop_stationkeep.business.libraries.dtosk.run_main_module import populate_design_assessment_results

    # Load inputs (cplx3)
    input_cplx = define_test_inputs_with_masterstructure()

    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()

    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)


	# Populate results from ULS, FLS and Design analyses and save mooring system as map++ file
    [devices,master_structure,master_structure_is_present,substation,substation_is_present]=populate_design_assessment_results(farm) 


    # pandas.set_option('display.max_columns', 500)

    # print('hierarchy')
    # d = pandas.read_json(hierarchy.hierarchy_as_pandas_table)
    # print(d)

    # print('bom')
    # d = pandas.read_json(bom.fill_pandas_table())
    # print(d)

    # print(master_structure.design_results.mooring.ancillaries.anc_list_name)
    # print(master_structure.design_results.mooring.ancillaries.anc_list_cost)

    # print('envimpact')
    # print(env_impact.farm.footprint)

    #farm.plot()
    assert (master_structure_is_present and (farm.master_structure.results.uls_results.tension_versus_mbl[0,0,0] == pytest.approx(0.043558404549626346, 0.001) ) and (env_impact.farm.footprint == pytest.approx(98073.65715, 0.1)) and master_structure.design_results.mooring.ancillaries.anc_list_cost[0] == pytest.approx(177120, 0.001)) 


def test_cplx_masterstructure_fixed_cplx3():

    from dtop_stationkeep.business.libraries.dtosk.run_main_module import populate_design_assessment_results

    # Load inputs (cplx3)
    input_cplx = define_test_inputs_with_masterstructure()

    # Define the masterstructure as a fixed structure
    input_cplx.master_structure_properties.positioning_type = 'fixed'

    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()

    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)


	# Populate results from ULS, FLS and Design analyses and save mooring system as map++ file
    [devices,master_structure,master_structure_is_present,substation,substation_is_present]=populate_design_assessment_results(farm) 


    # pandas.set_option('display.max_columns', 500)

    # print('hierarchy')
    # d = pandas.read_json(hierarchy.hierarchy_as_pandas_table)
    # print(d)

    # print('bom')
    # d = pandas.read_json(bom.bom_as_pandas_table)
    # print(d)

    # print('envimpact')
    # print(env_impact.farm.footprint)
    # print(farm.master_structure.results.uls_results.forces_on_fixed_structure)
    # print(farm.master_structure.results.design_results.anchor_and_foundation.gravity_foundation_design.base_length_x)
    # print(farm.master_structure.results.design_results.anchor_and_foundation.gravity_foundation_design.base_length_y)
    # print(farm.master_structure.results.design_results.anchor_and_foundation.gravity_foundation_design.device_radius)
    # print(farm.master_structure.results.design_results.anchor_and_foundation.gravity_foundation_design.thickness)
    # farm.plot()

    assert(master_structure_is_present and farm.master_structure.results.uls_results.forces_on_fixed_structure[0,0,0] == pytest.approx(181121.85719368092, 0.1) and farm.master_structure.results.design_results.anchor_and_foundation.gravity_foundation_design.thickness == pytest.approx(1.6, 0.0001) and farm.master_structure.results.design_results.anchor_and_foundation.foundation_type == 'shallow' )