# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
import os
import copy
from dtop_stationkeep.business.libraries.dtosk.rotation_module import rotation_about_z
import matplotlib.pyplot as plt
from dtop_stationkeep.business.libraries.dtosk.linear_wave_module import compute_jonswap_spectrum
from dtop_stationkeep.business.libraries.dtosk.frequency_analysis_module import compute_amplitude_from_spectrum,compute_excitation_force,compute_excitation_spectrum

def test_compute_transfer_function():

    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.entity.BodyPart import BodyPart
    from dtop_stationkeep.business.libraries.dtosk.forceModel.FirstOrderWaveForce import FirstOrderWaveForce

    body = Body()
    body.body_parts.append(BodyPart())
    body.body_parts[0].cylinder.length=2.0
    body.body_parts[0].cylinder.diameter=10.0
    body.body_parts[0].n_strip=1.0
    body.body_parts.append(BodyPart())
    body.body_parts[1].cylinder.length=2.0
    body.body_parts[1].cylinder.diameter=10.0
    body.body_parts[1].n_strip=1.0
    water_depth = 1000.0
    rho = 1025.0
    positioning_type = 'moored'

    # Omega array
    period = np.linspace(5, 6, num=2)
    omega_array = 2*np.pi / period

    # Beta array
    beta_array = np.linspace(0, 0, num=1) * np.pi / 180.0

    # Compute transfer function
    fo_wf = FirstOrderWaveForce()
    Hf = fo_wf.compute_transfer_function(positioning_type,body.position,body.body_parts,omega_array, beta_array,water_depth,rho)

    assert (abs(Hf[0,0,0]) == pytest.approx(469179.26, 0.1))

def test_compute_excitation_force():

    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.entity.BodyPart import BodyPart
    from dtop_stationkeep.business.libraries.dtosk.forceModel.FirstOrderWaveForce import FirstOrderWaveForce

    body = Body()
    body.body_parts.append(BodyPart())
    body.body_parts[0].cylinder.length=2.0
    body.body_parts[0].cylinder.diameter=10.0
    body.body_parts[0].n_strip=1.0
    body.body_parts.append(BodyPart())
    body.body_parts[1].cylinder.length=2.0
    body.body_parts[1].cylinder.diameter=10.0
    body.body_parts[1].n_strip=1.0
    water_depth = 1000.0
    rho = 1025.0
    positioning_type = 'moored'

    # Beta array
    beta_array = np.linspace(0, 0, num=1) * np.pi / 180.0

    # Compute wave spectrum
    hs = 5
    tp = 12.8657
    omega_array = np.linspace(2*np.pi/50, 2*np.pi/3, num=100)
    S = compute_jonswap_spectrum(hs,tp,omega_array)

    # Compute transfer function
    fo_wf = FirstOrderWaveForce()
    Hf = fo_wf.compute_transfer_function(positioning_type,body.position,body.body_parts,omega_array, beta_array,water_depth,rho)

    # Compute wave amplitude
    eta = compute_amplitude_from_spectrum(omega_array,S)

    # Compute force
    Fext = compute_excitation_force(Hf[:,:,0],eta)

    # Compute force spectrum
    Sff = compute_excitation_spectrum(Hf[:,:,0],S)

    assert ((max(abs(Fext[0,:])) == pytest.approx(48624.45599, 0.1)) and (max(abs(Sff[0,0,:])) == pytest.approx(59446767295.6076, 1.0))) 

def test_calc1():

    body_position_in_n = np.zeros(shape=(6), dtype=float)
    body_position_in_n[0] = 5.0
    beta = 10*np.pi/180.0

    # Compute euler angle of body in w
    body_position_in_w = np.zeros(shape=(6), dtype=float)
    body_position_in_w[3:6] = copy.deepcopy(body_position_in_n[3:6])
    body_position_in_w[5] = body_position_in_w[5] - beta

    # Compute body position in w
    body_position_in_w[0:3] = rotation_about_z( body_position_in_n[0:3], -beta ) # Note that we use a minus sign for -beta here because we want to compute the position in w, not the position in n after a rotation beta

    assert (body_position_in_w[0] == pytest.approx(4.92403877, 0.0001))





