# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
import os

def test_gravity_force1():

    from dtop_stationkeep.business.libraries.dtosk.forceModel.GravityForce import GravityForce
    
    # Init
    body_mass=7.5
    cog_position_in_b=np.array([0.0,0.0,0.0])
    rotation_center_in_b=np.array([0.0,0.0,0.0])
    body_position_in_n=np.array([0.0,0.0,0.0,0.0,0.0,0.0])
    gravityForce=GravityForce()

    force=gravityForce.compute_force(body_position_in_n,cog_position_in_b,body_mass,rotation_center_in_b)

    assert force[2] == pytest.approx(-73.575, 0.0001)

def test_gravity_force2():

    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    
    # Init
    body=Body()
    body.mass=7.5
    force=body.compute_static_force()

    assert force[2] == pytest.approx(-73.575, 0.0001)

def test_gravity_force3():

    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    
    # Init
    body=Body()
    body.mass=7.5
    alpha=20*np.pi/180.0
    body.position[3]=alpha
    force=body.compute_static_force()

    assert ((force[2] == pytest.approx(-73.575*np.cos(alpha), 0.0001)) and (force[1] == pytest.approx(-73.575*np.sin(alpha), 0.0001)))

def test_gravity_force4():

    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    
    # Init
    body=Body()
    body.mass=7.5
    alpha=20*np.pi/180.0
    body.position[4]=alpha
    force=body.compute_static_force()

    assert ((force[2] == pytest.approx(-73.575*np.cos(alpha), 0.0001)) and (force[0] == pytest.approx(73.575*np.sin(alpha), 0.0001)))

def test_hydrostatic_force():

    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    
    # Init
    body=Body()
    body.mass=0.0
    body.hydrostatic_force.reference_position[2]=-2.0
    body.hydrostatic_force.stiffness_matrix=np.array([[1,0,0,4,5,0],
				                                      [0,8,0,10,11,0],
				                                      [3,0,21,11,11,0],
                                                      [0,0,22,12,11,0],
                                                      [0,0,23,13,11,0],
                                                      [0,0,24,14,11,12]
                                                      ])
    body.position[2]=-2.0
    force=body.compute_static_force()
    body.position[2]=-4.0
    force1=body.compute_static_force()
    body.position[0]=2.0
    force2=body.compute_static_force()
    assert ((force[2] == pytest.approx(0.0, 0.0001)) and (force1[2] == pytest.approx(42.0, 0.0001))and (force2[2] == pytest.approx(36.0, 0.0001)))



def test_mooring_system_force():

    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    
    # Init
    body=Body()
    body.mass=0.0
   
    # Init
    n_bundle=4
    n_line_per_batch=3
    mooring_radius=400.0
    mooring_ref_angle=180*np.pi/180.0
    mooring_line_length=300.0
    water_depth=200.0
    batch_angle=10.0*np.pi/180.0
    fairlead_radius= 40.
    fairlead_depth= 14.
    line_diameter=0.0766
    line_density=11.35
    line_ea=7.536e5
    seabed_friction_coeff=1.0
    sea_water_density=1025.0

    # Populate mooring
    body.mooring_system_force.create_from_simple_input(body.position,n_bundle,n_line_per_batch,mooring_radius,mooring_ref_angle,mooring_line_length,water_depth,batch_angle,fairlead_radius,fairlead_depth,line_diameter,line_density,line_ea,seabed_friction_coeff,sea_water_density)
    
    # Compute force
    body.position=np.array([50,0,0,0,0,0])
    force1=body.compute_static_force()
    body.position=np.array([0,0,0,0,25*np.pi/180.0,0])
    force2=body.compute_static_force()
    
    assert ((force1[0] == pytest.approx(-8.26850663e+05, 100.0)) and (force2[4] == pytest.approx( 7.54758764e+06,100.0)))

def test_rotor_force():

    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")

    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.environment.Current import Current
    from dtop_stationkeep.business.libraries.dtosk.environment.Wave import Wave
    from dtop_stationkeep.business.libraries.dtosk.forceModel.RotorForce import RotorForce

    # Create rotor force model
    rotor1=RotorForce()
    rotor1.rotor_diameter=10.0
    rotor1.hub_position[2]=-10.0
    rotor1.read_thrust_coeff_curve_from_file(os.path.join(data_dir,'thrustcurv.txt'))
    
    # Add orbital velocity
    rotor1.include_orbital_velocity=True

    # Create body
    body=Body()
    body.mass=0.0
    body.rotor_force.append(rotor1)
    body.rotation_center_in_b=np.array([0.0,0.0,0.0])


    # Create current
    current=Current()
    water_depth=30.0
    current.velocity=3.5
    current.direction=20*np.pi/180.0
    current.profile_type='uniform'

    # Create wave
    wave=Wave()
    wave.hs=1.5
    wave.tp=10.0
    wave.direction=20*np.pi/180.0

    # Compute rotor force
    force1=body.rotor_force[0].compute_force(body.position,body.rotation_center_in_b,current,water_depth,wave)
    
    assert ((force1[0] == pytest.approx(43187.95559657, 0.001)) and (force1[1] == pytest.approx( 15719.13031596,0.001)) and (force1[3] == pytest.approx( 157191.30315961,0.001)))

def test_drag_coeff_cyl():
    from dtop_stationkeep.business.libraries.dtosk.forceModel.DragCoeffCyl import DragCoeffCyl

    dragcoef=DragCoeffCyl()

    surface_roughness = 3.e-3
    fluid_kinematic_viscosity = 1.45e-5
    diameter = 10.0
    vel=0.9

    coef = dragcoef.get_drag_coefficient(diameter,vel,fluid_kinematic_viscosity,surface_roughness)

    assert (coef == pytest.approx(0.40404784, 0.000001))

def test_wind_force_cyl():
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.entity.BodyPart import BodyPart
    from dtop_stationkeep.business.libraries.dtosk.environment.Wind import Wind

    # Create body
    body=Body()
    body.mass=0.0

    # body part no.1
    body.body_parts.append(BodyPart())
    body.body_parts[0].position_in_b[0]=5.0
    body.body_parts[0].cylinder.length=20
    body.body_parts[0].cylinder.diameter=10

    # body part no.2
    body.body_parts.append(BodyPart())
    body.body_parts[1].position_in_b[0]=-2.0
    body.body_parts[1].cylinder.length=20
    body.body_parts[1].cylinder.diameter=10


    # body part no.3
    body.body_parts.append(BodyPart())
    body.body_parts[2].cylinder.length=10
    body.body_parts[2].cylinder.diameter=10
    body.body_parts[2].position_in_b[2]=10.0
    body.body_parts[2].position_in_b[4]=90.0*np.pi/180.0

    # body part no.3
    body.body_parts.append(BodyPart())
    body.body_parts[3].cylinder.length=10
    body.body_parts[3].cylinder.diameter=10
    body.body_parts[3].position_in_b[2]=5
    body.body_parts[3].position_in_b[4]=40.0*np.pi/180.0


    # Create wind
    wind = Wind()
    wind.velocity=14.0
    wind.direction=20.0*np.pi/180.0

    # Compute wind force
    force = body.compute_static_force(wind = wind)

    assert ((force[0] == pytest.approx(23548.141, 0.1)) and (force[3] == pytest.approx(-47558.5787952, 0.1)) and (force[5] == pytest.approx(8252.963, 0.1)))

def test_geom_rect():
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.entity.BodyPart import BodyPart
    from dtop_stationkeep.business.libraries.dtosk.geometry_module import get_dry_rectangle_properties

    # Create body
    body=Body()
    body.mass=0.0

    # body part no.1
    body.body_parts.append(BodyPart())
    body.body_parts[0].position_in_b[0]=0.0
    body.body_parts[0].rectangle.dx=20
    body.body_parts[0].rectangle.dy=10
    body.body_parts[0].rectangle.dz=30


    [zmax,height,width,length,pp_n] = get_dry_rectangle_properties(body.position,body.body_parts[0].position_in_b,body.body_parts[0].rectangle)

    assert ((height == pytest.approx(15.0, 0.001)) and (width == pytest.approx(10.0, 0.001)) and (length == pytest.approx(20.0, 0.001)) and (pp_n[2] == pytest.approx(7.5, 0.001)))

def test_wind_coeff_rect():
    from dtop_stationkeep.business.libraries.dtosk.forceModel.WindCoeffRect import WindCoeffRect

    wind_coeff_rect=WindCoeffRect()

    height=11.0
    width=10.0
    length=20.0

    drag_coeff = wind_coeff_rect.get_drag_coefficient(height,width,length)

    assert ((drag_coeff == pytest.approx(0.755, 0.001)))

def test_wind_force_rect():
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.entity.BodyPart import BodyPart
    from dtop_stationkeep.business.libraries.dtosk.environment.Wind import Wind

    # Create body
    body=Body()
    body.mass=0.0

    # body part no.1
    body.body_parts.append(BodyPart())
    body.body_parts[0].position_in_b[0]=0.0
    body.body_parts[0].geometry_type='rectangle'
    body.body_parts[0].rectangle.dx=20
    body.body_parts[0].rectangle.dy=10
    body.body_parts[0].rectangle.dz=22

    # Create wind
    wind = Wind()
    wind.velocity=14.0
    wind.direction=0.0*np.pi/180.0

    # Compute force
    force = body.compute_static_force(wind = wind)

    assert ((force[0] == pytest.approx(10200.84, 0.01)) and (force[4] == pytest.approx(56104.63, 0.01)))

def test_current_force_cyl():
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.entity.BodyPart import BodyPart
    from dtop_stationkeep.business.libraries.dtosk.environment.Current import Current

    # Create body
    body=Body()
    body.mass=0.0

    # body part no.1
    body.body_parts.append(BodyPart())
    body.body_parts[0].position_in_b[2]=-5.0
    body.body_parts[0].cylinder.length=20
    body.body_parts[0].cylinder.diameter=10
    body.current_force.n_strips = 100

    # Create wind
    current = Current()
    current.velocity=2.0
    current.direction=0.0*np.pi/180.0
    water_depth = 100.0

    # Compute wind force
    force = body.compute_static_force(current = current, water_depth = water_depth)

    assert ((force[0] == pytest.approx(206025, 0.1)) and (force[4] == pytest.approx(-1545187.5, 0.1)))

def test_current_force_rect():
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.entity.BodyPart import BodyPart
    from dtop_stationkeep.business.libraries.dtosk.environment.Current import Current

    # Create body
    body=Body()
    body.mass=0.0

    # body part no.1
    body.body_parts.append(BodyPart())
    body.body_parts[0].position_in_b[0]=0.0
    body.body_parts[0].geometry_type='rectangle'
    body.body_parts[0].rectangle.dx=20
    body.body_parts[0].rectangle.dy=10
    body.body_parts[0].rectangle.dz=22
    body.current_force.n_strips = 100

    # Create current
    current = Current()
    current.velocity=2.0
    current.direction=0.0*np.pi/180.0
    water_depth = 100.0

    # Compute force
    force = body.compute_static_force(current = current, water_depth = water_depth)

    assert ((force[0] == pytest.approx(415647.64, 0.01)) and (force[4] == pytest.approx(-2286062.02, 0.01)))

def test_mean_wave_drift_fixed_cyl():
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.entity.BodyPart import BodyPart
    from dtop_stationkeep.business.libraries.dtosk.environment.Wave import Wave

    # Create body
    body=Body()
    body.mass=0.0

    # body part no.1
    body.body_parts.append(BodyPart())
    body.body_parts[0].cylinder.length=20
    body.body_parts[0].cylinder.diameter=10

    # Create wave
    wave=Wave()
    wave.hs=10.0
    wave.tp=11.0
    wave.direction = 20*np.pi/180.0
    water_depth = 100.0

    # Compute wind force
    force = body.compute_static_force(wave = wave, water_depth = water_depth)

    assert ((force[0] == pytest.approx(53734.16777152, 0.01)) and (force[1] == pytest.approx(19557.6376319, 0.01)))

def test_mean_wave_drift_floating_cyl():
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.entity.BodyPart import BodyPart
    from dtop_stationkeep.business.libraries.dtosk.environment.Wave import Wave

    # Create body
    body=Body()
    body.mass=0.0
    body.positioning_type='moored'

    # body part no.1
    body.body_parts.append(BodyPart())
    body.body_parts[0].position_in_b[1] = 5
    body.body_parts[0].cylinder.length=20
    body.body_parts[0].cylinder.diameter=10

    # Create wave
    wave=Wave()
    wave.hs=10.0
    wave.tp=11.0
    wave.direction = 20*np.pi/180.0
    water_depth = 100.0

    # Compute wind force
    force = body.compute_static_force(wave = wave, water_depth = water_depth)

    assert ((force[0] == pytest.approx(52839.08648594617, 0.01)) and (force[1] == pytest.approx(19231.854686701954, 0.01)) and (force[5] == pytest.approx(0.0, 0.01)))

def test_wave_force_rect():
    from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
    from dtop_stationkeep.business.libraries.dtosk.entity.BodyPart import BodyPart
    from dtop_stationkeep.business.libraries.dtosk.environment.Wave import Wave

    # Create body
    body=Body()
    body.mass=0.0

    # body part no.1
    body.body_parts.append(BodyPart())
    body.body_parts[0].position_in_b[0]=0.0
    body.body_parts[0].geometry_type='rectangle'
    body.body_parts[0].rectangle.dx=20
    body.body_parts[0].rectangle.dy=10
    body.body_parts[0].rectangle.dz=22

    # Create wave
    wave=Wave()
    wave.hs=10.0
    wave.tp=16.0
    wave.direction = 20.0*np.pi/180.0
    water_depth = 100.0

    # Compute force
    force = body.compute_static_force(wave = wave, water_depth = water_depth)

    assert ((force[0] == pytest.approx(281894.06444998, 0.01)) and (force[1] == pytest.approx(205202.09735223, 0.01)))
