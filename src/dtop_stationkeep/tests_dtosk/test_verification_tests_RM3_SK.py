# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import pytest
import sys
import os
import copy
from dtop_stationkeep.business.libraries.dtosk.inputs.CustomMooringInput import CustomMooringInput
import plotly.graph_objects as go
import pandas as pd
import plotly.express as px
from dtop_stationkeep.business.libraries.dtosk.inputs.MooringCheckInputs import MooringCheckInputs
from dtop_stationkeep.business.libraries.dtosk.run_mooring_check_module import run_mooring_check
from dtop_stationkeep.business.libraries.dtosk.mooring import (Mooring, LineTypeData, NodeData, LineData)
from dtop_stationkeep.business.libraries.dtosk.entity.Body import Body
from dtop_stationkeep.business.libraries.dtosk.catalogue.Lines import Lines
from dtop_stationkeep.business.libraries.dtosk.catalogue.AnchorCatalogue import AnchorCatalogue
from dtop_stationkeep.business.libraries.dtosk.inputs.Cplx import Cplx
from dtop_stationkeep.business.libraries.dtosk.inputs.Inputs import Inputs
from dtop_stationkeep.business.libraries.dtosk.run_analysis_cplx_module import run_analysis_cplx
import logging
from logging.handlers import RotatingFileHandler
import matplotlib.pyplot as plt



def define_custom_mooring():


    # Path definition
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    storage_dir = os.path.join(this_dir, "..","..","..","storage")
    mooring_file = os.path.join(data_dir,'mooring_input_example.json')

    # Load input
    mooring_check_inputs = MooringCheckInputs()

    mooring_check_inputs.water_depth = 70.0
    mooring_check_inputs.water_density = 1025.0
    mooring_check_inputs.device_position = np.array([0.0,0.0,0.0,0.0,0.0,0.0])
    mooring_check_inputs.html_plot_file_name = os.path.join(storage_dir,'mooring_check_plot_RM3.html')
    mooring_check_inputs.results_file_name = os.path.join(storage_dir,'mooring_check_results_RM3.json') 
    mooring_check_inputs.logo_file_name = os.path.join(data_dir,'dto_logo.png') 


    
    # Init two line type data
    mooring=Mooring.Mooring()
    ###############
    # Nylon line  #
    ###############
    # Material properties from Orcaflex documentation
    line_type=LineTypeData.LineTypeData()
    line_type.line_type_name='nylon_rope_1' 
    line_type.material='nylon'
    line_type.type='rope'
    line_type.quality=''
    line_type.catalogue_id='custom'
    line_type.diameter=0.146
    line_type.diameter_fls=0.146
    line_type.weight_in_air=0.6476e3*line_type.diameter**2
    od = 0.85046*line_type.diameter
    line_type.weight_in_water = line_type.weight_in_air - np.pi*od**2/4*1025
    line_type.ea=1.18*10**8*line_type.diameter**2
    line_type.cb=0.8
    line_type.mbl=139357000*line_type.diameter**2
    line_type.mbl_uls=139357000*line_type.diameter**2
    line_type.cost_per_meter=2392*line_type.diameter**2 # Qingdao Florescence Co., LTD, based on 8 Strands 80mm nylon rope. https://florescence.en.made-in-china.com/product-group/rokQtqdOHRYU/NYLON-ROPE-catalog-1.html
    line_type.ad=0.259 # T-N curve for fiber ropes!
    line_type.m=13.46 # T-N curve for fiber ropes!
    line_type.name='nylon_rope_1' 
    line_type.description=''
    mooring.line_dictionnary.append(line_type)

    # Init node properties
    # Fairlead nodes
    node=NodeData.NodeData()
    node.node_number=1
    node.type='Vessel' 
    node.position=np.array([6,0.00,-20])
    node.force=np.zeros(shape=(3), dtype=float) 
    node.name='node1' 
    node.description='node number 1'
    mooring.node_properties.append(node)
    node=NodeData.NodeData()
    node.node_number=2
    node.type='Vessel' 
    node.position=np.array([-3,5.196,-20])
    node.force=np.zeros(shape=(3), dtype=float) 
    node.name='node2' 
    node.description='node number 2'
    mooring.node_properties.append(node)
    node=NodeData.NodeData()
    node.node_number=3
    node.type='Vessel' 
    node.position=np.array([-3,-5.196,-20])
    node.force=np.zeros(shape=(3), dtype=float) 
    node.name='node3' 
    node.description='node number 3'
    mooring.node_properties.append(node)
    # Anchor nodes
    node=NodeData.NodeData()
    node.node_number=4
    node.type='Fix' 
    node.anchor_required = True
    node.position=np.array([350,0.00,-70])
    node.force=np.zeros(shape=(3), dtype=float) 
    node.name='node4' 
    node.description='node number 4'
    mooring.node_properties.append(node)
    node=NodeData.NodeData()
    node.node_number=5
    node.type='Fix' 
    node.anchor_required = True
    node.position=np.array([-175,303.108,-70])
    node.force=np.zeros(shape=(3), dtype=float) 
    node.name='node5' 
    node.description='node number 5'
    mooring.node_properties.append(node)
    node=NodeData.NodeData()
    node.node_number=6
    node.type='Fix' 
    node.anchor_required = True
    node.position=np.array([-175,-303.108,-70])
    node.force=np.zeros(shape=(3), dtype=float) 
    node.name='node6' 
    node.description='node number 6'
    mooring.node_properties.append(node)

    # Init line properties
    coeff = 0.98
    line=LineData.LineData()
    line.line_number=1.0 
    line.line_type_name='nylon_rope_1' 
    line.unstretched_length=coeff*np.linalg.norm(mooring.node_properties[3].position - mooring.node_properties[0].position)
    line.anchor_node_number=4
    line.fairlead_node_number=1 
    line.flags=['GX_POS','GY_POS','GZ_POS','LINEAR_SPRING'] 
    line.name='line1' 
    mooring.line_properties.append(line)
    line=LineData.LineData()
    line.line_number=2.0 
    line.line_type_name='nylon_rope_1' 
    line.unstretched_length=coeff*np.linalg.norm(mooring.node_properties[4].position - mooring.node_properties[1].position)
    line.anchor_node_number=5
    line.fairlead_node_number=2 
    line.flags=['GX_POS','GY_POS','GZ_POS','LINEAR_SPRING'] 
    line.name='line2' 
    mooring.line_properties.append(line)
    line=LineData.LineData()
    line.line_number=3.0 
    line.line_type_name='nylon_rope_1'  
    line.unstretched_length=coeff*np.linalg.norm(mooring.node_properties[5].position - mooring.node_properties[2].position)
    line.anchor_node_number=6
    line.fairlead_node_number=3 
    line.flags=['GX_POS','GY_POS','GZ_POS','LINEAR_SPRING'] 
    line.name='line3' 
    mooring.line_properties.append(line)

    mooring_check_inputs.custom_mooring_input.line_dictionnary = copy.deepcopy(mooring.line_dictionnary)
    mooring_check_inputs.custom_mooring_input.node_properties = copy.deepcopy(mooring.node_properties)
    mooring_check_inputs.custom_mooring_input.line_properties = copy.deepcopy(mooring.line_properties)
    mooring_check_inputs.custom_mooring_input.reference_system = 'device'
    
    # Run mooring check
    #run_mooring_check(mooring_check_inputs,show=False)


    return mooring_check_inputs.custom_mooring_input

def define_custom_mooring_semi_taut():


    # Path definition
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    storage_dir = os.path.join(this_dir, "..","..","..","storage")
    mooring_file = os.path.join(data_dir,'mooring_input_example.json')

    # Load input
    mooring_check_inputs = MooringCheckInputs()

    mooring_check_inputs.water_depth = 70.0
    mooring_check_inputs.water_density = 1025.0
    mooring_check_inputs.device_position = np.array([0.0,0.0,0.0,0.0,0.0,0.0])
    mooring_check_inputs.html_plot_file_name = os.path.join(storage_dir,'mooring_check_plot_RM3.html')
    mooring_check_inputs.results_file_name = os.path.join(storage_dir,'mooring_check_results_RM3.json') 
    mooring_check_inputs.logo_file_name = os.path.join(data_dir,'dto_logo.png') 

    # Init two line type data
    mooring=Mooring.Mooring()
    ###############
    # Nylon line  #
    ###############
    # Material properties from Orcaflex documentation
    line_type=LineTypeData.LineTypeData()
    line_type.line_type_name='nylon_rope_1' 
    line_type.material='nylon'
    line_type.type='rope'
    line_type.quality=''
    line_type.catalogue_id='custom'
    line_type.diameter=0.146
    line_type.diameter_fls=0.146
    line_type.weight_in_air=0.6476e3*line_type.diameter**2
    od = 0.85046*line_type.diameter
    line_type.weight_in_water = line_type.weight_in_air - np.pi*od**2/4*1025
    line_type.ea=1.18*10**8*line_type.diameter**2
    line_type.cb=0.8
    line_type.mbl=139357000*line_type.diameter**2
    line_type.mbl_uls=139357000*line_type.diameter**2
    line_type.cost_per_meter=2392*line_type.diameter**2 # Qingdao Florescence Co., LTD, based on 8 Strands 80mm nylon rope. https://florescence.en.made-in-china.com/product-group/rokQtqdOHRYU/NYLON-ROPE-catalog-1.html
    line_type.ad=0.259 # T-N curve for fiber ropes!
    line_type.m=13.46 # T-N curve for fiber ropes!
    line_type.name='nylon_rope_1' 
    line_type.description=''
    mooring.line_dictionnary.append(line_type)

    ###############
    # Chain line  #
    ###############
    steel_young_modulus = 200*10**9
    line_type=LineTypeData.LineTypeData()
    line_type.line_type_name='line_chain1' 
    line_type.material='steel'
    line_type.type='chain'
    line_type.quality='R3'
    line_type.catalogue_id='custom'
    line_type.diameter=0.090
    line_type.diameter_fls=0.090
    line_type.weight_in_air=162.0
    line_type.weight_in_water=162.0*(7850.0 - 1025)/7850.0
    line_type.ea=2*np.pi*line_type.diameter**2/4*steel_young_modulus
    line_type.cb=1.0
    line_type.mbl=6647000
    line_type.mbl_uls=6647000
    line_type.cost_per_meter=1.5*line_type.weight_in_air
    line_type.ad=6.0*(10**10)
    line_type.m=3
    line_type.name='line_chain1' 
    line_type.description=''
    mooring.line_dictionnary.append(line_type)

    # Init node properties
    # Fairlead nodes
    node=NodeData.NodeData()
    node.node_number=1
    node.type='Vessel' 
    node.position=np.array([6,0.00,-20])
    node.force=np.zeros(shape=(3), dtype=float) 
    node.name='node1' 
    node.description='node number 1'
    mooring.node_properties.append(node)
    node=NodeData.NodeData()
    node.node_number=2
    node.type='Vessel' 
    node.position=np.array([-3,5.196,-20])
    node.force=np.zeros(shape=(3), dtype=float) 
    node.name='node2' 
    node.description='node number 2'
    mooring.node_properties.append(node)
    node=NodeData.NodeData()
    node.node_number=3
    node.type='Vessel' 
    node.position=np.array([-3,-5.196,-20])
    node.force=np.zeros(shape=(3), dtype=float) 
    node.name='node3' 
    node.description='node number 3'
    mooring.node_properties.append(node)
    # Intermediate nodes (bottom connection chain-nylon)
    radius = 300
    node=NodeData.NodeData()
    node.node_number=4
    node.type='Connect' 
    node.anchor_required = False
    idx = 0
    node.position=np.array([radius*np.cos(120*idx*np.pi/180),radius*np.sin(120*idx*np.pi/180),-70])
    node.force=np.zeros(shape=(3), dtype=float) 
    node.name='node4' 
    node.description='node number 4'
    node.point_mass = 10000
    mooring.node_properties.append(node)
    node=NodeData.NodeData()
    node.node_number=5
    node.type='Connect' 
    node.anchor_required = False
    idx = 1
    node.position=np.array([radius*np.cos(120*idx*np.pi/180),radius*np.sin(120*idx*np.pi/180),-70])
    node.force=np.zeros(shape=(3), dtype=float) 
    node.name='node5' 
    node.description='node number 5'
    node.point_mass = 10000
    mooring.node_properties.append(node)
    node=NodeData.NodeData()
    node.node_number=6
    node.type='Connect' 
    node.anchor_required = False
    idx = 2
    node.position=np.array([radius*np.cos(120*idx*np.pi/180),radius*np.sin(120*idx*np.pi/180),-70])
    node.force=np.zeros(shape=(3), dtype=float) 
    node.name='node6' 
    node.description='node number 6'
    node.point_mass = 10000
    mooring.node_properties.append(node)
    # Anchor nodes
    node=NodeData.NodeData()
    node.node_number=7
    node.type='Fix' 
    node.anchor_required = True
    node.position=np.array([350,0.00,-70])
    node.force=np.zeros(shape=(3), dtype=float) 
    node.name='node7' 
    node.description='node number 7'
    mooring.node_properties.append(node)
    node=NodeData.NodeData()
    node.node_number=8
    node.type='Fix' 
    node.anchor_required = True
    node.position=np.array([-175,303.108,-70])
    node.force=np.zeros(shape=(3), dtype=float) 
    node.name='node8' 
    node.description='node number 8'
    mooring.node_properties.append(node)
    node=NodeData.NodeData()
    node.node_number=9
    node.type='Fix' 
    node.anchor_required = True
    node.position=np.array([-175,-303.108,-70])
    node.force=np.zeros(shape=(3), dtype=float) 
    node.name='node9' 
    node.description='node number 9'
    mooring.node_properties.append(node)


    # Nylon line properties
    coeff = 0.999
    line=LineData.LineData()
    line.line_number=1.0 
    line.line_type_name='nylon_rope_1' 
    line.unstretched_length=coeff*np.linalg.norm(mooring.node_properties[3].position - mooring.node_properties[0].position)
    line.anchor_node_number=4
    line.fairlead_node_number=1 
    line.flags=['GX_POS','GY_POS','GZ_POS','LINEAR_SPRING'] 
    line.name='line1' 
    mooring.line_properties.append(line)
    line=LineData.LineData()
    line.line_number=2.0 
    line.line_type_name='nylon_rope_1' 
    line.unstretched_length=coeff*np.linalg.norm(mooring.node_properties[4].position - mooring.node_properties[1].position)
    line.anchor_node_number=5
    line.fairlead_node_number=2 
    line.flags=['GX_POS','GY_POS','GZ_POS','LINEAR_SPRING'] 
    line.name='line2' 
    mooring.line_properties.append(line)
    line=LineData.LineData()
    line.line_number=3.0 
    line.line_type_name='nylon_rope_1'  
    line.unstretched_length=coeff*np.linalg.norm(mooring.node_properties[5].position - mooring.node_properties[2].position)
    line.anchor_node_number=6
    line.fairlead_node_number=3 
    line.flags=['GX_POS','GY_POS','GZ_POS','LINEAR_SPRING'] 
    line.name='line3' 
    mooring.line_properties.append(line)

    # Chain line properties
    coeff = 1.00
    line=LineData.LineData()
    line.line_number=6
    line.line_type_name='line_chain1'  
    line.unstretched_length=coeff*np.linalg.norm(mooring.node_properties[8].position - mooring.node_properties[5].position)
    line.anchor_node_number=9
    line.fairlead_node_number=6 
    line.flags=['GX_POS','GY_POS','GZ_POS'] 
    line.name='bottom_chain3' 
    mooring.line_properties.append(line)
    line=LineData.LineData()
    line.line_number=4
    line.line_type_name='line_chain1' 
    line.unstretched_length=coeff*np.linalg.norm(mooring.node_properties[6].position - mooring.node_properties[3].position)
    line.anchor_node_number=7
    line.fairlead_node_number=4
    line.flags=['GX_POS','GY_POS','GZ_POS'] 
    line.name='bottom_chain1' 
    mooring.line_properties.append(line)
    line=LineData.LineData()
    line.line_number=5
    line.line_type_name='line_chain1' 
    line.unstretched_length=coeff*np.linalg.norm(mooring.node_properties[7].position - mooring.node_properties[4].position)
    line.anchor_node_number=8
    line.fairlead_node_number=5 
    line.flags=['GX_POS','GY_POS','GZ_POS',] 
    line.name='bottom_chain2' 
    mooring.line_properties.append(line)


    mooring_check_inputs.custom_mooring_input.line_dictionnary = copy.deepcopy(mooring.line_dictionnary)
    mooring_check_inputs.custom_mooring_input.node_properties = copy.deepcopy(mooring.node_properties)
    mooring_check_inputs.custom_mooring_input.line_properties = copy.deepcopy(mooring.line_properties)
    mooring_check_inputs.custom_mooring_input.reference_system = 'device'
    
    # Run mooring check
    run_mooring_check(mooring_check_inputs,show=False)


    return mooring_check_inputs.custom_mooring_input

def define_RM3_SK1():

    # Get path to nemoh file
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    result_path = os.path.join(data_dir,'RM3_six_dofs')

    # Create input structure for cplx
    input_cplx = Cplx()

    # Project name
    input_cplx.name='RM3_SK1'



    # User inputs for steady force model
    input_cplx.device_properties.steady_force_model.method='main_dimensions'
    input_cplx.device_properties.steady_force_model.device_dry_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_wet_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_dry_width=0.0
    input_cplx.device_properties.steady_force_model.device_wet_width=6.0
    input_cplx.device_properties.steady_force_model.device_dry_height=0.0
    input_cplx.device_properties.steady_force_model.device_wet_height=40.0

    
    # User inputs if nemoh run is used (standalone)
    input_cplx.device_properties.hdb_source='nemoh_run'
    input_cplx.device_properties.path_to_nemoh_result_folder=result_path
    input_cplx.device_properties.critical_damping_coefficient = np.array([0.1,0.1,0.1,0.1,0.1,0.1],dtype=float)
    # User inputs if mc_module is used
    #input_cplx.device_properties.hdb_source='mc_module'
    #input_cplx.device_properties.mass_matrix=np.zeros(shape=(6,6), dtype=float)
    #input_cplx.device_properties.hydrostatic_matrix=np.zeros(shape=(6,6), dtype=float)
    #input_cplx.device_properties.cog=np.zeros(shape=(3), dtype=float)
    input_cplx.device_properties.machine_type='wec'
    
    # User input for mooring design
    input_cplx.device_properties.positioning_type  ='moored'
    input_cplx.device_properties.positioning_reference = 'seabed'
    input_cplx.device_properties.mooring_input_flag = 'design'
    input_cplx.device_properties.mooring_design_criteria.mooring_type = 'catenary'
    input_cplx.device_properties.mooring_design_criteria.nlines = 3
    input_cplx.device_properties.mooring_design_criteria.nlines_max = 10
    input_cplx.device_properties.mooring_design_criteria.fairlead_vertical_position = -20
    input_cplx.device_properties.mooring_design_criteria.fairlead_radius = 3

    # Constrains for pretension
    input_cplx.device_properties.mooring_design_criteria.pretension_coeff_min=0.01 # 3% of MBL
    input_cplx.device_properties.mooring_design_criteria.length_increment=0.5 # Winch 1.0m at a time (+or-)

    # Minimum horizontal egenperiod target [s]
    input_cplx.device_properties.mooring_design_criteria.Tmin = 40
    
    # Constrains for total offset
    input_cplx.device_properties.mooring_design_criteria.offset_max = 24

    # Proof load for anchor of catenary line installation
    input_cplx.device_properties.mooring_design_criteria.proof_load_coeff = 0.8
    input_cplx.device_properties.mooring_design_criteria.safety_factor = 1.7

    # Inputs for the masterstructure
    #input_cplx.master_structure_properties=MasterStructureProperties.MasterStructureProperties()

    # Inputs for motion analysis
    input_cplx.uls_analysis_parameters.weather_direction = np.array([0,180])

    # Parameters specific to foundation design
    input_cplx.device_properties.foundation_design_parameters.foundation_preference = 'evaluation'
    input_cplx.device_properties.foundation_design_parameters.dist_attachment_pa = 0
    input_cplx.device_properties.foundation_design_parameters.pile_tip = 'open_end'
    input_cplx.device_properties.foundation_design_parameters.deflection_criteria_for_pile = 10
    input_cplx.device_properties.foundation_design_parameters.SF = 1.8

    # Read from SC
    input_cplx.water_density=1025.0
    input_cplx.uls_analysis_parameters.hs_array=np.array([11.9])
    input_cplx.uls_analysis_parameters.tp_array=np.array([17.1])
    input_cplx.uls_analysis_parameters.current_vel=0.59
    input_cplx.uls_analysis_parameters.wind_vel=0.0
    input_cplx.water_depth=np.array([70])
    input_cplx.device_properties.foundation_design_parameters.soil_type = 'medium_dense_sand'
    input_cplx.device_properties.foundation_design_parameters.soil_slope = 0

    # Read from EC
    input_cplx.east=np.array([0])
    input_cplx.north=np.array([0])
    input_cplx.yaw=np.array([0])
    input_cplx.deviceId=np.array([0])
    

    return input_cplx

# def test_RM3_SK1():

#     # The users knows : environmental conditions, properties of the floater. Wants to have an idea of a catenary system.

#     # Load inputs (cplx1 or 2)
#     input_cplx = define_RM3_SK1()

#     # Import catalogue
#     catalogue_chains = Lines()

#     # Save input structure
#     inp = Inputs()
#     inp.sk_inputs = input_cplx
#     inp.catalogue_lines = catalogue_chains
#     inp.saveJSON('test_RM3_SK1_inputs.json')

#     # Run main
#     [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains)

#     assert (farm.body_array[0].mooring_system_force.line_dictionnary[0].diameter == pytest.approx(0.06,0.0001))

def test_RM3_SK2():

    # Load inputs (cplx1 or 2)
    input_cplx = define_RM3_SK1()

    # The users knows : the mooring system
    input_cplx.device_properties.mooring_input_flag = 'custom'
    moor = define_custom_mooring()
    moor.saveJSON('yololo.json')
    input_cplx.device_properties.custom_mooring_input.append(moor)
    input_cplx.device_properties.mooring_design_criteria.proof_load_coeff = 0.8

    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()

    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)

    assert (farm.body_array[0].results.uls_results.tension_versus_mbl[1,0,0] == pytest.approx(0.13079919797582565,0.0001))

def test_RM3_SK3():
    import json

    # Load inputs (cplx1 or 2)
    input_cplx = define_RM3_SK1()

    # The users knows : the mooring system and FLS parameters
    input_cplx.device_properties.mooring_input_flag = 'custom'
    input_cplx.device_properties.custom_mooring_input.append(define_custom_mooring())
    input_cplx.device_properties.mooring_design_criteria.proof_load_coeff = 0.8

    # FLS parameters
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    env_file = os.path.join(data_dir,'envs_RM3.json')
    with open(env_file) as json_file:
        data = json.load(json_file)
    input_cplx.fls_analysis_parameters.loadFromJSONDict(data.get('ENVS'))
    
    # # Write fls parameters to text file
    # fls_file = os.path.join(data_dir,'fls_env_RM3.txt')
    # f = open(fls_file, "w")
    # for idx in range(0,len(data.hs_dp_proba)):
    #     f.write(str(data.intervals_hs_min[idx]) +' '+\
    #          str(data.intervals_hs_max[idx]) +' '+\
    #         str(data.intervals_dp_min[idx]) +' '+\
    #         str(data.intervals_dp_max[idx]) +' '+\
    #         str(data.associated_tp[idx]) +' '+\
    #         str(data.associated_cur[idx]) +' '+\
    #         str(data.associated_dircur[idx]) +' '+\
    #         str(data.associated_wind[idx]) +' '+\
    #         str(data.associated_dirwind[idx]) +' '+\
    #         str(data.hs_dp_proba[idx]) + '\n')
    # f.close()


    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()

    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)
    
    # print(farm.body_array[0].results.fls_results.cdf_stress_range[-1,-1])
    # print(farm.body_array[0].results.fls_results.damage_one_year)
    # print(farm.body_array[0].results.fls_results.damage_lifetime)
    assert (farm.body_array[0].results.fls_results.cdf_stress_range[-1,-1]== pytest.approx(1.6214504295082404,0.001))



# def test_L_limits():

#     w = 70.0
#     r = 1190

#     x=np.linspace(1,50,50)

#     Lmax = w+r-x
#     Lmin = np.sqrt(w**2 + (r+x)**2)

#     plt.plot(x,Lmax,label='Lmax')
#     plt.plot(x,Lmin,label='Lmin')
#     plt.legend()
#     plt.show()


#     print(w*r/(2*r+w))

#     print(w*34/(w-2*34))



#     rmin = w*x/(w-2*x)
#     plt.plot(x,rmin,label='rmin')
#     plt.legend()
#     plt.show()  

#     assert False


