# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import atexit
import requests
import pytest
from pact import Consumer, Like, Provider, Term

MC_SINGLE_MACHINE_HYDRODYNAMIC = {
    "mass_matrix": [[1e6]],
    "hydrosatic_stiffness": [[1e6]],
    "added_mass": [[[0]]],
    "radiation_damping": [[[5e6]]],
    "excitation_force_real": [[[1e3]]],
    "excitation_force_imag": [[[2e3]]],
    "fitting_damping": [[[[[0.3]]]]],
    "fitting_stiffness": [[[[[0.4]]]]],
    "velocity_transformation_matrix": [[1.0]],
    "wave_direction": [0.0],
    "wave_frequency":[0.1]
}


pact = Consumer("sk").has_pact_with(Provider("mc"), port=1237)
pact.start_service()
atexit.register(pact.stop_service)

def test_mc_single_machine_hydrodynamic():
    pact.given(
        "mc 1 exists and it has single machine hydrodynamic"
    ).upon_receiving(
        "a request for mc"
    ).with_request(
        "GET", Term(r'/mc/\d+/single_machine_hydrodynamic', '/mc/1/single_machine_hydrodynamic')
    ).will_respond_with(200, body=Like(MC_SINGLE_MACHINE_HYDRODYNAMIC))

    with pact:
        uri = 'http://localhost:1237/mc/1/single_machine_hydrodynamic'
        result = requests.get(uri)
        assert result.status_code == 200
        

MC_WEC_CPLX1 = {
    "capture_width_ratio": 0.3,
    "machine_archetype": "Point Absorber"
}

pact = Consumer("sk").has_pact_with(Provider("mc"), port=1237)
pact.start_service()
atexit.register(pact.stop_service)

def test_mc_wec_cplx1():
    pact.given(
        "mc 1 exists and it has model wec1"
    ).upon_receiving(
        "a request for mc"
    ).with_request(
        "GET", Term(r'/mc/\d+/model/wec/complexity1', '/mc/1/model/wec/complexity1')
    ).will_respond_with(200, body=Like(MC_WEC_CPLX1))

    with pact:
        uri = 'http://localhost:1237/mc/1/model/wec/complexity1'
        result = requests.get(uri)
        assert result.status_code == 200
        

MC_WEC_CPLX2 = {
    "capture_width_ratio": [[[0.3]]],
    "tp_capture_width": [5],
    "hs_capture_width": [2],
    "wave_angle_capture_width": [0],
    "pto_damping": [1e6],
    "mooring_stiffness": 2e6,
    "machine_archetype": "Point Absorber"
}

pact = Consumer("sk").has_pact_with(Provider("mc"), port=1237)
pact.start_service()
atexit.register(pact.stop_service)

def test_mc_wec_cplx2():
    pact.given(
        "mc 1 exists and it has model wec2"
    ).upon_receiving(
        "a request for mc"
    ).with_request(
        "GET", Term(r'/mc/\d+/model/wec/complexity2', '/mc/1/model/wec/complexity2')
    ).will_respond_with(200, body=Like(MC_WEC_CPLX2))

    with pact:
        uri = 'http://localhost:1237/mc/1/model/wec/complexity2'
        result = requests.get(uri)
        assert result.status_code == 200
        

MC_WEC_CPLX3 = {
    "tp_capture_width": [5],
    "hs_capture_width": [2],
    "wave_angle_capture_width": [0],
    "pto_damping": [[[[[1e6]]]]],
    "mooring_stiffness": [[[2e6]]],
    "additional_damping": [[[1e6]]],
    "additional_stiffness": [[[2e6]]],
    "shared_dof": [0,0,1,0,0,0]
}

pact = Consumer("sk").has_pact_with(Provider("mc"), port=1237)
pact.start_service()
atexit.register(pact.stop_service)

def test_mc_wec_cplx3():
    pact.given(
        "mc 1 exists and it has model wec3"
    ).upon_receiving(
        "a request for mc"
    ).with_request(
        "GET", Term(r'/mc/\d+/model/wec/complexity3', '/mc/1/model/wec/complexity3')
    ).will_respond_with(200, body=Like(MC_WEC_CPLX3))

    with pact:
        uri = 'http://localhost:1237/mc/1/model/wec/complexity3'
        result = requests.get(uri)
        assert result.status_code == 200
        

MC_TEC_CPLX1 = {
    "number_rotor": 1
}

pact = Consumer("sk").has_pact_with(Provider("mc"), port=1237)
pact.start_service()
atexit.register(pact.stop_service)

def test_mc_tec_cplx1():
    pact.given(
        "mc 1 exists and it has tec at cpx1"
    ).upon_receiving(
        "a request for mc"
    ).with_request(
        "GET", Term(r'/mc/\d+/model/tec/complexity1', '/mc/1/model/tec/complexity1')
    ).will_respond_with(200, body=Like(MC_TEC_CPLX1))

    with pact:
        uri = 'http://localhost:1237/mc/1/model/tec/complexity1'
        result = requests.get(uri)
        assert result.status_code == 200
        

MC_TEC_CPLX2 = {
    "number_rotor": 1,
    "rotor_interdistance": 0
}

pact = Consumer("sk").has_pact_with(Provider("mc"), port=1237)
pact.start_service()
atexit.register(pact.stop_service)

def test_mc_tec_cplx2():
    pact.given(
        "mc 1 exists and it has tec at cpx2"
    ).upon_receiving(
        "a request for mc"
    ).with_request(
        "GET", Term(r'/mc/\d+/model/tec/complexity2', '/mc/1/model/tec/complexity2')
    ).will_respond_with(200, body=Like(MC_TEC_CPLX2))

    with pact:
        uri = 'http://localhost:1237/mc/1/model/tec/complexity2'
        result = requests.get(uri)
        assert result.status_code == 200
        

MC_TEC_CPLX3 = {
    "ct": [0],
    "cp_ct_velocity": [0],
    "number_rotor": 1,
    "rotor_interdistance": 0
}

pact = Consumer("sk").has_pact_with(Provider("mc"), port=1237)
pact.start_service()
atexit.register(pact.stop_service)

def test_mc_tec_cplx3():
    pact.given(
        "mc 1 exists and it has tec at cpx3"
    ).upon_receiving(
        "a request for mc"
    ).with_request(
        "GET", Term(r'/mc/\d+/model/tec/complexity3', '/mc/1/model/tec/complexity3')
    ).will_respond_with(200, body=Like(MC_TEC_CPLX3))

    with pact:
        uri = 'http://localhost:1237/mc/1/model/tec/complexity3'
        result = requests.get(uri)
        assert result.status_code == 200
        

MC_GENERAL = {
    "floating": True,
    "preferred_fundation_type": "Pile"
}

pact = Consumer("sk").has_pact_with(Provider("mc"), port=1237)
pact.start_service()
atexit.register(pact.stop_service)

def test_mc_general():
    pact.given(
        "mc 1 exists and it has general"
    ).upon_receiving(
        "a request for mc"
    ).with_request(
        "GET", Term(r'/mc/\d+/general', '/mc/1/general')
    ).will_respond_with(200, body=Like(MC_GENERAL))

    with pact:
        uri = 'http://localhost:1237/mc/1/general'
        result = requests.get(uri)
        assert result.status_code == 200
        

MC_DIMENSIONS = {
    "height": 10,
    "draft": 3,
    "mass": 700000,
    "wet_frontal_area": 10,
    "dry_frontal_area": 100,
    "characteristic_dimension": 8,
    "dry_profile": "cylinder",
    "wet_profile": "cylinder",
    "hub_height": 7
}

pact = Consumer("sk").has_pact_with(Provider("mc"), port=1237)
pact.start_service()
atexit.register(pact.stop_service)

def test_mc_dimensions():
    pact.given(
        "mc 1 exists and it has dimensions"
    ).upon_receiving(
        "a request for mc"
    ).with_request(
        "GET", Term(r'/mc/\d+/dimensions', '/mc/1/dimensions')
    ).will_respond_with(200, body=Like(MC_DIMENSIONS))

    with pact:
        uri = 'http://localhost:1237/mc/1/dimensions'
        result = requests.get(uri)
        assert result.status_code == 200
        

MC_MACHINE = {
    "complexity": 1,
    "type": "WEC"
  }

pact = Consumer("sk").has_pact_with(Provider("mc"), port=1237)
pact.start_service()
atexit.register(pact.stop_service)

def test_mc_machine():
    pact.given(
        "mc 1 exists and it has machine"
    ).upon_receiving(
        "a request for mc"
    ).with_request(
        "GET", Term(r'/mc/\d+', '/mc/1')
    ).will_respond_with(200, body=Like(MC_MACHINE))

    with pact:
        uri = 'http://localhost:1237/mc/1'
        result = requests.get(uri)
        assert result.status_code == 200