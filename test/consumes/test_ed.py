# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import atexit
import requests
import pytest
from pact import Consumer, Like, Provider, Term


ED_STUDY_RESULTS_CPX3 = {
          "collection_point_dict": [{"location": [1010.0, 1250.0, -50.0], "mass": 28823.53, "height": 5.0, "width": 12.5, "length": 4.5, "CP_height_seabed": 70.0}],
          "umbilical_dict": [{"marker": 6, "db ref": 48, "device": "Device001", "seabed_connection_point": [1220.0, 1250.0, -50.0], "length": 107.35455276791944, "x_coordinates": [0.0, 0.74, 1.52, 2.34, 3.21, 4.13, 5.11, 6.16, 7.28, 8.49, 9.79, 11.19, 12.7, 14.33, 16.1, 17.99, 19.99, 22.09, 24.23, 26.37, 28.46, 30.45, 32.56, 34.71, 36.79, 38.73, 40.49, 42.08, 43.5, 44.77, 45.93, 46.98, 48.1, 49.3, 50.6, 52.0, 53.51, 55.15, 56.91, 58.8, 60.81, 62.9, 65.04, 67.19, 69.34, 71.49, 73.63, 75.78, 77.93, 80.07], "z_coordinates": [0.0, -2.02, -4.02, -6.0, -7.96, -9.9, -11.81, -13.69, -15.52, -17.29, -19.0, -20.63, -22.16, -23.55, -24.77, -25.79, -26.56, -27.03, -27.18, -26.99, -26.48, -25.69, -25.3, -25.38, -25.92, -26.84, -28.07, -29.51, -31.12, -32.85, -34.66, -36.53, -38.36, -40.14, -41.85, -43.48, -45.0, -46.39, -47.62, -48.64, -49.4, -49.87, -50.02, -50.02, -50.02, -50.02, -50.02, -50.02, -50.02, -50.02], "voltage_rating": 11000.0, "current_rating": 167.0, "max_device_offset": 15, "diameter": 82.6, "mbr": 1.1563999999999999, "mass": 1375.64123916812, "materials": [{"material_name": "low_alloyed_steel", "material_quantity": 999.36}, {"material_name": "copper", "material_quantity": 147.18}, {"material_name": "polypropylene", "material_quantity": 146.75}, {"material_name": "polyethylene", "material_quantity": 74.29}], "cost": 12360.141456621917, "failure_rate_repair": 0.0, "failure_rate_replacement": 0.0009786441030323538}, {"marker": 11, "db ref": 48, "device": "Device002", "seabed_connection_point": [1070.0, 1250.0, -50.0], "length": 107.35455276791944, "x_coordinates": [0.0, 0.74, 1.52, 2.34, 3.21, 4.13, 5.11, 6.16, 7.28, 8.49, 9.79, 11.19, 12.7, 14.33, 16.1, 17.99, 19.99, 22.09, 24.23, 26.37, 28.46, 30.45, 32.56, 34.71, 36.79, 38.73, 40.49, 42.08, 43.5, 44.77, 45.93, 46.98, 48.1, 49.3, 50.6, 52.0, 53.51, 55.15, 56.91, 58.8, 60.81, 62.9, 65.04, 67.19, 69.34, 71.49, 73.63, 75.78, 77.93, 80.07], "z_coordinates": [0.0, -2.02, -4.02, -6.0, -7.96, -9.9, -11.81, -13.69, -15.52, -17.29, -19.0, -20.63, -22.16, -23.55, -24.77, -25.79, -26.56, -27.03, -27.18, -26.99, -26.48, -25.69, -25.3, -25.38, -25.92, -26.84, -28.07, -29.51, -31.12, -32.85, -34.66, -36.53, -38.36, -40.14, -41.85, -43.48, -45.0, -46.39, -47.62, -48.64, -49.4, -49.87, -50.02, -50.02, -50.02, -50.02, -50.02, -50.02, -50.02, -50.02], "voltage_rating": 11000.0, "current_rating": 167.0, "max_device_offset": 15, "diameter": 82.6, "mbr": 1.1563999999999999, "mass": 1375.64123916812, "materials": [{"material_name": "low_alloyed_steel", "material_quantity": 999.36}, {"material_name": "copper", "material_quantity": 147.18}, {"material_name": "polypropylene", "material_quantity": 146.75}, {"material_name": "polyethylene", "material_quantity": 74.29}], "cost": 12360.141456621917, "failure_rate_repair": 0.0, "failure_rate_replacement": 0.0009786441030323538}]
          }

pact = Consumer("sk").has_pact_with(Provider("ed"), port=1236)
pact.start_service()
atexit.register(pact.stop_service)

def test_ed_study_results_cpx3():
    pact.given(
        "ed 1 exists at cpx 3"
    ).upon_receiving(
        "a request for ed"
    ).with_request(
        "GET", Term(r'/api/energy-deliv-studies/\d+/results', '/api/energy-deliv-studies/1/results')
    ).will_respond_with(200, body=Like(ED_STUDY_RESULTS_CPX3))

    with pact:
        uri = 'http://localhost:1236/api/energy-deliv-studies/1/results'
        result = requests.get(uri)
        assert result.status_code == 200


ED_STUDY_RESULTS_CPX1 = {
          "collection_point_dict": [{"location": None, "CP_height_seabed": None, "height": None, "width": None, "length": None, "mass": None}],
          "umbilical_dict": None
          }

pact = Consumer("sk").has_pact_with(Provider("ed"), port=1236)
pact.start_service()
atexit.register(pact.stop_service)

def test_ed_study_results_cpx1():
    pact.given(
        "ed 2 exists at cpx 1"
    ).upon_receiving(
        "a request for ed"
    ).with_request(
        "GET", Term(r'/api/energy-deliv-studies/\d+/results', '/api/energy-deliv-studies/1/results')
    ).will_respond_with(200, body=Like(ED_STUDY_RESULTS_CPX1))

    with pact:
        uri = 'http://localhost:1236/api/energy-deliv-studies/1/results'
        result = requests.get(uri)
        assert result.status_code == 200








