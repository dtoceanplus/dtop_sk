# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import atexit
import requests
import pytest
from pact import Consumer, Like, Provider, Term

EC_FARM = {
  "layout": {
      "deviceID": [0],
      "easting": [595451.7045415622],
      "northing": [5391879.706434956]
    }
}

pact = Consumer("sk").has_pact_with(Provider("ec"), port=1234)
pact.start_service()
atexit.register(pact.stop_service)

def test_ec_farm():
    pact.given(
        "ec 1 exists and it has farm"
    ).upon_receiving(
        "a request for ec"
    ).with_request(
        "GET", Term(r'/ec/\d+/farm', '/ec/1/farm')
    ).will_respond_with(200, body=Like(EC_FARM))

    with pact:
        uri = 'http://localhost:1234/ec/1/farm'
        result = requests.get(uri)
        assert result.status_code == 200

EC_INPUT_FARM = {
  "orientation_angle": 12.2,
  "nb_devices": 2,
}

pact = Consumer("sk").has_pact_with(Provider("ec"), port=1234)
pact.start_service()
atexit.register(pact.stop_service)

def test_ec_inputs_farm():
    pact.given(
        "ec 1 exists and it has inputs farm"
    ).upon_receiving(
        "a request for ec"
    ).with_request(
        "GET", Term(r'/ec/\d+/inputs/farm', '/ec/1/inputs/farm')
    ).will_respond_with(200, body=Like(EC_INPUT_FARM))

    with pact:
        uri = 'http://localhost:1234/ec/1/inputs/farm'
        result = requests.get(uri)
        assert result.status_code == 200