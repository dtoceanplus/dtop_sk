# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import atexit
import requests
import pytest
from pact import Consumer, Like, Provider, Term

SC_STATISTICS = {
    "waves": {
        "EXC": {
            "hs_tp": {
                "longitude": -2.000272812034975,
                "latitude": 49.76047415657464,
                "return_periods": [
                    0.0
                ],
                "return_values1": [
                    [
                        0.0
                    ]
                ],
                "return_values2": [
                    [
                        0.0
                    ]
                ]
            }
        },
        "ENVS": {
            "__type__": "dtosc:point:outputs:ENVSWaves",
            "name": "none",
            "description": "Environments for fatigue analysis",
            "latitude": 49.76047415657464,
            "longitude": -2.000272812034975,
            "hs_dp_proba": [
                0.0
            ],
            "intervals_hs_min": [
                0.0
            ],
            "intervals_hs_max": [
                1.0
            ],
            "intervals_dp_min": [
                270.0
            ],
            "intervals_dp_max": [
                300.0
            ],
            "associated_tp": [
                10.039157656194618
            ],
            "associated_cur": [
                1.356811216240315
            ],
            "associated_dircur": [
                227.42927319828564
            ],
            "associated_wind": [
                5.042167287364836
            ],
            "associated_dirwind": [
                256.95436280208645
            ]
        }
    },
    "currents": {
        "EXT": {
            "mag": {
                "return_periods": [
                    1.0
                ],
                "return_values": [
                    3.3
                ]
            }
        }
    },
    "winds": {
        "EXT": {
            "gust10": {
                "return_periods": [
                    1.0
                ],
                "return_values": [
                    30.43
                ]
            }
        }
    }
}

pact = Consumer("sk").has_pact_with(Provider("sc"), port=1239)
pact.start_service()
atexit.register(pact.stop_service)

def test_sc_point_statistics():
    pact.given(
        "sc 1 exists and it has statistics"
    ).upon_receiving(
        "a request for sc"
    ).with_request(
        "GET", Term(r'/sc/\d+/point/statistics', '/sc/1/point/statistics')
    ).will_respond_with(200, body=Like(SC_STATISTICS))

    with pact:
        uri = 'http://localhost:1239/sc/1/point/statistics'
        result = requests.get(uri)
        assert result.status_code == 200

SC_FARM = {
    "info": {
        "zonenumber": 2,
        "zoneletter": "A"
    },
    "direct_values": {
        "bathymetry": {
            "latitude": [
                49.70429789742758
            ],
            "longitude": [
                -2.056850330175936
            ],
            "value": [
                50.0
            ]
        }
    }
}

def test_sc_farm():
    pact.given(
        "sc 1 exists and it has farm"
    ).upon_receiving(
        "a request for sc"
    ).with_request(
        "GET", Term(r'/sc/\d+/farm', '/sc/1/farm')
    ).will_respond_with(200, body=Like(SC_FARM))

    with pact:
        uri = 'http://localhost:1239/sc/1/farm'
        result = requests.get(uri)
        assert result.status_code == 200
