artifacts: stationkeep
	docker container rm --force dtop-sk-container 2>/dev/null || exit 0
	docker run --name=dtop-sk-container --interactive dtop-sk-image ./artifacts.sh
	docker cp dtop-sk-container:/app/dredd-hooks-output.txt _build/dredd-hooks-output.txt
	docker cp dtop-sk-container:/app/report.xml _build/report.xml
	docker cp dtop-sk-container:/app/dist/. _build/
	docker container rm --force dtop-sk-container

stationkeep:
	docker build --rm --tag dtop-sk-image .

develop: stationkeep
	docker run --rm --interactive --tty --volume $(PWD):/app --name dev dtop-sk-image bash

impose:
	make -f ci.makefile stationkeep-deploy
	docker-compose run --rm contracts

impose-local:
	make -f ci.makefile stationkeep-deploy
	docker-compose run --rm contracts

verify:
	make -f ci.makefile stationkeep-deploy
	docker-compose --project-name sk -f verifiable.docker-compose -f verifier.docker-compose run verifier
	docker-compose --project-name sk -f verifiable.docker-compose stop sk
	docker cp `docker-compose --project-name sk -f verifiable.docker-compose ps -q`:/app/.coverage.make-verify
run:
	docker-compose --f docker-compose-test.yml -f docker-compose.yml up nginx dtop sc mc ec ed cm
