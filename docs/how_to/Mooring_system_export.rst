.. _sk-Mooring-system-export:

How to export a mooring system
==============================

The attempt of this how-to guide is to describe how to export a mooring system file in order to use it in an other Station Keeping entity.

1. Define the Line types, Node properties, Line properties and Solver Options (if needed) of the mooring system.

2. Click on the "Export Mooring System" button. The Mooring System file will then be automatically downloaded and named with the name of the entity and saved with the extension ".ms".

3. This file could then be used in an entity to load the fully defined Mooring System by using the button "Load" located next to the "Define" button. Once click on this button, select the downloaded file. Note that if no name where set in the initial definition of the mooring system, the name will be set to "none". This name and all fields of the mooring system can be edited after if needed.