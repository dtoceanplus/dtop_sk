.. _sk-Nemoh-files:

How to use Nemoh files in SK module
===================================

Requirements for Nemoh folder for SK module
-------------------------------------------

The attempt of this how-to guide is to describe the folder format of the results that could be imported from Nemoh. Note that this feature is only available in standalone mode. In integrated mode, all hydrodynamic data would be automatically filled by the outputs of the Machine Characterisation module.

To load results from Nemoh make sur that the folder hierarchy is as follow :

├── mesh
    └── KH.dat

    └── Hydrostatics.dat

    └── Inertia_hull.dat

    └── mesh.tec

    └── mesh_info.dat

    └── mesh.dat

    └── Description_Full.tec
    
├── results
    └── ExcitationForce.tec

    └── RadiationCoefficients.tec
├── input.txt

├── Nemoh.cal

├── mesh.png

Note that the image mesh.png is optional and would be displayed in the Graphical User Interface to make the import more clear. The folder could contains other files but these files would not be taken into account.

Export Nemoh files
------------------

This how-to guide explain how export the Nemoh files of the default databases. Note that this feature is only available in standalone mode. In integrated mode, all hydrodynamic data would be automatically filled by the outputs of the Machine Characterisation module.

1. Create an entity using the Main Module.

2. In the Device Properties page, make sure that the 'Positioning type' located in the 'Type of machine' section is set to 'moored'.

3. In the 'Floating structure hydrodynamics' section, select the Nemoh device to use. Here we consider the 'RM3_6dofs' machine. An image should be displayed to show the corresponding mesh.

4. Once this database is selected, click on the 'Export inputs' button located on the top right of the page. The operation will take about 5 seconds and a file is downloaded with the name '{entityName}_{EntityId}.dtosk'. This file is basically an archive which contains all inputs files including a Nemoh folder that contains all Nemoh files that Station Keeping module require. The use of this file is described in the :ref:`How to Export/Import Station Keeping inputs <sk-export_import>`.