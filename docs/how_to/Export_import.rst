.. _sk-export_import:

How to Export/Import Station Keeping inputs
===========================================

The attempt of this how-to guide is to describe how export the inputs of a Station Keeping entity. Note that this feature only export the inputs of the entity and do not consider the outputs.

1. Create an entity using the Main Module.

2. Fill the different input pages.

3. Click on the 'Export inputs'. The operation will take about 5 seconds and a file is downloaded with the name '{entityName}_{EntityId}.dtosk'. This file is basically an archive which contains all inputs. Note that the entity is automatically saved when you click on this button.

4. Click on 'Import Inputs' to import this file or another that was downloaded this way.