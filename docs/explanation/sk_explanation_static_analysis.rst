.. _sk-static_analysis:

Static Analysis
***************

Objectives
==========

•	Compute static loads and reaction forces applying on the considered device from the following sources: wind, current, mooring system, device buoyancy and weight (hydrostatic restoring forces), mean wave drift forces 

•	Compute position of the device that corresponds to static equilibrium

Inputs
======

.. csv-table:: Inputs used to perform a static analysis.
    :header: "Inputs description", "Origin of the Data", "Data Model in SK", "Units"
    :widths: 30, 30, 30, 30

    "Hs – significant wave height",  	"SC", 	"float", 	"[m]" 
    "Tp – wave peak period", 	"SC", 	"float", 	"[s]" 
    "Dir – wave direction", 	"SC", 	"float", 	"[deg]" 
    "Vw – wind velocity",  	"SC", 	"float", 	"[m/s]" 
    "Dirw – wind direction", 	"SC", 	"float", 	"[deg]" 
    "Vc– current velocity",  	"SC", 	"float", 	"[m/s]" 
    "Dirc– current direction", 	"SC", 	"float", 	"[deg]" 
    "Pos_type – positioning type", 	"User/MC", 	"string", 	"‘moored’ or ‘fixed’" 
    "P_dry - Device profile, dry part", 	"User/MC", 	"string", 	"‘cylinder’ or ‘rectangle’" 
    "H_dry - Device height, dry part", 	"User/MC", 	"float", 	"[m]" 
    "B_dry - Device width, dry part", 	"User/MC", 	"float", 	"[m]" 
    "P_wet - Device profile, wet part", 	"User/MC", 	"string", 	"‘cylinder’ or ‘rectangle’" 
    "H_wet - Device height, wet part", 	"User/MC", 	"float", 	"[m]" 
    "B_wet - Device width, wet part", 	"User/MC", 	"float", 	"[m]" 
    "M – Device mass", 	"User/MC", 	"float", 	"[kg]" 
    "cog – Center of gravity position of the device", 	"User/MC", 	"float", 	[m]" 
    "K - Hydrostatic restoring matrix", 	"User/MC", 	"float", 	"[N/m], [N.m/m] , [N/rad] , [N.m/rad]" 
    "Mooring system model", 	"User/Computed", 	"MAP++ model", 	"Only required if Pos_type is ‘moored’" 
    "Dr – rotor diameter", 	"User/MC", 	"float", 	"Optional [m]"
    "Ph – hub position",	"User/MC",	"float",	"Optional [m]"
    "Ct – rotor thrust coefficients",	"User/MC", 	"float array",	"Optional" 
..

Methods and Outputs: steady and reaction forces
===============================================

The steady and reaction forces are computed using the inputs given in the :ref:`Input table <sk-table1>` The models used for the calculation of the wind force, current force, mean wave drift force, mooring system force and buoyancy and weight forces are detailed in the following Sections. We consider the device to be a 6-dofs rigid body (3 translations and 3 rotations). 

1- Wind force model
-------------------
The wind force where :math:`F_w` is computed as:

.. math::
    :label: eq1

    F_w = \frac{1}{2} ϱ_a C_w V_w^2 A_{p_{dry}}
    
where :math:`ϱ_a` is the air specific mass, :math:`C_w` are the wind force coefficients and :math:`A_{p_{dry}}` the projected area.

:math:`C_w`  are the wind force coefficients computed from the main dimensions :math:`H_{dry}` and :math:`B_{dry}`, using the shape coefficients for a cylinder if :math:`P_{dry}` is ‘cylinder’ and for a rectangle if :math:`P_{dry}` is ‘rectangle’. 
Default values of the shape coefficients of a cylinder are taken from [SKRef1]_, Figure 6.6. Default values of the shape coefficients of a rectangular box are taken from [SKRef1]_, Table 5.5, where we assume that the length and the width of the box are equal to :math:`B_{dry}`. 

:math:`A_{p_{dry}}` is computed from the main dimensions :math:`H_{dry}` and :math:`B_{dry}`. 

The wind force is then projected on the local axes of the device. The obtained force components depend on the wind direction and the device orientation. 

2- Current force model
----------------------
The current force where :math:`F_c` is computed as:

.. math::
    :label: eq2

    F_c = \frac{1}{2} ϱ C_c V_c^2 A_{p_{wet}}

where :math:`ϱ` is the water specific mass :math:`C_c` are the current force coefficients and :math:`A_{p_{wet}}` the projected area.

:math:`C_c`  are the wind force coefficients computed from the main dimensions :math:`H_{wet}` and :math:`B_{wet}`, using the shape coefficients for a cylinder if :math:`P_{wet}` is ‘cylinder’ and for a rectangle if :math:`P_{wet}` is ‘rectangle’. 
Default values of the shape coefficients of a cylinder are taken from [SKRef1]_, Figure 6.6. Default values of the shape coefficients of a rectangular box are taken from [SKRef2]_. 

The current force is then projected on the local axes of the device. The obtained force components depend on the current direction and the device orientation. 

3- Rotor force model
--------------------
In case of a tidal energy converter, the user has the possibility to model a rotor. In that case, the rotor force applied on the device is calculated as follows:

.. math::
    :label: eq3

    F_{rotor} = \frac{1}{2} ϱ C_t V_c^2 A_{rotor}

where :math:`A_{rotor} = \frac{π D_r^2}{4}` is the rotor area and :math:`C_t` the rotor thrust coefficients. The current force is then projected on the local axes of the device. 
The obtained force components depend on the current direction. It is assumed that the rotor always faces the current.

4- Mean wave drift force
------------------------
The following table summarizes the approach used for the calculation of the mean wave drift force :math:`F_{wa}` . The selected approach depends on the device profile type (cylinder or rectangular), and if the device is fixed or floating. In all cases, the mean wave drift force is a function of the device dimensions (:math:`B_{wet}`, :math:`H_{wet}` and :math:`P_{wet}`). The mean wave drift force is computed for the following regular wave: 
	
•	A period T equal to Tp;

•	A wave height H equal to Hmax, where Hmax is the maximum wave height as computed in [SKRef1]_, Section 3.5.11. 

.. csv-table:: Models for mean wave drift calculation.
   :header: "Device type", 	"Device profile", 	"Approach"
   :widths: 50, 50, 50

    "Floating", 	"cylinder", 	"[SKRef3]_"
    "Fixed", 	"cylinder", 	"[SKRef4]_"
    "Floating or fixed", 	"rectangular", 	"[SKRef5]_" 
..

For a floating cylinder, the mean wave drift force F_wa is given by (see details in [SKRef3]_):

.. math::
    :label: eq4

    F_{wa}=\frac{5}{16} ρgA^2 rπ^2 k^3 r^3


where :math:`A=\frac{H}{2}` is the wave amplitude, r is the cylinder radius, ρ is the water density and k is the wave number.
For a fixed cylinder, the mean wave drift force F_wa is given by (see details in [SKRef4]_):

.. math::
    :label: eq5

    F_{wa}=\frac{5}{16} ρgA^2 rπ^2 k^3 r^3 \frac{1+2kh}{sinh⁡(2kh)}


where :math:`A=\frac{H}{2}` is the wave amplitude, r is the cylinder radius, ρ is the water density, k is the wave number and h is the water depth.
For a floating or fixed rectangular box, the mean wave drift force F_wa is given by (see details in [SKRef5]_):


.. math::
    :label: eq6

    F_{wa}=\frac{1}{2} ρgA^2 C_r^2 B_{wet}


where :math:`A=\frac{H}{2}` is the wave amplitude, ρ is the water density, :math:`B_{wet}` is the width of the wet part of the device, and :math:`C_r` is the reflection coefficient corresponding to the wave period T. The reflection coefficients documented in [SKRef5]_ are used by default; they corresponds to a barge with a draft/water depth ratio of 0.175. The figure below shows them as function of the wave frequency.

The mean wave drift force is then projected on the local axes of the device. The obtained force components depend on the wave direction and the device orientation. For a device that is not surface-piercing, i.e. that is totally submerged, the mean wave drift force is zero.

.. figure:: ../images/fig-2-4.png
    :width: 500px
    :align: center
    :height: 400px
    :figclass: align-center

    Default reflexion coefficients for a rectangular box.
..


5- Mooring system force
-----------------------
The mooring system force :math:`F_{moor}` that applies on the device is computed using the MAP++ library developed by NREL [SKRef6]_. The modelling capabilities and necessary inputs are detailed in the section :ref:`Mooring System Design <sk-mooring_system_design>`.
For a given position of the device, the tension in each line segment is computed based on a quasi-static approach by using: 

•	Catenary equations

•	Linear spring equation for taut lines 
 
The mooring system force :math:`F_{moor}` is a function of the device position and orientation. It is the sum of all the forces from the lines applying at the fairleads of the device.
More details about the modelling possibilities in MAP++ library can be found on the dedicated website: https://map-plus-plus.readthedocs.io/en/latest/input_file.html . 

6- Weight and buoyancy force
----------------------------
The weight and buoyancy forces are represented by the hydrostatic matrix K. Given a reference position :math:`x_0`, the hydrostatic force of a device at position :math:`x` is given by: 

.. math::
    :label: eq7

    F_{res} (x)=-K(x-x_0)

Methods and Outputs: equilibrium calculation
============================================
The static equilibrium calculation consists in finding the equilibrium position x_eq of the device so that, for a given environmental condition, the sum of the static forces that applies on the device at that position is equal to zero: 

.. math::
    :label: eq8

    F_w (x_{eq})+F_c (x_{eq})+F_{rotor} (x_{eq} ) + F_{wa} (x_{eq})+F_{moor} (x_{eq})+F_{res} (x_{eq}) = 0


This problem is solved with a gradient descend optimisation algorithm, using the function ‘minimize’ of the scipy.optimize python library. 
The SK module also include the possibility to model a ‘master structure’. A master structure is a rigid body moored to the seabed. The devices can be moored to a master structure. The mooring system of a master structure is to be user-defined similarly to a device mooring system, as specified in the section :ref:`Mooring System Design <sk-mooring_system_design>`. The following figure shows 4 devices (small vertical cylinders) moored to a master structure (black rectangle). When Static Analysis has been run independently for all the devices, the forces that the mooring lines of the devices apply on the master structure are summed and static equilibrium calculation is performed for the master structure (with the devices modelled by forces at that point). The problem is thus decoupled in two parts: 

•	Perform Dynamic Analysis for each device, considering the master structure as fixed

•	Perform Static Analysis for the master structure, using the sum of mooring forces from each device as external forces applied on the master structure. Static and maximum of the dynamic forces are applied. Dynamic forces are applied with a phase lag due to the distance between the devices. The phase lag is approximated to the phase lag of a wave of period Tp, for the considered wave spectrum of peak period Tp.

.. figure:: ../images/fig-2-5.png
    :width: 600px
    :align: center
    :height: 400px
    :figclass: align-center

    Example of devices moored to a master structure.
..

