.. _sk-foundation_design:

Foundation Design
*****************

Objectives
==========

The foundation design process of the SK module aims to:

•	Step 1: Attribute soil mechanical parameters to the type of soil define by the user. 

•	Step 2: determined the most suitable type of foundation depending on seabed conditions and the type of structure;

•	Step 2: Performed the dimensioning of the type of foundation based on the foundations suitability study;

•	Step 3: Determine the dimensions, weight and capacity of the foundation considering its environments (loads and soil conditions).


The following foundation/anchor types are available: shallow foundations, gravity anchors, piles anchors/foundations, drag anchors and suction caisson

In order to achieve the foundation design, soil mechanical parameters, determined on :ref:`sk-soil_properties` are affected by a safety coefficient as is proposed in [SKRef16]_.

.. math:: 𝑐_{𝑢𝑑} = \frac{𝑐_𝑢}{𝛾_𝑐} ; 𝜑′_𝑑 = arctan ( \frac{tan𝜑′}{𝛾_𝜑} )
    
Where,

-	:math:`𝑐_𝑢`: undrained shear strength in Pa;

-	:math:`𝑐_{𝑢𝑑}`: design undrained shear strength in Pa;

-	:math:`𝜑′`: effective internal friction angle of soil in degrees;

-	:math:`𝜑′_𝑑`: design effective internal friction angle of soil in degrees and;

-	:math:`𝛾_𝑐` and :math:`𝛾_𝜑`) are safety factors equal to 1.3 [SKRef16]_. This value can be change by the user for CPL3.

Foundations/anchors designs are performed by using the design undrained shear strength and design effective internal friction angle of soil.

Inputs
======

All inputs needed to perform the foundation design are regrouped in Table 1

.. csv-table:: Table 1 : Inputs for foundation/anchor design.
    :header: "Inputs description", "Origin of the Data", "Data model in SK", "Units"
    :widths: 30, 30, 30, 30

    "Level of complexity", "User/SG", "integer", "[-]"
    "Soil type", "SC", "String", "
    | 'very_soft_clay' or
    | 'soft_clay' or
    | 'firm_clay' or
    | 'stiff_clay' or
    | 'very_stiff_clay' or
    | 'hard_clay' or
    | 'very_loose_sand' or
    | 'loose_sand' or
    | 'medium_dense_sand'
    | or 'dense_sand' or
    | 'very_dense_sand' or
    | 'gravels_pebbles' or
    | 'custom'"
    "Safety factor", "User or default", "float", "[-]"
    "Soil slope", "User/SC", "float", "[°]"
    "Seabed connexion type", "EC", "string", "'moored' or 'fixed'"
    "Device geometry", "MC", "dic", "
    | 'Geometry': 'cylider',
    | 'rectangular' or
    | 'contact_points',
    | 'lr' or 'lx': float,
    | 'ly': float
    | 'r1', 'r2', 'r3': float,
    | ':math:`alpha_{01}`', ':math:`alpha_{12}`',
    | ':math:`alpha_{23}`': float [°]"
    "Internal friction angle of soil", "
    | Catalogue / Pandas
    | on stand-alone mode", "float", "[deg]"
    "Undrained shear strength", "
    | Catalogue / Pandas
    | on stand-alone mode", "float", "[Pa]"
    "Relative density index", "
    | Catalogue / Pandas
    | on stand-alone mode", "float", "[-]"
    "Buoyant weight of soil", "
    | Catalogue / Pandas
    | on stand-alone mode", "float", "[Kg/m^3]"
    "Type of pile tip", "User / default data", "string", "
    | 'closed_end' or
    | 'open_end'"
    "Deflection criteria", "User / default data", "float", "[%]"
..

Methods and Outputs : Shallow foundations and gravity anchors
=============================================================

In this module shallow foundations are gravity foundations for fixed devices and deadweight anchors
are gravity anchors for floating devices.

.. image:: ../images/fig-2-8.png
    :align: center

.. centered:: Figure 1 : a), b), and d) shallow foundations [SKRef13]_, c) gravity anchor [SKRef14]_

Figure 1 shows circular, rectangular and contact points shallow foundations, the loading on this type of foundation may include overturning moments, which create uplift (tensile) as well as downwards (compressive) pressures, lateral and gravitational loadings [SKRef13]_.

The type of loading will determine the resistance mechanism developed by the soil-structure interaction:

•	For compressive loads (downward), resistance is derived from the bearing capacity of the soil.

•	For tensile loads (upward), resistance depends on the submerged weight of the foundation. 

•	For lateral loads, resistance depends on the soil-structure friction developed on the surfaces.

Figure 1 also shows a gravity anchor, this is a heavy object placed on the seafloor. The primary purpose is to resist the uplift and lateral forces from the mooring line. Its behaviour is practically the same as the behavior of a shallow foundation subjected to an uplift load. 

The SK module propose to design circular and rectangular mattresses or contact points shallow foundations (the geometry is chosen depending on the device geometry), as well as circular gravity anchors. They are considered to be in concrete (density = 2400 :math:`kg/m^3`).

For circular and rectangular geometries, their minimal dimensions are established as a function of the load eccentricity (eccentricity calculation is explained below). As a general guide it is recommended that the eccentricity be less than or equal to 1/6 the foundation width/length/radius [SKRef13]_, in order to avoid extremely eccentric loads. The minimal foundation/anchor dimensions for gravity foundations with fixed devices (length :math:`l_x`, width :math:`l_y` and radius :math:`r`) when there is an eccentric load case will be:

-   For rectangular geometry:

.. math:: l_{x,min}=max⁡(6*e_x,device~~l_x)

.. math:: l_{y,min}=max⁡(6*e_y,device~~l_y)

-   For circular geometry:

.. math:: r_{min}=max(6*e_r,device~~r)

The initial radius for gravity anchors depends on the eccentricity due to the overturning moment generated by the horizontal load and the thickness of the anchor:

.. math:: M_h=thickness*\sqrt{H_x^2+H_y^2}

.. math:: e_r=\frac{M_h}{Weight}

.. math:: 2*radius>6*e_r

The minimal/initial value of the foundations thickness is 0.3 m. Dimensions will be adjusted during the foundation’s verification.

For fixed devices, when there is no eccentricity (:math:`M_{x,uls}=0` and :math:`M_{y,uls}=0`) minimal foundation dimension is determined:

-   For rectangular geometry:

.. math:: l_{x,min}=max⁡(1,device~~l_x)

.. math:: l_{y,min}=max⁡(1,device~~l_y )

-   For circular geometry

.. math:: r_min=max⁡ (1,device~~r)

For contact points geometry, the inputs defined by the user are:

.. image:: ../images/fig-2-8-2.png
    :align: center
    :width: 400px

.. centered:: Figure 2 : Contact points geometry schema

The initial value of the contact points foundations is a homogeneous radius of 2.0 m (r1=r2=r3), and a 120° angle between each leg of the foundation (:math:`α_{01}=0°`, :math:`α_{12}=α_{23}=120°`). Dimensions will be adjusted during the foundation’s verification. The tools DTOcean+ also allows the user to fix the radius and angles dimensions to perform a design assessment on those given values.

In order to perform the design of shallow foundations and gravity anchors three conditions are verified:

1) Bearing capacity :
~~~~~~~~~~~~~~~~~~~~~

The bearing capacity Q_u is the normal resistance of the soil. The following condition is verified:

.. math:: Q_u>F_{n,bc}

.. math:: F_{n,bc}=((V_{uls,up} + W_b )*cosβ)-(H_{uls,res}*sin⁡β)

where,

-   :math:`V_{uls,up}` is the vertical (upwards) load from ULS (:ref:`link <sk-uls_analysis>`) load analysis (N);

-   :math:`V_{uls,down}` is the vertical (downwards) load from ULS (:ref:`link <sk-uls_analysis>`) load analysis (N). Its value is equal to 0 for anchor design;

-   :math:`H_{uls,res}` is the resultant horizontal load from ULS (N). :math:`H_{uls,res}= \sqrt{H_{uls,x}^2+H_{uls,y}^2}`;

-   :math:`W_b` is the foundation’s buoyant weight (N);

-   β correspond to the slope of the seabed (degrees).

In the methodology implemented it is considered that the embedment depth of the foundation is equal to zero, it means that there no bearing resistance given by overburden of the soil. In order to determine :math:`Q_u` the following equation is used, it takes into account the soil resistance due to cohesion, for cohesive soils, and friction for cohesionless type of soils.

.. math:: Q_u=A'q_c~~~~or~~~~Q_u=A'q_γ

where,

-  	:math:`q_c`: bearing capacity stress for cohesion factor (Pa);

-  	:math:`q_γ`: bearing capacity stress for friction (Pa);

-  	A': effective surface of the foundation (m2) equal to the foundation surface when overturning moment from ULS analysis is equal to 0, otherwise the eccentricity methodology is used.

**Effective surface A'**

To perform the design when overturning moments are > 0, the overturning moment is replaced by an eccentricity so the effective surface A^' can be calculated. The load center, is the point where the resultant of horizontal and vertical load is applied, it implies an eccentricity on x and y axis for rectangular design (Figure 3a) and on the radius axes for circular design (Figure 3b):

.. math:: e_x= \frac{Overturning~~moment}{Vertical~~load}=\frac{M_{x,uls}}{V_{uls,down}+W_b};

.. math:: e_y=\frac{M_{y,uls}}{V_{uls,down}+W_b};

.. math:: e_r=\frac{\sqrt{M_{x,uls}^2+M_{y,uls}^2}}{V_{uls,down}+W_b}

.. image:: ../images/fig-2-9.png
    :align: center

.. centered:: Figure 3 : Effective area and eccentricity for a) rectangular and b) circular design [SKRef17]_

When there is an eccentric load the method of the effective base dimension is considered. For rectangular shape, the effective length (L’), width (B’) and the area are calculated using the following expressions [SKRef13]_ :

.. math:: L' = max⁡(l_x-2e_x~~;~~l_y-2e_y );

.. math:: B' = min⁡(l_x-2e_x~~;~~l_y-2e_y );

.. math:: A'=B'∙L'

For circular foundation/anchor the effective depends on the radius r and :math:`e_r` [SKRef16]_ :

.. math:: A' = 2 [ r^2 arccos \frac{e_r}{r} - e_r \sqrt{r^2-e_r^2}]

This is recognized as the area of a circle segment and its mirrored imaged with the midpoint of their common secant located in the load application point (Figure 3). The width :math:`b_e` and length :math:`l_e` of this double segment area is :

.. math:: b_e = 2(r-e)

.. math:: l_e = 2r \sqrt{1-(1- \frac{B'}{2r})^2}

Based on this, the effective foundation area A' can now be represented by a rectangle with the following dimensions :

.. math:: L'= \sqrt{A' \frac{l_e}{b_e}}~~;~~B'=\frac{L'}{l_e} b_e

For contact points shallow foundations, the bearing acceptability criteria is assumed to be always true because the contact points geometry is associated to rocky seabed. Indeed, it seems relevant to consider that a failure mode on bearing capacity does not occur for rocky soil occur due to the nature and physical properties of this latter.

**Bearing capacity stress for cohesion** :math:`q_c`

The bearing capacity stress is determined using formulations from [SKRef12]_ and [SKRef15]_ :

.. math:: q_c= c_{ud} N_c K_c

Where :math:`N_c` is the bearing capacity factor and :math:`K_c` is a correction factors that takes into account load inclination and the impact of the foundation shape on the failure case of the foundation, :math:`K_c=i_c s_c`. They are calculated with the following formulations, parameters are presented on Table 2 :

-   :math:`N_c=\frac{N_q-1}{tan⁡ \varphi_d}~~~~` for :math:`~~~~\varphi_d ≠0~~~~` and :math:`~~~~N_c=2+π~~~~` for :math:`~~~~\varphi_d=0`

-   :math:`i_c=0.5+ (0.5*\sqrt{1-(\frac{H_{uls,res}}{A'*c_{ud} })})`

-   :math:`s_c=1+(\frac{B'}{L'}~~\frac{N_q}{N_c})`

.. csv-table:: Table 2 : Parameters description for :math:`q_c` calculation
    :header: "Symbol", "Description", "Units"
    :widths: 30, 60, 30

    ":math:`N_c`", "Bearing capacity factor", "[-]"
    ":math:`K_c`", "Correction factor", "[-]"
    ":math:`i_c`", "Inclination factor", "[-]"
    ":math:`s_c`", "Shape factor", "[-]"
..

**Bearing capacity stress for friction** :math:`q_γ`

The bearing capacity stress is equal to:

.. math:: q_γ=γ_b (B'/2) N_γ K_γ f_z

where,

-   :math:`N_γ=\frac{3}{2} (N_q-1) tan⁡ \varphi_d~~~~` and :math:`~~~~N_q=e^{(π tan⁡ \varphi_d) \frac{(1+sin⁡ \varphi_d )}{(1-sin⁡ \varphi_d)}}`

-   :math:`i_γ=(1-\frac{H_{uls,res}}{(V_{uls,down}+W_b)})^4`

-   :math:`s_γ=1-0.4(\frac{B'}{L'})`

Where :math:`N_γ` and :math:`N_q` are the bearing capacity factor, :math:`f_z` is the depth attenuation factor = 1 and :math:`K_γ` is a correction factors that takes into account load inclination and the impact of the foundation shape on the failure case of the foundation, :math:`K_γ=i_γ s_γ`.

2) Sliding resistance :
~~~~~~~~~~~~~~~~~~~~~~~

To verify the sliding resistance the lateral resistance of the foundation should be bigger than the maximal horizontal load that can be applied:

.. math:: H_u>F_{h,sr}

.. math:: F_{h,sr} = (V_{uls,up}+ V_{uls,down}+W_b) sin β +(H_{uls,res} cos⁡β)

In order to determine the lateral load capacity of the foundation H_u, formulation is proposed on the literature and standards, they give different expressions for drained and undrained [SKRef15]_.

For drained conditions the cohesion of the soil can be very small, so it will be neglected. Lateral resistance is calculating as function of the maximal downward load from ULS analysis :math:`(V_{uls,down})` :

.. math:: H_u= F_{n,sr} tan(\varphi_d)

.. math:: F_{n,sr} = (V_{uls,up} + W_b) cos β - (H_{uls,res} sin ⁡β)

For undrained conditions (cohesive soil) :

.. math:: H_u=A' c_{ud}

In case of contact points shallow foundations, the sliding resistance is assessed considering the same formulation as for cohesionless soils.

3) Overturning moment :
~~~~~~~~~~~~~~~~~~~~~~~

The overturning moment condition is verified in each axe (formulation for x axis is presented below). The stability moment of the foundation/anchor should be bigger than the overturning moment applied by the environmental loads :

.. math:: \frac{(V_{uls,down} + W_b) cos ⁡β * \frac{l_x}{2}}{SF} > M_{uls,x} + (H_{uls,res} * z_{ap})

-   :math:`M_{uls,x}` is the maximal overturning moment on the x axis from ULS analysis (Nm);

-   :math:`z_{ap}` is the vertical distance between de seabed and the application point of maximal horizontal load (m);

-   :math:`l_x` is the foundation’s length on the x axis (m).

The final design of the foundation/anchor is achieved when the three conditions presented are achieved, Figure 4 presents a diagram of the design procedure. Outputs are summarized on the following table.

.. csv-table:: Table 3 : Outputs for shallow foundations and gravity anchor
    :header: "Outputs", "Description", "Units"
    :widths: 30, 60, 30

    "Geometry", "
    | Length on the x and y axis for rectangular foundations
    | Diameter for circular geometry
    | Radius and angles for contact points geometry", "[m]"
    "Bearing capacity", "Axial capacity of the foundation", "[N]"
    "Sliding resistance", "Lateral capacity of the foundation", "[N]"
    "Stability moment", "
    | Stability moment on the x and y axis for rectangular and contact points geometry
    | Stability moment on radial axis for circular geometry", "[Nm]"
    "Weight", "Weight of the foundation", "[kg]"
    "Fooprint", "Footprint surface on the soil", "[m²]"
..

.. image:: ../images/fig-2-10.png
    :align: center

.. centered:: Figure 4 : Design procedure for shallow foundations and gravity anchors

Methods and Outputs : Pile foundations / anchors
================================================

The loading on this type of foundation will be the combination of structure weight and environmental loading: overturning moments, uplift (tensile) as well as downwards (compressive) pressures and lateral loadings. The SK module is able to perform the design for open-end and closed-end piles, the type of pile is defined by the user, in case there no input data the default value is “open_end”.

To perform the dimensioning of this type of solution for foundations and anchoring the methodology describe in [SKRef13]_ was implemented without any special hypothesis to simplify it.

In order to perform the design of a steel piles the following conditions are verified:

1) Lateral capacity :
~~~~~~~~~~~~~~~~~~~~~

The lateral resistance of the pile should be bigger than the horizontal load applied by external loads. It is calculated following the formulation from [SKRef13]_ :

.. math:: H_u > H_{uls,res}

.. centered:: :math:`H_u = \frac{y_{max} (EI)}{A_y T^3+aB_y T^2}~~~~` and :math:`~~~~ y_{max}=d_{crit}~~*~~D`

Where,

-   :math:`d_{crit}` corresponds to the deflection criteria in %. If there is no input data for this variable a value of 5% is adopted for fixed structures and 10% for moored structures;

-   :math:`D` is the pile diameter in (m);

-   :math:`y_max` is the maximal displacement (m);

-   :math:`E` is the Young modulus of steel equal to 200e+09 Pa;

-   :math:`I` inertia modulus (:math:`m^4`);

-   :math:`T` is the pile-soil stiffness in (m);

-   :math:`a` is the distance of the pile load attachment point above the seafloor surface in m, default value equal to 0;

-   :math:`A_y` and :math:`B_y` are deflections coefficients given by the figures in [SKRef13]_ Section 5.3. Each curve is implemented in the code as an exponential equation.

2) Axial capacity in tension :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The axial resistance of the pile in tension is given by the friction resistance. The following condition is verified :

.. math:: (A_s f_s) + W_b > V_{uls,up}

Where :

-   :math:`A_s` is the surface area of the pile below seafloor (m²);

-   :math:`f_s` is the average unit skin frictional resistance (N/m²). It is calculated using the methodology proposed by [SKRef13]_ for cohesive (:math:`c_{u,d} > 0~~` and :math:`~~\varphi'_d = 0`) and non-cohesive soil (:math:`c_{u,d} = 0~~` and :math:`~~\varphi'_d > 0`) soil. The maximal value for this parameter is given by [SKRef13]_ Section 5.3.

3) Axial capacity in compression
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The axial resistance of the pile in compression is given by the friction resistance of the shaft :math:`(A_s f_s)` plus the tip resistance :math:`(A_p q_p)` :

.. math:: \frac{(A_s f_s )+(A_p q_p)}{SF} > V_{uls,down} + W_b

Where :

-   :math:`A_p` is the area of the Section of the pile (m²);

-   :math:`q_p` is the unit soil bearing capacity of the pile (Pa). It is calculated using the methodology proposed by [SKRef13]_ for cohesive and non-cohesive soil.

4) Stress analysis of the Section :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The stress on the steel pile Section is verified following the methodology on [SKRef17]_, the allowable steel stress on the Section :

.. math:: \frac{σ_{max,steel}}{γ_s} > \frac{V_{uls,min}}{A_p} + \frac{M_{max,sec}}{S_p}

Where :

-   :math:`σ_{max,steel}` is the allowable stress of the pile equal to 250+06 Pa;

-   :math:`γ_s` safety factors equal to 1;

-   :math:`M_{max,sec}` is the maximal moment on the Section in (Nm);

-   :math:`S_p` is the Section modulus of the pile (:math:`m^3`).

Figure 5 show the loop of design to determine the dimensions of a pile foundation/anchor. Outputs when designing a pile are presented on Table 4.

.. image:: ../images/fig-2-11.png
    :align: center

.. centered:: Figure 5 : Design procedure for piles

.. csv-table:: Table 4 : Outputs for pile design
    :header: "Outputs", "Description", "Units"
    :widths: 30, 60, 30

    "Pile geometry", "
    | External diameter
    | Wall thickness
    | Length
    | Maximal deflection :math:`y_{max}`
    | Inertia moment
    | Stiffness", "
    | [m]
    | [m]
    | [m]
    | [m]
    | [:math:`m^4`]
    | [m]"
    "Lateral resistance", "
    | Lateral capacity
    | Lateral load for design", "
    | [N]
    | [N]"
    "Axia resistance", "
    | Skin friction in tension
    | Skin friction in compression
    | Tension capacity
    | Compression capacity
    | Tension load for design
    | Compression load for design", "
    | [N/m²]
    | [N/m²]
    | [N]
    | [N]
    | [N]
    | [N]"
    "Stress on the Section", "Maximal stress on the Section", "[Pa]"
    "Mass", "Mass of the foundation", "[kg]"
    "Footprint", "Footprint surface on the soil", "[m²]"
..

Methods and Outputs : Drag Anchors
==================================

Drag anchors are design to resist horizontal loads, they are currently used with catenary mooring lines. Different types existed on the market, each manufacturer develop its own catalogue and specification for each type. For permanent floating structures STEVIN anchors are currently used (Figure 6). The methodology developed on the SK module is based on industrials data and catalogues.

.. image:: ../images/fig-2-12.png
    :align: center
    :width: 400px

.. centered:: Figure 6 : Stevin drag anchors [SKRef9]_

**Drag anchor design :**

There is no standard geometry for drag anchors, each manufacturer has its precis design. The SK module proposed two types of anchors with different geometry. The most suitable type and size of anchor will be determine depending on the Ultimate Holding Capacity (UHC) of the anchor and the type of soil. Anchor type n°1 is considered to be more suitable for soft soils and n°2 for hard soils.

As the mooring lines are design through a quasi-static model, to guarantee a safety factor of 1.8 must be taken into account to determine the anchor design [SKRef13]_ :

.. math:: UHC ≥ H_{uls,res}*1.8

As suggested in [SKRef13]_, in order to determine the geometry and weight of the anchor needed the following power law is considered :

.. math:: UHC=m (W)^b

With :math:`W` equal to the anchors weight, :math:`m` and :math:`b` are parameters that depends on the soil type and anchor geometry. Three type of soils are considered :

-   Soft soil: friction angle and/or undrained shear strength inferior to 25° and 20 kPa;

-	Mid soil: sandy soils with a friction angle between 25° and 30° or/and undrained shear strength between 20 and 75 kPa

-	Hard soil: sandy soils with a friction angle superior to 30° or/and undrained shear strength between 20 and 75 kPa

.. csv-table:: Table 5 : Soil type definition for drag anchor design
    :header: "Type of soil", "Internal friction angle of soil [°]", "Undrained shear stength [kPa]"
    :widths: 20, 40, 40

    "Soft soil", ":math:`\varphi ≤ 25°`", ":math:`c_u ≤ 20`"
    "Mid soil", ":math:`25°<\varphi ≤ 30°`", ":math:`20<c_u ≤ 75`"
    "Hard soil", ":math:`\varphi > 30°`", ":math:`c_u > 75`"
..

The relationship between the UHC and the weight for both anchors is presented on Figures 7 and 8.

In order to determine the geometry of the anchor a power law between the weight and its dimension was establish applying a curve fitting to the information from the manufacture’s catalogues. The relationship between the weight and the dimensions A, B and EF are presented on Figures 9 and 10, bullets point on the graph corresponds to the data available on the catalogue.

.. image:: ../images/fig-2-13.png
    :align: center
    :width: 400px

.. centered:: Figure 7 : UHC vs Anchor weight for drag anchor n°1

.. image:: ../images/fig-2-14.png
    :align: center
    :width: 400px

.. centered:: Figure 8 : UHC vs Anchor weight for drag anchor n°2

.. image:: ../images/fig-2-15.png
    :align: center
    :width: 400px

.. centered:: Figure 9 : Dimensions for anchor type n°1

.. image:: ../images/fig-2-16.png
    :align: center
    :width: 400px

.. centered:: Figure 10 : Dimensions for anchor type n°2

Figure 11 summarize the steps to design a drag anchor. Inputs data for design are :math:`H_{uls,res}` and the Soil type. First Soil type is used to determine the more suitable anchor (n°1 or n°2) from catalogue (user can introduce a new type of anchor). Then, the UHC is calculated in order to be able to determine the anchors weight and size.

Figure 11 presents the loop design to determine the size and the weight of a drag anchor. Outputs given by the SK module for drag anchor design are presented on Table 6.

.. csv-table:: Table 6 : Outputs for drag anchor design
    :header: "Outputs", "Description", "Units"
    :widths: 40, 40, 20

    "Type of anchor from catalogue", "Anchor n°1, Anchor n°2", "[-]"
    "Weight", "Anchor weight", "[kg]"
    "UHC", "Ultimate Holding Capacity of the anchor", "[N]"
    "Anchor geometry", "
    | Length A
    | Length B
    | Length EF", "
    | [m]
    | [m]
    | [m]"
    "Mass", "Mass of the foundation", "[kg]"
    "Footprint", "Footprint surface on the soil", "[m²]"
..

.. image:: ../images/fig-2-17.png
    :align: center

.. centered:: Figure 11 : Procedure to design drag anchors

**Drag anchor catalogue :**

Drag anchor catalogue (Table 7) has the information needed to perform the design of the foundation. Multiplicative and exponential coefficients mx and bx are summarizes. This parameters describes the relation between the weight of the anchor and its ultimate capacity and dimensions (Figure 7,Figure 8, Figure 9, Figure 10).

The suitability of the anchor to the type of soil is define, a value of 1 to 3 is designated:

-   1 for high reliability

-   2 for medium reliability

-   3 for low reliability

.. centered:: Table 7 : Catalogue for drag anchors

.. csv-table:: Anchor UHC per soil type
    :header: "", "Anchor UHC in Soft Soil", " Anchor UHC in Soft Soil", "Anchor UHC in Medium Soil", "Anchor UHC in Medium Soil", "Anchor UHC in stiff clay and sand", "Anchor UHC in stiff clay and sand"

    "Name ID", "m_soft_soil", "b_soft_soil", "m_mid_soil", "b_mid_soil", "b_stiff_soil_sand", "b_stiff_soil_sand"
    "anchor_n_1", "569.27", "0.9377", "569.27", "0.9377", "779.05", "0.9286"
    "anchor_n_2", "784.77", "0.9002", "1040.2", "0.9082", "1238.8", "0.9101"
..

.. csv-table:: Reliability per anchors and soil type
    :header: "Name ID", "Rel_soft_soil", "Rel_mid_soil", "Rel_stiff_soil_sand"

    "anchor_n_1", "1", "3", "3"
    "anchor_n_2", "3", "1", "1"
..

.. csv-table:: Dimension parameters per anchors and soil type
    :header: "Name ID", "m_A", "b_A", "m_B", "b_B", "m_EF", "b_EF"

    "anchor_n_1", "0.2265", "0.336", "0.275", "0.3359", "0.164", "0.3359"
    "anchor_n_2", "0.258", "0.3333", "0.2781", "0.3334", "0.1551", "0.3334"
..

The user is able to add new types of anchors, however the full table must be filled. As an input information the user must specified a choice of anchor as “custom” in order to only take into account user anchors.

Methods and Outputs : Suction caisson anchors
=============================================

Suction caisson comprise large diameter cylinders, in a range of 3 to 8 m, open at the bottom, and generally with a length to diameter ratio L/D in the range 3 to 6 [SKRef18]_.

The axial holding capacity of a suction anchor depends on the nature of the soil, in particular, its permeability. Once suction in the caisson is induced, an accumulation of excess negative water pressures in the soil takes places, for cohesive soils with high permeability it gives an advantageous plug stability that can resist uplift due to the reverse bearing capacity (Figure 12a) [SKRef19]_. Conversely it is uncertain whether additional capacity based on the reverse end bearing should be considered for cohesionless soils, in [SKRef20]_ it is recommended not to consider the reverse bearing capacity when the permeability is less than :math:`10^{-5} [m⁄s]`.

.. image:: ../images/fig-2-18.png
    :align: center
    :width: 400px

.. centered:: Figure 12 : Iplift resistance of suction anchors : a) on cohesive soils; b) on cohesion soils [SKRef18]_

Once installed, the caisson acts like a short rigid pile. The maximum holding capacity is obtained if the chain is attached at a depth where the anchor failure mode is large translational displacement with minimal rotation [SKRef21]_ (Figure 13), the optimum load attachment point is generally at a depth between 0.6 and 0.7 of the length [SKRef14]_, [SKRef18]_ .

.. image:: ../images/fig-2-19.png
    :align: center
    :width: 400px

.. centered:: Figure 13 : Suction failure mode [SKRef18]_

In order to achieved the suction caisson design the SK module considers formulation available from several documents and the following hypothesis from [SKRef18]_:

-	Diameter range: from 3 to 8 m;

-	Ration between the diameter and the wall thickness:  100 to 250;

-	Ratio between length and diameter: from 3 to 6;

-	Optimum load attachment point: 0.7 of the length;

-	Horizontal failure occurs as pure translation.

1) Ultimate load capacity on cohesive soils :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The **uplift capacity** of suction caissons in cohesive soils is calculated as the sum of the reverse end bearing capacity, the external friction resistance between the caisson and the soil and the suction caisson’s weight [SKRef14]_, [SKRef18]_ :

.. math:: V_u=A_{cs} N_c s_{u,d} + α A_s + W_{sc}

.. centered:: :math:`N_c=6.2 (1+0.34 \arctan⁡(\frac{L}{D_{ext}}))~~~~` for :math:`~~~~\frac{L}{D_{ext}} ≤ 4.5`

.. centered:: :math:`N_c=9~~~~` for :math:`~~\frac{L}{D_{ext}} > 4.5`

Where :

-   :math:`A_{cs}` is the cross-Section of the caisson :math:`(\frac{π D^2_{ext}}{4}) (m²)`;

-   :math:`N_c` end bearing capacity factor;

-   :math:`α` adhesion factor (Pa); 

-   :math:`A_s` is the shaft surface of the caisson (:math:`π D_{ext} L`)  (m);

-   :math:`W_{sc}` is the caisson’s weight (N);

The first term of :math:`V_u 's` equation represents the contribution of the reverse end bearing capacity, its formulation is inspired by the Brinch-Hansen’s bearing capacity equation, this equation takes into account a shape factor, [SKRef23]_ suggest a value of 1.2 for it but, this factor is not taken into account in [SKRef6]_ and [SKRef12]_.

The second term represents the contribution of the friction resistance, as recommended on the literature the “α-method” is considered, the adhesion factor α varies between 0 (smooth) and 1 (rough). In their study, Andersen and Jostad [SKRef21]_ recommended a lower bound adhesion α=0.65.

To determine the lateral capacity the method developed by [SKRef24]_ is recommended and mention by [SKRef14]_, [SKRef18]_, [SKRef21]_ and [SKRef25]_. The maximum horizontal resistance is [SKRef18]_ :

.. math:: H_u=L D_{ext} N_p c_{u,d}

Where,

-   :math:`L` is the embedded length of caisson (m);

-   :math:`D_{ext}` is the external diameter of caisson (m);

-   :math:`c_{u,d}` is the design undrained shear strength (Pa).

The value of the lateral bearing capacity factor :math:`N_p` depends on the location of the pad-eye, a simplified equation for uniform soils is proposed by [SKRef26]_ :

.. math:: N_p = \frac{3.6}{\sqrt{(0.75- (\frac{z_{ap}}{L}))^2+(0.45 (\frac{z_{ap}}{L}))^2 }}

Where :math:`z_{ap}` is the application point of the load, the pad-eye depth (m).

2) Ultimate load capacity on non-cohesive soil :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The horizontal and vertical loading capacity of suction caissons in cohesionless soils is calculated using the formulation proposed in the literature [SKRef27]_ [SKRef22]_ [SKRef23]_.

The uplift resistance is calculated as the sum of the external and internal friction resistance between the caisson and the soil and the suction caisson’s weight. This is based on the work of [SKRef22]_ [SKRef23]_, there is no taken into account of the effect of the stress enhancement :

.. math:: V_u=W_b+ (\frac{π}{2} (D_{ext}+D_{int}) L^2 γ' K tan⁡ δ)

Where :

-	:math:`D_{int}` is the internal diameter (m);

-	:math:`K` is the average coefficient of earth pressure equal to :math:`1-sin⁡ \varphi_d'` [SKRef17]_ ;

-	:math:`δ` is the interface friction angle between the pile and the soil, :math:`δ= 0.5 \varphi_d'` [SKRef30]_

-	The lateral ultimate capacity is calculated as follow [SKRef27]_, [SKRef30]_ :

.. math:: H_u=1/2 D_{ext} N_q γ'L^2

Where the bearing capacity factor :math:`N_q=e^{π tan⁡ \varphi'_d}  tan^2⁡(45°+ \varphi_d'/2)` [SKRef17]_

3) Design varification :
~~~~~~~~~~~~~~~~~~~~~~~~

In order to achieve the design of the caisson, it is important to verify the general loading conditions of vertical and horizontal loading simultaneously. Inclined loads are applied at the pad-eye, the pure vertical and pure horizontal capacities will be reduced by the presence of loading in the orthogonal direction. Interaction between vertical and horizontal loading can be modelled by developing a failure envelope [SKRef18]_.

The shape of the failure envelopes has been modelled by an elliptical relationship [SKRef18]_ :

.. math:: (\frac{H}{H_u})^a+(\frac{V}{V_u})^b = 1

-   :math:`a=\frac{L}{D} + 0.5`

-   :math:`b=\frac{L}{3D}+4.5`

Where, :math:`H_u` and :math:`V_u` are the uniaxial horizontal and vertical capacities, respectively. To guarantee the stability of the anchor, the design load case should be inside the ellipse, this means :

.. math:: (\frac{H_d}{H_u})^a+(\frac{V_d}{V_u})^b < 1

4) Load transfer from mudline :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For suction caisson design the pad-eye is estimated at depth :math:`z_a= 0.7L` from the mudline, it means that a certain length of the chain is embedded (Figure 14). The design load on the anchor :math:`H_d` and :math:`V_d` are calculated by taking into account the load reduction due to soil bearing resistance against the mooring line :

.. math:: H_d=T_a  cos⁡ θ_a

.. math:: V_d= T_a  sin⁡ θ_a

.. image:: ../images/fig-2-20.png
    :align: center
    :width: 400px

.. centered:: Figure 14 : Loads on the anchor line [SKRef17]_

The load :math:`T_m` and the angle :math:`θ_m` at the mudline are obtained from ULS analysis of the mooring lines of the floating device, for anchors design a safety factor of 1.8 is applied to the loads values obtained [SKRef13]_ :

.. math:: T_m=1.8* sqrt{H_{uls,res}^2+V_{uls}^2}

.. math:: H_{uls,res}=sqrt{H_{uls,x}^2+H_{uls,y}^2}

.. math:: θ_m=arctan⁡ \frac{V_{uls}}{H_{uls}}

Where, :math:`H_{uls,res}` is the resultant horizontal load from ULS analysis and :math:`V_{uls}` is the vertical (upward) load.

To determine the load design :math:`T_a` and the angle :math:`θ_a` at the pad-eye the methodology explained in [SKRef14]_, [SKRef31]_ is used where :

.. math:: θ_a=sqrt{θ_m^2+\frac{2Qz_a}{T_a}}

.. math:: T_a=\frac{T_m}{e^{μ(θ_a-θ_m)}}

Where :math:`Q` is the vertical soil resistance of the soil (N) and :math:`μ` is the friction coefficient between the chain and the soil. Both parameters depend on the soil properties.

**Cohesive soils :**

The vertical soil resistance for **cohesive soils** is calculated with the following equations [SKRef27]_ :

.. math:: Q=E_n d_b N_c c_{ud}

Where :

-	:math:`E_n` is the normal area multiplier for chain equal to 2.5;

-	:math:`d_b` is the nominal diameter of the chain and actual diameter of the wire or rope [m];

-	:math:`N_c` is the bearing capacity factor equal to 11.5 [SKRef32]_;

For cohesive soils :math:`μ` is estimated between 0.4 and 0.6 [SKRef31]_. In order to perform the anchor design, the most conservative values are adopted: :math:`μ=0.4`.

**Cohesionless soil :**

The vertical soil resistance for non-cohesive soils is calculated using the same approach, but changing the bearing capacity factor :

.. math:: Q=E_n d_b N_q γ' z_a

Where :math:`γ'` is the buoyant unit weight of the soil (:math:`N/m^3`).

Different values for the bearing capacity factor :math:`N_q` can be found on the literature, values used for piles design suggested in [SKRef17]_ and [SKRef13]_ are taken into account and adapted to soil types propose by the module. For the user_preference (CL3) type of soil the value will be interpolated.

.. csv-table:: Table 8 : Bearing capacity factor for piles/suction in non-cohesive soil
    :header: "Soil Type", "Friction angle [°]", "Nq"

    "Very loose sand", "25", "5"
    "Loose sand", "30", "8"
    "Medium dense sand", "32", "12"
    "Dense sand", "35", "20"
    "Very dense sand", "38", "40"
    "Gravels, pebbles", "45", "50"
..

For non-cohesive soils it’s equal to the tangent of the interface friction angle δ between the chain and the soil ( :math:`δ=0.5 \varphi_d'~~` to :math:`~~0.7 \varphi_d'` [SKRef33]). In order to perform the anchor design, the most conservative values are adopted: :math:`tan⁡ 0.5 \varphi_d'`.

Figure 15 show the procedure to design a suction anchor, outputs are summarized on Table 9:

.. csv-table:: Table 9 : Outputs for suction caisson design
    :header: "Outputs", "Description", "Units"
    :widths: 30, 60, 30

    "Suction geometry", "
    | External diameter
    | Wall thickness
    | Length
    | Pad-eye depth", "
    | [m]
    | [m]
    | [m]
    | [m]"
    "Ultimate capacity", "
    | Lateral capacity
    | Uplift capacity
    | Design verification", "
    | [N]
    | [N]
    | [-]"
    "Mass", "Mass of the foundation", "[kg]"
    "Footprint", "Footprint surface on the soil", "[m²]"
..

.. image:: ../images/fig-2-21.png
    :align: center

.. centered:: Figure 15 : Design procedure for suction anchors