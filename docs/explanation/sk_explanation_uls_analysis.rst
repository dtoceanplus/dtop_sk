.. _sk-uls_analysis:

ULS Analysis
************

Ultimate Limit State analysis (ULS) is performed for:

•	the mooring system and anchors of a floating device, using a method based on DNVGL-OS-E301 [SKRef7]_, Section 2.7

•	the foundation of a fixed device

Objectives
==========

•	For floating devices, compute the maximum expected tension experienced by each line segment and compare it to its corroded Minimum Breaking Load (MBLc)

•	Compute the maximum expected load on the foundation (fixed devices) or anchor (floating devices). 


Inputs
======

.. csv-table:: Inputs used to perform a static analysis.
    :header: "Inputs description", "Origin of the Data", "Data Model in SK", "Units"
    :widths: 30, 30, 30, 30

    "Hs – significant wave height",  	"SC", 	"float", 	"[m]" 
    "Inputs necessary for :ref:`Static Analysis <sk-static_analysis>` and :ref:`Dynamic Analysis <sk-dynamic_analysis>`", "various", "various"
    "MBLc – Corroded Minimum Breaking Load of each line segment", 	"User",	"float array", "[N]" 
    "Sf– safety factor", 	"User",	"float", 	"[-]" 
    "Hs – Hs values of the 100 years {Hs,Tp} contour",	"SC",	"float array",	"[m]"
    "Tp – Tp values of the 100 years {Hs,Tp} contour",	"SC",	"float array",	"[s]"
    "Vw – 100 years wind velocity",	"SC",	"float",	"[m/s]"
    "Vc – 10 years current velocity",	"SC",	"float",	"[m/s]"
..

Method and outputs
==================

As recommended in [SKRef7]_, Section 1.2.5, the environmental conditions used for the ULS Analysis are as follows:

•	Waves: 100-year return period contour of {Hs,Tp}

•	Wind velocity: 100-year return period

•	Current: 10-year return period

The waves, wind and current are assumed to be colinear. All directions from 0 deg to 360 deg are to be studied. The ULS analysis consists in the following steps:

•	**Step 1**: select an environmental condition: waves ({Hs,Tp}), wind (Vw), current (Vc) and a direction

•	**Step 2**: perform a :ref:`Static Analysis <sk-static_analysis>`. The main output is the device static equilibrium position :math:`x_{eq}` under mean loads.

•	**Step 3**: perform a :ref:`Dynamic Analysis <sk-dynamic_analysis>`. The main output is the device motion response spectra :math:`S_{xx}`.

•	**Step 4**: compute the device maximum expected dynamic excursions of each degree of freedom :math:`X_{max}` as follows:


.. math::
    :label: eq12

    X_{max} = x_{eq} + σ_{xx} \sqrt{2ln(⁡N_x)}


where :math:`σ_{xx}` is the standard deviations of the motion responses and :math:`N_x` is number of wave-frequency platform oscillations during the duration of the environmental state. 

The standard deviations :math:`σ_{xx}` are determined from the motion spectra :math:`S_{xx}` as follows:


.. math::
    :label: eq13

    σ_{xx}=\sqrt{ \int_0^{\infty} S_{xx} (ω)dω}


Considering that the duration of a sea state is 3 hours, we use the mean period of the motion spectra to determine the number of oscillations N_x of each degrees of freedom as follows:


.. math::
    :label: eq14

    N_x=\frac{3600*3}{2π \sqrt { \frac {\int_0^{\infty} S_{xx} (ω) dω}   {\int_0^{\infty} S_{xx} (ω) ω^2 dω} }}


The first two components of :math:`X_{max}` are the horizontal translations; they correspond to the maximum offset :math:`O_{uls}` in the horizontal plane.

•	**Step 5**: compute the maximum expected tension in each line segment :math:`T_{max}`, equal to the tension in the line when the device position is equal to :math:`X_{max}`. Since the tension value can vary along the line segment, :math:`T_{max}` is taken as the maximum of the tensions at both ends of the line segment.

•	**Step 6**: compare the maximum tension of each line segment to its corroded Minimum Breaking Load (MBLc) by computing the following criteria:


.. math::
    :label: eq15

    C_{uls} = \frac {T_{max}*S_f} {MBL_c} < 1.0


The safety factor :math:`S_f` is to be chosen by taking into account that the approach that is implemented uses a quasi-static model of the mooring lines, and a frequency domain calculation. The default value is 1.7. For further details about the choice of :math:`S_f` value, we refer to [SKRef7]_, Section 4.2.

•	**Step 7**: perform steps 1-6 for all {Hs,Tp} couples and all directions from 0deg to 360deg.

The main outputs of the ULS Analysis are given in the following table:


.. csv-table:: Outputs from ULS Analysis.
    :header: "Outputs description", "Data Model in SK", "Units"
    :widths: 30, 30, 30

    "Tmax – maximum tension in all line segments, for all environmental conditions, and all weather directions",  	"float array",	"[N]"
    "Culs – ULS design criteria for all line segments, environmental conditions, and weather directions",	"float array",	"[-]" 
..


The ULS analysis of a fixed device only includes estimation of the maximum loads using the Static Analysis.
The ULS analysis of a master structure consists in the following steps:

•	Perform ULS Analysis for each device

•	Perform Static Analysis for the master structure: 

    •	The total external force applied on the master structure is the magnitude of the complex sum of the forces from the devices, where their magnitude corresponds to the maximum tensions obtained from the ULS analysis, and the phase corresponds to the position of the device and the wave peak period.

    •	The maximum tensions of the mooring lines of the master structure :math:`T_{max}`  are obtained from the tensions computed for the master structure at equilibrium position.

