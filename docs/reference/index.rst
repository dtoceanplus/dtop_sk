.. _sk-reference:

*************
API Reference
*************

This section of the documentation describes the technical reference manual for the Station Keeping module. 
It is formed of two components. 

1. The OpenAPI specification for the Station Keeping tool. 
   This documentation, powered by `ReDoc <https://redoc.ly/redoc/>`_, describes the *implementation layer* of the Station Keeping module, including the endpoints and API routes provided by the module. 
   This API description is available `here <https://dtoceanplus.gitlab.io/api-cooperative/dtop-sk.html>`_. 

2. The documentation describing the source code of the module itself, extracted from the *docstrings* of the code and converted into a technical reference manual using `Sphinx <https://pypi.org/project/Sphinx/>`_. 
   This is divided into sections on the business logic and on the service layer. 
   The :ref:`business logic <sk-business-logic>` includes descriptions of the database models and calculation methods. 
   The :ref:`service layer <sk-service-layer>` details the *Flask* routes and functions that implement the API. 

.. toctree::
   :maxdepth: 1

   OpenAPI description <https://dtoceanplus.gitlab.io/api-cooperative/dtop-sk.html>
   business_logic
   service_layer
