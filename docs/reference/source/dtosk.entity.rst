entity
======

.. toctree::
   :maxdepth: 4

   dtosk.entity.geometry

Body
----

.. automodule:: dtosk.entity.Body
   :members:
   :undoc-members:
   :show-inheritance:

BodyPart
--------

.. automodule:: dtosk.entity.BodyPart
   :members:
   :undoc-members:
   :show-inheritance:

Farm
----

.. automodule:: dtosk.entity.Farm
   :members:
   :undoc-members:
   :show-inheritance:
