foundationdesign
================

AnchorCatalogue
---------------

.. automodule:: dtosk.foundations.foundationdesign.AnchorCatalogue
   :members:
   :undoc-members:
   :show-inheritance:

DragAnchors
-----------

.. automodule:: dtosk.foundations.foundationdesign.DragAnchors
   :members:
   :undoc-members:
   :show-inheritance:

GravityFoundation
-----------------

.. automodule:: dtosk.foundations.foundationdesign.GravityFoundation
   :members:
   :undoc-members:
   :show-inheritance:

PileFoundation
--------------

.. automodule:: dtosk.foundations.foundationdesign.PileFoundation
   :members:
   :undoc-members:
   :show-inheritance:

SuctionCaisson
--------------

.. automodule:: dtosk.foundations.foundationdesign.SuctionCaisson
   :members:
   :undoc-members:
   :show-inheritance:

dataframe\_materials
--------------------

.. automodule:: dtosk.foundations.foundationdesign.dataframe_materials
   :members:
   :undoc-members:
   :show-inheritance:

parameters\_for\_piles
----------------------

.. automodule:: dtosk.foundations.foundationdesign.parameters_for_piles
   :members:
   :undoc-members:
   :show-inheritance:

parameters\_for\_suction
------------------------

.. automodule:: dtosk.foundations.foundationdesign.parameters_for_suction
   :members:
   :undoc-members:
   :show-inheritance: