foundations
===========

.. toctree::
   :maxdepth: 4

   dtosk.foundations.basic
   dtosk.foundations.foundationdesign
   dtosk.foundations.foundationtype
   dtosk.foundations.inputs
   dtosk.foundations.outputs

catalogue\_example
------------------

.. automodule:: dtosk.foundations.catalogue_example
   :members:
   :undoc-members:
   :show-inheritance:

drag\_anchor\_example
---------------------

.. automodule:: dtosk.foundations.drag_anchor_example
   :members:
   :undoc-members:
   :show-inheritance:

example
-------

.. automodule:: dtosk.foundations.example
   :members:
   :undoc-members:
   :show-inheritance:

foundation\_main\_module
------------------------

.. automodule:: dtosk.foundations.foundation_main_module
   :members:
   :undoc-members:
   :show-inheritance:

gravitydesign\_example
----------------------

.. automodule:: dtosk.foundations.gravitydesign_example
   :members:
   :undoc-members:
   :show-inheritance:

piledesign\_example
-------------------

.. automodule:: dtosk.foundations.piledesign_example
   :members:
   :undoc-members:
   :show-inheritance: