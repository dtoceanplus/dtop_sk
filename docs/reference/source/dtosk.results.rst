results
=======

BodyResults
-----------

.. automodule:: dtosk.results.BodyResults
   :members:
   :undoc-members:
   :show-inheritance:

DesignBodyResults
-----------------

.. automodule:: dtosk.results.DesignBodyResults
   :members:
   :undoc-members:
   :show-inheritance:

FLSBodyResults
--------------

.. automodule:: dtosk.results.FLSBodyResults
   :members:
   :undoc-members:
   :show-inheritance:

ULSBodyResults
--------------

.. automodule:: dtosk.results.ULSBodyResults
   :members:
   :undoc-members:
   :show-inheritance: