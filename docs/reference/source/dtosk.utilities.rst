utilities
=========

.. toctree::
   :maxdepth: 4

   dtosk.utilities.Python_for_Nemoh

PandasLine
----------

.. automodule:: dtosk.utilities.PandasLine
   :members:
   :undoc-members:
   :show-inheritance:

PandasTable
-----------

.. automodule:: dtosk.utilities.PandasTable
   :members:
   :undoc-members:
   :show-inheritance:

PlotlyPlot
----------

.. automodule:: dtosk.utilities.PlotlyPlot
   :members:
   :undoc-members:
   :show-inheritance:

Scatter3D
---------

.. automodule:: dtosk.utilities.Scatter3D
   :members:
   :undoc-members:
   :show-inheritance: