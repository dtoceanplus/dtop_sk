.. _sk-study-tutorial2:

Assessing mooring system of a wave energy converter in ultimate limit state condition in standalone mode
========================================================================================================

Once the entity is created in the Main Module with a complexity level set to 3, the Station Keeping module will open on the Device Positioning page. With this page the inputs are devided in five pages and this tutorial is then divided into seven parts, one per input page, one for the "Run Module" page where the user can launch the calculation and one to show a part of the outputs.

Device Positioning
~~~~~~~~~~~~~~~~~~

- The water density is set to the default value of 1025 :math:`kg/m^{3}`.

- This example consider only one device which is positioned at the location [north, east] = [0, 0] with a yaw = 0° and with a water depth of 70 m.

- Click on "Next Page"

Device Properties
~~~~~~~~~~~~~~~~~

- As described in the tiel this tutorial consider a moored Wave Energy Converter (WEC) and the corresponding options has to be set in the Type of Machine section.

- The wind forces are neglected in this example.

- The Current and Mean Wave Drift Force Model consider a Cylinder device profile with horizontal and vertical main dimensions reqpectively set to 6 and 40.

- In the Floating structure hydrodynamic, two default object are set using Nemoh data. Select the default object "RM3_6dofs" for this example.

- The desired mooring system is considered to be anchored on the seabed. The default values of the safety factors are used and a lifetime of 25 years is considered.

- A custom mooring system is considered, click on define and use to "Set to Default" button to automatically filled the mooring system inputs.

- (Optional) Click on "Check and plot" and set the water depth to 70 m and the water density to 1025 to display a 3D plot of the mooring system, as well as other data: tension in the lines, mooring forces and mooring stiffness matrix.

- No additional ancillaries are defined at this stage.

- It is installed on a "medium_dense_sand" seabed type.

- If we assume that the seabed is flat, the soil slope is 0. We can use the default soil safety factor, and we set the load safety factor equal to 1.8.

- The Design load and the Foundation type selection are both set to automatic so the Station Keeping module will automatically design the foundation

- Click on "Next Page"

Masterstructure Properties
~~~~~~~~~~~~~~~~~~~~~~~~~~

- In this example, no masterstructure were defined in the previous page as explained by the displayed message "Masterstructure not present. A master structure model is used when several floating devices are moored together. This is not the case here.

- Click on "Next Page"

Substation Properties
~~~~~~~~~~~~~~~~~~~~~

- No substation is required in this example.

- Click on "Next Page"

Analysis Parameters
~~~~~~~~~~~~~~~~~~~

- Set the weather direction to [0, 180]. This does not matter much in this example, because we always assume that the rotor faces the current.

- Define Hs = [11.9] and Tp = [17.1]: their value will be used to compute a water particle velocity which will be added to the current velocity in order to compute drag forces on the pile foundation. For the forces on the rotors, however, this does not change anything, since only the velocity from the current is used.

- Set the current velocity value to 0.59.

- Set the wind velocity to zero, since we are not interested in wind in this example.

- Click on "Next Page"

Run Module
~~~~~~~~~~

- On this page an input summary describe if all pages where filled. To launch the calculation click on "Run Module".

- The log of the calculation will be displayed and the module allow the user to click on "See Results"

Outputs overview
~~~~~~~~~~~~~~~~

- In the "Design Assessment" page, the dimensions of the Pile foundation are displayed and summarised in the following table.

.. csv-table:: Anchor and foundation design assessment.
   :header: "Type", "Length", "Width", "Height", "Mass"
   :align: center
   
   "Drag anchor", "5.47 m", "5.9 m", "3.29 m", "9535.48 kg"
..